<STYLE>
tbody IMG {
	display: inline-block;
}
tbody IMG.focus {
	border:2px dotted red;
}

IMG:HOVER{
	cursor:pointer;
}
IMG.editable + I[class^="icon-"], [class*=" icon-"]{	
	display:inline-block;
	position: absolute;
	margin-left: -14px;
	padding:1px;
	background-color: rgba(255,255,255,1);
}
.img_control INPUT{
	float: left;
}
</STYLE>
<style type="text/css">

.box{width:500px;height:500px;padding:3px;border:solid 1px #ccc;margin:50px auto;position:relative;cursor: pointer;display: none;}

#r_b{position:absolute;width:20px;height:20px;bottom:0;right:0;z-index:99;background:#f00;cursor:nw-resize;
/*阻止选中*/
-moz-user-select:none; 
-khtml-user-select:none; 
user-select:none;
}
</style>



<div class="box" id="box" width="500px" height="375px">
	<div id="r_b"></div>	
</div>
<div class="widget">
	<table cellpadding="0" cellspacing="0" width="100%" class="tLight">
		<form id="list_orderby" method="post">
		<input id="order_by" type="hidden" name="order_by" value="">
		<thead>
			<tr>
				<td title="題目"><input type="checkbox" />題目</td>
				<td title="群組名稱">群組名稱</td>
				<td title="選項類型">選項類型</td>
				<td title="指令圖片">指令圖片</td>
				<td title="選項圖片">選項圖片</td>
				<td title="新增資料" width="150px;" style="cursor: pointer;" onclick="defoption_list.add_edit(0);">新增資料<i class="icon-plus-sign"></i></td>
			</tr>			
		</thead>
		</form>
		<tbody>
			{content}
			<tr id="user_{auto_index}" class="user_row">
				<td><input type="checkbox" />{defoptions_select_name}</td>
				<td>{group_names}</td>
				<td>{select_optionName}</td>							
				<td>				
					<img  src="uploads/temp/{pic_url}" style="width: 60px;" />					
				</td>
				<td>		
					{option_photo}		
						<img  src="uploads/temp/{option_url}" style="width: 60px;" title="{photo_title}" />
					{/option_photo}
				</td>
				<td>	
					<a href="javascript:void(0);" class="tablectrl_small bGreen" original-title="Edit" title="修改" alt="修改" onclick="defoption_list.view({auto_index});"><span class="iconb" data-icon="" ></span></a>
				</td>				
			</tr>
			{/content}
		</tbody>
	</table>
</div>

<script src="js/defoption_list.js"></script>
<script>




	var pageChange = function(info){

		var css 		= (info.css) ? info.css : 'pageChange' ; 
		var click 		= this.customClick = (typeof info.click == 'function') ? info.click : null;
		var This 		= this;
		var pageAt 		= this.pageAt 	 = parseInt(info.pageStart);
		var pageLimit 	= this.pageLimit = parseInt(info.pageLimit);   //9
		var dataCount 	= this.dataCount = parseInt(info.dataCount);  
		var dataLimit 	= this.dataLimit = parseInt(info.dataLimit);
		var diff 		= this.diff = Math.floor( pageLimit / 2 ); //4

		var pageCount 	= ( ( dataCount - ( dataCount % dataLimit ) ) / dataLimit ); 
		if((dataCount % dataLimit)) pageCount++; //302
		this.pageCount 	= pageCount;

		//page style 
		var pageEl 	 = this.pageEl = $("<div>").addClass(css).css("position","relative").appendTo(info.appendTo);
		var prev 	 = $("<div>").addClass("prev").html('<').appendTo(pageEl).click(function(e){
			This.click( This.pageAt - 1 , this );
		});	
		this.pageBox = $("<ol>").addClass("pages").appendTo(pageEl);
		var next 	 = $("<div>").addClass("next").html('>').appendTo(pageEl).click(function(e){
			This.click( This.pageAt + 1 , this );
		});	

		this.changeTo(pageAt);
		
		//再加入上下頁
	}	

	pageChange.prototype.click = function(page,el){
		if(page <= 0 || page > this.pageCount )return;
		if(typeof this.customClick == 'function')this.customClick(page,el);
		this.changeTo(page);
		return this;
	}

	pageChange.prototype.where = function(){
		return this.pageAt
	}
	pageChange.prototype.changeTo = function(page){
		var pageAt 		= parseInt(page);
		var diff 		= parseInt(this.diff);

		if(pageAt <= 0 || pageAt > this.pageCount )return;

		var This 		= this;
		var box 		= this.pageBox.empty();
		var pageCount 	= this.pageCount;
		var pageLimit 	= this.pageLimit;
		var start		= 1;
		var end			= 1;

		if( pageCount - pageAt > diff){			
			start = (pageAt - diff < 1)?1:(pageAt - diff);
			end = start + pageLimit -1;	
		} else {
			end = pageCount;
			start = end - pageLimit + 1 ;			
		}
		if(start < 1) start = 1;

		
		for(var i = start; i<=end; i++){
			if(i>pageCount){console.log(i);break;}

			var tmp = $("<li>").addClass("page").html(i).attr("page",i).click(function(){
				This.click($(this).attr("page"), this);
			}).appendTo(box);

			if(pageAt == i)tmp.addClass('select');
			delete tmp ;
		}

		this.pageAt = pageAt;

		return this;
	}

$(document).ready(function() {
//
	this.pageControl = new pageChange ( {
	
		pageLimit 	: 9,
		pageStart 	: 1,
		dataCount 	: 240,
		dataLimit 	: 20,
		appendTo 	: 'DIV.wrapper',
		click 		: function(page,el){
			console.log(page);
		}
	
	} );
	
	
	var btn_rb=$("#r_b");
	var box=$("#box");
	var od=$("#r_b");
	var e=window.event;
	var mouseD,moveD;
	var moveX,moveY;
	var odrag;

	$("#box").mouseup(function(){
		mouseD=false;
		moveD=false;
		odrag="";
	});	
	$("#box").mousedown(function(e){
		if(mouseD) return false;
		var e=e?e:event;		
		
		moveD=true;
		odrag=this;		
		moveX = e.clientX-$(this).offset().left;
		moveY = e.clientY-$(this).offset().top;
	});
	$("#r_b").mousedown(function(){
		mouseD=true;
		odrag=this;
		moveD=false;
	});
	$("#r_b").mouseup(function(){
		mouseD=false;
		odrag="";
	});
	$("#box").mousemove(function(e){
		var e=e?e:event;		
		if(mouseD==true && odrag){	
			var w = e.clientX-$(this).offset().left;
			var h = e.clientY-$(this).offset().top;

			
			if(h != w*3/4)  h = w*3/4;
			 
			if(w<500) w=500;
			if(h<375) h=375;
			$(this).width(w);
			$(this).height(h);			
		}

		if(moveD==true && odrag){		
			$(this).offset({ top: (e.clientY - moveY), left: (e.clientX - moveX)});
			
		}
	});
	
	$("IMG.editable").each(function(){
		this.addEventListener("dragenter", dragEnter, false);
		this.addEventListener("dragexit", dragExit, false);
		this.addEventListener("dragover", dragOver, false);
		this.addEventListener("dragleave", dragLeave, false);
		this.addEventListener("drop", drop, false);
		$(this).hover(
				function(){	$(this).after('<i class="icon-pencil"></i>');},
				function(){	$(this).next('I').remove();});
		
		$(this).load(function() {

			if( typeof( $(this).attr("data-src") ) == "undefined" || 
				typeof( $(this).attr("data-width") ) == "undefined" ||
				typeof( $(this).attr("data-height") ) == "undefined")
			{				
				return false;
			}
				
			
			var width = this.naturalWidth;
			var height = this.naturalHeight;
			
			if(width == $(this).attr("data-width") && height == $(this).attr("data-height")) 
			{
				if(false == confirm("是否變更圖片")){
					reduction_img(this);					
				}	 
			}	
			else {
				alert('圖片規格不符，請使用圖片編輯。');
				reduction_img(this);		
			}	
			
		});
		$(this).error(function(){
			alert('圖片讀取有誤!');
			reduction_img(this);
		});
		
		
	});


	$("IMG.editable").on('click', function() {	
		var img_box = $(this).clone();	
		$.fancybox.open(img_box.width(this.naturalWidth),
				{  
			
			fitToView: false,
	  		autoCenter   : true,
	  		autoSize    : true,
		  	helpers : {
		        overlay : {
		        	closeClick: true
		        }
	        },
	        beforeClose : function(){
	        	return(confirm("是否關閉 新增/編輯 資料視窗!?"));	        	
	        }
		});


				
		//增加INPUT功能
		var img_control = document.createElement('div');
		$(img_control).attr("id","img_control");
		
		var img_input = document.createElement('input');
		$(img_input).attr("type","file").attr("name","img_file");
		$(img_control).append($(img_input));
		
		
		var select_input = document.createElement('input');
		$(select_input).attr("type","text").attr("name","img_google");
		$(img_control).append($(select_input));
		
		var google_img_select = document.createElement('div');
		$(google_img_select).attr("id","google_img_select").hide();
		$(google_img_select).css( "position", "fixed" ).css( "background", "white" ).css( "top", "60px" ).css( "right", "30px" );
		$(select_input).after(google_img_select);
		

		$(img_box).before($(img_control));
		$(img_input).on('click', function(){
			$(img_box).addClass("Upload_img");			
		});
		$(img_input).on('change', handleFileInput);
		
		
		
		
		$(select_input).on('keyup', function(){
			$("#google_img_select").show();

			if(!this.value) {
				$("#google_img_select").html('');
				return false;
			}
			
				$.ajax({
					  url: '../code/text.php',
					  dataType: 'json',
					  type: "POST",
					  data: {						  
						  keys:$(this).val()
					  },
					  success: function(data){
						  $("#google_img_select").html('');
						  for(var key in data){
							  var img_text = '<img src="'+data[key]['url']+'" width="100px">';
							  $("#google_img_select").append(img_text);
						   }
						 
					  	/**/
					  }
					});
			

		});



		
		$(img_box).load(function() {
			if( typeof( $(this).attr("data-src") ) == "undefined" || 
					typeof( $(this).attr("data-width") ) == "undefined" ||
					typeof( $(this).attr("data-height") ) == "undefined")
					return false;

			JcropAPI = $(this).data('Jcrop');
			if(typeof(JcropAPI) != "undefined" ) JcropAPI.destroy();
			if(typeof($("#cut_control")) != "undefined" ) $("#cut_control").remove();
			
			$.fancybox.update();
			var width = this.naturalWidth;
			var height = this.naturalHeight;
			
			if(width<$(this).attr("data-width") || height<$(this).attr("data-height")) {
				console.info(width);
				console.info(height);
				
				alert('圖片檔案需等於500x375');	
				reduction_img(this);	
			}	
			else if((width>$(this).attr("data-width") || height>$(this).attr("data-height") )) {
				$(this).width(this.naturalWidth);
				$(this).height(this.naturalHeight);

				if($(this).hasClass("img_cut")) {
					$(this).removeClass("img_cut");
					return false;
				}
				alert('進入裁圖功能');	

				$(this).addClass("img_cut");
				
				//裁圖功能
				var cut_control = document.createElement('div');
				$(cut_control).attr("id","cut_control");
				
				 
				 //裁圖按鈕
				 var cut_box = document.createElement('input');
				$(cut_box).attr("type","button").attr("name","cut_input").val("選取範圍");
				$(cut_control).append($(cut_box));
				 
				var cut_input = document.createElement('input');
				$(cut_input).attr("type","button").attr("name","cut_input").val("確定裁圖");
				$(cut_control).append($(cut_input));

				var cut_cancel = document.createElement('input');
				$(cut_cancel).attr("type","button").attr("name","cut_cancel").val("取消裁圖");
				$(cut_control).append($(cut_cancel));
				
				$(cut_cancel).on('click', function(){
					JcropAPI = $("IMG.img_cut").data('Jcrop');
					if(typeof(JcropAPI) != "undefined" ) JcropAPI.destroy();
					$('#cut_control INPUT[type="text"]').remove();					
				});
				
				$(cut_control).append('<canvas id="myCanvas" width="'+width+'" height="'+height+'" style="display:none;"></canvas>');
				$(this).before($(cut_control));
				
				context = document.getElementById('myCanvas').getContext("2d");
				
				$(cut_box).on('click', function(){	

					var jcrop_input = document.createElement('input');
						$(jcrop_input).attr("type","text").attr("id","cut_x").width(30);
						$(this).after($(jcrop_input));
					jcrop_input = document.createElement('input');
						$(jcrop_input).attr("type","text").attr("id","cut_y").width(30);
						$(this).after($(jcrop_input));
					jcrop_input = document.createElement('input');
						$(jcrop_input).attr("type","text").attr("id","cut_w").width(30);
						$(this).after($(jcrop_input));
					jcrop_input = document.createElement('input');
						$(jcrop_input).attr("type","text").attr("id","cut_h").width(30);
						$(this).after($(jcrop_input));
					
						
					
					img =  $("IMG.img_cut").get(0);

					$(img).Jcrop({
						onSelect: showCoords,
						minSize: [500, 375],
					    aspectRatio: 4 / 3
					})
							
				});
				function showCoords(c){
					$("#cut_x").val(c.x);
					$("#cut_y").val(c.y);
					$("#cut_w").val(c.w);
					$("#cut_h").val(c.h);
				}
				$(cut_input).on('click', function(){					

					var img=$(".Upload_img").get(0);
					if(typeof(img) == "undefined"){
						t_img = new Image();
					   

						t_img.crossOrigin = "anonymous";
						
						t_img.onload = function() {
							 document.getElementById("myCanvas").width = $("#cut_w").val();
							 document.getElementById("myCanvas").height = $("#cut_h").val();
							 context.drawImage( this,-1 * $("#cut_x").val(), -1 * $("#cut_y").val() );
							 $('#myCanvas').show();
						    $("IMG.img_cut").width( $("#cut_w").val() ).height( $("#cut_h").val() );
						   
						    $("IMG.img_cut").get(0).src = document.getElementById("myCanvas").toDataURL();
						    $('#box').hide();
						    $('#myCanvas').remove();
						}
						t_img.src = $("IMG.img_cut").attr("src");

						
						return false;
						

					}

					
					 context.drawImage(img,0 , 0);	
					 oldBack = context.getImageData($("#cut_x").val(), $("#cut_y").val(), $("#cut_w").val(), $("#cut_h").val());					 
					 $('#myCanvas').attr("width", $("#cut_w").val()).attr("height", $("#cut_h").val());
				     context.putImageData(oldBack, 0, 0);
				     $("IMG.img_cut").width( $("#cut_w").val()).height($("#cut_h").val());
				     $("IMG.img_cut").attr("src",$("#myCanvas")[0].toDataURL());	
				     $('#box').hide();
				     $('#myCanvas').remove();
				});
				
			}
			
			
			
			return false;
			if($(".Upload_img").hasClass("Upload_img")){			
				$.ajax({
					  url: '../code/upload.php',
					  dataType: 'html',
					  type: "POST",
					  data: {
						  file:'fadonis',
						  img:$(this).attr('src')
					  },
					  success: function(data){
						  console.info(data);
					  	/**/
					  }
					});
				$(".Upload_img").removeClass('Upload_img');
			}
			return false;
			
		});
		$(img_box).error(function() {
			alert('圖片讀取有誤!');
			reduction_img(this);			
		});
		
		$(img_box).bind("dragenter", dragEnter);
		$(img_box).bind("dragexit", dragExit);
		$(img_box).bind("dragover", dragOver);
		$(img_box).bind("dragleave", dragLeave);
		$(img_box).bind("drop", function(evt){

			if(false == $(this).hasClass("archived")){
				$(this).addClass("archived")
				$(this).attr('data-src',$(this).attr('src'));
				$(this).attr('data-width',this.naturalWidth);
				$(this).attr('data-height',this.naturalHeight);
			}
			evt.stopPropagation();
			evt.preventDefault();

			var dat = $(evt.originalEvent.dataTransfer.getData('text/html'));
		    var img = dat.attr('src');
		    if (!img) img = dat.find("img").attr('src');
		    
			var files = evt.originalEvent.dataTransfer.files;
			var count = files.length;

			
			if (count > 0){
				$(this).addClass('Upload_img');
				handleFiles(files);
			}
				
			else if(img){		
				$(this).attr('src',img);		
			}

		});
	});
	
	
});


function reduction_img(evt){
	var reduction =  $(evt).attr("data-src");
	$(evt).removeAttr( "data-src" );
	$(evt).attr('src',reduction);
	$(evt).removeClass('Upload_img');	
	
}


function dragEnter(evt) {
	evt.stopPropagation();
	evt.preventDefault();
	$(this).addClass("focus");
}

function dragExit(evt) {
	evt.stopPropagation();
	evt.preventDefault();	
	$(this).removeClass("focus");
}

function dragLeave(evt) {
	evt.stopPropagation();
	evt.preventDefault();
	$(this).removeClass("focus");
}
function dragOver(evt) {
	evt.stopPropagation();
	evt.preventDefault();	
}

function drop(evt) {

	$(this).attr('data-src',$(this).attr('src'));
	$(this).attr('data-width',this.naturalWidth);
	$(this).attr('data-height',this.naturalHeight);
	

	
	$(this).removeClass("focus");
	evt.stopPropagation();
	evt.preventDefault();

	var dat = $(evt.dataTransfer.getData('text/html'));
    var img = dat.attr('src');
    if (!img) img = dat.find("img").attr('src');
    
	var files = evt.dataTransfer.files;
	var count = files.length;

	
	// Only call the handler if 1 or more files was dropped.
	if (count > 0){
		$(this).addClass('Upload_img');
		handleFiles(files);
	}
	else if(img){		
		$(this).attr('src',img);		
	}
}


function handleFiles(files) {
	var file = files[0];

	//document.getElementById("droplabel").innerHTML = "Processing " + file.name;

	var reader = new FileReader();

	// init the reader event handlers
// 	reader.onprogress = handleReaderProgress;
	reader.onloadend = handleReaderLoadEnd;

	// begin the read operation
	reader.readAsDataURL(file);
}

function handleReaderProgress(evt) {
	if (evt.lengthComputable) {
		var loaded = (evt.loaded / evt.total);

		//$("#progressbar").progressbar({ value: loaded * 100 });
	}
}

function handleReaderLoadEnd(evt) {
	$(".Upload_img").attr('src',evt.target.result);

	if($(".Upload_img")[0].naturalWidth >= $(".Upload_img").attr("data-width"))	
		$(".Upload_img").width($(".Upload_img")[0].naturalWidth);
	if($(".Upload_img")[0].naturalHeight >= $(".Upload_img").attr("data-height"))	
		$(".Upload_img").height($(".Upload_img")[0].naturalHeight);
}

function handleFileInput(evt) {   
	var files = evt.target.files;
	var count = files.length;
	if(count > 0 ){
		$('.Upload_img').attr('data-src',$(".Upload_img").attr('src'));
		$('.Upload_img').attr('data-width',$(".Upload_img")[0].naturalWidth);
		$('.Upload_img').attr('data-height',$(".Upload_img")[0].naturalHeight);
		var reader = new FileReader();
	  	reader.onloadend = handleReaderLoadEnd;
	  	reader.readAsDataURL(files[0]);
	}
 }
</script>