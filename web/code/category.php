<?php 
	$start = microtime(true);
	session_start();
	header("Content-type: text/html; charset=utf-8");

	include_once("include/db.php");
	include_once("include/htmltpl.php");
	include_once("include/function.php");

	
	$curPageURL	=  curPageURL();
	$curServerURL = curPageURL(true);	
	$categoryID = $_GET['category'];
	$category = explode("/", $_GET['category']);	
	$category = array_filter($category);
	$category_name = array();
	
	
	include_once('global_load.php');
	
	
	//判斷文章是否上架，導回首頁
	if(is_numeric($categoryID) ) {
		$category_total = CategoryOnlineCkeck($categoryID,$curServerURL);
	}else{
		//熱門分類
		$category_total = GetArticleTotal();
	}
	
	
	$category_title = $category_name[$categoryID]['name'];
	
	if(!$category_title) $category_title = ($categoryID)?$categoryID:'New';
	$category_url = $curServerURL."/category/$category_title/";
	
	$pageNum = (int)((isset($_GET['page']) && $_GET['page'] > 0)?$_GET['page']:1);
	$pageRow = 18;
	$pageTotal = ceil($category_total / $pageRow);
	if($pageNum > $pageTotal ) $pageNum = $pageTotal;
	  
	$limit = (($pageNum - 1)*$pageRow).", $pageRow";
	
	
	
	
	
	
	if(is_numeric($categoryID)) $where = "AND `category` LIKE  '%\"$categoryID\"%' "; 	
	
	if($categoryID == 'Hot') $order = ' `daily_views` DESC, `views` DESC ';	
	else $order = ' `online_time` DESC ';
	
	$sql = " SELECT `news_article_onshelf`.title , $img_tm AS img, $listUrl AS url , nas.* 	
	FROM  news_article_onshelf  
	INNER JOIN (
		SELECT DISTINCT `id` , `category` , `online_time` , `views`, `daily_views`, `tag`
		FROM  `news_article_state` 
		WHERE `online` =  '1' $where  ORDER BY $order LIMIT $limit 
	) AS nas ON nas.id =  `news_article_onshelf`.id ";
	
	$memcached->set_prefix("Category[$categoryID]");
	$listKey = 'pagelist_'.$pageNum;
	$keyValues[] = [
			'key' => $listKey ,
			'func' => $memcached->SQLfunc( $sql ),
			'expire' => 7200
	];

	$keyValues  = $memcached->getMultiWithSet($keyValues,86400);
	
	
	foreach($keyValues[$listKey] AS $row){
		if($row['category']) {			
			$row['category'] = array_values(array_intersect_key( $category_name,	array_flip( json_decode($row['category'])) ));			
		}

		if($row['tag']){
			$row['tag'] = array_values(array_intersect_key( $tag_name,	array_flip( json_decode($row['tag'])) ));
		}
		
		$row['online_time'] = date('Y-m-d', $row['online_time']);		
		$data['article_list'][] = $row;
	}
	if($category_title == 'New') $category_title = 'meoments 找話題'; 
	
	$head = array(   
   'url'  => $curPageURL,
   'baseUrl'   => $baseUrl,
   'title'  => $category_title,
   'link:img_src' => $article['img'],
   'meta' => array(
            
     ['name' => 'name="keywords"'     ,'content'=> $category_title ],
     ['name' => 'name="description"'    ,'content'=> $category_title ],
     ['name' => 'property="og:description"'    ,'content'=> $category_title ],
     
     ['name' => 'property="og:title"'   ,'content'=> $category_title ],
     ['name' => 'property="og:image"'   ,'content'=> $ImgServerUrl.'/n'.$article['id'].'/f.jpg'],
     ['name' => 'property="og:type"'   ,'content'=> "article" ],
     ['name' => 'property="og:url"'   ,'content'=> $curPageURL ],
   	 ['name' => 'property="fb:admins"'    ,'content'=> '100004835161372' ],
   	 ['name' => 'property="fb:app_id"'    ,'content'=> '588005798009398' ],
   )
 );
	
	
	
	
	
	$data['HEAD'][] = $head;
	$data['MENU'] = $category_name;
	$data['mobile_MENU'] = $category_name;
	$data['curServerURL'] = $curServerURL;
	
	$data['pageUrl'] = $category_url;
	$data['pageNum'] = $pageNum;
	$data['pageTotal'] = $pageTotal;
	
	$HTML['HTML'][] = $data;
	
	echo tpl_get_html("./tpl/","category.tpl.html",$HTML);
	
	
?>