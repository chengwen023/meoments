<div>
	<form id="form_folder">
		<div class="tb-green">
			<table>
				<thead>
					<tr class="column1">
						<th scope="col" colspan="2">
							<?php echo (!empty($group_info['auto_index']) && $group_info['auto_index'] > 0)? '編輯':'新增';?>管理員
						</th>
					</tr>
				</thead>
				<tr>
					<td width="66">
						名稱
					</td>
					<td>
						<input id="admin_name" type="text" name="id" value="<?php echo empty($group_info['group_name'])? '':$group_info['group_name'];?>" /><br />
						<label class="error" generated="true" for="admin_name">(*)</label>
					</td>
				</tr>
				
				<tr>
					<td width="66">
						權限
					</td>
					<td>
						<ul style="list-style: none;margin: 0;">
							<?php foreach($admin_menu AS $key => $menu): ?>
							<li>
								<span style="margin-right: 10px;float: left;"><?php echo $menu['child_name'];?></span>
								<span style="float: right;">
									<?php foreach ($admin_competence AS $key =>$value):?>
									<input class="account_competence" name="competence[<?php echo $menu['auto_index'];?>][]" type="checkbox" value="<?php echo $key;?>" checked="checked" /><?php echo $value;?>
									<?php endforeach;?>
								</span>
							</li>
							<?php endforeach;?>
						</ul>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td colspan="2">
						<input type="submit" value="送出" style="padding:3px;font-size:14px;" />
					</td>
				</tr>
			</table>
		</div><!-- / tb-green-->
	</form>
</div>
<script>
competence_change('<?php echo $group_info['auto_index']?>');
function competence_change(value){
	
	$.fancybox.showLoading();
	$.ajax({
		url: '_admin/account/ajax/groupdefault',
	  cache: false,
	  dataType: 'json',
	  type: 'POST',
	  async : false,
	  data : {
		  value:value
	  },
	  success: function(data){
	  	$.fancybox.hideLoading();
	  	if(data && value == data['index']){
	  		var competence_default = data['default'];
	  		$('INPUT[class="account_competence"]').attr('checked', false);
	  		for(var k in competence_default){
	  			$('INPUT[name="competence['+k+'][]"]').each(function(){
		  			for(var key in competence_default[k]){
		  				if( $(this).val() == competence_default[k][key]){
		  					$(this).attr('checked', true);
		  				}
		  			}
		  			
		  		});
	  			
	  		}
	  		
	  		/**/
	  	}
	  	
	  }
	});
	
}

	$(function(){
		// 註冊驗證
		$("#form_folder").validate({			
			submitHandler: function(form) {
				
				$.fancybox.showLoading();
				// 透過 Ajax 驗證是否註冊
				$.ajax({
				  url: '_admin/account/actions/validate',
				  cache: false,
				  dataType: 'json',
				  type: "POST",
				  data: $("#form_folder").serialize(),
				  success: function(data){
					
					  
				  	$.fancybox.hideLoading();
				  	
				  	if(data.success == 'Y' ){
				  		alert(data.msg);
				  		location.reload();
				  	}else{
				  		alert(data.msg);
				  		$('input[name="id"]').focus();
				  		return false;
				  	}
				  	/**/
				  }
				});
				//form.submit();
				return false;
			}
		});
		// end of $("#signupForm").validate
	});
</script>