<?php 
class Topic extends \Phalcon\Mvc\Model
{
	
	
	
	public function initialize()
	{
		$this->belongsTo("create_account_index", "AdminAccount", "auto_index");
		$this->belongsTo("update_account_index", "AdminAccount", "auto_index");
		
		$this->belongsTo("options_select", "TopicSelect", "auto_index");
	}
	
	public function beforeSave()
	{
		
	}
	
	public function afterFetch()
	{
		
	}
	
	public function get_TopicSelect_field($field = false)
	{
		if($field) $field = $this->TopicSelect->{$field};
		return $field;
		
	}
	
	public function get_AdminAccount_field($field = false)
	{
		if($field) $field = $this->AdminAccount->{$field};
		return $field;
	
	}
}

?>