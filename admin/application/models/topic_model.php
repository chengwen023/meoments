<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *	Adonis
 *	後台預設資料讀取
 */

class Topic_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
		
		
	}
	/**
	 *	後台帳號資料讀取
	 */
	function topic_group($index = '',$field='*',$select_type = true){
		
		
		$this->db->flush_cache();
		$this->db->select($field,$select_type)->from('topic_group')->where('online','1');
		if($index){
			$this->db->where('auto_index',$index);			
		}
		
		$query = $this->db->get();
		$data = $query->result_array();
		
		if($index && is_array($data)) 	$data = array_pop($data);		
		return $data;
	}
	
	function onshelf_topic_by_index($index){
		
		$result = array('success' => 'N', 'msg' => '');
	
		$this->db->flush_cache();
		$query = $this->db->select("*")
		->from("topic")
		->where("auto_index", $index)
		->where("online", '0')
		->get();
		$temp = $query->result_array();
		$data = array_pop($temp); 
		
		if(!is_array($data)){		
			$result['msg'] = '無此資料';
			
		}else{		
			$date = date("Y-m-d H:i:s");
			$account_id = $this->config->item('admin_accountIndex');
			
			$topic_shelf = array();
			//產生上架圖片
			$newDir= '../uploads/q'.$data['auto_index'].'/';
			//題目圖片
			$oldFile = "../ci_gamesapp/uploads/temp/".$data['topic_imgurl'];
			$topic_imgurl =  $this->move_onshelf_photo($oldFile , 't.'.pathinfo($oldFile, PATHINFO_EXTENSION), $newDir );
// 			echo '<br>';
			//指令圖片
			$oldFile = "../ci_gamesapp/uploads/temp/".$data['options_imgurl'];
			$options_imgurl =  $this->move_onshelf_photo($oldFile , 's.'.pathinfo($oldFile, PATHINFO_EXTENSION), $newDir );
// 			echo '<br>';
			//選項圖片
			$this->db->select('auto_index,text_imgurl')->from('topic_options')->where('topic_index',$data['auto_index']);
			$query = $this->db->get();
			$option = $query->result_array();
			$list = 0;
			foreach ($option AS $item){
				$list++;
				if(!$item['text_imgurl']) continue;
				$oldFile = "../ci_gamesapp/uploads/temp/".$item['text_imgurl'];
				$newFile = 'q'.$data['auto_index'].'/'.$this->move_onshelf_photo($oldFile , $list.'.'.pathinfo($oldFile, PATHINFO_EXTENSION), $newDir );
				//$this->db->update('topic_options', array('text_imgurl' => $newFile), array('auto_index' => $item['auto_index']));
			}
			
			
			/**/
			
			//轉至上架資料表
			
			$topic_shelf['topic_title'] = $data['topic'];
			$topic_shelf['topic_optiontitle'] = $data['topic_optiontitle'];
			$topic_shelf['topic_imgurl'] = $data['topic_imgurl'];
			$topic_shelf['options_imgurl'] = $data['options_imgurl'];
			$topic_shelf['topic_group'] = $data['topic_group'];
			$topic_shelf['options_select'] = $data['options_select'];
			$topic_shelf['topic_fromindex'] = $data['auto_index'];
			$topic_shelf['online'] = '1';
			$topic_shelf['create_time'] = $date;
			$topic_shelf['create_account_index'] = $account_id;
			$topic_shelf['update_time'] = $date;
			$topic_shelf['update_account_index'] = $account_id;
			
			$this->db->insert('topic_onshelf', $topic_shelf);
			
			
			
			
			
			//設定為上架狀態
			$this->db->flush_cache();
			$topic = array("online"=>'1');
			$this->db->where('auto_index', $index);
			$this->db->update('topic', $topic);
			$result['success'] = 'Y';
			$result['msg'] = '上架完成';
		}
		return $result;
		
	}
	
	

	
	
	
	function move_onshelf_photo($img,$newUrl,$newDir = false){
		//是否建立目錄		
		if(false !== $newDir && false === is_dir( $newDir ) ) {			
			mkdir($newDir);
			chmod($newDir, 0777);			
		}
		
		$filename = $newDir.$newUrl;
		
		if (preg_match('/^(http|https).*$/', $img)){
		
			file_put_contents($filename, file_get_contents($img));
		
			//}else if(preg_match("/^data:image\/(".pathinfo($filename, PATHINFO_EXTENSION).");base64,/", $img)){
		}else if(preg_match("/^data:image\/(png|jpeg|gif);base64,/", $img)){
			//delet前置data
		
			$img = preg_replace("/^data:image\/(png|jpeg|gif);base64,/", '', $img);
		
		
			$img = str_replace(' ', '+', $img);
			//轉換base64 data
			$data = base64_decode($img);
			file_put_contents($filename, $data);
		}
		
		if( is_file($filename) === false ) return 'no file';
		
		return $newUrl;
	}
	
	
	
	function del_topic_by_index($index){
		$this->db->flush_cache();
		$this->db->delete('topic', array('auto_index' => $index));
		$this->db->delete('topic_options', array('topic_index' => $index));
		
		
		$tmp = array('success' => 'Y', 'msg' => '已刪除', 'index' => $index);
		return $tmp;
		
	}
	
	function topic_post_type($index = '',$field='*'){
		$this->db->flush_cache();
		$this->db->select($field)->from('topic_post_type');
		if($index){
			$this->db->where('auto_index',$index);
		}
	
		$query = $this->db->get();
		$data = $query->result_array();
	
		if($index && is_array($data)) 	$data = array_pop($data);
		return $data;
	}
	
	function topic_post_group($index = '',$field='*'){
		$this->db->flush_cache();
		$this->db->select($field)->from('topic_post_group');
		if($index){
			$this->db->where('auto_index',$index);
		}
	
		$query = $this->db->get();
		$data = $query->result_array();
	
		if($index && is_array($data)) 	$data = array_pop($data);
		return $data;
	}
	
	function topic_post_fb($index = '',$field='*'){
		$this->db->flush_cache();
		$this->db->select($field)->from('topic_post_fb');
		if($index){
			$this->db->where('topic_onshelfindex',$index);
		}
	
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	
	function topic_select($index = '',$field='*'){
		$this->db->flush_cache();
		$this->db->select($field)->from('topic_select')->where('online','1');
		if($index){
			$this->db->where('auto_index',$index);
		}
	
		$query = $this->db->get();
		$data = $query->result_array();
	
		if($index && is_array($data)) 	$data = array_pop($data);
		return $data;
	}
	
	function cutText_for_pofans($text , $cut = false){
		
		$text = str_replace(' ', '',$text);
		$text = str_replace('<br>', '_br_',$text);
		$text = str_replace('<br/>', '_br_',$text);
		
		$search = array ('@ ]*?>.*?@si',
				'@<[\/\!]*?[^<>]*?>@si',
				'@([\r\n])[\s]+@',
				'@&(quot|#34);@i',
				'@&(amp|#38);@i',
				'@&(lt|#60);@i',
				'@&(gt|#62);@i',
				'@&(nbsp|#160);@i',
				'@&(iexcl|#161);@i',
				'@&(cent|#162);@i',
				'@&(pound|#163);@i',
				'@&(copy|#169);@i');
		
		$replace = array ('',
				'',
				'_br_',
				'"',
				'&',
				'<',
				'>',
				' ',
				chr(161),
				chr(162),
				chr(163),
				chr(169));
		$text = preg_replace($search, $replace, $text  );
		
		$cut_text = '';
		$cut_index = 0;
		foreach(explode('_br_', $text) AS $v){			
			if($v) $cut_text .= $v."\r\n";			
			if($cut_index++ > 2 && $cut) break;
		}
		return $cut_text;
	}
	
	function pofans_options($index = null,$rand = false){
		if($index === null){
			return false;
		}
		
		$this->db->flush_cache();
		$this->db->select('*')->from('topic_options')->where('topic_index',$index);
		if($rand){
			$this->db->order_by('auto_index','random');
			$this->db->limit(4);
		}
	
		$query = $this->db->get();
		$data = $query->result_array();
	
		if(count($data) == 12){
			$check = false;
			foreach($data AS $k => $v){
				$this->db->flush_cache();
				$this->db->select('auto_index')->from('topic_defaultoptions')->where('option_name',$v['choose']);
				$total_sold = $this->db->count_all_results();
				if($total_sold) $check = true;
				else $check= false;
			}
				
			if($check) {
		
				foreach(array_rand($data, 4) AS $v){
					$data[$v]['horo'] = true;
					$temp[] = $data[$v];
				}
		
				return $temp;
			}
		}
		
		return $data;
	}
	
	function topic_set_defoption_select($default_index, $topic_index){
		
		$this->db->flush_cache();
		$this->db->select('defoptions_option_pic.pic_url AS pic_url, topic_defaultoptions.option_name AS option_name')->from('defoptions_option_pic')->where('defoptions_select_index',$default_index);
		$this->db->join('topic_defaultoptions', 'topic_defaultoptions.auto_index = defoptions_option_pic.topic_defaultoptions_index');
		$this->db->where('pic_url !=', 'default_pic.jpg');
		$query = $this->db->get();
		$defoption = $query->result_array();
		foreach($defoption AS $value){
			
			$update_input = array('text_imgurl'=>$value['pic_url']);
			
			$this->db->where('topic_index', $topic_index);
			$this->db->where('choose', $value['option_name']);
			$this->db->update('topic_options', $update_input);
			$this->db->limit(1);
		}
		
		$this->db->flush_cache();
		$this->db->select('pic_url')->from('defoptions_select_pic');
		$this->db->where('auto_index',$default_index);
		$query = $this->db->get();
		
		$select_pic = array_pop($query->result_array());
		$update_input = array('options_imgurl' => $select_pic['pic_url']);
		
		$this->db->where('auto_index', $topic_index);
		$this->db->update('topic', $update_input);
		$this->db->limit(1);
		
	}
	
	function topic_defaultselect($index = ''){
		$this->db->flush_cache();
		$this->db->select('*')->from('topic_select')->where('online','1')->where('defaultoptions','1');
		if($index){
			$this->db->where('auto_index',$index);
		}
	
		$query = $this->db->get();
		$data = $query->result_array();
	
		if($index && is_array($data)) 	$data = array_pop($data);
		return $data;
	}
	
	function topic_defaultselect_pic($index = ''){
		$this->db->flush_cache();
		$this->db->select('*')->from('defoptions_select_pic')->where('online','1');
		if($index){
			$this->db->where('auto_index',$index);
		}
	
		$query = $this->db->get();
		$data = $query->result_array();
	
		if($index && is_array($data)) 	$data = array_pop($data);
		return $data;
	}
	
	function topic_defaultoption_pic($index = ''){
		$data = array();
		if(!$index ){
			
			$this->db->flush_cache();
			$this->db->select('auto_index,pic_url')->from('defoptions_select_pic');
			$this->db->where('online','1');
			$query = $this->db->get();
			foreach ($query->result_array() AS $select_pic){				
				$select_list['select_pic'] = $select_pic['pic_url'];
				$select_list['defselect_index'] = $select_pic['auto_index'];
				$this->db->flush_cache();
				$this->db->select('pic_url,option_name')->from('defoptions_option_pic');
				$this->db->join('topic_defaultoptions', 'topic_defaultoptions.auto_index = defoptions_option_pic.topic_defaultoptions_index');
				$this->db->where('defoptions_select_index',$select_pic['auto_index']);				
				$this->db->order_by('topic_defaultoptions_index','ASC');
				
				$query = $this->db->get();
				$options = $query->result_array();
				$select_list['options'] = $options;
				$data[] = $select_list;
			}
			
			return $data;
		}
		
		$this->db->flush_cache();
		$this->db->select('pic_url')->from('defoptions_option_pic');
		$this->db->where('defoptions_select_index',$index);
		
	
		$query = $this->db->get();
		$options = $query->result_array();
		
		$this->db->flush_cache();
		$this->db->select('pic_url')->from('defoptions_select_pic');
		$this->db->where('auto_index',$index);
		$query = $this->db->get();
		$select_pic = array_pop($query->result_array());
		$data['select_pic'] = $select_pic['pic_url'];
		$data['options'] = $options;
		return $data;
	}
	
	function check_fans_exist_and_insert($add_input){
		$result = array('success' => 'N', 'msg' => '');
	
		$this->db->flush_cache();
		$query = $this->db->select("COUNT(*) AS qun")
		->from("topic_group")
		->where("name", $add_input['name'])
		->where("online", '1')
		->get();
		$temp = $query->row_array();
	
		if($temp['qun'] == 0){
				
			$this->db->insert('topic_group', $add_input);
			$result['success'] = 'Y';
			$result['msg'] = '新增成功';
		}else{
			$result['msg'] = '此名稱已經有人用了哦!!!';
		}
		return $result;
	}
	
	function check_fans_exist_and_update($add_input,$index){
		$result = array('success' => 'N', 'msg' => '');
	
		$this->db->flush_cache();
		$query = $this->db->select("COUNT(*) AS qun")
		->from("topic_group")
		->where("auto_index", $index)
		->where("online", '1')
		->get();
		$temp = $query->row_array();
	
		if($temp['qun'] == 0){
			$result['msg'] = '無該筆資料';
		}else{
				
			$this->db->where('auto_index', $index);
			$this->db->update('topic_group', $add_input);
			$result['success'] = 'Y';
			$result['msg'] = '修改成功';
		}
		return $result;
	}

	
	function check_topic_exist_and_insert($add_input,$add_options){
		$result = array('success' => 'N', 'msg' => '');
		
		$this->db->flush_cache();
		$query = $this->db->select("COUNT(*) AS qun")
		->from("topic")
		->where("topic", $add_input['topic'])
		->where("online", '1')
		->get();
		$temp = $query->row_array();
	
		if($temp['qun'] == 0){
	
			$this->db->insert('topic', $add_input);		

			//重新建立選項
			$topic_index = $this->db->insert_id();
			$select_index =$add_input['options_select'];
			if($topic_index && $select_index){
				$this->db->delete('topic_options', array('select_index' => $topic_index));				
				foreach($add_options AS $value){					
						$add_option = array('choose'=>$value['choose'],'title'=>$value['title'],'text'=>$value['text'],'text_imgurl'=>$value['text_imgurl'],'topic_index'=>$topic_index,'select_index'=>$select_index);
						$this->db->insert('topic_options', $add_option);						
					
				}
			}
			
			
			
			
			$result['success'] = 'Y';
			$result['msg'] = $topic_index;
		}else{
			$result['msg'] = '此題目已經有了哦!!!';
		}
		return $result;
	}
	
	function check_topic_exist_and_update($add_input,$add_options,$index){
		$result = array('success' => 'N', 'msg' => '');
	
		$this->db->flush_cache();
		$query = $this->db->select("COUNT(*) AS qun")
		->from("topic")
		->where("auto_index", $index)
		->where("online", '0')
		->get();
		$temp = $query->row_array();
	
		if($temp['qun'] == 0){
			$result['msg'] = '無該筆資料';
		}else{
	
			$this->db->where('auto_index', $index);
			$this->db->update('topic', $add_input);
			
			//重新建立選項
			$topic_index = $index;
			$select_index =$add_input['options_select'];
			if($topic_index && $select_index){
				$this->db->delete('topic_options', array('topic_index' => $topic_index));
				foreach($add_options['title'] AS $key => $title){
					if($title){
						$add_option = array('choose'=>$add_options['choose'][$key],'title'=>$title,'text'=>$add_options['text'][$key],'text_imgurl'=>$add_options['text_imgurl'][$key],'topic_index'=>$topic_index,'select_index'=>$select_index);
						$this->db->insert('topic_options', $add_option);
					}
				}
			}
			
			$result['success'] = 'Y';
			$result['msg'] = '修改成功';
		}
		return $result;
	}
	
	
	
	
	function get_ifunso_tests_img($url){
		$temp = getimagesize($url);
		if(is_array($temp)){
			$tmp['width'] = $temp[0];
			$tmp['height'] = $temp[1];
				
			if($tmp['width'] >= 272 && $tmp['height'] >= 204){
				$img = file_get_contents($url);
		
				// 設定時區
				date_default_timezone_set("Asia/Taipei");
		
				// 暫存資料夾路徑
				$temp_targetPath = BASEPATH.'../uploads/temp/'.date('Ymd').'/';
				if(!is_dir($temp_targetPath)){
					mkdir($temp_targetPath, 0777);
				}
		
				// 取得副檔名
				// 拆解 "."
				$temp1 = explode(".", $url);
				// 有些圖片後面會帶 get 參數，所以在拆解 "?"
				$temp2 = explode("?", $temp1[COUNT($temp1)-1]);
				$ext = ".".$temp2[0];
		
				// 另取檔名
				$thumb = date('Ymd').'_'.uniqid();
				$targetFile = rtrim($temp_targetPath,'/') . '/' .$thumb.$ext;
		
				// 搬圖片
				if(file_put_contents($targetFile, $img)){
					chmod($targetFile,0777);
					$url = date('Ymd').'/'.$thumb.$ext;
						
					$md5_files = md5_file($targetFile);
						
					//echo $md5_file;exit();
						
					/*
					 // 搜尋資料庫是否存在相同圖片
					$this->db->flush_cache();
					$query = $this->db->select("COUNT(*) AS qun")
					->from("img_upload")
					->where("md5_file", $md5_files)
					->get();
					$is_exist_file = $query->row_array();
					*/
					$is_exist_file['qun'] = 0;
					if($is_exist_file['qun'] == 0){
						// 記錄到資料庫
						$insert_data = array(
								'file_path' => $thumb.$ext,
								'type' => 'quote',
								'fk_category' => 0,
								'md5_file' => $md5_files,
								'online' => 'Y',
								'create_time' => date('Y-m-d H:i:s')
						);
						if($this->db->insert('img_upload', $insert_data)){
							$data = 'Y';
						}else{
							$data = 'N,資料寫入失敗，請稍候再試!!';
						}
					}else{
						$data = 'N,圖片重複!!';
						unlink($targetFile);
					}
				}else{
					$data = 'N,圖片錯誤!!';
				}
		
				return $url;
			}else{
				$data = 'N,圖片大小錯誤';
			}
		}else{
			$data = 'N,圖片網址抓取失敗';
		}
		
	}
	
}