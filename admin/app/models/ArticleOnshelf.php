<?php 
class ArticleOnshelf extends \Phalcon\Mvc\Model
{
	
	
	
	public function initialize()
	{
		$this->belongsTo("create_account_index", "AdminAccount", "auto_index");
		$this->belongsTo("update_account_index", "AdminAccount", "auto_index");
	}
	
	public function beforeSave()
	{
		
	}
	
	public function afterFetch()
	{
		
	}
	
	public function get_AdminAccount_field($field = false)
	{
		if($field) $field = $this->AdminAccount->{$field};
		return $field;
		
	}
	
	
}

?>