
function set_span_editor(){
	$("SPAN.editable").on('click', function() {
			if(typeof($(this).attr("data-editable")) == "undefined") return false;
			
			$(this).attr("id",$(this).attr("data-editable"));
			CKEDITOR_destroy();
			CKEDITOR.replace(this,{ width:'440px' });
		});	
}
function CKEDITOR_destroy(){
	for ( instance in CKEDITOR.instances ){
		var changed_id = CKEDITOR.instances[instance].name;
        CKEDITOR.instances[instance].destroy();
        change_text($('#'+changed_id));
	}

}

function change_text(this_el){
	var text = $(this_el).html();
	var id = $(this_el).attr("data-editable");
	// console.info(id);
	// console.info(text);
}