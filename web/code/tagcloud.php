<?php 
	$start = microtime(true);
	session_start();
	header("Content-type: text/html; charset=utf-8");

	include_once("include/db.php");
	include_once("include/htmltpl.php");
	include_once("include/function.php");

	
	$curPageURL	=  curPageURL();
	$curServerURL = curPageURL(true);	
	$tagID = $_GET['tag'];
	$tag = explode("/", $_GET['tag']);	
	$tag = array_filter($tag);
	$tag_name = array();
	
	
	include_once('global_load.php');
	
	$head = array(   
   'url'  => $curPageURL,
   'baseUrl'   => $baseUrl,
   'title'  => '話題雲',
   'link:img_src' => $article['img'],
   'meta' => array(
            
     ['name' => 'name="keywords"'     ,'content'=> $tag_title ],
     ['name' => 'name="description"'    ,'content'=> $tag_title ],
     ['name' => 'property="og:description"'    ,'content'=> $tag_title ],
     
     ['name' => 'property="og:title"'   ,'content'=> $tag_title ],
     ['name' => 'property="og:image"'   ,'content'=> $ImgServerUrl.'/n'.$article['id'].'/f.jpg'],
     ['name' => 'property="og:type"'   ,'content'=> "article" ],
     ['name' => 'property="og:url"'   ,'content'=> $curPageURL ],
   	 ['name' => 'property="fb:admins"'    ,'content'=> $fb_admin ],
   	 ['name' => 'property="fb:app_id"'    ,'content'=> $fb_app_id ],
   )
 );
	
	
	
	
	
	$data['HEAD'][] = $head;
	$data['MENU'] = $tag_name;
	$data['mobile_MENU'] = $tag_name;
	$data['curServerURL'] = $curServerURL;
	
	
	$HTML['HTML'][] = $data;
	
	echo tpl_get_html("./tpl/","tag.tpl.html",$HTML);
	
	
?>