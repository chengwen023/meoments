<?php
use Phalcon\Image\Adapter\GD as GdAdapter;
use Phalcon\Image\Adapter\Imagick as ImagickAdapter;


class ImgController extends \Phalcon\Mvc\Controller
{

	public function indexAction()
	{
		
		$imageGD = new GdAdapter('lines.jpg');
		header('Content-type: image/png');
		
		echo $imageGD
		->text('測試', $offset_x = 0,$offset_y = 66,$opacity = 100, $color = '#336699',$size='66px','./app/fonts/dftpf4.ttf')
		->render ('png',100);
// 		->save('s.png');

		// 		echo $base64 = 'data:png;base64,' .base64_encode($base);
	}

	public function textAction($text='',$size = 20,$textRGB='0,0,0',$bgRGB='0,0,0,127',$x = 0,$y = 0)
	{
		if(!$text )$text  = "今天天氣真好";
		
		
		$ch=(strlen($text)-mb_strlen($text,'utf-8'))/2;		
		$en=mb_strlen($text,'utf-8') - $ch;
		
		
		$image = imagecreate($ch*$size*1.4+$en*$size*0.7, $size*1.4); // width = 800, height = 600
		imagesavealpha($image, true);
		
		$RGB = explode(',', $textRGB);
		$color = imagecolorallocate($image, $RGB[0], $RGB[1], $RGB[2]); // 字的顏色
		$border = imagecolorallocate($image, 0, 0, 0);
		$fill = imagecolorallocate($image, 255, 0, 0);
		
		// Fill the selection
		imagefilltoborder($image, 50, 50, $border, $fill);
		
		
		$RGB = explode(',', $bgRGB);
		$bgcolor = imagecolorallocatealpha($image, $RGB[0], $RGB[1], $RGB[2], $RGB[3]);
		imagefill($image, 0, 0, $bgcolor);
		
		
		$RGB = explode(',', $textRGB);
		$color = imagecolorallocate($image, $RGB[0], $RGB[1], $RGB[2]); // 字的顏色
		$strokecolor = imagecolorallocate($image, 0, 0, 0); // border的顏色
		$font[]  = './app/fonts/dftpf4.ttf'; // 字型
		$font[]  = './app/fonts/dffnh5.ttf'; // 字型
		
		$font[]  = './app/fonts/dftlf3.ttf'; // 字型
		$font[]  = './app/fonts/dfttmo9.ttf'; // 字型
		$font[]  = './app/fonts/dfttnc5.ttf'; // 字型
		$font[]  = './app/fonts/dfttww5.ttf'; // 字型
		$font[]  = './app/fonts/dftyf3.ttf'; // 字型
		
		
		$index = rand(0,count($font)-1);

		
		// imagettftext($image, 大小, 旋轉, 與左邊的距離, 與上面的距離, $black, $font, $text);
		imagettftext($image, $size, 0, $x, $y+$size, $color, $font[$index], $text);
		
		$this->imagettfstroketext($image, $size, 0, $x, $y+$size, $color, $strokecolor,$font[$index], $text,2);
		
		header('Content-type: image/png');
		imagepng($image);
		imagedestroy($image);

	}
	
	function imagettfstroketext(&$image, $size, $angle, $x, $y, &$textcolor, &$strokecolor, $fontfile, $text, $px) {
		for($c1 = ($x-abs($px)); $c1 <= ($x+abs($px)); $c1++)
		for($c2 = ($y-abs($px)); $c2 <= ($y+abs($px)); $c2++)
			$bg = imagettftext($image, $size, $angle, $c1, $c2, $strokecolor, $fontfile, $text);
			return imagettftext($image, $size, $angle, $x, $y, $textcolor, $fontfile, $text);
	}
}



?>