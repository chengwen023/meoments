<div>
	<form id="form_folder">
		<div class="tb-green">
			<table>
				<tr>
					<td width="66">
						粉絲團網址
					</td>
					<td>
						<input id="admin_name" type="text" name="name" value="" /><span style="color:#A73939;">(*)</span>
					</td>
				</tr>				
				<tr>
					<td colspan="2">
						<input type="submit" value="送出" style="padding:3px;font-size:14px;" />
					</td>
				</tr>
			</table>
		</div>
	</form>
</div>
<script>
$(function(){
	// 註冊驗證
	$("#form_folder").validate({			
		submitHandler: function(form) {
			$.fancybox.showLoading();
			// 透過 Ajax 驗證是否註冊
			$.ajax({
			  url: 'source/actions/validate',
			  cache: false,
			  dataType: 'html',
			  type: "POST",
			  data: $("#form_folder").serialize(),
			  success: function(data){
				
				$.fancybox.hideLoading();
				
				if(data == 'Y' ){
					console.log(data);
					location.reload();
				}else{
					console.log(data);
					return false;
				}
			  }
			});
			//form.submit();
			return false;
		}
	});
	// end of $("#signupForm").validate
});
</script>