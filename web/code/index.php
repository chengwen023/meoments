<?php 
	$start = microtime(true);
	
	header("Content-type: text/html; charset=utf-8");

	include_once("include/db.php");
	include_once("include/htmltpl.php");
	include_once("include/function.php");

	
	$curPageURL	=  curPageURL();
	$curServerURL = curPageURL(true);
	$article = array();
	$category_tables = array();
	$category_name = array();
	$account = array();
	
	
	
	if ( $_SERVER["SERVER_NAME"] == '127.0.0.1' || $_SERVER["SERVER_NAME"] == 'localhost'||  $_SERVER["SERVER_NAME"] == 'shoda.tw') {
		$ImgServerUrl = $curServerURL.'/news/file';
		$baseUrl = '/git/web/';
		$articleUrl = 'http://127.0.0.1/news/tpl/code/adonis.php?id=';
	}else{
		$ImgServerUrl = 'http://file.toments.com/';
		$baseUrl = '/web/';
		$articleUrl = 'http://toments.com/';
	}
	$img_tm = "CONCAT('$ImgServerUrl/n', `news_article_onshelf`.id, '/f.jpg')";
	$listUrl = "CONCAT('$articleUrl', `news_article_onshelf`.id,'/',title)";
	
	$memcached = new iplayfun_memcached('toments');
	$memcached->deleteAllKeys();
	
	//分類列表
	$keyValues[] = [
			'key' => 'categories' ,
			'func' => $memcached->SQLfunc( "SELECT * FROM `categories`" )
	];
	//廣告資訊
	$sql = "SELECT * FROM `ad_code` ";
	$keyValues[] = [
			'key' => 'ad_code' ,
			'func' => $memcached->SQLfunc( $sql )
	];
	
	//推薦文章列表
	$sql = "SELECT id, title, $img_tm AS img, $listUrl AS url FROM `news_article_onshelf` ORDER BY id DESC LIMIT 10 ";
	$keyValues[] = [
			'key' => 'article_list' ,
			'func' => $memcached->SQLfunc( $sql )
	];
	
	//推薦文章列表
	$sql = "SELECT id, title, $img_tm AS img, $listUrl AS url FROM `news_article_onshelf` ORDER BY id DESC LIMIT 19 ";
	$keyValues[] = [
			'key' => 'top_list' ,
			'func' => $memcached->SQLfunc( $sql )
	];
	
	//熱門文章列表
	$sql = "SELECT id, title, $img_tm AS img, $listUrl AS url FROM `news_article_onshelf` ORDER BY id DESC LIMIT 19 ";
	$keyValues[] = [
			'key' => 'hot_list' ,
			'func' => $memcached->SQLfunc( $sql )
	];
	
	$global  = $memcached->getMultiWithSet($keyValues,86400);

	foreach ( $global['categories'] AS $row ) {
		if ( $row['parent_id'] == 0 ) {
			$category_tables[$row['id']] = $row;
		} else {
			$category_tables[$row['parent_id']]['child_categroy'][] = $row;
		}
	
		$category_name[$row['id']] = $row['name'];
	}
	
	
// 	$loop_css = ['1'=>'box-w2-h2','2'=>'box-w2','5'=>'box-w2-h2','9'=>'box-w2','10'=>'box-w2','13'=>'box-w2'];
	$loop_css = ['1'=>'box-w2-h2','2'=>'box-w2','9'=>'box-w2','10'=>'box-w2','13'=>'box-w2'];
	foreach ( $global['top_list'] AS $key => $row ) {
		$index++;
		$row['index'] = $index;
		$row['class'] = $loop_css[$index];
		$loop['loop_article'][] = $row;
		
		
	}
	
	$index = 0;
	$top_css = ['8'=>'box-w2'];
	foreach ( $global['hot_list'] AS $key => $row ) {
		
		if($index++ >7)break;	
		if($index == 1){
			$top['top_one'][] = $row;
			continue;
		}	
		$row['class'] = $top_css[$index];
		$top['top_article'][] = $row;	
	}
	
	
	
	$index = 0;
	$top_css = ['1'=>'box-w2','3'=>'box-w2'];
	$top2['css'] = 'box-w2';
	$top2['class'] ='special-po';
	foreach ( $global['hot_list'] AS $key => $row ) {
		if($index++ >5)break;
		$row['class'] = $top_css[$index];
		$top2['top2_article'][] = $row;	
	}
	
	$data['top2'][] = $top2;
	
	
	$top2 = array();
	$index = 0;
	$top_css = ['1'=>'box-w2','4'=>'box-w2','8'=>'box-w2','9'=>'box-w2'];
	$top2['css'] = 'box-h2';
	$top2['class'] ='miss-po';
	foreach ( $global['top_list'] AS $key => $row ) {
		if($index++ >9)break;
		$row['class'] = $top_css[$index];
		$top2['top2_article'][] = $row;	
	}
	
	$data['top2'][] = $top2;
	
	$head = array(			
			'url'		=> $curPageURL,
			'baseUrl'   => $baseUrl,
			'title'		=> 'toments',
			'meta' => array(
					['name' => 'keywords' 			,'content'=> $article['title'] ],
					['name' => 'description'		,'content'=> $article['fb_post_content'] ],
	
					['property' => 'og:description'		,'content'=> $article['fb_post_content'] ],
					['property' => 'og:title'			,'content'=> $article['title'] ],
					['property' => 'og:image'			,'content'=> $curServerURL.'/news/file/n'.$article['id'].'/t.jpg' ],
					['property' => 'og:site_name'		,'content'=> "Toments" ],
					['property' => 'og:url'				,'content'=> $curPageURL ],
					['property' => 'og:type'			,'content'=> "website" ],
	
					// 					['property' => 'fb:admins' 			,'content'=> '588005798009398' ],
			// 					['property' => 'fb:app_id' 			,'content'=> '588005798009398' ],
			)
	);
	
	
	
	$data['loop'][] = $loop;
	$data['top'][] = $top;
	
	
	$data['photo_list'] = $global['article_list'];
	$data['HEAD'][] = $head;
	$data['MENU'] = $category_tables;
	$data['DM728x90'] = $DM['DM_728x90'];
	$data['DM300x250'] = $DM['DM_300x250'];
	$data['DM334x280'] = $DM['DM_334x280'];
	$data['curPageURL'] = $curPageURL;
	$HTML['HTML'][] = $data;
	
	echo tpl_get_html("./tpl/","toments.tpl.html",$HTML);
	
?>