<?php
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\GraphSessionInfo;
use Facebook\FacebookClientException;
use Facebook\FacebookServerException;

use Phalcon\Events\Event,
	Phalcon\Mvc\User\Plugin,
	Phalcon\Mvc\Dispatcher,
	Phalcon\Mvc\Dispatcher\Exception as DispatchException,
	Phalcon\Acl;



class Facebookapi extends Plugin
{

	public $appid = '806536466070254'; // your AppID
	public $secret = 'ef661858f47225ac815f193ebc0da010'; // your secret
	private $access_token;
	public $session = FALSE;
	public $session_long='';
	
	public function initialize()
	{
		
		
	}
	
	public function onConstruct($session_token = false ,$appid = null, $secret = null)
	{
		if(isset($session_token) && $session_token ) $this->access_token = $session_token;
		else $this->access_token = 'CAALdimeOZBu4BAJA0nnXhLg1zMI86ycYpaIwFF1O3wYQ5chej44hBGyyWHmJcS0aWGnyw0b2lpLVlyxorwrz7NvZArGluLPFZCK8e9ydW45uLDgYLoedkaDkHHz62Y1kVI1NDWVZAx7JAnKvvZABngIu0iDuzRY7Yx438qZBxZAZBvn6mdX9bgfZCzl9HdRl7F0QZD';
		
		if(!empty($appid) && !empty($secret)){
			$this->appid = $appid;
			$this->secret = $secret;
		}
		FacebookSession::setDefaultApplication($this->appid ,$this->secret);	
		
		$this->session = new FacebookSession($this->access_token);
	}
	
	public function getSession()
	{
		return $this->session;
	}
	
	public function get_user_info($id=''){
		$instruct = !empty($id) ? "/".$id : "/me";
		$request = (new FacebookRequest(
				$this->session,
				'GET',
				$instruct
		))->execute()->getGraphObject()->asArray();
		return $request;
	}
}