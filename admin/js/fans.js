
var fans = new Object;

fans.add_edit = function(index){
	
	url = 'fb_fans/actions/add_fans';
	 
	$.fancybox.showLoading();
	$.ajax({
		url: url,
	  cache: false,
		dataType: 'html',
	  type: 'POST',
	  data:{
		index:index  
	  },
	  success: function(data){
	  	$.fancybox.hideLoading();
	  	$.fancybox.open(data);
	  }
	});
}
fans.pic_list = function(index){
	
	url = 'fb_fans/actions/pic_list';
	 
	$.fancybox.showLoading();
	$.ajax({
		url: url,
	  cache: false,
		dataType: 'html',
	  type: 'POST',
	  data:{
		index:index  
	  },
	  success: function(data){
	  	$.fancybox.hideLoading();
	  	$.fancybox.open(data);
	  }
	});
}
fans.add_group = function(type){
	
	if(typeof(type) == "undefined") type = 'fans';
	var url = 'fb_fans/actions/add_'+type;
	 
	$.fancybox.showLoading();
	$.ajax({
		url: url,
	  cache: false,
		dataType: 'html',
	  type: 'POST',
	  async : false,
	  success: function(data){
	  	$.fancybox.hideLoading();
	  	$.fancybox.open(data);
	  }
	});
}

fans.refresh = function(){	
	$.ajax({
		url: 'fb_fans/fansid_refresh',
	  cache: false,
	  dataType: 'json',
	  async : false,
	  type: 'POST',
	  success: function(data){		  
	  	if(data.success == 'Y'){		  		
	  		location.reload();
	  	}
	  }
	});
	
}


fans.igotest_refresh = function(){
	
	$.ajax({
		url: 'fb_fans/igotest_fansid_refresh',
	  cache: false,
	  dataType: 'json',
	  async : false,
	  type: 'POST',
	  success: function(data){		  
	  	if(data.success == 'Y'){		  		
	  		location.reload();
	  	}
	  }
	});
	
}


fans.group_edit = function(index){
	
	
	url = 'fb_fans/actions/edit_fansid';
	 
	$.fancybox.showLoading();
	$.ajax({
		url: url,
	  cache: false,
		dataType: 'html',
	  type: 'POST',
	  data:{
		index:index  
	  },
	  success: function(data){
	  	$.fancybox.hideLoading();
	  	$.fancybox.open(data);
	  }
	});
}

fans.del = function(index){
	$.fancybox.open($('.del_box').html());
	$('.confirm_yes').click(function(){
		
		$.fancybox.showLoading();
		$.ajax({
			url: 'fb_fans/actions/del',
		  cache: false,
		  dataType: 'json',
		  async : false,
		  type: 'POST',
		  data: {
			  index:index
		  },
		  success: function(data){
			  
		  	$.fancybox.hideLoading();
		  	
		  	if(data.success == 'Y'){		  		
		  		$.fancybox.open(data.msg,{
		  			beforeClose : function(){
		  				$('table tr#fans_'+data.index).remove();
		  			}
		  		});
		  	}
		  }
		});
	});
	$('.confirm_no').click(function(){
		$.fancybox.close(true);
	});
}