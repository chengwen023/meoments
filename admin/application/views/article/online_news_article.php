<style>
#tips_box{
	position: fixed;
	font-size: 16px;
	display: none;
	padding: 5px 10px;
	background: #fff;
	box-shadow: 1px 1px 2px 1px rgba(33,66,99,.5);
	border-radius:5px;
	z-index: 10000;
}

.online:not(.off) A:not(:first-child ) {
	display: none;
}

.online.off A:first-child {
	display: none;
}

[class^="icon-"] {
	margin-right: 0px;
}

.google_ad_1 {
	background: url(images/google_online.png) right bottom no-repeat;
}

/*.google_ad_0 {
	opacity: 0.4;
	background: url(images/google_offline.png) right bottom no-repeat;
}*/


</style>

<div class="widget adonis">
	<form id="list_orderby" method="post">
		<input id="order_by" type="hidden" name="order_by">
		<input id="fans_selected" type="hidden" name="fans_selected" value="{fans}">
		<input id="days_selected" type="hidden" name="days_selected" value="{days}">
		作者：<select name="fans">
		{fans_list}
	　		<option id="{auto_index}" value="{auto_index}">{id}</option>
		{/fans_list}
		</select>
		
		&nbsp;日期：
		<select name="days">
		{days_list}
	　		<option id="{day}" value="{day}">{day}</option>
		{/days_list}
		</select>

		&nbsp;<input type="submit" value="查詢">
	</form>

	<form id="list_orderby" method="post">
		
		編號：<input id="article_id" name="article_id" style="width:60px; height:30px; background:rgb(238,233,233);">
		&nbsp;<input type="submit" value="查詢">
	</form>

	<table cellpadding="0" cellspacing="0" width="100%" class="tDefault tLight">
		<thead>
			<tr >
				<td title="編號" tips ="使用滑鼠點選 可以排序該欄位" class="order_control" data-orderby = "id">編號<i class="icon-chevron-{by-id}"></i></td>
				<td title="標題">標題</td>
				<td title="封面圖片" class="rowhide">封面圖片</td>
				<td title="上線時間" class="rowhide order_control" data-orderby="online_time">上線時間<i class="icon-chevron-{by-online_time}"></i></td>
				<td title="建立者" class="order_control" data-orderby="create_user_id">建立者<i class="icon-chevron-{by-create_user_id}"></i></td>
				<td title="來源" class="order_control" data-orderby="fb_sitename">來源<i class="icon-chevron-{by-fb_sitename}"></i></td>
				
				<td title="點擊數" class="order_control" data-orderby="views">點擊數<i class="icon-chevron-{by-views}"></i></td>
				<td title="頂次數" class="order_control" data-orderby="like_click_count">頂次數<i class="icon-chevron-{by-like_click_count}"></i></td>
				<td title="頂留言" class="order_control" data-orderby="like_message_count">頂留言<i class="icon-chevron-{by-like_message_count}"></i></td>
				<td title="噓次數" class="order_control" data-orderby="unlike_click_count">噓次數<i class="icon-chevron-{by-unlike_click_count}"></i></td>
				<td title="噓留言" class="order_control" data-orderby="unlike_message_count">噓留言<i class="icon-chevron-{by-unlike_message_count}"></i></td>
				<td title="google廣告" class="order_control" data-orderby="google_ad">google<i class="icon-chevron-{by-google_ad}"></i></td>
				<td title="上下架" class="order_control" data-orderby="online">上下架<i class="icon-chevron-{by-online}"></i></td>
			</tr>
		</thead>
		
		<tbody>
			{content}
			<tr id="user_{auto_index}" class="user_row" >
				<td style="white-space:nowrap;">{id}</td>
				<td><a href="{front_url}{id}" target="_blank">{article_title}</a></td>
				<td><img src="http://file.toments.com/n{id}/t_m.jpg" style="height: 84px;"/></td>
				<td>{online_time}</td>
				<td>{user_name}</td>
				<td><a href="{source_url}" target="_blank" alt="{fb_title}" title = "{fb_title}">{fb_sitename}</td>
				
				<td>{views}</td>
				<td>{like_click_count}</td>
				<td>{like_message_count}</td>
				<td>{unlike_click_count}</td>
				<td>{unlike_message_count}</td>
				<td><div class="google_ad_{google_ad}" alt="" style="width: 40px; height: 40px;"></div></td>

				<td style="white-space:nowrap;">
					<span class="article_online {off}" data-index="{id}"> 
						<a href="javascript:void(0);" class="view tablectrl_small bBlue" original-title="View" title="觀看" alt="觀看" onclick="article.add_edit_news_article({id}, 'view');">
						<span class="iconb" data-icon="" ></span></a>	
					</span>	
					<!-- <a href="javascript:void(0);" class="delete tablectrl_small bRed {edit_control}" original-title="Del" title="刪除" alt="刪除" onclick="article.del({id});">
					<span class="iconb" data-icon="" ></span></a> -->
					<span class="online {off}" data-index="{id}"> 
						<a href="javascript:void(0);" class="tablectrl_small bBlue" original-title="Edit" title="上架中"
							alt="上架中">上架中<span class="iconb" data-icon=""></span></a>
						<a href="javascript:void(0);" class="tablectrl_small bGold" original-title="Edit" title="下架中" 
							alt="下架中">下架中<span class="iconb" data-icon=""></span></a>
					</span>
					<a href="http://i-kuso.com/pofans2/message/index.php?id={pofans2_id}" target="_blank">{pofans2_id}</a>		
				</td>														
			</tr>			
			{/content}
		</tbody>
	</table>
	
</div>
{page_list}
<script src="js/article.js"></script>
<script src="js/span_editor.js"></script>
<style>

thead TD{
white-space: nowrap;
}
.hide, .topicview_{
	display: none;
}
.topicview_hide{
	display: initial;
}
/*.tLight tbody td{
	padding: 0px 16px;
}*/
.external_link{
	background: none;
}
	.more_page{
		text-align: center;
	}
	
	.more_page SPAN {
		-moz-box-shadow:inset 0px 1px 0px 0px #ffffff;
		-webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;
		box-shadow:inset 0px 1px 0px 0px #ffffff;
		background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #dfdfdf));
		background:-moz-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
		background:-webkit-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
		background:-o-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
		background:-ms-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
		background:linear-gradient(to bottom, #ededed 5%, #dfdfdf 100%);
		filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#dfdfdf',GradientType=0);
		background-color:#ededed;
		-moz-border-radius:6px;
		-webkit-border-radius:6px;
		border-radius:6px;
		border:1px solid #dcdcdc;
		display:inline-block;
		cursor:pointer;
		color:#777777;
		font-family:arial;
		font-size:15px;
		font-weight:bold;
		padding:6px 14px;
		text-decoration:none;
		text-shadow:0px 1px 0px #ffffff;
	}
	.more_page SPAN.page_selected {		
		color:#c92200;
		text-shadow:0px 1px 0px #ded17c;
	}
	
	.more_page SPAN:hover {
		color:#ffffff;
		text-shadow:0px 1px 0px #528ecc;
		-moz-box-shadow:inset 0px 1px 0px 0px #bbdaf7;
	    -webkit-box-shadow:inset 0px 1px 0px 0px #bbdaf7;
	    box-shadow:inset 0px 1px 0px 0px #bbdaf7;
		background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #378de5), color-stop(1, #79bbff));
		background:-moz-linear-gradient(top, #378de5 5%, #79bbff 100%);
		background:-webkit-linear-gradient(top, #378de5 5%, #79bbff 100%);
		background:-o-linear-gradient(top, #378de5 5%, #79bbff 100%);
		background:-ms-linear-gradient(top, #378de5 5%, #79bbff 100%);
		background:linear-gradient(to bottom, #378de5 5%, #79bbff 100%);
		filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#378de5', endColorstr='#79bbff',GradientType=0);
		background-color:#378de5;
		
		border:1px solid #84bbf3;
	}
	.more_page SPAN:active {
		position:relative;
		top:1px;
	}
	form#list_orderby {
		margin: 20px 30px 20px 10px;
		display: inline-block;
	}
	.tLight tbody td.red_string{
		color: red; 
	}
	.tLight tbody td.blue_string{
		color: blue; 
	}
	.tLight tbody td.green_string{
		color: green; 
	}
	.tLight tbody td{
		max-width: 320px;
		overflow: hidden;
		text-overflow : ellipsis;
	}
</style>

<script>
$(document).ready(function() {
	var fans = $('#fans_selected').val();
    $('#'+fans).attr("selected", "selected");

    var day = $('#days_selected').val();
    $('#'+day).attr("selected", "selected");
});

function order_by(this_td){

	$('#order_by').val( $(this_td).attr('data-orderby'));

	$('#list_orderby').submit();

}

$(".order_control").live( "click", function() {
	var order_name = $(this).attr('data-orderby');
	console.info(this);
	if(order_name) order_by(this);
	
	return false;
});	

$('[tips]').hover(
	function() {
	    var x = $(this).offset().left;
	    var y = $(this).offset().top;
		var tips_box = document.createElement('div');
	    $(tips_box).attr("id","tips_box").html($(this).attr('tips')).appendTo('body');

	    var tips_width = $('#tips_box').width()+40;
	    var tips_height = $('#tips_box').height();
	  
	    var this_width = $(this).width();
	    var this_height = $(this).height();
	 
	    var diff_x = (tips_width + x > $(window).width())?$(window).width()-(tips_width + x):((this_width-$('#tips_box').width())/2);
	    var c_x = x + diff_x ;

	    var diff_y = (tips_height + y > $(window).height())?$(window).height()-(tips_height + y):(this_height+(tips_height)/2);
	    var c_y = y + diff_y ;
		 
	    $(tips_box).css('left',c_x).css('top',c_y).show();
	  			   
	}, function() {
		$('#tips_box').remove();
	}
);

$(".online").click(function() {
	var index= $(this).attr('data-index'); 
	var online = $(this).hasClass("off")
	console.info(index);
	if(online) {
		$(this).removeClass("off");
		$('.article_online[data-index='+index+']').removeClass("off");
		
	} else { 
		$(this).addClass("off");
		$('.article_online[data-index='+index+']').addClass("off");
	}

	if(index)
		$.ajax({
		  url: 'news/news_article_online/',
		  dataType: 'html',
		  type: "POST",
		  data: {
			  online:online,
			  index:index
		  },
		  success: function(data){
		  	console.info(data);
		  	// if ( data ) alert(data);
		  },error: function(data){
		  	console.info(data);
		  }
		});
	
});
</script>