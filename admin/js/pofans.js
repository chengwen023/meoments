var error_pofans = new Array();
function pofans_view(this_span){ 
	$('.pofans').removeClass('focus');	
	$(this_span).next('.pofans').addClass('focus');
}

function pofans(this_span){
	var pofans_div = $(this_span).parent('DIV[class="pofans focus"]');
	
	var img_url = $(pofans_div).children('INPUT[name="photo_url"]').val();
	var img_text = $(pofans_div).children('TEXTAREA[name="photo_message"]').html();
	var link_url = $(pofans_div).children('INPUT[name="link_url"]').val();
	var link_text = $(pofans_div).children('TEXTAREA[name="link_message"]').html();
	
	var text = $(pofans_div).children('TEXTAREA[name="text_message"]').html(); 

	var onshelf_index = $(pofans_div).attr('data-index');
	
	//url = 'http://192.168.1.122/pofans/aip_rec_article.php';
	url = 'http://i-kuso.com/pofans/aip_rec_article.php';

	var pofans_test = true;

	$.ajax({
		url: '_admin/topic/onshelf_timesadd',
		dataType: 'html',
	  type: 'POST',
	  data:{
		  onshelf_index:onshelf_index
	  },
	  success: function(data){
	  }
	});

	if($(pofans_div).children('INPUT[name="pofans_img_check"]').attr('checked')){
		$.ajax({
			url: url,
			dataType: 'json',
		  type: 'POST',
		  data:{
			  message:img_text,
			  img_url:img_url,
			  test:pofans_test
		  },
		  success: function(data){		 
			  if(data.state){
			  		pofans_log(onshelf_index,data.index);
			  	  }
		  },
		  error: function(data){
			  alert('error');
		  }
		});
	};
	if($(pofans_div).children('INPUT[name="pofans_link_check"]').attr('checked')){
		$.ajax({
			url: url,
			dataType: 'json',
		  type: 'POST',
		  data:{
			  message:link_text,
			  url:link_url,
			  test:pofans_test
		  },
		  success: function(data){		 
			  if(data.state){
			  		pofans_log(onshelf_index,data.index);
			  	  }
		  },
		  error: function(data){
			  alert('error');
		  }
		});
	};
	if($(pofans_div).children('INPUT[name="pofans_text_check"]').attr('checked')){
		$.ajax({
			url: url,
			dataType: 'json',
		  type: 'POST',
		  data:{
			  message:text,
			  test:pofans_test
		  },
		  success: function(data){		 
		  	  if(data.state){
		  		pofans_log(onshelf_index,data.index);
		  	  }
		  },
		  error: function(data){
			  alert('error');
		  }
		});
	};
	
		
}
function pofans_bat(){
	var data = '<div style="text-align: center;background-color: rgba(33,33,99,0.3);" >Pofans 批次傳送</div><div style="width:400px;height:600px;"><ol id="pofans_batlog"></ol></div>'; 

	$.fancybox.open(data,{	  
  		autoCenter   : true,
	  	helpers : {
	        overlay : {
	        	closeClick: true
	        }
        },
        beforeClose : function(){
            
            if( $('INPUT[class="pofans_bat"]:checked').length > 0 && confirm("是否重新發送 Pofans Error視窗!?") ){
            	pofans_exec();
            	return false;
            }else
        		return(confirm("是否關閉 Pofans 批次視窗!?"));	        	
        }
	  });

	pofans_exec();
}

function pofans_exec(){
	url = 'http://i-kuso.com/pofans/aip_rec_article.php';

	var pofans_test = true;


	/**/
	$('INPUT[class="pofans_bat"]').each( function(){
		if($(this).prop("checked")){

			var this_checkbox = $(this);
			var onshelf_index = $(this).attr('data-onshelf');

			var img_text = $(this).parents("TD").prev().find('TEXTAREA[name="photo_message"]').html();
			var link_text = $(this).parents("TD").prev().find('TEXTAREA[name="link_message"]').html();

			
			
			$.ajax({
				url: '_admin/topic/onshelf_timesadd',
				dataType: 'html',
			  type: 'POST',
			  data:{
				  onshelf_index:onshelf_index
			  },
			  success: function(data){
			  }
			});
			
			if( $(this).attr('data-type') == 'img' ){				
				var img_url = $(this).attr('data-img');				
				pofans_sendingBox("圖片:"+$(this).attr('data-title'),onshelf_index,"data-imgid");
				
				
				$.ajax({
					url: url,
					dataType: 'json',
				  type: 'POST',
				  data:{
					  message:img_text,
					  img_url:img_url,
					  test:pofans_test
				  },
				  success: function(data){	
					  pofans_sended(onshelf_index,"data-imgid");
					  $(this_checkbox).prop("checked", false);
					  if(data.state){
					  		pofans_log(onshelf_index,data.index);
					  	  }
				  },
				  error: function(data){
					  pofans_senderror(onshelf_index,"data-imgid");
				  }
				});

				
			}

			if( $(this).attr('data-type') == 'link' ){
				
				var link_url = 'http://i-gotest.com/q'+ $(this).val()+'.html';

				pofans_sendingBox("連結:"+$(this).attr('data-title'),onshelf_index,"data-linkid");
				
				$.ajax({
					url: url,
					dataType: 'json',
				  type: 'POST',
				  data:{
					  message:link_text,
					  url:link_url,
					  test:pofans_test
				  },
				  success: function(data){	
					 
					  pofans_sended(onshelf_index,"data-linkid");
					  $(this_checkbox).prop("checked", false);
					  if(data.state){
					  		pofans_log(onshelf_index,data.index);
					  	  }
				  },
				  error: function(data){
					  pofans_senderror(onshelf_index,"data-linkid");
				  }
				});
			}

		} 

		
	});
}

function pofans_sendingBox(title,index,type){	
	var add_li = document.createElement('LI');
	add_li.innerHTML=title;
	$(add_li).appendTo("#pofans_batlog").addClass('sending').attr(type,index);
	
}

function pofans_sended(index,type){	
	$('#pofans_batlog li['+type+'='+index+']').removeClass('sending').addClass('sended');
	
}
function pofans_senderror(index,type){	
	$('#pofans_batlog li['+type+'='+index+']').removeClass('sending').addClass('error');
	
}
function pofans_select_all(this_input){	
	if($(this_input).prop("checked")){
		$('INPUT[class="pofans_bat"]').each(function(){		
			if($(this).attr("data-type") == "link")
			$(this).attr('checked', $(this_input).prop("checked"));
		})
	}	
	else
	$('INPUT[class="pofans_bat"]').attr('checked', $(this_input).prop("checked"));	
}
function edit_pofans(this_span){
	var pofans_div = $(this_span).parent('DIV[class="pofans focus"]');
	var index = $(pofans_div).attr("data-index");
	console.info(index);
	alert('未完成');
	url = '/admin/topic/actions/edit_pofans'; 
	
	
	$.ajax({
		url: url,
	  cache: false,
		dataType: 'html',
	  type: 'POST',
	  data:{
		  index:index
	  },
	  success: function(data){
			
	  	$.fancybox.open(data,{	  
	  		autoCenter   : true,
		  	helpers : {
		        overlay : {
		        	closeClick: false
		        }
	        },
	        afterShow : function(){
	        	//$('.fancybox-inner').css('width', '620px');
	        },
	        beforeClose : function(){
	        	return(confirm("是否關閉 新增/編輯 資料視窗!?"));	        	
	        }
		  });
	  },error:function(data){
		  console.info(data);
		  	return false;

		}

		  
	});
}


function pofans_log(onshelf_index,pofans_index){
	$.ajax({
		url: '_admin/topic/pofans_log',
		dataType: 'html',
	  type: 'POST',
	  data:{
		  onshelf_index:onshelf_index,
		  pofans_index:pofans_index
	  },
	  success: function(data){
	  }
	});
}