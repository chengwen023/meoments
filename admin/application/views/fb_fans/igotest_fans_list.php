<div class="widget">
	<table cellpadding="0" cellspacing="0" width="100%" class="tLight">
		<form id="list_orderby" method="post">
			<input id="order_by" type="hidden" name="order_by" value="">
			<thead>
				<tr>
					<td title="名稱">名稱</td>
					<td title="粉絲團ID">粉絲團ID</td>
					<td title="粉絲團群組">粉絲團群組</td>
					<td title="動作" class="order_control" data-orderby = "online">動作<i class="icon-chevron-{by-online}"></i></td>					
				</tr>
			</thead>
		</form>
		<tbody>
			{content}
			<tr id="user_{auto_index}" class="user_row">
				<td><a href="http://www.facebook.com/{fans_id}" target="_blank">{fans_name}</a></td>
				<td>{fans_id}</td>
				<td>{group_names}</td>
				<td>

					<span class="online {off}" data-index="{auto_index}"> 
						<a href="javascript:void(0);" class="tablectrl_small bBlue" original-title="Edit" title="上架"
							alt="上架">上架<span class="iconb" data-icon=""></span></a>
						<a href="javascript:void(0);" class="tablectrl_small bGold" original-title="Edit" title="下架" 
							alt="下架">下架<span class="iconb" data-icon="">
					</span></a> 
							
				</span></td>
			</tr>

			{/content}
		</tbody>
	</table>
</div>
<style>
<!--
.online A {
	position: relative;
}

.online A:first-child {
	z-index: 100;
}

.online A:not(:first-child ) {
	left: -53px;
	z-index: 1;
	opacity: 0;
	filter: alpha(opacity = 0);
	-moz-transform: rotateY(180deg);
	-webkit-transform: rotateY(180deg);
	transform: rotateY(180deg);
}

.online.off A:first-child {
	opacity: 0;
	filter: alpha(opacity = 0);
	-moz-transform: rotateY(-180deg);
	-webkit-transform: rotateY(-180deg);
	transform: rotateY(-180deg);
}

.online.off A:not(:first-child ) {
	opacity: 1;
	filter: alpha(opacity = 100);
	-moz-transform: rotateY(0deg);
	-webkit-transform: rotateY(0deg);
	transform: rotateY(0deg);
}
-->
</style>

<script src="js/fans.js"></script>

<script>
$(".online").click(function() {
	var index= $(this).attr('data-index'); 
	var online = $(this).hasClass("off")
	
	if(online) $(this).removeClass("off");
	else $(this).addClass("off");

	if(index)
		$.ajax({
		  url: 'fb_fans/igotest_fansid_online/',
		  dataType: 'html',
		  type: "POST",
		  data: {
			  online:online,
			  index:index
		  },
		  success: function(data){
			  console.info(data);
		  	/**/
		  }
		});
	
});
function order_by(this_td){

	$('#order_by').val( $(this_td).attr('data-orderby'));

	$('#list_orderby').submit();

}

$(".order_control").live( "click", function() {
	var order_name = $(this).attr('data-orderby');
	if(order_name) order_by(this);
	
	return false;
});	

</script>