<style>
#choose_list LI INPUT[type="text"],#choose_list LI TEXTAREA{
	width:350px;
}
#choose_list LI TEXTAREA{
	height:80px;
}

#choose_list LI{	
	width: 150px;
	float:left;
	margin-bottom: 20px;		
}

#choose_list LI DIV:not([class="imgurl_box"], [class="imgurl_box imgedit_focus"]){
	width:400px;
}



#choose_list LI DIV.del_btn{
	float:none;
	position: relative;
	cursor: pointer;
	text-align: center;
	width:80px;	
	border: 1px solid #D17A1C; 
	color:#D17A1C;
	margin-left: 415px;
	margin-bottom: 10px;
}

#choose_list LI DIV.del_btn:HOVER{
	background-color:#D17A1C;
	color: #fff;
}
.imgurl_box{
	width:80px;
	height: 60px;
	border-style: dotted;
	text-align: center;
	line-height: 60px;	
	cursor: pointer;
	margin-left: 20px;
	
}

.imgurl_box.imgedit_focus{
	color:#D17A1C;
}

.submit_btn{	
	color: #66a3d3;	
	border-color: #66a3d3;
}
.submit_btn:HOVER{	
	color: #fff;
	background-color: #678197;
	border-color: #678197;
}
</style>


	
	<div id="topic_info" style="width:1200px;">
		<form id="form_folder" onsubmit="return false">
		<div class="tb-green" style="width:600px;float: left;">
			<table>
				<thead>
					<tr class="column1">
						<th scope="col" colspan="3">
							<?php echo (!empty($topic['auto_index']) && $topic['auto_index'] > 0)? '編輯':'新增';?>資料
							<input type="hidden" name="auto_index" value="<?php echo (!empty($topic['auto_index'])?$topic['auto_index']:'');?>">
							<input type="submit" class="submit_btn" value="<?php echo (!empty($topic['auto_index']) && $topic['auto_index'] > 0)? '編輯完成':'新增完成';?>" style="float:right ;padding:3px;font-size:14px;" />							
						</th>
					</tr>
				</thead>							
				<tr>
					<td width="66">
						分類
					</td>
					<td colspan="2">
						<ul style="width: 500px;list-style: none;">
						<?php foreach($topic_group AS $value):?>
						<li style="float: left;margin-right: 10px;">
						<input class="fans_group" name="defoptions_group_index[]" type="checkbox" value="<?php echo $value['auto_index'];?>"  <?php echo (isset($value['checked']) && $value['checked'])?'checked="checked"':'';?> /><?php echo $value['defoptions_group_name'];?>
						</li>
						<?php endforeach;?>
						</ul>
					</td>
				</tr>
				<tr>
					<td width="66">
						選項類型
					</td>
					<td >
						<SELECT id="choose_type" name="topic_select" onchange="chooseType_changed(this.value)">
								<option value="0">請選擇</option>
								<?php foreach($topic_select AS $value):?>
								<option value="<?php echo $value['auto_index'];?>" <?php echo (isset($value['selected']) && $value['selected'])?'selected':'';?>><?php echo $value['select_name']?></option>
								<?php endforeach;?>
						</SELECT>
						<div>
							<input style="float:left;width: 100%;" type="text" name="defoptions_select_name" value="<?php echo empty($topic['defoptions_select_name'])?'':$topic['defoptions_select_name'];?>" />
						</div>
					</td>
					<td>						
						<div id="options_imgurlbox" class="imgurl_box" >
							<span>
							<?php if(!empty($topic['pic_url'])):?>
							<img src="uploads/temp/<?php echo $topic['pic_url']; ?>" width="80px" />
							<?php else:?>
							<img src="uploads/temp/default_pic.jpg" width="70px" />
							<?php endif;?>
							</span><input name="pic_url" type="hidden" value="<?php echo empty($topic['pic_url'])? 'default_pic.jpg':$topic['pic_url'];?>"/></div>
					</td>
				</tr>				
			</table>
			</div>
			<div class="tb-green"style="width:550px;height:600px;float: right;">
			<table>
				<thead>
					<tr class="column1">
						<th scope="col" >
							題目選項
						</th>
					</tr>
				</thead>
				<tr>
					<td width="66">					
							<ol id="choose_list">					
								<?php foreach($topic_options AS $value):?>
									<li>
										<div>
											<?php echo $value['title']?><br/>
											<input type="hidden" name="options[choose][]" value="<?php echo $value['topic_defaultoptions_index']?>"/>
										</div>
										<div class="imgurl_box" ><span><img src="uploads/temp/<?php echo $value['pic_url']?>" width="70px" /></span>
											<input type="hidden" name="options[text_imgurl][]" value="<?php echo $value['pic_url']?>" /></div>
										
									</li>
								<?php endforeach;?>
							</ol>							
						</td>						
					</tr>				
				</table>
			</div>	
			</form>
			<div id="img_selectbox" style="padding-top:15px;width:595px;height:600px;float: left;overflow:scroll;"></div>
		</div><!-- / tb-green-->
		
			
			
		
	
	<div id="img_clipbox" style="padding-top:15px;width:500px;float: left;overflow:visible;"></div>




<script>





var default_inputs = '';
set_imgurlbox();



function set_imgurlbox(){
	$(".imgurl_box").live( "click", function() {
		$(".imgurl_box").removeClass("imgedit_focus");
		$(this).addClass("imgedit_focus");
		$(this).children('INPUT[type="hidden"]').val('');
		img_select(this);
		return false;
	});	

	$(".del_btn").live( "click", function() {
		if($("#choose_list").children("li").length > 1) $(this).parent("li").remove();
		else alert('至少需有一個選項!');
		return false;
	});	
}

function img_select(this_div){

	var value = $(this_div).parents('FORM').find('INPUT[name="topic"]').val();
	
	if($(this_div).children('INPUT[type="hidden"]').attr("name") == "options[text_imgurl][]"){		
		var temp_value = $(this_div).parents('li').find('INPUT[name="options[title][]"]').val();
		if (temp_value) value = temp_value;
	}
	
	$.ajax({
		url: '_admin/choose/actions/search_page',
	  cache: false,
	  dataType: 'html',
	  type: 'POST',
	  data:{
		  def_value:value
	  },
	  success: function(data){
	  	$('#img_selectbox').html(data);
	  }
	});

}
//確認選好圖片，進入裁圖畫面
function img_check(){
	if($('.img_select').length < 1){
		alert('圖片最少選擇一張!!');
		return false;
	}else if($('.img_select').length > 1){
		alert('圖片最多只能選擇一張!!');
		return false;
	}
	
	$.ajax({
		url: '_admin/choose/actions/check_img',
	  cache: false,
	  dataType: 'html',
	  async : false,
	  type: 'POST',
	  data : 'img='+$('.img_select').attr('rel')+'&alt='+$('.img_select').attr('alt')+'&item='+$('input[name="search_item"]').val()+'&pg='+$('.img_pg').attr('rel'),
	  success: function(data){		  
	  	tmp = data.split(',');
	  	if(tmp[0] == 'N'){
	  		alert(tmp[1]);	  		
	  	}else{
	  		$('#topic_info').hide();
	  		$('#img_clipbox').html(data);
	  		
		  }
	  }
	});
}


function chooseType_changed(type){

	$.ajax({
		  url: '_admin/defoption_pic/choose_data/',
		  dataType: 'html',
		  type: "POST",
		  data: {
			  type:type
		  },
		  success: function(data){

			  $('#choose_list').html(data);
			  default_inputs = data;
			  set_imgurlbox();
		  	/**/
		  }
		});
}



	$(function(){
		
		// 註冊驗證
		$("#form_folder").validate({	
			rules: {
				defoptions_select_name: {
					required: true
				},
				topic_select: {
					required: function(element) {
						if( $(element).val() < 1 ){
							alert('請選擇選項類型!');
							$(element).focus();							
						}
						return true;
                    }
				},
				'options[choose][]': {
					required: true
				},
				'options[title][]': {
					required: true
				},'options[text][]': {
					required: true
				}				
			},
			messages: {
				topic: "題目資料不得為空",									
				'options[choose][]': "選項資料不得為空",
				'options[title][]': "選項資料不得為空",
				'options[text][]': "選項資料不得為空"					
			},		
			submitHandler: function(form) {
				
				// 透過 Ajax 驗證是否註冊
				$.ajax({
				  url: '_admin/defoption_pic/actions/validate',
				  cache: false,
				  dataType: 'json',
				  type: "POST",
				  data: $("#form_folder").serialize(),
				  success: function(data){		
					  	console.info(data);
				  	if(data.success == 'Y' ){
				  		alert(data.msg);
				  		location.reload();
				  	}else{
				  		alert(data.msg);
				  		$('input[name="id"]').focus();
				  		return false;
				  	}
				  	/**/
				  }
				});
				//form.submit();
				return false;
			}
		});
		// end of $("#signupForm").validate
	});
</script>