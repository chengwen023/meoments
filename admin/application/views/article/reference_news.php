<style>
.title {
	position: relative;

	/*background: rgba(16,78,139,.6);*/
	border-color: rgb(211,211,211);
	border-style: solid;
	border-width: 1px;
	/*border-right-width: 1px; 
	border-bottom-width: 1px;
	border-left-width: 1px;*/
	color: rgb(240,255,240);
	font-weight: bold;

	text-align: center;
	margin: 0 auto;
	padding: 10px 30px;
	width: 100%;
	height: 125px;
}

.news_photo {
	float: left;
}

.news_title {
	float: right;
	cursor: pointer;
	font-size: 18px;
	width: 360px;
	height: 120px;
	padding: 10px;
}

.no_match {
	text-align: center;
	color: rgba(178,34,34,.6);
	font-size: 18px;
	font-weight: bold;
	padding-top: 20px;
}

</style>

<?php if ( empty($reference_news) ) { ?>
	<div class="no_match">沒有匹配文章</div>
<?php } else { ?>
	<ul>
		{reference_news}
		<li id="{id}" >
			<div class="title">
				<div class="news_photo" style="width: 140px;height: 105px;"><img src="{article_img}"></div>
				<div class="news_title"><a href="http://toments.com/{id}" target="_blank">{title}<a/></div>
			</div>
			
		</li>
		{/reference_news}
	</ul>
<?php } ?>


<script type="text/javascript">

</script>