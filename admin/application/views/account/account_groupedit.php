<div>
	<form id="form_folder">
		<div class="tb-green">
			<table>
				<thead>
					<tr class="column1">
						<th scope="col" colspan="2">
							<?php echo (!empty($group_info['auto_index']) && $group_info['auto_index'] > 0)? '編輯':'新增';?>管理員
							<input type="hidden" name="auto_index" value="<?php echo (!empty($group_info['auto_index'])?$group_info['auto_index']:'');?>">
						</th>
					</tr>
				</thead>
				<tr>
					<td width="66">
						名稱
					</td>
					<td>
						<input id="admin_name" type="text" name="group_name" value="<?php echo empty($group_info['group_name'])? '':$group_info['group_name'];?>" /><br />
						<label class="error" generated="true" for="admin_name">(*)</label>
					</td>
				</tr>
				<tr>
					<td width="66">
						預設對應
					</td>
					<td>
						<select name="group_defaultstatus">
						<?php foreach($group_defaultstatus AS $value => $name):?>
							<option value="<?php echo $value;?>" <?php if(isset($group_info['group_defaultstatus']) && $value == $group_info['group_defaultstatus']):?>SELECTED<?php endif;?>><?php echo $name;?></option>
							<?php endforeach;?>
						</select>
					</td>
				</tr>
				<tr>
					<td width="66">
						權限
						<input type="checkbox" onchange="allselect_change(this)"/>
					</td>
					<td>
						<ul style="list-style: none;margin: 0;">
							<?php foreach($admin_menu AS $key => $menu): ?>
							<li>
								<span style="margin-right: 10px;float: left;"><input class="menu_competence" type="checkbox" onchange="menuselect_change(this,'<?php echo $menu['auto_index'];?>')"/><?php echo $menu['child_name'];?></span>
								<span style="float: right;">
									<?php foreach ($admin_competence AS $key =>$value):?>
									<input class="account_competence" name="group_default[<?php echo $menu['auto_index'];?>][]" type="checkbox" value="<?php echo $key;?>" 
									<?php if(isset($group_info['group_default'][$menu['auto_index']]) && is_array($group_info['group_default'][$menu['auto_index']]) &&  in_array($key, $group_info['group_default'][$menu['auto_index']])): ?>checked="checked"<?php endif;?>
									/><?php echo $value;?>
									<?php endforeach;?>
								</span>
							</li>
							<?php endforeach;?>
						</ul>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td colspan="2">
						<input type="submit" value="送出" style="padding:3px;font-size:14px;" />
					</td>
				</tr>
			</table>
		</div><!-- / tb-green-->
	</form>
</div>
<script>

function allselect_change(this_input){	
	$('INPUT[class="account_competence"]').attr('checked', $(this_input).prop("checked"));	
	$('INPUT[class="menu_competence"]').attr('checked', $(this_input).prop("checked"));
	
}
function menuselect_change(this_input,index){	
	$('INPUT[name="group_default['+index+'][]"]').attr('checked', $(this_input).prop("checked"));	
}

	$(function(){
		// 註冊驗證
		$("#form_folder").validate({			
			submitHandler: function(form) {
				
				$.fancybox.showLoading();
				// 透過 Ajax 驗證是否註冊
				$.ajax({
				  url: 'account/actions/validate_group',//_admin/account/actions/validate_group
				  cache: false,
				  dataType: 'json',
				  type: "POST",
				  data: $("#form_folder").serialize(),
				  success: function(data){
					
				  	$.fancybox.hideLoading();
				  	
				  	if(data.success == 'Y' ){
				  		alert(data.msg);
				  		location.reload();
				  	}else{
				  		alert(data.msg);
				  		$('input[name="group_name"]').focus();
				  		return false;
				  	}
				  	/**/
				  }
				});
				//form.submit();
				return false;
			}
		});
		// end of $("#signupForm").validate
	});
</script>