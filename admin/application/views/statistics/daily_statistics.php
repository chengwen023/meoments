<style>
#tips_box{
	position: fixed;
	font-size: 16px;
	display: none;
	padding: 5px 10px;
	background: #fff;
	box-shadow: 1px 1px 2px 1px rgba(33,66,99,.5);
	border-radius:5px;
	z-index: 10000; 
}

.article_total {
	float: left;
	margin: 25px 10px 20px 50px;
	font-size: 16px;
	font-weight: bold;
}
.article_total span{
	color: rgb(0,0,255);
}

</style>

<div class="daily_left daily_total">
	<form id="list_orderby" method="post">
		<input id="order_by" type="hidden" name="order_by">
		&nbsp;日期：
		<input class="date" name="start_date" type="date" onchange="change_start();" value="{start_date}" />
		<input class="date" name="end_date" type="date" onchange="change_end();" value="{end_date}" />

		&nbsp;<input class="submit" type="submit" value="查詢" onclick="send_date();">
		<br>
		<input class="date_button" type="button" value="昨日" onclick="change_date_yesterday();">
		<input class="date_button" type="button" value="本月" onclick="change_date_month('this');">
		<input class="date_button" type="button" value="上月" onclick="change_date_month('last');">
	</form>

	<div class="article_total">總觀看次數：<span>{views_total}</span></div>

	<table cellpadding="0" cellspacing="0" width="100%" class="tDefault tLight">
		<thead>
			<tr>
				<td title="作者" tips ="使用滑鼠點選 可以排序該欄位" class="order_control" data-orderby = "account_id">作者<i class="icon-chevron-{by-account_id}"></i></td>
				<td title="發文數" tips ="使用滑鼠點選 可以排序該欄位" class="order_control" data-orderby = "article_count">發文數<i class="icon-chevron-{by-article_count}"></i></td>
				<td>來源(改/翻/參)</td>
				<td>文章時間</td>
				<td title="頂噓" tips ="使用滑鼠點選 可以排序該欄位" class="order_control" data-orderby = "like_unlike">頂噓<i class="icon-chevron-{by-like_unlike}"></i></td>
				<td title="觀看次數" tips ="使用滑鼠點選 可以排序該欄位" class="order_control" data-orderby = "views">觀看次數<i class="icon-chevron-{by-views}"></i></td>
				<td>觀看平均</td>
				<td>貢獻度(%)</td>
			</tr>			
		</thead>
		
		<tbody class="statistics_tbody">
			{content}
			<tr id="{auto_index}">
				<td class="rank_{views} show_author_daily" onclick="show_author_daily('{id}');">{id}</td>
				<td>{article_count}</td>
				<td>
					<div>內 {inner_1} / {inner_2}</div>
					<div>外 {outer_1} / {outer_2} / {outer_3}</div>
				</td>
				<td class="article_time" data-outer="{outer_3}">{article_time}</td>
				<td>{like_unlike}</td>
				
				<td class="show_table rank" onclick="show_table('{id}', '{account_id}', 'daily_views');">{views}</td>												
				<td>{average_views}</td>
				<td>{contribution}</td>

			</tr>			
			{/content}

			<tr class="author_average">
				<td>平均</td>
				<td>{article_count_avg}</td>
				<td>
					<div>內 {inner_1_avg} / {inner_2_avg}</div>
					<div>外 {outer_1_avg} / {outer_2_avg} / {outer_3_avg}</div>
				</td>
				<td>{article_time_avg}</td>
				<td>{like_unlike_avg}</td>
				
				<td >{views_avg}</td>												
				<td>{average_views_avg}</td>
				<td>{contribution_avg}</td>
			</tr>

			<tr class="author_total">
				<td>總和</td>
				<td>{article_count_total}</td>
				<td>
					<div>內 {inner_1_total} / {inner_2_total}</div>
					<div>外 {outer_1_total} / {outer_2_total} / {outer_3_total}</div>
				</td>
				<td>{article_time_total}</td>
				<td>{like_unlike_total}</td>
				
				<td >{views_total}</td>												
				<td>{average_views_total}</td>
				<td>{contribution_total}</td>
			</tr>
		</tbody>
	</table>
</div>

<div class="daily_left daily_author">
	<div class="daily_author_name"></div>
	<table cellpadding="0" cellspacing="0" width="100%" class="tDefault tLight">
		<thead>
			<tr>
				<td>日期</td>
				<td>總觀看次數</td>
				<!-- <td title="作者" tips ="使用滑鼠點選 可以排序該欄位" class="order_control" data-orderby = "account_id">作者<i class="icon-chevron-{by-account_id}"></i></td> -->
				<td title="發文數" tips ="使用滑鼠點選 可以排序該欄位" class="order_control" data-orderby = "article_count">發文數<i class="icon-chevron-{by-article_count}"></i></td>
				<td>來源(改/翻/參)</td>
				<td>文章時間</td>
				<td title="頂噓" tips ="使用滑鼠點選 可以排序該欄位" class="order_control" data-orderby = "like_unlike">頂噓<i class="icon-chevron-{by-like_unlike}"></i></td>
				<td title="觀看次數" tips ="使用滑鼠點選 可以排序該欄位" class="order_control" data-orderby = "views">觀看次數<i class="icon-chevron-{by-views}"></i></td>
			</tr>			
		</thead>
		
		<tbody class="daily_statistics">
			{daily}
			<tr id="{auto_index}">
				<td>{date}</td>
				<td>{day_total_views}</td>										
				
				{person}
				<!-- <td data-name="{id}">{id}</td> -->
				<td data-name="{id}">{article_count}</td>
				<td data-name="{id}">
					<div>內 {inner_1} / {inner_2}</div>
					<div>外 {outer_1} / {outer_2} / {outer_3}</div>
				</td data-name="{id}">
				<td data-name="{id}" class="article_time">{article_time}</td>
				<td data-name="{id}">{like_unlike}</td>
				<td data-name="{id}" class="show_table" onclick="show_table('{id}', '{account_id}', 'daily_views');">{views}</td>
				{/person}
			</tr>	
			{/daily}

			{author}
			<tr class="author_average">
				<td data-name="{id_total_author}">平均</td>
				<td data-name="{id_total_author}"></td>
				<td data-name="{id_total_author}">{article_count_avg_author}</td>
				<td data-name="{id_total_author}">
					<div>內 {inner_1_avg_author} / {inner_2_avg_author}</div>
					<div>外 {outer_1_avg_author} / {outer_2_avg_author} / {outer_3_avg_author}</div>
				</td>
				<td data-name="{id_total_author}">{article_time_avg_author}</td>
				<td data-name="{id_total_author}">{like_unlike_avg_author}</td>
				
				<td data-name="{id_total_author}">{average_views_total_author}</td>												
			</tr>

			<tr class="author_total">
				<td data-name="{id_total_author}">總和</td>
				<td data-name="{id_total_author}"></td>
				<td data-name="{id_total_author}">{article_count_total_author}</td>
				<td data-name="{id_total_author}">
					<div>內 {inner_1_total_author} / {inner_2_total_author}</div>
					<div>外 {outer_1_total_author} / {outer_2_total_author} / {outer_3_total_author}</div>
				</td>
				<td data-name="{id_total_author}">{article_time_total_author}</td>
				<td data-name="{id_total_author}">{like_unlike_total_author}</td>
				
				<td data-name="{id_total_author}">{views_total_author}</td>												
			</tr>
			{/author}
		</tbody>
	</table>
</div>

<div class="daily_right">

</div>

<style>

thead TD{
white-space: nowrap;
}
.hide, .topicview_{
	display: none;
}
.topicview_hide{
	display: initial;
}
.external_link{
	background: none;
}

form#list_orderby {
	float: left;
	margin: 20px 10px 12px 0;
}
.daily_left {
	float: left;
	width: 800px;
}
.daily_right {
	float: left;
	width: 800px;
}
input.date {
	width: 150px;
}
input.submit {
	font-size: 14px;
	padding: 4px;
}

.show_table,
.show_author_daily {
	cursor: pointer;
}

.gold:BEFORE {
	background: url(images/gold.png) no-repeat left;
}
.silver:BEFORE {
	background: url(images/silver.png) no-repeat left;
}
.copper:BEFORE {
	background: url(images/copper.png) no-repeat left;
}
.gold:BEFORE, 
.silver:BEFORE,
.copper:BEFORE {
	content: '';
	padding: 10px 0px 10px 30px;
	position: absolute;
	margin-left: -30px;
}

.article_time.red_string {
	color: red;
}

.article_time.purple_string {
	color: rgb(153,50,204);
}


.daily_total {
	margin: 0 30px 0 0;
}

.daily_author_name {
	width: 800px;
	height: 98px;
	font-size: 18px; 
	font-weight: bold;
	text-align: center;
	line-height: 98px;
}

.daily_total .statistics_tbody tr.author_average,
.daily_author .daily_statistics tr.author_average {
	background: rgba(162,205,90,.5);
}

.daily_total .statistics_tbody tr.author_total,
.daily_author .daily_statistics tr.author_total {
	/*background: rgba(132,112,255,.5);*/
	background: rgba(30,144,255,.5);
}

.daily_total .statistics_tbody tr.author_average td,
.daily_total .statistics_tbody tr.author_total td,
.daily_statistics tr.author_average td,
.daily_statistics tr.author_total td {
	color: black;
}

.date_button {
	margin: 10px 0 0 2px;
	padding: 5px;
	cursor: pointer;
	background: rgba(255,185,15,.5);
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
}
</style>

<script>
$(document).ready(function() {
    rank();

    $(".daily_statistics td").each(function(){
		if ( $(this).attr('data-name') ) {
			$(this).hide();
		}
	});

	$("tbody td").each(function(){
		var num = formatNumber($(this).html());
		if ( num != 'NaN' ) {
			$(this).html(num);
		}
	});

	$(".article_total span").html( formatNumber($(".article_total span").html()) );
});

function change_start(){
	$('input[name = "end_date"]').val( $('input[name = "start_date"]').val());
}
function change_end(){
 	if( $('input[name = "end_date"]').val() < $('input[name = "start_date"]').val() ) $('input[name = "end_date"]').val( $('input[name = "start_date"]').val());
}
function send_date(){
 	var s = $('input[name = "start_date"]').val();
 	var e = $('input[name = "end_date"]').val();
}

function order_by(this_td){
	$('#order_by').val( $(this_td).attr('data-orderby'));
	$('#list_orderby').submit();
}

$(".order_control").live( "click", function() {
	var order_name = $(this).attr('data-orderby');
	console.info(this);
	if(order_name) order_by(this);
	
	return false;
});	

$('[tips]').hover(
	function() {
	    var x = $(this).offset().left;
	    var y = $(this).offset().top;
		var tips_box = document.createElement('div');
	    $(tips_box).attr("id","tips_box").html($(this).attr('tips')).appendTo('body');

	    var tips_width = $('#tips_box').width()+40;
	    var tips_height = $('#tips_box').height();
	  
	    var this_width = $(this).width();
	    var this_height = $(this).height();
	 
	    var diff_x = (tips_width + x > $(window).width())?$(window).width()-(tips_width + x):((this_width-$('#tips_box').width())/2);
	    var c_x = x + diff_x ;

	    var diff_y = (tips_height + y > $(window).height())?$(window).height()-(tips_height + y):(this_height+(tips_height)/2);
	    var c_y = y + diff_y ;
		 
	    $(tips_box).css('left',c_x).css('top',c_y).show();
	  			   
	}, function() {
		$('#tips_box').remove();
	}
);

//顯示詳細資訊
function show_table(author_name, author_id, table) {
	$('.daily_right').html('<div style="text-align: center;">更新中</div>');
	var start_date = $('input[name = "start_date"]').val();
 	var end_date = $('input[name = "end_date"]').val();

	var url='editor_statistics/show_table';
	$.ajax({
		url: url,
		cache: false,
		dataType: 'html',
		type: 'POST',
		data:{
			author_name: author_name,
			author_id: author_id,
			table: table,
			start_date: start_date,
			end_date: end_date
		},
		success: function(data){
			$('.daily_right').html(data);
			
		},error: function(data){
			$('.daily_right').html('失敗');
		}
	});
}

function show_author_daily(author_name) {
	$(".daily_statistics td").each(function(){
		if ( $(this).attr('data-name') ) {
			$(this).hide();
		}
	});
	$(".daily_statistics td[data-name="+author_name+"]").show();
	$(".daily_author_name").html(author_name);
	
}

//計算前三名
function rank() {
	var rankArray = new Array();
    $('.rank').each(function(){
        var num = parseInt($(this).text(), 10);
        rankArray.push(num);
    });
    rankArray = rankArray.sort(sortNumber);
    var gold = rankArray.pop();
    $('.rank_'+gold).addClass('gold');
    var silver = rankArray.pop();
    $('.rank_'+silver).addClass('silver');
    var copper = rankArray.pop();
    $('.rank_'+copper).addClass('copper');

    $('.article_time').each(function(){
    	var outer = $(this).attr('data-outer');
    	console.info(outer);
        var num = parseInt($(this).text(), 10);
        if ( num < 480 ) {
        	$(this).addClass('red_string');
        } else if ( (num-outer*20) < 240 ) {
        	$(this).addClass('purple_string');
        }
    });
}

function formatNumber(str, glue) {
	// 如果傳入必需為數字型參數，不然就噴 isNaN 回去
	if(isNaN(str)) {
		return 'NaN';
	}

	// 決定三個位數的分隔符號
	var glue= (typeof glue== 'string') ? glue: ',';
	var digits = str.toString().split('.'); // 先分左邊跟小數點
	var integerDigits = digits[0].split(""); // 獎整數的部分切割成陣列
	var threeDigits = []; // 用來存放3個位數的陣列

	// 當數字足夠，從後面取出三個位數，轉成字串塞回 threeDigits
	while (integerDigits.length > 3) {
		threeDigits.unshift(integerDigits.splice(integerDigits.length - 3, 3).join(""));
	}
	threeDigits.unshift(integerDigits.join(""));
	digits[0] = threeDigits.join(glue);

	return digits.join(".");
}

function sortNumber(a,b) {
	return a-b;
}

function change_date_yesterday() {
	var dt = new Date();
	var year = dt.getFullYear();
	var day = dt.getDate();
	if ( day == 1 ) {
		var Month = dt.getMonth();
		var day = new Date(year,Month,0).getDate();
	} else {
		var Month = dt.getMonth() + 1;
		day = day - 1;
	}
	if ( Month < 10 ) Month = '0' + Month;

	var date = year + '-' + Month + '-' + day;
	$('input[name="start_date"]').val(date);
	$('input[name = "end_date"]').val(date);

	$("#list_orderby").submit();
}

function change_date_month(mode) {
	var dt = new Date();
	var year = dt.getFullYear();

	if (mode == 'this') {
		var day = dt.getDate();
		if ( day != 1 ) day = day - 1;
		var Month = dt.getMonth() + 1;
	} else {
		var Month = dt.getMonth();
		var day = new Date(year,Month,0).getDate();
	}
	
	if ( Month < 10 ) Month = '0' + Month;
    var start_date = year + '-' + Month + '-01';
    var end_date = year + '-' + Month + '-' + day;
	$('input[name="start_date"]').val(start_date);
	$('input[name="end_date"]').val(end_date);

	$("#list_orderby").submit();
}

</script>