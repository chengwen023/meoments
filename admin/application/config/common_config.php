<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 這邊收錄不會因為環境影響的config setting
 * 
 */

//後台主選單
$config['admin_menuGroup'] = array('topic'=>'測驗管理', 'account'=>'帳號管理');

//後台選單
$config['admin_menu'] = array();

//帳號群組
$config['admin_group'] = array();

//帳號功能
$config['admin_competence'] = array('A'=>'新增','D'=>'刪除','E'=>'修改','V'=>'觀看','O'=>'上下架');
//帳號功能對應
$config['admin_competencestatus'] = array('S'=>'自己','G'=>'群組','A'=>'全部');

$config['admin_accountIndex'] = '';

$config['admin_accountGroupIndexArray'] = array();
$config['defaultstatus'] = '';

$config['post_type'] = array('message'=>'純文字', 'link'=>'連結', 'photo'=>'圖片');
?>