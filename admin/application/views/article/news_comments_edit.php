<style type="text/css">
.comment_menu {
	float: right;
}
.update_comment {
	padding: 10px;
	font-size: 16px;
}
.update_comment:hover {
	color: rgb(255, 113, 9);
}
.topic_insert{
	font-size: 28px;
	cursor: pointer;
	color: rgb(35, 35, 35);
	padding-top: 5px;
	margin-right: 20px;
}
.topic_insert:HOVER{	
	color: rgb(255, 113, 9);
}

#comment_name {
	padding: 0 10px 10px 10px;
	/*background-color: rgb(255,245,240);*/
}

#comment_name div {
  	font-size: 16px;
  	font-weight: bold;
  	padding: 20px 10px 5px 10px;
  	text-align: center;
}
#comment_name div.updating {
 	font-size: 24px; 
}

.comment_content TD.checked.good {
	background: url(images/check_green.png) no-repeat left;
}

.comment_content TD.checked.bad {
	background: url(images/not_pass.gif) no-repeat left;
}

</style>
<div class="comment_menu">
	<span class="update_comment" onclick="update_comment('{fb_id}', '{sid}');" style="cursor: pointer;">更新留言</span>
	<span class="icon-publish topic_insert" onclick="topic_insert('{article_id}','{fb_id}');" title="存檔" alt="新增"></span>
</div>

<div id="comment_name">
<?php if ( !empty($fb_comment) ) { ?>
<div>FB留言</div>
<table cellpadding="0" cellspacing="0" width="100%" class="tDefault tLight">
	<thead>
		<tr >
			<td>編號</td>
			<td>頭像</td>
			<td>姓名</td>
			<td>內容</td>
			<td>讚數</td>
			<td>回覆數</td>
			<td><input class="like_name" value="{like_name}" style="border: 1px solid #8f8f8f;width: 40px;"></td>
			<td><input class="unlike_name" value="{unlike_name}" style="border: 1px solid #8f8f8f;width: 40px;"></td>	
		</tr>
	</thead>
	
	<tbody>
		{fb_comment}
		<tr id="{fb_comment_id}" class='comment_content'>
			<td>{cid}</td>
			<td><img class="photo" src="http://graph.facebook.com/{author_id}/picture?width=45&height=45" /></td>
			<td>{author_name}</td>
			<td>{message}</td>
			<td>{like_count}</td>
			<td>{comment_count}</td>
			<td class="good optional {good}"></td>
			<td class="bad optional {bad}"></td>												
		</tr>			
		{/fb_comment}
	</tbody>
</table>
<?php } ?>

<?php if ( !empty($article_comment) ) { ?>
<div>網站留言</div>
<table cellpadding="0" cellspacing="0" width="100%" class="tDefault tLight">
	<thead>
		<tr >
			<td>編號</td>
			<td>頭像</td>
			<td>姓名</td>
			<td>內容</td>
			<td>讚數</td>
			<td>回覆數</td>
			<td class="like_name">{like_name}</td>
			<td class="unlike_name">{unlike_name}</td>	
		</tr>
	</thead>
	
	<tbody>
		{article_comment}
		<tr id="{fb_comment_id}" class='comment_content'>
			<td>{cid}</td>
			<td><img class="photo" src="http://graph.facebook.com/{author_id}/picture?width=45&height=45" /></td>
			<td>{author_name}</td>
			<td>{message}</td>
			<td>{like_count}</td>
			<td>{comment_count}</td>
			<td class="good optional {good}"></td>
			<td class="bad optional {bad}"></td>												
		</tr>			
		{/article_comment}
	</tbody>
</table>
<?php } ?>
</div>




<script type="text/javascript">
$(function() {
	comment_opt_init();
});

//留言選擇
function comment_opt_init(){
	$(".comment_content .optional").bind("click",function(){
		if($(this).hasClass("checked")) {
			$(this).removeClass("checked");
		} else {
			$(this).parent().find(".checked").removeClass("checked");
			$(this).addClass("checked");
		}

		// var topic_index = $('INPUT[name="auto_index"]').val();
		// if(topic_index < 1) return false;
	});

	$('INPUT.like_name').bind('change',function(){
		$('TD.like_name').html($(this).val());
	});
	$('INPUT.unlike_name').bind('change',function(){
		$('TD.unlike_name').html($(this).val());
	});
}

function comment_opt_destroy(){
	$(".comment_content .optional").unbind();
}

//新增文章
function topic_insert(auto_index, fb_id){

	var topic_array = get_post_val();
	// console.info(topic_array);
	// return;

	if(confirm("是否確定更新留言? ")){
		if( topic_array ){
			url = 'news/update_article_comment'; 
			$.fancybox.showLoading();
			$.ajax({
				url: url,
			  cache: false,
				dataType: 'html',
			  type: 'POST',
			  data:{
				  topic_array: topic_array,
				  auto_index: auto_index,
				  fb_id: fb_id
			  },
			  success: function(data){
			  	console.info(data);
			  	$.fancybox.hideLoading();
			  	location.reload();
			  },
			  error: function(data){
			  	console.info(data);
			  }
			});
		}
	}
}

//取出各欄位的資料，存於物件中
function get_post_val(){
	var topic_array = {};

	topic_array['comment_good'] = new Array();
	$('.good.checked').each(function() {
		topic_array['comment_good'].push($(this).parent().attr('id'));
	});

	topic_array['comment_bad'] = new Array();
	$('.bad.checked').each(function() {
		topic_array['comment_bad'].push($(this).parent().attr('id'));
	});

	if ( $('INPUT.like_name').val() ) {
		topic_array['like_name'] = $('INPUT.like_name').val();
	} else {
		topic_array['like_name'] = '頂';
	}

	if ( $('INPUT.unlike_name').val() ) {
		topic_array['unlike_name'] = $('INPUT.unlike_name').val();
	} else {
		topic_array['unlike_name'] = '噓';
	}
	
	return topic_array;
}

//更新留言
function update_comment(id, sid) {
	var like_name = $('INPUT.like_name').val();
	var unlike_name = $('INPUT.unlike_name').val();

	$('#update_comment').hide();
	$('#comment_name').html('<div class="updating">更新中...</div>');
	
	var url='news/update_comment';
	$.ajax({
		url: url,
		cache: false,
		dataType: 'html',
		type: 'POST',
		data:{
			id:id,
			sid:sid,
			like_name: like_name,
			unlike_name: unlike_name
		},
		success: function(data){
			$('#comment_name').html(data);
			$('#comment_name').show();
			$('#update_comment').show();
			comment_opt_destroy();
			comment_opt_init();
			
		},error: function(data){
			$('#update_comment').show();
		}
	});
}
</script>

