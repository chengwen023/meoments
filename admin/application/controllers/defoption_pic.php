<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Defoption_pic extends CI_Controller {
	
	var $menu_index;
	
	
	public function __construct(){
		parent::__construct();
		
		$this->userInfo = $this->session->userdata('userInfo');
		$this->load->model('theme_model', 'theme');
		$this->load->model('defoption_model', 'defoption');
		
		$this->data['admin_menu'] = $this->config->item('admin_menu');
		$this->data['admin_menuGroup'] = $this->config->item('admin_menuGroup');
		$this->data['userInfo'] = $this->userInfo;
		$this->data['menu'] = $this->config->item('admin_accountMenu');
		$this->data['breadcrumbs'] = $this->config->item('breadcrumbs');
		$this->data['title'] =$this->config->item('admin_title');
		
		$this->data['topbar_control']= array();
		$this->data['select_result'] = array();
		
		
	}
	/*
	* index
	*/
	public function index(){
		$this->lists();
	}
	
	/**
	 *	list
	 */
	
		function view(){
			$data = $this->data;
			
			$this->parser->parse('defoption/defoption_view', $data);
		}
function lists(){
			
		$data = $this->data;
		$this->menu_index = '套圖列表';//
		if(false === $activestatus = $this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, 'V')){
			$this->parser->parse('template', $data);
			return false;
		} 
		
		$defaultstatus = (isset($activestatus))?$activestatus:$this->config->item('defaultstatus');
		
		
		$this->db->flush_cache();
		$this->db->select("*");
		$this->db->from("defoptions_select_pic");
		$this->db->where("defoptions_select_pic.online",'1');
		/*
		if($defaultstatus == 'S'){
			$this->db->where('create_account_index',$this->config->item('admin_accountIndex'));			
		}else if($defaultstatus == 'G'){
			$this->db->where_in('create_account_index',$this->config->item('admin_accountGroupIndexArray'));
		}
		/**/
		
		
		$query = $this->db->get();
		
		$data['content'] = $query->result_array();
		foreach($data['content'] AS $key => $list){
			$temp_group = $this->defoption->defoption_group();
			$fans_groupname = array();
			$temp_groupindex = json_decode($list['defoptions_group_index']);
				
			foreach($temp_group AS $value){
				if(is_array($temp_groupindex) && in_array($value['auto_index'],$temp_groupindex) )
					$fans_groupname[] = $value['name'];
			}
			$select_option =  $this->defoption->topic_defaultselect($list['topic_select']);
			$data['content'][$key]['group_names'] = ($temp_group)?implode(',', $fans_groupname):'無';
			$data['content'][$key]['select_optionName'] = (isset($select_option['select_name']))?$select_option['select_name']:'無';
			

			$this->db->flush_cache();
			$this->db->select("pic_url AS option_url,topic_defaultoptions.option_name AS photo_title")->from("defoptions_option_pic")
			->where("defoptions_select_index",$list['auto_index'])
			->join('topic_defaultoptions','topic_defaultoptions.auto_index = defoptions_option_pic.topic_defaultoptions_index')
			->order_by('topic_defaultoptions_index','ASC');
			$option_photo = $this->db->get();
			$data['content'][$key]['option_photo'] = $option_photo->result_array();			
		}
		
		$data['main_content'] = $this->parser->parse('defoption/defoption_list', $data, true);
		$this->parser->parse('template', $data);
	}
	
	function choose_data(){
		$type = $this->input->post('type');
		
		$this->db->select("*")->from("topic_defaultoptions")->where("select_index",$type);
			$query = $this->db->get();
			$data = $query->result_array();
			foreach($data AS $item){
				echo '<li><div>選項:'.$item['option_name'].'<input type="hidden" name="options[choose][]" value="'.$item['auto_index'].'"/></div><div class="imgurl_box" ><span><img src="uploads/temp/default_pic.jpg" width="70px" /></span><input type="hidden" value="default_pic.jpg" name="options[text_imgurl][]"/></div></li>';
			}
	}
	
	/**
	 *	actions
	 */
	function actions($ac){
	
	
		switch($ac){		

			case 'add_defoption':
				
				$index = $this->input->post('index');
				/**/
				$this->menu_index = '套圖列表';//
				$purview = ($index)?'E':'A';
				if(!$this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, $purview)){
					echo $this->load->view('purview_error', '');
					return false;
				}
				
				
				$defaultstatus = (isset($activestatus))?$activestatus:$this->config->item('defaultstatus');
				
				/**/
				/**/
				
				
				
				$data = $this->data;
				
						
				$data['competence'] = $this->config->item('admin_group');
				$data['admin_competence'] = $this->config->item('admin_competence');
				$data['topic_group'] = $this->defoption->defoption_group();
				
				$data['topic_select'] = $this->defoption->topic_defaultselect();
				$data['topic_options'] = array();
				
				
				if($index){
					$this->db->select('*')->from('defoptions_select_pic')->where('auto_index',$index);
					$query = $this->db->get();
					$result = $query->result_array();
					if(is_array($result)){
						$data['topic'] = array_pop($result);
						
						$fans = json_decode($data['topic']['defoptions_group_index']);
				
						foreach($data['topic_group'] AS $key => $value){
							$data['topic_group'][$key]['checked'] = ((is_array($fans)) && in_array($value['auto_index'], $fans))?true:false;
						}
						foreach($data['topic_select'] AS $key => $value){
							if($value['auto_index'] == $data['topic']['topic_select']) {
								$data['topic_select'][$key]['selected'] = true;
							}
							
						}
						
						$this->db->select('defoptions_option_pic.*, topic_defaultoptions.option_name AS title')->from('defoptions_option_pic');
						$this->db->where('defoptions_select_index',$index);
						$this->db->join('topic_defaultoptions','topic_defaultoptions.auto_index = defoptions_option_pic.topic_defaultoptions_index');
						
						$query = $this->db->get();
						$data['topic_options'] = $query->result_array();
						
					}
				}
				/**/
				
				echo $this->load->view('defoption/defoption_edit', $data, true);
				
				break;
			
			case 'validate':
				
			
				$index = (int) $this->input->post('auto_index');
			
				$date = date("Y-m-d H:i:s");
				$account_id = $this->config->item('admin_accountIndex');
			
				$add_options = $this->input->post("options");
			
				$add_input = array("defoptions_select_name"=>'','defoptions_group_index'=>'','topic_select'=>'','pic_url'=>'');
			
				foreach ($add_input AS $key => $value){
					if(!$value){
						switch ($key){
							case "defoptions_group_index":
								$add_input[$key] = json_encode( $this->input->post("defoptions_group_index"),true);
								break;
							default:
								$add_input[$key] = $this->input->post($key);
								break;
						}
					}
				}
			
				if($index) {					
					$result = $this->defoption->check_defoption_exist_and_update($add_input,$add_options,$index);
				}
				else {					
						
					$result = $this->defoption->check_defoption_exist_and_insert($add_input,$add_options);
					if($result['success'] == 'Y'){						
						$result['msg']='新增成功';
					}
						
				}
				header('Content-type: text/plain; charset=utf-8');
				echo json_encode($result);
			
			
				break;
		}
		
		
	}
}