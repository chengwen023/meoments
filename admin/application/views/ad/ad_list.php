<STYLE>

@media only screen and (max-width: 480px) and (min-width: 320px){
	.tLight tfoot {
	  display: block;
	}
}
tfoot{
	background: #A3B1BB;
}

#add_row TD{
	padding: 9px 16px;
}
#add_row TD INPUT {
	line-height: 24px;
  height: 30px;
  font-size: 12px;
  padding: 1px 2px;
}

</STYLE>

<div class="widget">
	<table cellpadding="0" cellspacing="0" width="100%" class="tLight">
		<form id="list_orderby" method="post">
		<input id="order_by" type="hidden" name="order_by" value="">
		<thead>
			<tr>
				<td title="名稱" class="order_control" data-orderby = "id">名稱<i class="icon-chevron-{by-name}"></i></td>
				<td title="寬度" class="order_control" data-orderby = "width">寬度<i class="icon-chevron-{by-width}"></i></td>
				<td title="高度" class="order_control" data-orderby = "height">高度<i class="icon-chevron-{by-height}"></i></td>
				<td title="Google" >Google</td>
				<td title="Google" >Other</td>
				<td title="功能" >功能</td>
			</tr>
		</thead>
		</form>
		<tbody>
			{content}
			<tr class="user_row">
				<td>{name}</td>
				<td>{width}</td>				
				<td>{height}</td>
				<td>				
					<textarea name="code" data-name="google_code" data-id="{auto_index}">{google_code}</textarea>
				</td>
				<td>				
					<textarea name="code" data-name="other_code" data-id="{auto_index}">{other_code}</textarea>
				</td>
				<td title="功能" >
				<a href="javascript:void(0);" class="tablectrl_small bRed {edit_control}" original-title="Del" title="刪除" alt="刪除" onclick="ad_del({auto_index});">
								<span class="iconb" data-icon="" ></span></a>	
				</td>
			</tr>
			{/content}
		</tbody>
		<tfoot>			
			<tr id="add_row" class="user_row">
				<td width="150px;"><input type="text" name="name" placeholder="名稱" /></td>
				<td width="80px;"><input type="number" name="width"  placeholder="寬度" /></td>				
				<td width="80px;"><input type="number" name="height"  placeholder="高度" /></td>
				<td>				
					<textarea name="google_code" placeholder="google_code" ></textarea>
				</td>
				<td>				
					<textarea name="other_code"  placeholder="other_code"></textarea>
				</td>
				<td><span onclick="add_row();">新增</span></td>
			</tr>
			
		</tfoot>
	</table>
</div>

<script src="js/account.js"></script>
<script>
function ad_del(id){
	$.ajax({
		  url: 'ad/del_row',		  
		  dataType: 'html',
		  type: "POST",
		  data: {
			  id:id
		  },
		  success: function(data){
			  $.fancybox.hideLoading();
			  if(data) {
				  alert(data);			  			  
			  }
			  location.reload();
		  },
		  error: function(msg) {
			console.log('error');
		  }
		});
	
}
function add_row(){
	var add_input = {}; 
	var check = false;
	
	$('#add_row').find('input,textarea').each(function(){

		
		if($(this).val() == '') check = true;
		
		add_input[$(this).attr('name')] = $(this).val();

	});

	if(check) {
		alert('有欄位為空值!');
		return false;
	}
	
	$.fancybox.showLoading();		
	$.ajax({
	  url: 'ad/add_row',		  
	  dataType: 'html',
	  type: "POST",
	  data: {
		  add_input:add_input
	  },
	  success: function(data){
		  $.fancybox.hideLoading();
		  if(data) {
			  alert(data);			  			  
		  }
		  location.reload();
	  },
	  error: function(msg) {
		console.log('error');
	  }
	});
	
}
$(function(){
	
	$('textarea[name="code"]').on('change',function(){
		var id = $(this).attr('data-id');
		var code = $(this).val();
		var filed = $(this).attr('data-name');
		
		$.fancybox.showLoading();		
		$.ajax({
		  url: 'ad/update_code',		  
		  dataType: 'html',
		  type: "POST",
		  data: {
				id:id,
				code:code,
				filed:filed
		  },
		  success: function(data){
			  console.info(data);
			  $.fancybox.hideLoading();			  
			  location.reload();
			  
		  },
		  error: function(msg) {
			console.log(msg);
		  }
		});
		
		return false;
	})
	

});
</script>
<script>
function order_by(this_td){

	$('#order_by').val( $(this_td).attr('data-orderby'));

	$('#list_orderby').submit();

}

$(".order_control").live( "click", function() {
	var order_name = $(this).attr('data-orderby');
	if(order_name) order_by(this);
	
	return false;
});	

</script>

