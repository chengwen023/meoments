
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News_tag extends CI_Controller {
	
	var $menu_index;
	
	public function __construct(){
		parent::__construct();
		
		$this->userInfo = $this->session->userdata('userInfo');
		$this->load->model('theme_model', 'theme');
		$this->load->model('article_model', 'article');
		
		$this->data['admin_menu'] = $this->config->item('admin_menu');
		$this->data['admin_menuGroup'] = $this->config->item('admin_menuGroup');
		$this->data['userInfo'] = $this->userInfo;
		$this->data['menu'] = $this->config->item('admin_accountMenu');
		$this->data['breadcrumbs'] = $this->config->item('breadcrumbs');
		$this->data['title'] =$this->config->item('admin_title');
		
		$this->data['topbar_control']= array();
		$this->data['select_result'] = array();	
	}
	
	/*//新增標籤
	function create_group(){
		$auto_index = $this->input->post ( 'auto_index' );
		
		$add_input = array (
			'defoptions_group_name' => $topic_array['defoptions_group_name']
		);

		$add_options = isset($topic_array['options'])?$topic_array['options']:false;
		
		$date = date ( "Y-m-d H:i:s" );
		$add_input['create_time'] = $date;
		$add_input['update_time'] = $date;

		// $account_id = $this->config->item ('admin_accountIndex');
		// $add_input ['create_account_index'] = $account_id;
		// $add_input ['update_account_index'] = $account_id;
	
		if($auto_index)
			$result = $this->article->check_fans_exist_and_update($add_input,$auto_index);
		else
			$result = $this->article->check_topic_exist_and_insert($add_input);
	}*/
	
	public function index(){
		$this->lists();
	}
	
	// function view(){
	// 	$data = $this->data;
	// 	$this->parser->parse('defoption/defoption_view', $data);
	// }

	function lists(){
		$data = $this->data;
		$this->menu_index = '文章標籤管理';
		if( false === $activestatus = $this->admin_model->checkCompetence_getMenuInfo($this->menu_index, 'V') ) {
			$this->parser->parse('template', $data);
			return false;
		} 
		
		$defaultstatus = (isset($activestatus)) ? $activestatus : $this->config->item('defaultstatus');
		
		$this->db->flush_cache();
		$this->db->select("*");
		$this->db->from("news_tags");
		
		$query = $this->db->get();
		
		$data['content'] = $query->result_array();
		foreach($data['content'] AS $key => $list){
			$data['content'][$key]['off'] = ($list['online']) ? '' : 'off';
		}
		
		$data['main_content'] = $this->parser->parse('source/news_tag_list', $data, true);
		$this->parser->parse('template', $data);
	}

	//上下架標籤
	function tag_online(){
		$index = $this->input->post('index');
		$online = $this->input->post('online');
	
		if($online == 'true') $add_input = array('online' => '1');
		else $add_input = array('online' =>'0');

		$add_input ['update_time'] = date( "Y-m-d H:i:s" );
	
		$this->db->where('id', $index);
		$this->db->update('news_tags', $add_input);

		$this->load->driver('cache');
		$this->cache->memcached->delGlobalMemcached('news_tags');
	}
	
	//新增標籤、更改標籤名稱
	function update_tag() {
		$index = $this->input->post('index');
		$filed = $this->input->post('filed');
		$value = $this->input->post('value');
		if ( empty($value) ) return;
		$value = trim($value);
		
		if($index && $filed) {
			$add_input = array($filed => $value, 'update_time' => $date);
			$this->db->where('id', $index);
			$this->db->update('news_tags', $add_input);

		} elseif($value) {
			$date = date ("Y-m-d H:i:s");
			$add_input ['name'] = $value;
			$add_input ['create_time'] = $date;	
			$add_input ['update_time'] = $date;

			// $account_id = $this->config->item('admin_accountIndex');
			// $add_input['create_account_index'] = $account_id;
			// $add_input['update_account_index'] = $account_id;
				
			$this->db->insert('news_tags', $add_input);
		}	

		$this->load->driver('cache');
		$this->cache->memcached->delGlobalMemcached('news_tags');
	}
	
	//刪除標籤
	function del(){
		$this->menu_index = '文章分類管理';
		if (! $this->admin_model->checkCompetence_getMenuInfo($this->menu_index, 'D')) {
			$result = array (
					'success' => 'N',
					'msg' => '權限不足'
			);
			echo json_encode ( $result );
			break;
		}
		
		$index = $this->input->post('index');
		$this->db->delete('news_tags', array('id' => $index));

		$this->load->driver('cache');
		$this->cache->memcached->delGlobalMemcached('news_tags');
		
		echo json_encode(array('success' => 'Y', 'msg' => '成功'));
	}
}
