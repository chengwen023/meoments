<?php
session_start();
// include required files form Facebook SDK
require_once ('Facebook/Entities/AccessToken.php');

// added in v4.0.5
require_once ('Facebook/HttpClients/FacebookHttpable.php');
require_once ('Facebook/HttpClients/FacebookCurl.php');
require_once ('Facebook/HttpClients/FacebookCurlHttpClient.php');

// added in v4.0.0
require_once ('Facebook/FacebookSession.php');

require_once ('Facebook/FacebookRedirectLoginHelper.php');
require_once ('Facebook/FacebookRequest.php');
require_once ('Facebook/FacebookResponse.php');
require_once ('Facebook/FacebookSDKException.php');
require_once ('Facebook/FacebookRequestException.php');
require_once ('Facebook/FacebookOtherException.php');
require_once ('Facebook/FacebookAuthorizationException.php');
require_once ('Facebook/GraphObject.php');
require_once ('Facebook/GraphSessionInfo.php');
require_once ('Facebook/FacebookPermissionException.php');
require_once ('Facebook/GraphUser.php');
require_once ('Facebook/GraphUserPage.php');

// added in v4.0.5
use Facebook\FacebookHttpable;
use Facebook\FacebookCurl;
use Facebook\FacebookCurlHttpClient;

// added in v4.0.0
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookOtherException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\GraphSessionInfo;
use Facebook\GraphUser;




// init app with app id and secret
FacebookSession::setDefaultApplication ( '788431084588360', '41622ae60d6313b7b1689b049860978d' );

$URL = 'http://toments.com/web/code/facebook_mobile.php';
// login helper with redirect_uri
$helper = new FacebookRedirectLoginHelper ( $URL );

if (isset ( $_SESSION ) && isset ( $_SESSION ['fb_token'] )) {
	// create new session from saved access_token
	$session = new FacebookSession ( $_SESSION ['fb_token'] );
	
	// validate the access_token to make sure it's still valid
	try {
		if (! $session->validate ()) {
			$session = null;
		}
	} catch ( Exception $e ) {
		// catch any exceptions
		$session = null;
	}
}

if (! isset ( $session ) || $session === null) {
	// no session exists
	
	try {
		$session = $helper->getSessionFromRedirect ();
	} catch ( FacebookRequestException $ex ) {
		// When Facebook returns an error
		// handle this better in production code
		print_r ( $ex );
	} catch ( Exception $ex ) {
		// When validation fails or other local issues
		// handle this better in production code
		print_r ( $ex );
	}
}

// see if we have a session
if (! isset ( $session )) {	
	$FB_URL = $helper->getLoginUrl ( array (
			'email',
			'user_about_me',
			'publish_actions' 
	) );
} else {
	$FB_URL = $_SESSION ['redirect'];
}

if ($_SESSION ['fb_token']) {
	
	setcookie ( 'fb_access_token', $_SESSION ['fb_token'], time () + 3600 );
	
	if ($session) {
		
		try {
			
			$user_profile = (new FacebookRequest ( $session, 'GET', '/me' ))->execute ()->getGraphObject ( GraphUser::className () )->asArray ();
		} catch ( FacebookRequestException $e ) {
			
			echo "Exception occured, code: " . $e->getCode ();
			echo " with message: " . $e->getMessage ();
		}
		
		if ($user_profile ['id'] && $article_id && $_COOKIE ['compose' . $article_id] && false == $_COOKIE ['composedone' . $article_id]) {
			
			$message = $_COOKIE ['compose' . $article_id];
			$status = $_COOKIE ['composestatus' . $article_id];
			$author_id = $user_profile ['id'];
			$author_name = $user_profile ['name'];
			$create_time = strtotime ( "now" );
			
			$sql = "INSERT facebook_comments_onshelf(news_article_id, fb_comment_id, author_id, author_name, message, create_time, update_time, status) VALUES
			('$article_id', '', '$author_id', '$author_name', '$message', '$create_time', '$create_time', '$status')";
			mysql_query ( $sql );
			
			
			$filed_name2 = ($status == 2) ? 'daily_like_message_count' : 'daily_unlike_message_count';
			$sql = "UPDATE `news_article_state` SET $filed_name2 = $filed_name2 +1  WHERE `id`= $article_id LIMIT 1 ";
			mysql_query($sql);
			
			$filed_name = ($status == 2) ? 'like_count' : 'unlike_count';
			$sql = "UPDATE `news_article_state` AS nas,
			(SELECT `news_article_id`, COUNT(*) AS `total`
			FROM `facebook_comments_onshelf`
			WHERE `status`= $status AND `news_article_id` =  $article_id
			GROUP BY `news_article_id`) AS fco
			SET nas.`$filed_name`=fco.`total`
			WHERE nas.`id`= fco.`news_article_id` ";
			
			mysql_query ( $sql );
			
			
			
			
			setcookie ( 'composedone' . $article_id, true, time () + 600 );
			
			$memcached = new iplayfun_memcached ( 'toments' );
			$ArticleKey = "Article[" . $article_id . "]";
			$memcached->set_prefix ( $ArticleKey );
// 			$memcached->deleteMulti ( $memcached->getAllKeys () );
			
			if (! isset ( $_COOKIE ['undo_share'] )) {
				setcookie ( 'undo_share', '' );
				$post_facebook = array (
						'message' => $message,
						'link' => $_SESSION ['redirect']
				);
				try {
					
					$response = (new FacebookRequest ( $session, 'POST', '/me/feed', $post_facebook ))->execute ()->getGraphObject ();
				} catch ( FacebookRequestException $e ) {
					
					echo "Exception occured, code: " . $e->getCode ();
					echo " with message: " . $e->getMessage ();
				}
			}
		}
	}
}
?>