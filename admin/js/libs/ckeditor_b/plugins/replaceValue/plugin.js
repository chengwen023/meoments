CKEDITOR.plugins.add('replaceValue', {	
	requires : 'menubutton',
	init : function(editor) {
		var valuesConfigStrings = editor.config.replaceValue,
		plugin = this,		
		items = {},
		parts,
		curValueId, // 2-letter value identifier.
		valueButtonId, // Will store button namespaced identifier, like "value_en".
		i;
		
		// Registers command.
		editor.addCommand( 'replaceValue', {
			contextSensitive: true,			
			exec: function( editor, valueId ) {
				var item = items[ 'value_' + valueId ];
				 console.info(item.replace);	
				 editor.insertHtml('&nbsp;<span>'+item.replace+'</span>&nbsp;');
				
					var range = editor.createRange();
					 
					 console.info(range);
					 range.moveToElementEditEnd( range.root );
					 editor.getSelection().selectRanges( [ range ] );		
					
					
			}
		} );
		
		for ( i = 0; i < valuesConfigStrings.length; i++ ) {
			parts = valuesConfigStrings[ i ].split( ':' );
			curValueId = parts[ 0 ];
			valueButtonId = 'value_' + curValueId;

			items[ valueButtonId ] = {
				label: parts[ 1 ],
				langId: curValueId,
				group: 'replaceValue',
				order: i,
				icon: this.path + 'i-gotest.png',
				// Tells if this value is left-to-right oriented (default: true).
				replace: ( '' + parts[ 2 ] ).toLowerCase() ,
				onClick: function() {			
					editor.execCommand( 'replaceValue', this.langId );
					
				},
				role: 'menuitemcheckbox'
			};

			
		}

		editor.addMenuGroup( 'replaceValue', 1 );		
		editor.addMenuItems( items );
		
		
		editor.ui.add( 'ReplaceValue', CKEDITOR.UI_MENUBUTTON, {
			label : '換頁標籤',
			icon: this.path + 'i-gotest.png',
			command: 'replaceValue',
			onMenu: function() {
				var activeItems = {};				
				for ( var prop in items )
					activeItems[ prop ] = CKEDITOR.TRISTATE_OFF;
				return activeItems;
			}
		} );
		
						
	}
});
