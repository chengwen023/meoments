var article_group = new Object;

article_group.add = function(index){
	var value = prompt("請輸入分類名稱", "請輸入資料 ") 
	
	$.ajax({
		url: 'news_tag/update_tag/',
	  	dataType: 'html',
	  	type: "POST",
	  	data: {	
		  	value:value
	  	},
	  	success: function(data){
		  	location.reload();
	  	}
	});
}

article_group.del = function(index){
	$.ajax({
		url: 'news_tag/del',
	  	cache: false,
	  	dataType: 'json',
	  	type: 'POST',
	  	data: {
	  		index:index
	  	},
	  	success: function(data){		
		  	location.reload();
	  	}/*,
	  	error: function(data){
	  		console.info(data);
	  	}*/
	});
}



var article = new Object;
// article.pofans = function(index){
// 	$.ajax({
// 		url: 'pofans/schedule_add_from_article',
// 	  cache: false,
// 	  dataType: 'html',
// 	  async : false,
// 	  type: 'POST',
// 	  data: 'index='+index,
// 	  success: function(data){		
// 		  console.info(data)
// 	  }
// 	});
// }

// article.add_edit = function(index){
// 	url = 'article/actions/add_article';
	 
// 	$.fancybox.showLoading();
// 	$.ajax({
// 		url: url,
// 	  	cache: false,
// 		dataType: 'html',
// 	  	type: 'POST',
// 	  	data:{
// 			index:index
// 	  	},
// 	  	success: function(data){
// 		  	$.fancybox.hideLoading();
// 		  	$.fancybox.open(data,{	  
// 		  		wrapCSS : 'fancybox-background',
// 		  		autoCenter   : true,
// 			  	helpers : {
// 			        overlay : {
// 			        	closeClick: false
// 			        }
// 		        },
// 		        afterShow : function(){
// 		        	//$('.fancybox-inner').css('width', '620px');
// 		        },
// 		        beforeClose : function(){
// 		        	var ask = confirm("是否關閉 新增/編輯 資料視窗!?");
// 		        	if(ask) {
// 		        		$('.add_topic_option').die('click');
// 			        	$('.topic_option_remove').die('click');
// 			        	$("DIV.fb_view_control SPAN").die('click');
// 			        	$("IMG.editable").die('click');
// 		        		clear_img_list();
// 		        	}

// 		        	return(ask);
// 		        }
// 			});
// 	  	}
// 	});
// }

// article.horoscope = function(index){
// 	$.ajax({
// 		url: 'article/horo_add',
// 	  cache: false,
// 	  dataType: 'html',
// 	  async : false,
// 	  type: 'POST',
// 	  data: 'index='+index,
// 	  success: function(data){
// 		  	$.fancybox.hideLoading();
// 		  	$.fancybox.open(data,{	  
// 		  		wrapCSS : 'fancybox-background',
// 		  		autoCenter   : true,
// 			  	helpers : {
// 			        overlay : {
// 			        	closeClick: false
// 			        }
// 		        },
// 		        afterShow : function(){
// 		        	//$('.fancybox-inner').css('width', '620px');
// 		        },
// 		        beforeClose : function(){
		        	
// 		        	return(confirm("是否關閉 新增/編輯 資料視窗!?"));	        	
// 		        }
// 			  	});
// 		  }
// 	});
// }

// article.del = function(index){
// 	$.ajax({
// 		url: 'article/actions/del',
// 	  cache: false,
// 	  dataType: 'json',
// 	  async : false,
// 	  type: 'POST',
// 	  data: 'index='+index,
// 	  success: function(data){		
// 		  	if(data.success == 'Y'){
// 		  		$.fancybox.close(true);
// 		  		location.reload();
// 		  	}else{
// 		  		$.fancybox.close(true);
// 		  		alert(data.msg);
// 		  	}
// 	  }
// 	});
// }

article.add_edit_news = function(index, from){
	url = 'news/actions/add_news';
	 
	$.fancybox.showLoading();
	$.ajax({
		url: url,
		cache: false,
	 	dataType: 'html',
	 	type: 'POST',
	 	data:{
		index: index,
		from: from
	},
		success: function(data){
		  	$.fancybox.hideLoading();
		  	$.fancybox.open(data,{
		  		width: 1800,
			    height: 900,
			    autoDimensions: false,
			    autoSize: false, 
		  		// wrapCSS : 'fancybox-background',
		  		autoCenter   : true,
			  	helpers : {
			        overlay : {
			        	closeClick: false
			        }
		        },
		        afterShow : function(){
		        	//$('.fancybox-inner').css('width', '620px');
		        },
		        beforeClose : function(){
		        	var ask = confirm("是否關閉 新增/編輯 資料視窗!?");
		        	if(ask) {
		        		$('.add_topic_option').die('click');
			        	$('.topic_option_remove').die('click');
			        	$("DIV.fb_view_control SPAN").die('click');
			        	$("IMG.editable").die('click');
			        	if ( typeof clear_img_list == 'function') {
							clear_img_list();
							update_action(index, 'facebook_post_content');
						}
		        	}

		        	return(ask);
		        }
			});
		}
	});
}

article.add_edit_news_article = function(index, active){
	url = 'news/actions/edit_news_article';
	 
	$.fancybox.showLoading();
	$.ajax({
		url: url,
	  	cache: false,
		dataType: 'html',
	  	type: 'POST',
	  	data:{
		  	index: index,
		  	active: active
	  	},
	  	success: function(data){
		  	$.fancybox.hideLoading();
		  	$.fancybox.open(data,{
		  		width: 1800,
			    height: 900,
			    autoDimensions: false,
			    autoSize: false,   
		  		// wrapCSS : 'fancybox-background',
		  		autoCenter   : true,
			  	helpers : {
			        overlay : {
			        	closeClick: false
			        }
			    },
			    afterShow : function(){
		        	//$('.fancybox-inner').css('width', '620px');
		        },
		        beforeClose : function(){
		        	var ask = confirm("是否關閉 新增/編輯 資料視窗!?");
		        	if(ask) {
		        		$('.add_topic_option').die('click');
			        	$('.topic_option_remove').die('click');
			        	$("DIV.fb_view_control SPAN").die('click');
			        	$("IMG.editable").die('click');
		        		if ( typeof clear_img_list == 'function') {
							clear_img_list();
							update_action(index, 'news_article_state');
						}
		        	}

		        	return(ask);
		        }
			});
		}
	});
}

article.edit_news_comment = function(article_id, fb_id){
	url = 'news/actions/edit_news_comment';
	 
	$.fancybox.showLoading();
	$.ajax({
		url: url,
	  	cache: false,
		dataType: 'html',
	  	type: 'POST',
	  	data:{
		  	article_id: article_id,
		  	fb_id: fb_id
	  	},
	  	success: function(data){
		  	$.fancybox.hideLoading();
		  	$.fancybox.open(data,{
		  		width: 1800,
			    height: 900,
			    autoDimensions: false,
			    autoSize: false,   
		  		// wrapCSS : 'fancybox-background',
		  		autoCenter   : true,
			  	helpers : {
			        overlay : {
			        	closeClick: false
			        }
			    },
			    afterShow : function(){
		        	//$('.fancybox-inner').css('width', '620px');
		        },
		        beforeClose : function(){
		        	var ask = confirm("是否關閉 編輯 資料視窗!?");
		        	if(ask) {
		        	}

		        	return(ask);
		        }
			});
		}
	});
}

article.lock_fb_article = function(id, action){
	$.ajax({
		url: 'news/lock_fb_article',
	  	cache: false,
	  	dataType: 'json',
	  	type: 'POST',
	  	data: {
	  		id: id,
	  		action: action
	  	},
	  	success: function(data){
	  		if ( action == 'lock' ) {
	  			$('#user_'+id).addClass('off');
	  			$('span[data-index='+id+']').addClass('off');
	  		} else {
	  			$('#user_'+id).removeClass('off');
	  			$('span[data-index='+id+']').removeClass('off');
	  		}
	  		// console.info( $('span[data-index='+id+']') );
	  		
		  	// location.reload();
	  	},
	  	error: function(data){
	  		console.info(data);
	  	}
	});
}
