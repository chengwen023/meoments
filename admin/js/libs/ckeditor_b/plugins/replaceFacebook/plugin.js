CKEDITOR.plugins.add('replaceFacebook', {	
	requires : 'menubutton',
	init : function(editor) {
		var facebooksConfigStrings = editor.config.replaceFacebook,
		plugin = this,		
		items = {},
		parts,
		curfacebookId, 
		facebookButtonId, 
		i;
		
		// Registers command.
		editor.addCommand( 'replaceFacebook', {
			contextSensitive: true,			
			exec: function( editor, facebookId ) {
				var item = items[ 'facebook_' + facebookId ];
				 console.info(item.replace);	
				 editor.insertHtml('&nbsp;<span>'+item.replace+'</span>&nbsp;');
				
					var range = editor.createRange();
					 
					 console.info(range);
					 range.moveToElementEditEnd( range.root );
					 editor.getSelection().selectRanges( [ range ] );		
					
					
			}
		} );
		
		for ( i = 0; i < facebooksConfigStrings.length; i++ ) {
			parts = facebooksConfigStrings[ i ].split( ':' );
			curfacebookId = parts[ 0 ];
			facebookButtonId = 'facebook_' + curfacebookId;

			items[ facebookButtonId ] = {
				label: parts[ 1 ],
				langId: curfacebookId,
				group: 'replaceFacebook',
				order: i,
				icon: this.path + 'facebook.png',
				// Tells if this facebook is left-to-right oriented (default: true).
				replace: ( '' + parts[ 2 ] ).toLowerCase() ,
				onClick: function() {			
					editor.execCommand( 'replaceFacebook', this.langId );
					
				},
				role: 'menuitemcheckbox'
			};

			
		}

		editor.addMenuGroup( 'replaceFacebook', 1 );		
		editor.addMenuItems( items );
		
		
		editor.ui.add( 'ReplaceFacebook', CKEDITOR.UI_MENUBUTTON, {
			label : '換頁標籤',
			icon: this.path + 'facebook.png',
			command: 'replaceFacebook',
			onMenu: function() {
				var activeItems = {};				
				for ( var prop in items )
					activeItems[ prop ] = CKEDITOR.TRISTATE_OFF;
				return activeItems;
			}
		} );
		
						
	}
});
