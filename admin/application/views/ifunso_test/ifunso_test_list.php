<script src="js/topic.js"></script>
<script>
	$(function(){
		$('.fans_form').submit(function(e){
			e.preventDefault();
			fans.add_fans();
		});
	});
</script>
<div class="row-fluid">
	<div class="span5">
		<h3 class="heading">
			<div style="float:left;margin-right:10px;">
				<?php echo $title;?>
			</div>
			<div id="add_div" style="float:right;font-size:14px;">				
			</div>
			<div class="clear"></div>
		</h3>
		<table class="table table-bordered table-striped table_vam" id="smpl_tbl">
			<thead>
				<tr>
					<th>題目</th>
					<th>題目圖片</th>
					<th style="width:60px">動作</th>
				</tr>
			</thead>
			<tbody id="fans_box">
				<?php if(empty($content)): ?>
						<tr class="user_row">
							<td colspan="3"  style="text-align: center;">尚無資料</td>
						</tr>
				<?php else: ?>
					<?php foreach($content as $indexi => $i): ?>
						<tr id="<?php echo 'user_'.$i['pk_question']; ?>" class="user_row">
							<td><?php echo $i['question_slogan']; ?></td>
						
							
							<td>
							<?php if(!empty($i['file_path'])):?>
							<img src="<?php echo $i['file_path']; ?>" width="60px" />
							<?php endif;?>
							</td>								
							<td>
								<a href="javascript:void(0);" class="sepV_a del_news_btn" title="編輯" onclick="topic.ifunso_test(<?php echo $i['pk_question']; ?>);">新增</a>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php endif; ?>
			</tbody>
		</table>
	</div>
	<div class="clear"></div>
</div>
<!-- hide elements (for later use) -->
<div class="hide">
	<div class="del_box">
		<!-- confirmation box -->
		<div id="confirm_dialog" class="cbox_content">
			<div class="sepH_c tac"><strong>Are you sure you want to delete this row(s)?</strong></div>
			<div class="tac">
				<a href="javascript:void(0);" class="btn btn-gebo confirm_yes">Yes</a>
				<a href="javascript:void(0);" class="btn confirm_no">No</a>
			</div>
		</div>
	</div>
</div>