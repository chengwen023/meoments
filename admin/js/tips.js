$.fn.outerHTML = function(){
	 
    // IE, Chrome & Safari will comply with the non-standard outerHTML, all others (FF) will have a fall-back for cloning
    return (!this.length) ? this : (this[0].outerHTML || (
      function(el){
          var div = document.createElement('div');
          div.appendChild(el.cloneNode(true));
          var contents = div.innerHTML;
          div = null;
          return contents;
    })(this[0]));
 
}
$.fn.extend({
    getMaxZ : function(){
        return Math.max.apply(null, jQuery(this).map(function(){
            var z;
            return isNaN(z = parseInt(jQuery(this).css("z-index"), 10)) ? 0 : z;
        }));
    }
});




Tips = function(tagfiled){
	
	
	
	this.def_config = {
			tipbox_name: "tips_box",
			filed_name : "tips",
			class_Name : '',
			max_zindex : 10000,
			top_zindex : $("*").getMaxZ(),
			opacity_time : 500
	}
	this.config = this.def_config;
	
	if(typeof(tagfiled) == 'object'){		
		var config_key = $.map(this.config, function(el,index) {return index});
		
		for(var k in tagfiled)				
			if($.inArray(k,config_key)>=0) this.config[k] = tagfiled[k];
		
	}
	
	
	var tipbox_name = this.config.tipbox_name;
	var max_zindex = this.config.max_zindex;
	var top_zindex = this.config.top_zindex;
	
	top_zindex =(top_zindex<max_zindex)?max_zindex:(top_zindex + max_zindex);	
	
	if( typeof($('#'+tipbox_name)) == 'object' && $('#'+tipbox_name).length )
	{
		console.info("Tips: please check id["+tipbox_name+"] is unused.");
		return false;
	}
	
	if( $.isArray(this.config.filed_name) ){
		
		var tips_list = this.config.filed_name ;
		
		var set_config = this.config;
		for(var k in tips_list){
			set_config['filed_name'] = 	tips_list[k];
			new Tips(set_config);		
		}
	}
	else if($.isArray(tagfiled) ) {
		
		var tips_list = tagfiled ;
		for(var k in tips_list){
			new Tips(tips_list[k]);			
		}
	}
	else if(typeof(tagfiled) == 'string') this.createTips(tagfiled);
	else{
		
		
		this.createTips(this.config);	
	}
	
	/**/
	
	
}
Tips.prototype.setClassName = function(class_Name){		config.class_Name = class_Name; 	}

Tips.prototype.remove = function(filed_name){	
	var out_tips = this.mouseout_tips;
	var over_tips = this.mouseover_tips;
	var filed_name = filed_name||this.config.filed_name;
	$('['+filed_name+']').off('mouseover',over_tips).off('mouseout',out_tips);

}


Tips.prototype.onceTips = function(el_id,tips,viewtime,class_Name){
	
	var time = viewtime || 3000;
	var config = this.config;
	var filed_name = config.filed_name;
	var class_Name = class_Name || config.class_Name;
	var el = el_id;
	var tips =tips||el.attr(filed_name);
	var tipbox_name = 'OnceTips'||config.tipbox_name;
	
	var tips_box = document.createElement('div');
	  
	if(el.css( "position" ) == 'static') el.css( "position",'relative' );  
	$(tips_box).css({'position':'absolute','opacity': '0','display':'none','z-index':config.top_zindex});
	
	$(tips_box).addClass(class_Name).attr("id",tipbox_name).html(tips).appendTo(el);
	
	
	var x = el.offset().left;
	var y = el.offset().top - $(window).scrollTop();	  	 
	
	var diff_x = (($(el).width()-$('#'+tipbox_name).outerWidth())/2);	
	if($('#'+tipbox_name).outerWidth() + x > $(window).width()) {
		diff_x = $(el).width()-$('#'+tipbox_name).outerWidth();		
		$(tips_box).addClass('arrow_right');
	}
	if(diff_x + x <0) {
		diff_x = 0 ;
		$(tips_box).addClass('arrow_left');
	}
				    
	var c_x = diff_x ;
	
	var diff_y = ( y-$('#'+tipbox_name).outerHeight() > 0 )?( -$('#'+tipbox_name).outerHeight() ):( $(el).height() );
	var c_y = diff_y ;
	
	if(c_y>0)  $(tips_box).addClass('bottom');
	else $(tips_box).addClass('top');
	
	$(tips_box).css('left',c_x).css('top',c_y).show().animate({'opacity': '1'}, config.opacity_time);
	
	
	
	if(viewtime !== false)
		$(tips_box).delay(time).animate({'opacity': '0'}, config.opacity_time).queue(function() { $(this).remove(); });
	return tips_box;
	
}

Tips.prototype.createTips = function(new_config){
	if(typeof(new_config) == 'string'){
		
		var set_config = this.config;
		set_config.filed_name = new_config;
		
		
		return this.createTips(set_config);
	}
	
	var config = (typeof(new_config) == 'object')?new_config: this.config;
	var filed_name = config.filed_name;
	
		
	if(filed_name && typeof(filed_name) == 'string'){
		
		
	this.mouseover_tips = function() {		
		
		
		  var tipbox_name = config.tipbox_name;
		  var filed_name = config.filed_name;
		  var tips =$(this).attr(filed_name);
		
		  var tips_box = document.createElement('div');
		  
		  
		  $(tips_box).css({'position':'fixed','opacity': '0','display':'none','z-index':config.top_zindex});
		  
		  
		  $(tips_box).addClass(config.class_Name).attr("id",tipbox_name).html(tips).appendTo('body');
		
	
		  var x = $(this).offset().left;
		  var y = $(this).offset().top - $(window).scrollTop();	  	 
		  
		  var diff_x = ($('#'+tipbox_name).outerWidth() + x > $(window).width())?$(window).width()-($('#'+tipbox_name).outerWidth() + x):(($(this).width()-$('#'+tipbox_name).width())/2);
		  var c_x = x + diff_x ;
	
		  var diff_y = (($(this).height()+($('#'+tipbox_name).outerHeight())/2) + y > $(window).height())?(-$('#'+tipbox_name).outerHeight()):($(this).height()+($('#'+tipbox_name).outerHeight())/2);
		  var c_y = y + diff_y ;
		  
		
		  $(tips_box).css('left',c_x).css('top',c_y).show().animate({'opacity': '1'}, config.opacity_time);
		  			   
	};
	
	this.mouseout_tips = function(){
		$('#'+config.tipbox_name).remove();
	};
//		
	
	var out_tips = this.mouseout_tips;
	var over_tips = this.mouseover_tips;
		$('['+filed_name+']').each(function(){
			
			
			$(this).off('mouseover',over_tips).off('mouseout',out_tips);			
			$(this).on('mouseover',over_tips).on('mouseout', out_tips)
			
//			
			
		})
//		
//		$(document).on('mouseover','['+filed_name+']',this.mouseover).on('mouseout','['+filed_name+']', this.mouseout);
//		
		return true;
	}
	return false;
}


