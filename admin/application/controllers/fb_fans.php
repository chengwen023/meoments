<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fb_fans extends CI_Controller {
	
	
	var $fans_info;
	
	
	public function __construct(){
		parent::__construct();
		
		
		$this->userInfo = $this->session->userdata('userInfo');
		$this->load->model('topic_model', 'topic');
		$this->load->model('theme_model', 'theme');
		
		
		$this->data['admin_menu'] = $this->config->item('admin_menu');
		$this->data['admin_menuGroup'] = $this->config->item('admin_menuGroup');
		$this->data['userInfo'] = $this->userInfo;
		$this->data['menu'] = $this->config->item('admin_accountMenu');
		$this->data['breadcrumbs'] = $this->config->item('breadcrumbs');
		$this->data['title'] =$this->config->item('admin_title');
		
		$this->data['topbar_control']= array();
		$this->data['select_result'] = array();
		
		$gid = 0;
		$fid = 0;
		$get = array('gid'=>$gid);
// 		$get = array('fb_id'=>0); //讀取全部粉絲團id
		$value = $this->curl->simple_get('http://i-kuso.com/pofans/ajaxfans.php',$get);
		$value = json_decode($value,true);
		if(is_array($value)) $this->fans_info = $value;
	}
	/*
	* index
	*/
	public function index(){
		$this->lists();
	}
	
	/**
	 *	list
	 */
	function fansid_refresh(){
		
		$get = array('fb_id'=>0); //讀取全部粉絲團id
		$value = $this->curl->simple_get('http://i-kuso.com/pofans/ajaxfans.php',$get);
		$value = json_decode($value,true);
		
		
		foreach ($value AS $fans){
			if($fans['state']==1) continue;
			$this->db->flush_cache();
			$query = $this->db->select("auto_index")
			->from("ikuso_fansid")
			->where("fans_id", $fans['fb_id'])
			->limit(1)
			->get();
			$temp = $query->row_array();
		
			$add_input = array('fans_id' => $fans['fb_id'], 'fans_name' => $fans['name']);
			
			if(isset($temp['auto_index']) && $temp['auto_index'] >0 ){				
				$this->db->where('auto_index', $temp['auto_index']);
				$this->db->update('ikuso_fansid', $add_input);				
			}else{				
				$this->db->insert('ikuso_fansid', $add_input);
			}
			$checked_id[] = $fans['fb_id'];
		}
		$this->db->flush_cache();
		$this->db->where_not_in('fans_id', $checked_id);
		$this->db->delete('ikuso_fansid'); 
		
		$result = array('success' => 'Y', 'msg' => '');
		echo json_encode($result);
	}
	
	function igotest_fansid_refresh(){
	
		$get = array('fb_id'=>0); //讀取全部粉絲團id
		$value = $this->curl->simple_get('http://pofans.i-gotest.com/ajaxfans.php',$get);
		$value = json_decode($value,true);
	
	
		foreach ($value AS $fans){
			if($fans['state']==1) continue;
			$this->db->flush_cache();
			$query = $this->db->select("auto_index")
			->from("igotest_fansid")
			->where("fans_id", $fans['fb_id'])
			->limit(1)
			->get();
			$temp = $query->row_array();
	
			$add_input = array('fans_id' => $fans['fb_id'], 'fans_name' => $fans['name']);
				
			if(isset($temp['auto_index']) && $temp['auto_index'] >0 ){
				$this->db->where('auto_index', $temp['auto_index']);
				$this->db->update('igotest_fansid', $add_input);
			}else{
				$this->db->insert('igotest_fansid', $add_input);
			}
			$checked_id[] = $fans['fb_id'];
		}
		$this->db->flush_cache();
		$this->db->where_not_in('fans_id', $checked_id);
		$this->db->delete('igotest_fansid');
	
		$result = array('success' => 'Y', 'msg' => '');
		echo json_encode($result);
	}
	
	function igotest_fansid_online(){
	
		$index = $this->input->post('index');
		$online = $this->input->post('online');
	
		if($online == 'true') $add_input = array('online' => '1');
		else $add_input = array('online' =>'0');
	
		$this->db->where('auto_index', $index);
		$this->db->update('igotest_fansid', $add_input);
	}
	
	function fansid_online(){
	
		$index = $this->input->post('index');
		$online = $this->input->post('online');
		
		if($online == 'true') $add_input = array('online' => '1');
		else $add_input = array('online' =>'0');
		
		$this->db->where('auto_index', $index);
		$this->db->update('ikuso_fansid', $add_input);
	}
	
	function fansid_lists(){
		$defaultstatus= 'S';
		$activestatus = 'A';
	
		$order_name = $this->input->post('order_by');	
		$orderBy = $this->theme->order_by($order_name);
		$data = array_merge($this->data, $orderBy);
	
		$this->menu_index = 'I-Kuso 粉絲團管理';//
		if(!$this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, 'V')){
			$this->load->view('template', $data);
			return false;
		}
		
		$data['topbar_control'][] = array('current_url'=>'JAVASCRIPT:fans.refresh();','i_class'=>'icon-refresh', 'control_name'=>'重載資料');
		$data['select_result'] = array();
	
		$this->db->flush_cache();
		$this->db->select("*");
		$this->db->from("topic_group");	
		$query = $this->db->get();
	
		$data['title'] = $this->admin_model->getTitle($this->menu_index);
		$data['topic_group'] = $query->result_array();
		
		$this->db->flush_cache();
		$this->db->select("*");
		$this->db->from("ikuso_fansid");

		if(is_array($orderBy) ){
			foreach($orderBy AS $key => $value){
				if(!$key) continue;
				if($value == "down") $this->db->order_by('ikuso_fansid.'.str_replace('by-','', $key).' DESC');
				else $this->db->order_by('ikuso_fansid.'.str_replace('by-','', $key).' ASC');
			}
		}else $this->db->order_by('auto_index','ASC');
		
		
		$query = $this->db->get();
		$data['content'] = $query->result_array();
		foreach($data['content'] AS $key => $list){
			$temp_group = json_decode($list['topic_group']);
			$fans_groupname = $this->admin_model->get_fansgroupdefault($data['topic_group'], $temp_group);
			$data['content'][$key]['group_names'] = ($temp_group)?implode(',', $fans_groupname):'無';
			
			$data['content'][$key]['off'] = ($list['online'])?'':'off';
		}
	
	
		$data['main_content'] = $this->parser->parse('fb_fans/ikuso_fans_list', $data, true);
		$this->parser->parse('template', $data);
	
	}
	
	function lists(){
		$defaultstatus= 'S';
		$activestatus = 'A';
	
		$order_name = $this->input->post('order_by');	
		$orderBy = $this->theme->order_by($order_name);
		$data = array_merge($this->data, $orderBy);
	
		$this->menu_index = 'I-Kuso 粉絲團管理';//
		if(!$this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, 'V')){
			$this->load->view('template', $data);
			return false;
		}
		
		$data['topbar_control'][] = array('current_url'=>'JAVASCRIPT:fans.igotest_refresh();','i_class'=>'icon-refresh', 'control_name'=>'重載資料');
		$data['select_result'] = array();
	
		$this->db->flush_cache();
		$this->db->select("*");
		$this->db->from("topic_group");	
		$query = $this->db->get();
	
		$data['title'] = $this->admin_model->getTitle($this->menu_index);
		$data['topic_group'] = $query->result_array();
		
		$this->db->flush_cache();
		$this->db->select("*");
		$this->db->from("igotest_fansid");

		if(is_array($orderBy) ){
			foreach($orderBy AS $key => $value){
				if(!$key) continue;
				if($value == "down") $this->db->order_by('ikuso_fansid.'.str_replace('by-','', $key).' DESC');
				else $this->db->order_by('igotest_fansid.'.str_replace('by-','', $key).' ASC');
			}
		}else $this->db->order_by('auto_index','ASC');
		
		
		$query = $this->db->get();
		$data['content'] = $query->result_array();
		foreach($data['content'] AS $key => $list){
			$temp_group = json_decode($list['topic_group']);
			$fans_groupname = $this->admin_model->get_fansgroupdefault($data['topic_group'], $temp_group);
			$data['content'][$key]['group_names'] = ($temp_group)?implode(',', $fans_groupname):'無';
			
			$data['content'][$key]['off'] = ($list['online'])?'':'off';
		}
	
	
		$data['main_content'] = $this->parser->parse('fb_fans/igotest_fans_list', $data, true);
		$this->parser->parse('template', $data);
	}
	
	
	/**
	 *	actions
	 */
	function actions($ac){
	
	
		switch($ac){		
			case 'pic_list':
			
				$index = $this->input->post('index');
				$this->menu_index = '測驗分類';//
				$purview = ($index)?'E':'A';
				if(!$this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, $purview)){
					echo $this->load->view('purview_error', '');
					return false;
				}
			
				$data['status'] = $this->config->item('admin_group');
			
			
				$data['competence'] = $this->config->item('admin_group');
				$data['admin_competence'] = $this->config->item('admin_competence');
			
				if($index){
					$this->db->select('*')->from('topic_group')->where('auto_index',$index);
					$query = $this->db->get();
					$result = $query->result_array();
					if(is_array($result))  {
						$data['topic_group'] = array_pop($result);
						
						$this->db->flush_cache();
						$this->db->select('*')->from('topic_group_pic')->where('group_index',$index);
						$query = $this->db->get();
						$data['group_pic'] = $query->result_array();
					}
				}
			
			
			
				echo $this->load->view('fb_fans/fanspic_edit', $data, true);
				break;
			case 'add_fans':
				
				$index = $this->input->post('index');
				$this->menu_index = '測驗分類';//
				$purview = ($index)?'E':'A';
				if(!$this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, $purview)){
					echo $this->load->view('purview_error', '');
					return false;
				}
	
				$data['status'] = $this->config->item('admin_group');
	
				
				$data['competence'] = $this->config->item('admin_group');
				$data['admin_competence'] = $this->config->item('admin_competence');
				$data['fans_group'] = $this->fans_info;
				
				if($index){
					$this->db->select('*')->from('topic_group')->where('auto_index',$index);
					$query = $this->db->get();
					$result = $query->result_array();					
					if(is_array($result))  {
						$data['topic_group'] = array_pop($result);

						$fans = json_decode($data['topic_group']['fans_gids']);
						
						foreach($data['fans_group'] AS $key => $value){
							$data['fans_group'][$key]['checked'] = ((is_array($fans)) && in_array($value['gid'], $fans))?true:false;
						}
					}	
				}
				
				
				
				echo $this->load->view('fb_fans/fans_edit', $data, true);
				break;
			case 'edit_fansid':
			
				$index = $this->input->post('index');
				$this->menu_index = '測驗分類';//
				$purview = ($index)?'E':'A';
				if(!$this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, $purview)){
					echo $this->load->view('purview_error', '');
					return false;
				}
			
				$data['status'] = $this->config->item('admin_group');
			
			
				$data['competence'] = $this->config->item('admin_group');
				$data['admin_competence'] = $this->config->item('admin_competence');
				
				$this->db->flush_cache();
				$this->db->select("*");
				$this->db->from("topic_group");
				$query = $this->db->get();
				
				$data['title'] = $this->admin_model->getTitle($this->menu_index);
				$data['topic_group'] = $query->result_array();
			
				if($index){
					$this->db->flush_cache();
					$this->db->select('*')->from('ikuso_fansid')->where('auto_index',$index);
					$query = $this->db->get();
					$result = $query->result_array();
					if(is_array($result))  {
						$data['ikuso_fansid'] = array_pop($result);
			
						$fans = json_decode($data['ikuso_fansid']['topic_group']);
			
						foreach($data['topic_group'] AS $key => $value){
							$data['topic_group'][$key]['checked'] = ((is_array($fans)) && in_array($value['auto_index'], $fans))?true:false;
						}
					}
				}
			
			
			
				echo $this->load->view('fb_fans/fansid_edit', $data, true);
				break;
			case 'validate_pic':
					
				$this->menu_index = '測驗分類';//
				if(!$this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, 'E')){
					$result = array('success' => 'N', 'msg' => '權限不足');
					echo json_encode($result);
					break;
				}
					
				$index = (int) $this->input->post('auto_index');
				
				$add_input = array("pic_url	"=>'', "group_index	"=>'');
					
				$pic_url = $this->input->post("pic_url");
					
				if(is_array($pic_url)) {
					foreach ($pic_url AS $url){
						if(empty($url)) continue;
						$add_input = array("pic_url	"=>$url, "group_index	"=>$index);
						$this->db->insert('topic_group_pic', $add_input);
					}
				}
					
				$result = array('success' => 'Y', 'msg' => '修改完成');
				header('Content-type: text/plain; charset=utf-8');
				echo json_encode($result);
				break;
			case 'validate_fansid':
			
				$this->menu_index = '測驗分類';//
				if(!$this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, 'E')){
						$result = array('success' => 'N', 'msg' => '權限不足');
								echo json_encode($result);
								break;
				}
			
				$index = (int) $this->input->post('auto_index');
			
				$date = date("Y-m-d H:i:s");
				$account_id = $this->config->item('admin_accountIndex');
				$online = '1';
				$add_input = array("topic_group"=>'');
			
				$add_input["topic_group"] = json_encode( $this->input->post("fans_gids"),true);
			
				if($index) {
					$this->db->where('auto_index', $index);
					$this->db->update('ikuso_fansid', $add_input);
					$result['success'] = 'Y';
					$result['msg'] = '修改成功';
				}
			
			
				header('Content-type: text/plain; charset=utf-8');
				echo json_encode($result);
				break;
			case 'validate':
				
				$this->menu_index = '測驗分類';//				
				if(!$this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, 'E')){
					$result = array('success' => 'N', 'msg' => '權限不足');
					echo json_encode($result);
					break;
				}
				
				$index = (int) $this->input->post('auto_index');
				
				$date = date("Y-m-d H:i:s");
				$account_id = $this->config->item('admin_accountIndex');
				$online = '1';
				$add_input = array("name"=>'','class_name'=>'','fans_gids'=>0,'online'=>$online);
	
				foreach ($add_input AS $key => $value){
					if(!$value){
						switch ($key){		
							case "fans_gids":
								$add_input[$key] = json_encode( $this->input->post("fans_gids"),true);
								break;
							default:
								$add_input[$key] = $this->input->post($key);
								break;
						}
					}
				}
				
				if($index) {
					
					$add_input['update_time'] = $date;					
					$add_input['update_account_index'] = $account_id;
					
					$result = $this->topic->check_fans_exist_and_update($add_input,$index);
				}
				else {
					$add_input['create_time'] = $date;
					$add_input['update_time'] = $date;
					$add_input['create_account_index'] = $account_id;
					$add_input['update_account_index'] = $account_id;
					
					$result = $this->topic->check_fans_exist_and_insert($add_input);
				}
				
				header('Content-type: text/plain; charset=utf-8');
				echo json_encode($result);
				break;
			case 'del':
				$index = $this->input->post('index');
				$result = $this->admin_model->del_fans_by_pk($index);
				
				header('Content-type: text/plain; charset=utf-8');
				echo json_encode($result);
				break;
		}
	}
}