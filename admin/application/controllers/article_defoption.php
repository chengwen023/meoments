<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Article_defoption extends CI_Controller {
	
	var $menu_index;
	
	
	public function __construct(){
		parent::__construct();
		
		$this->userInfo = $this->session->userdata('userInfo');
		$this->load->model('theme_model', 'theme');
		$this->load->model('defoption_model', 'defoption');
		
		$this->data['admin_menu'] = $this->config->item('admin_menu');
		$this->data['admin_menuGroup'] = $this->config->item('admin_menuGroup');
		$this->data['userInfo'] = $this->userInfo;
		$this->data['menu'] = $this->config->item('admin_accountMenu');
		$this->data['breadcrumbs'] = $this->config->item('breadcrumbs');
		$this->data['title'] =$this->config->item('admin_title');
		
		$this->data['topbar_control']= array();
		$this->data['select_result'] = array();
		
		
	}
	
	function create_defoptions(){
		$topic_array = $this->input->post ( 'topic_array' );		
		$auto_index = $this->input->post ( 'auto_index' );
	
		$date = date ( "Y-m-d H:i:s" );
		$account_id = $this->config->item ( 'admin_accountIndex' );
	
		$add_input = array (
				'defoptions_group_name' => $topic_array['defoptions_group_name']
		);
		$add_options = isset($topic_array['options'])?$topic_array['options']:false;
	
		$add_input ['create_time'] = $date;
		$add_input ['create_account_index'] = $account_id;
		$add_input ['update_time'] = $date;
		$add_input ['update_account_index'] = $account_id;
	
		if($auto_index)
			$result = $this->defoption->check_defoption_exist_and_update($add_input,$add_options,$auto_index);
		else
			$result = $this->defoption->check_defoption_exist_and_insert($add_input,$add_options);
	
	}
	
	/*
	* index
	*/
	public function index(){
		$this->lists();
	}
	
	/**
	 *	list
	 */
	
	
	function view(){
			$data = $this->data;
			
			$this->parser->parse('defoption/defoption_view', $data);
		}

	function lists(){
			
		$data = $this->data;
		$this->menu_index = '文章預設選項';//
		if(false === $activestatus = $this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, 'V')){
			$this->parser->parse('template', $data);
			return false;
		} 
		
		$defaultstatus = (isset($activestatus))?$activestatus:$this->config->item('defaultstatus');
		
		
		$this->db->flush_cache();
		$this->db->select("*");
		$this->db->from("article_defoption_group");
		$this->db->where("article_defoption_group.online",'1');
		
		
		$query = $this->db->get();
		
		$data['content'] = $query->result_array();
		foreach($data['content'] AS $key => $list){
					
		}
		
		$data['main_content'] = $this->parser->parse('defoption/article_defoption_list', $data, true);
		$this->parser->parse('template', $data);
	}
	
	function choose_data(){
		$type = $this->input->post('type');
		
		$this->db->select("*")->from("topic_defaultoptions")->where("select_index",$type);
			$query = $this->db->get();
			$data = $query->result_array();
			foreach($data AS $item){
				echo '<li><div>選項:'.$item['option_name'].'<input type="hidden" name="options[choose][]" value="'.$item['auto_index'].'"/></div><div class="imgurl_box" ><span><img src="uploads/temp/default_pic.jpg" width="70px" /></span><input type="hidden" value="default_pic.jpg" name="options[text_imgurl][]"/></div></li>';
			}
	}
	
	/**
	 *	actions
	 */

	
	
	function actions($ac){
	
	
		switch($ac){		

			case 'add_defoptions' :
			
				$index = $this->input->post ( 'index' );
				if (! $index) {
					$purview = 'A';
// 					
				}else{
					$purview = 'E';
				}
			
				$this->menu_index = '文章列表'; //
				
				if ($this->admin_model->checkCompetence_getMenuInfo ( $this->menu_index, $purview )) {
					$onshelf_purview = true;
				} else {
					$onshelf_purview = false;
				}
			
				
				/*default value*/
				$defoptions = array('choose'=>'','title'=>'','text'=>'','pic_url' => 'http://goquiz88.com/img/goquiz88_facebook_share.jpg');
				$data ['auto_index'] = 0;
				$data ['online'] = 0;
				$data ['defoptions_group_name'] = '';
				$data ['article_options'][] = $defoptions;
				
				$this->db->select ( 'article_defoption_group.* ' )->from ( 'article_defoption_group' );				
				$this->db->where ( 'article_defoption_group.auto_index', $index );
					
				$query = $this->db->get ();
				$result = $query->result_array ();
				if (is_array ( $result ) && count($result)) {
					$data = array_pop ( $result );
					
					
					$this->db->select ( '*' )->from ( 'article_defoptions' )->where ( 'defoptions_group_index', $index );
					$query = $this->db->get ();
					$data ['article_options'] = $query->result_array ();
					if (!is_array ( $data ['article_options'] ) || !count($data ['article_options']) )
					$data ['article_options'][] = $defoptions;
					
				}
				$this->parser->parse ( 'defoption/article_defoptions_edit', $data );
				break;
			case 'del' :
				$this->menu_index = '文章預設選項'; //
				if (! $this->admin_model->checkCompetence_getMenuInfo ( $this->menu_index, 'D' )) {
					$result = array (
							'success' => 'N',
							'msg' => '權限不足'
					);
					echo json_encode ( $result );
					break;
				}
			
				$index = $this->input->post ( 'index' );
				$result = $this->defoption->del_defoption_by_index ( $index );
			
				header ( 'Content-type: text/plain; charset=utf-8' );
				echo json_encode ( $result );
				break;
		}
		
		
	}
}