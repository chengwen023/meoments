<style>
#tips_box{
	position: fixed;
	font-size: 16px;
	display: none;
	padding: 5px 10px;
	background: #fff;
	box-shadow: 1px 1px 2px 1px rgba(33,66,99,.5);
	border-radius:5px;
	z-index: 10000; 
}
</style>

<div class="widget">
	<div class="list_controllers" style="position: absolute;text-align: right;right: 0;margin-top: -25px;">
		<a  href="javascript:void(0);" onclick="article.add_edit(0);" title="新增">新增資料<i class="icon-plus-sign"></i></a>		
	</div>
	<table cellpadding="0" cellspacing="0" width="100%" class="tDefault tLight">
		<form id="list_orderby" method="post">
		<input id="order_by" type="hidden" name="order_by" value="">
		<thead>
			<tr >
				<td title="題目" tips ="使用滑鼠點選 可以排序該欄位" class="order_control" data-orderby = "article_title">題目<i class="icon-chevron-{by-article_title}"></i></td>
				<td title="群組名稱" >群組名稱</td>
				<td title="選項類型" class="order_control" data-orderby = "options_select">選項類型<i class="icon-chevron-{by-options_select}"></i></td>
				<td title="題目圖片">題目圖片</td>
				<td title="選項圖片">選項圖片</td>				
				<td title="建立人員" class="order_control" data-orderby = "create_account_index">建立人員<i class="icon-chevron-{by-create_account_index}"></i></td>
				<td title="建立日期" class="order_control" data-orderby = "create_time" style="width:80px">建立日期<i class="icon-chevron-{by-create_time}"></i></td>
				<td title="修改日期" class="order_control" data-orderby = "update_time" style="width:80px">修改日期<i class="icon-chevron-{by-update_time}"></i></td>
				<td title="審核">審核</td>
				<td title="上線排序" tips ="使用滑鼠點選 可以排序該欄位,本欄位依照是否上線做為排序依據" class="order_control" data-orderby = "online" >上線排序<i class="icon-chevron-{by-online}"></i></td>
				
			</tr>			
		</thead>
		</form>
		<tbody>
			{content}
			<tr id="user_{auto_index}" class="user_row">
							<td style="white-space:nowrap;">{article_title}</td>
							<td>{group_names}</td>
							<td >{select_optionName}</td>							
							<td><img tips="adonis"src="{article_imgurl}" style="width: 60px;" />
							</td>
							<td><img src="{facebook_imgurl}" style="width: 60px;" />
							</td>
							<td class="create_id">{id}</td>
							<td style="white-space:nowrap;">{create_time}</td>
							<td style="white-space:nowrap;">{update_time}</td>
							<td style="white-space:nowrap;">{fail_state}</td>
							<td style="white-space:nowrap;">
								<a href="javascript:void(0);" class="tablectrl_small bBlue topicview_{edit_control}" original-title="View" title="觀看" alt="觀看" onclick="article.add_edit({auto_index});">
								<span class="iconb" data-icon="" ></span></a>				
								<a href="javascript:void(0);" class="tablectrl_small bBlue {edit_control}" original-title="View" title="觀看" alt="觀看" onclick="article.add_edit({auto_index});">
								<span class="iconb" data-icon="" ></span></a>								
								<a href="javascript:void(0);" class="tablectrl_small bGreen {edit_control}" original-title="Edit" title="編輯" alt="編輯" onclick="article.add_edit({auto_index});">
								<span class="iconb" data-icon="" ></span></a>
								<a href="javascript:void(0);" class="tablectrl_small bRed {edit_control}" original-title="Del" title="刪除" alt="刪除" onclick="article.del({auto_index});">
								<span class="iconb" data-icon="" ></span></a>								
							</td>							
						</tr>			
			{/content}
		</tbody>
	</table>
	
</div>
{page_list}
<script src="js/article.js"></script>
<script src="js/span_editor.js"></script>
<style>

thead TD{
white-space: nowrap;
}
.hide, .topicview_{
	display: none;
}
.topicview_hide{
	display: initial;
}
.tLight tbody td{
	padding: 0px 16px;
}
.external_link{
	background: none;
}
	.more_page{
		text-align: center;
	}
	
	.more_page SPAN {
		-moz-box-shadow:inset 0px 1px 0px 0px #ffffff;
		-webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;
		box-shadow:inset 0px 1px 0px 0px #ffffff;
		background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #dfdfdf));
		background:-moz-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
		background:-webkit-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
		background:-o-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
		background:-ms-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
		background:linear-gradient(to bottom, #ededed 5%, #dfdfdf 100%);
		filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#dfdfdf',GradientType=0);
		background-color:#ededed;
		-moz-border-radius:6px;
		-webkit-border-radius:6px;
		border-radius:6px;
		border:1px solid #dcdcdc;
		display:inline-block;
		cursor:pointer;
		color:#777777;
		font-family:arial;
		font-size:15px;
		font-weight:bold;
		padding:6px 14px;
		text-decoration:none;
		text-shadow:0px 1px 0px #ffffff;
	}
	.more_page SPAN.page_selected {		
		color:#c92200;
		text-shadow:0px 1px 0px #ded17c;
	}
	
	.more_page SPAN:hover {
		color:#ffffff;
		text-shadow:0px 1px 0px #528ecc;
		-moz-box-shadow:inset 0px 1px 0px 0px #bbdaf7;
	    -webkit-box-shadow:inset 0px 1px 0px 0px #bbdaf7;
	    box-shadow:inset 0px 1px 0px 0px #bbdaf7;
		background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #378de5), color-stop(1, #79bbff));
		background:-moz-linear-gradient(top, #378de5 5%, #79bbff 100%);
		background:-webkit-linear-gradient(top, #378de5 5%, #79bbff 100%);
		background:-o-linear-gradient(top, #378de5 5%, #79bbff 100%);
		background:-ms-linear-gradient(top, #378de5 5%, #79bbff 100%);
		background:linear-gradient(to bottom, #378de5 5%, #79bbff 100%);
		filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#378de5', endColorstr='#79bbff',GradientType=0);
		background-color:#378de5;
		
		border:1px solid #84bbf3;
	}
	.more_page SPAN:active {
		position:relative;
		top:1px;
	}

</style>

<script>
function order_by(this_td){

	$('#order_by').val( $(this_td).attr('data-orderby'));

	$('#list_orderby').submit();

}

$(".order_control").live( "click", function() {
	var order_name = $(this).attr('data-orderby');
	if(order_name) order_by(this);
	
	return false;
});	

$('[tips]').hover(
		  function() {
			  var x = $(this).offset().left;
			  var y = $(this).offset().top;
		  	  var tips_box = document.createElement('div');
			  $(tips_box).attr("id","tips_box").html($(this).attr('tips')).appendTo('body');

			  var tips_width = $('#tips_box').width()+40;
			  var tips_height = $('#tips_box').height();
			  
			  var this_width = $(this).width();
			  var this_height = $(this).height();
			 
			  var diff_x = (tips_width + x > $(window).width())?$(window).width()-(tips_width + x):((this_width-$('#tips_box').width())/2);
			  var c_x = x + diff_x ;

			  var diff_y = (tips_height + y > $(window).height())?$(window).height()-(tips_height + y):(this_height+(tips_height)/2);
			  var c_y = y + diff_y ;
				 
			  $(tips_box).css('left',c_x).css('top',c_y).show();
			  			   
		  }, function() {
		   	$('#tips_box').remove();
		  }
		);
</script>