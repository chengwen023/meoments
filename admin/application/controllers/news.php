<?php
ini_set('post_max_size', '1024M');
ini_set('upload_max_filesize', '1024M');
ini_set('memory_limit', '1024M');

if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
date_default_timezone_set('Asia/Taipei');
class News extends CI_Controller {
	var $menu_index;
	public function __construct() {
		parent::__construct ();
		
		$this->menu_index = 6; //
		$this->load->helper('url');
		$this->userInfo = $this->session->userdata('userInfo');
		$this->load->model('article_model', 'article');
		$this->load->model('topic_model', 'topic');
		$this->load->model('theme_model', 'theme');
		$this->load->model('news_model', 'news');
		$this->load->model('statistics_model', 'statistics');
		
		$this->data ['admin_menu'] = $this->config->item ( 'admin_menu' );
		$this->data ['admin_menuGroup'] = $this->config->item ( 'admin_menuGroup' );
		$this->data ['userInfo'] = $this->userInfo;
		$this->data ['menu'] = $this->config->item ( 'admin_accountMenu' );
		$this->data ['breadcrumbs'] = $this->config->item ( 'breadcrumbs' );
		$this->data ['title'] = $this->config->item ( 'admin_title' );
		
		$this->data ['topbar_control'] = array ();
		$this->data ['select_result'] = array ();
	}

	/*
	 * index
	 */
	public function index($pg = 1) {
		$this->news_article($pg);
	}

	/*
	 * 文章列表
	 */
	function news_article($pg = 1, $user = '') {
		$page_one = 20;
		$order_name = $this->input->post('order_by');
		$orderBy = $this->theme->order_by($order_name ,'news_article');
		$data = array_merge($this->data, $orderBy);

		if ( $pg || $user )
			$this->menu_index = '文章列表';
		
		if ( false === $activestatus = $this->admin_model->checkCompetence_getMenuInfo ($this->menu_index, 'V') ) {
			$this->parser->parse ( 'template', $data );
			return false;
		}

		$defaultstatus = isset($activestatus) ? $activestatus : $this->config->item('defaultstatus');

		$purview = 'O';
		if ( $this->admin_model->checkCompetence_getMenuInfo($this->menu_index, $purview) ) {
			$onshelf_purview = true;
		} else {
			$onshelf_purview = false;
		}
		
		if ($this->menu_index)
			$data['breadcrumbs'] = $this->config->item('breadcrumbs');
		
		$data['onshelf_purview'] = $onshelf_purview;

		$fans_list = $this->admin_model->get_user_array();
		array_unshift($fans_list, array('auto_index' => 'all', 'id'=>'全部'));
		$days_list = $this->statistics->get_day_list(14);
		array_unshift($days_list, array('day' => '全部'));

		if ( !empty($this->input->post('fans')) ) {
			$pg = 1;
			$fans = $this->input->post('fans');
			$this->session->set_userdata('news_article-fans', $fans);
		} else {
			if ( !empty($this->session->userdata('news_article-fans')) && empty($this->input->post('days')) ) {
				$fans = $this->session->userdata('news_article-fans');
			} else {
				$fans = 'all';
			}
		}
		
		if ( !empty($this->input->post('days')) ) {
			$days = $this->input->post ('days');
			$this->session->set_userdata('news_article-days', $days);
		} else {
			if ( !empty($this->session->userdata('news_article-days')) ) {
				$days = $this->session->userdata('news_article-days');
			} else {
				$days = '全部';
			}
		}



		$article_id = $this->input->post('article_id');
		if ( !empty($article_id) ) {
			$pg = 1;
			$this->db->flush_cache();
			$this->db->select('na.*, na.title AS article_title, nas.fb_id, nas.online, nas.like_message_count, nas.unlike_message_count, 
				nas.daily_views, nas.views, nas.google_ad, nas.pofans2_id, (nas.daily_like_click_count+nas.daily_unlike_click_count) AS daily_click_total, 
				(nas.like_click_count+nas.unlike_click_count) AS click_total, nas.source_mode,
				nas.like_count, nas.share_count,
				fpc.fb_group_id, fpc.fb_post_id, fpc.title AS fb_title');
			$this->db->from('news_article AS na');
			$this->db->where('na.id', $article_id);
			
			$this->db->join ('news_article_state as nas', 
					'nas.id = na.id');
			$this->db->join ('facebook_post_content AS fpc', 
					'fpc.id = nas.fb_id', 'left');

		} else {
			$earlier_time = strtotime($days);
			$later_time = strtotime($days) + 86400;

			$this->db->flush_cache();
			$this->db->select('na.*, na.title AS article_title, nas.fb_id, nas.online, nas.like_message_count, nas.unlike_message_count, 
				nas.daily_views, nas.views, nas.google_ad, nas.pofans2_id, (nas.daily_like_click_count+nas.daily_unlike_click_count) AS daily_click_total, 
				(nas.like_click_count+nas.unlike_click_count) AS click_total, nas.source_mode, 
				nas.like_count, nas.share_count,
				fpc.fb_group_id, fpc.fb_post_id, fpc.title AS fb_title');
			$this->db->from('news_article AS na');
			if ( $fans != 'all' ) $this->db->where('create_user_id', $fans);
			if ( $days != '全部' ) {
				$this->db->where('na.create_time >=', $earlier_time);
				$this->db->where('na.create_time <', $later_time);
			}
			$this->db->join ('news_article_state as nas', 
					'nas.id = na.id');
			$this->db->join ('facebook_post_content AS fpc', 
					'fpc.id = nas.fb_id', 'left');

			if ( is_array($orderBy) && !empty($orderBy) ) {
				foreach ( $orderBy as $key => $value ) {
					if (!$key)
						continue;

					$key = str_replace('by-', '', $key);
					if ($value == "down")
						$this->db->order_by($key . ' DESC');
					else {
						$this->db->order_by($key . ' ASC');
					}
					
					// if ( $this->news->check_field('news_article', $key) == 1 ) {
					// 	if ($value == "down")
					// 		$this->db->order_by($key . ' DESC');
					// 	else {
					// 		$this->db->order_by($key . ' ASC');
					// 	}
					// } elseif ( $this->news->check_field('news_article_state', $key) == 1 ) {
					// 	if ($value == "down")
					// 		$this->db->order_by('nas.' . $key . ' DESC');
					// 	else {
					// 		$this->db->order_by('nas.' . $key . ' ASC');
					// 	}
					// } else {
					// 	$this->db->order_by('create_time DESC');
					// }
				}
			} else {
				$this->db->order_by('create_time DESC');
			}
		}



		$total_query = clone $this->db;
		$page['total_page'] = $total_query->get()->num_rows;
		$this->db->limit($page_one, ($pg - 1) * $page_one);
		$query = $this->db->get();
		
		$data['title'] = $this->admin_model->getTitle($this->menu_index);
		$data['content'] = $query->result_array();

		$category = $this->news->get_categories_array();
		$account_lists = $this->admin_model->get_user_id_array();
		foreach ( $data['content'] as $key => $list ) {
			$data['content'][$key]['create_time'] = date('Y-m-d H:i:s', $data['content'][$key]['create_time']);
			$data['content'][$key]['create_user_id'] = $account_lists[$data['content'][$key]['create_user_id']];
			$data['content'][$key]['update_user_id'] = $account_lists[$data['content'][$key]['update_user_id']];
			$data['content'][$key]['off'] = ($data['content'][$key]['online']) ? '' : 'off';
			// if ( empty($data['content'][$key]['fb_title']) ) {
			// 	$data['content'][$key]['fb_title'] = empty($data['content'][$key]['fb_group_id']) ? '' : '來源';
			// }

			if ( empty($data['content'][$key]['fb_title']) ) {
				$data['content'][$key]['fb_title'] = empty($data['content'][$key]['fb_group_id']) ? '自訂來源' : '來源';
			}

			$data['content'][$key]['source'] = empty($data['content'][$key]['fb_group_id']) ? '站外' : '站內';

			if ( $data['content'][$key]['source_mode'] == '1' ) {
				$data['content'][$key]['source_mode'] = '改寫';
			} elseif ( $data['content'][$key]['source_mode'] == '2' ) {
				$data['content'][$key]['source_mode'] = '翻譯';
			} elseif ( $data['content'][$key]['source_mode'] == '3' ) {
				$data['content'][$key]['source_mode'] = '參考';
			}

			if ( empty($data['content'][$key]['fb_group_id']) ) {
				$data['content'][$key]['source_url'] = $data['content'][$key]['article_source'];
			} else {
				$data['content'][$key]['source_url'] =' https://www.facebook.com/'.$data['content'][$key]['fb_group_id'].'/posts/'.$data['content'][$key]['fb_post_id'];
			}
			
			if ( $data['content'][$key]['like_message_count'] + $data['content'][$key]['unlike_message_count'] >= 10 ) {
				$data['content'][$key]['comment_count'] = 'bBlue';
			} else {
				$data['content'][$key]['comment_count'] = 'bRed';
			}

			if ( $data['content'][$key]['pofans2_id'] == 0 ) {
				$data['content'][$key]['pofans2_id'] = '';
			}

			$data['content'][$key]['article_category'] = '';
			$category_array = json_decode($list['category'], true);
			foreach ($category_array as $index => $value) {
				if ( $index != 0 ) $data['content'][$key]['article_category'] .= ','; 
				$data['content'][$key]['article_category'] .= $category[$value];
			}
		}
		
		$page['start'] = 1;
		$page['end'] = ceil ($page['total_page'] / $page_one);
		$page['url'] = 'news/news_article';
		$page['page_num'] = $pg;
		$page['url_end'] = '/' . $user;
		$data['page_list'] = $this->theme->page($page);

		$data['days'] = $days;
		$data['days_list'] = $days_list;
		$data['fans'] = $fans;
		$data['fans_list'] = $fans_list;

		$account_id = $this->config->item('admin_accountIndex');
		$data['group'] = $this->admin_model->get_groupIndex($account_id);

		// if ( $this->news->server_name() == '127.0.0.1' ) {
		// 	$data['front_url'] = 'http://127.0.0.1/news/web/code/news_article_review.php?id=';
		// 	$data['front_url_online'] = 'http://127.0.0.1/news/web/code/news_article.php?id=';
		// } else {
			$data['front_url'] = 'http://meoments.com/web/code/news_article_review.php?id=';
			$data['front_url_online'] = 'http://meoments.com/';
		// }

		$data['main_content'] = $this->parser->parse('article/news_article', $data, true);
		$this->parser->parse('template', $data);
	}

	function online_news_article($pg = 1, $user = '') {
		$page_one = 20;
		$topSearch = $this->input->post ( 'topSearch' );
		$order_name = $this->input->post('order_by');
		$orderBy = $this->theme->order_by($order_name ,'online_news_article');
		$data = array_merge($this->data, $orderBy);

		if ( $pg || $user )
			$this->menu_index = '線上文章';
		
		if ( false === $activestatus = $this->admin_model->checkCompetence_getMenuInfo ($this->menu_index, 'V') ) {
			$this->parser->parse ( 'template', $data );
			return false;
		}

		$defaultstatus = isset($activestatus) ? $activestatus : $this->config->item('defaultstatus');

		$purview = 'O';
		if ( $this->admin_model->checkCompetence_getMenuInfo($this->menu_index, $purview) ) {
			$onshelf_purview = true;
		} else {
			$onshelf_purview = false;
		}
		
		if ($this->menu_index)
			$data['breadcrumbs'] = $this->config->item('breadcrumbs');
		
		$data['onshelf_purview'] = $onshelf_purview;

		$fans_list = $this->admin_model->get_user_array();
		array_unshift($fans_list, array('auto_index' => 'all', 'id'=>'全部'));
		$days_list = $this->statistics->get_day_list(7);
		array_unshift($days_list, array('day' => '全部'));

		if ( !empty($this->input->post('fans')) ) {
			$pg = 1;
			$fans = $this->input->post('fans');
			$this->session->set_userdata('news_article-fans', $fans);
		} else {
			if ( !empty($this->session->userdata('news_article-fans')) && empty($this->input->post('days')) ) {
				$fans = $this->session->userdata('news_article-fans');
			} else {
				$fans = 'all';
			}
		}
		
		if ( !empty($this->input->post('days')) ) {
			$days = $this->input->post ('days');
			$this->session->set_userdata('news_article-days', $days);
		} else {
			if ( !empty($this->session->userdata('news_article-days')) ) {
				$days = $this->session->userdata('news_article-days');
			} else {
				$days = '全部';
			}
		}



		$article_id = $this->input->post('article_id');
		
			
			$earlier_time = strtotime($days);
			$later_time = strtotime($days) + 86400;

			$this->db->flush_cache();
			$this->db->select('na.title AS article_title, na.article_source, na.create_user_id,aa.id AS user_name, nas.*,fpsl.fb_sitename AS fb_sitename, fpc.fb_group_id, fpc.fb_post_id, fpc.title AS fb_title');
			$this->db->from('news_article_onshelf as na');
			if ( $fans != 'all' ) $this->db->where('na.create_user_id', $fans);
			if ( $days != '全部' ) {
				$this->db->where('na.create_time >=', $earlier_time);
				$this->db->where('na.create_time <', $later_time);
			}
			$this->db->where('nas.online', 1);
			$this->db->join ('news_article_state as nas', 
					'nas.id = na.id');
			$this->db->join ('facebook_post_content AS fpc', 
					'fpc.id = nas.fb_id', 'left');
			$this->db->join ('facebook_post_source_list AS fpsl',
					'fpc.fb_group_id = fpsl.fb_group_id', 'left');
			$this->db->join ('admin_account AS aa',
					'na.create_user_id = aa.auto_index', 'left');

			if ( !empty($article_id) ) {
				$this->db->where('na.id', $article_id);
			}
			if ($topSearch) {
				$this->db->where ( "( na.title like '%$topSearch%' or fpc.title like '%$topSearch%' or nas.id = '$topSearch' or aa.id like '%$topSearch%' or fpsl.fb_sitename like '%$topSearch%')" );
			}
			
			if ( is_array($orderBy) && !empty($orderBy) ) {
				foreach ( $orderBy as $key => $value ) {
					if (!$key)
						continue;

					$key = str_replace('by-', '', $key);
					if ( $this->news->check_field('news_article', $key) == 1 ) {
						if ($value == "down")
							$this->db->order_by($key . ' DESC');
						else {
							$this->db->order_by($key . ' ASC');
						}
					} elseif ( $this->news->check_field('news_article_state', $key) == 1 ) {
						if ($value == "down")
							$this->db->order_by('nas.' . $key . ' DESC');
						else {
							$this->db->order_by('nas.' . $key . ' ASC');
						}
					} else {
						if ($value == "down")
							$this->db->order_by($key . ' DESC');
						else {
							$this->db->order_by($key . ' ASC');
						}
					}
				}
			} else {
				$this->db->order_by('na.create_time DESC');
			}
		



		$total_query = clone $this->db;
		$page['total_page'] = $total_query->get()->num_rows;
		$this->db->limit($page_one, ($pg - 1) * $page_one);
		$query = $this->db->get();
		
		$data['title'] = $this->admin_model->getTitle($this->menu_index);
		$data['content'] = $query->result_array();

		$account_lists = $this->admin_model->get_user_id_array();
		foreach ( $data['content'] as $key => $list ) {
			$data['content'][$key]['online_time'] = date('Y-m-d H:i:s', $data['content'][$key]['online_time']);
			
			$data['content'][$key]['off'] = ($data['content'][$key]['online']) ? '' : 'off';

			if ( empty($data['content'][$key]['fb_title']) ) {
				$data['content'][$key]['fb_title'] = empty($data['content'][$key]['fb_group_id']) ? '自訂來源' : '來源';
			}

			if ( empty($data['content'][$key]['fb_group_id']) ) {
				$data['content'][$key]['source_url'] = $data['content'][$key]['article_source'];
			} else {
				$data['content'][$key]['source_url'] =' https://www.facebook.com/'.$data['content'][$key]['fb_group_id'].'/posts/'.$data['content'][$key]['fb_post_id'];
			}
			
			if ( $data['content'][$key]['like_message_count'] + $data['content'][$key]['unlike_message_count'] >= 10 ) {
				$data['content'][$key]['comment_count'] = 'bBlue';
			} else {
				$data['content'][$key]['comment_count'] = 'bRed';
			}

			if ( $data['content'][$key]['pofans2_id'] == 0 ) {
				$data['content'][$key]['pofans2_id'] = '';
			}
		}
		
		$page['start'] = 1;
		$page['end'] = ceil ($page['total_page'] / $page_one);
		$page['url'] = 'news/news_article';
		$page['page_num'] = $pg;
		$page['url_end'] = '/' . $user;
		$data['page_list'] = $this->theme->page($page);

		$data['days'] = $days;
		$data['days_list'] = $days_list;
		$data['fans'] = $fans;
		$data['fans_list'] = $fans_list;

		if ( $this->news->server_name() == '127.0.0.1' ) {
			$data['front_url'] = 'http://127.0.0.1/news/';
		} else {
			$data['front_url'] = 'http://meoments.com/';
		}

		$data['main_content'] = $this->parser->parse('article/online_news_article', $data, true);
		$this->parser->parse('template', $data);
	}

	/*
	 * 新聞統計
	 */
	function news_statistics($pg = 1, $user = '') {
		$this->menu_index = '新聞統計';
		$page_one = 50;
		$fans_list = $this->news->get_fans_array(1);
		$days_list = $this->statistics->get_day_list(7);

		if ( !empty($this->input->post('fans')) ) {
			$pg = 1;
			$fans = $this->input->post('fans');
			$this->session->set_userdata('news_statistics-fans', $fans);
		} else {
			if ( !empty($this->session->userdata('news_statistics-fans')) && empty($this->input->post('days')) ) {
				$fans = $this->session->userdata('news_statistics-fans');
			} else {
				$fans = isset($fans_list['0']['fb_group_id']) ? $fans_list['0']['fb_group_id'] : '';
			}
		}

		if ( !empty($this->input->post('days')) ) {
			$days = $this->input->post ('days');
			$this->session->set_userdata('news_statistics-days', $days);
		} else {
			if ( !empty($this->session->userdata('news_statistics-days')) ) {
				$days = $this->session->userdata('news_statistics-days');
			} else {
				$days = date('Y-m-d');
			}
		}

		$earlier_time = strtotime($days);
		$later_time = strtotime($days) + 86400;

		$order_name = $this->input->post('order_by');
		$orderBy = $this->theme->order_by($order_name, 'news_statistics');
		$data = array_merge($this->data, $orderBy);
		
		
		 
		if ( false === $activestatus = $this->admin_model->checkCompetence_getMenuInfo($this->menu_index, 'V') ) {
			$this->parser->parse('template', $data);
			return false;
		}

		$defaultstatus = isset($activestatus) ? $activestatus : $this->config->item('defaultstatus');
		
		//
		$purview = 'O';
		if ( $this->admin_model->checkCompetence_getMenuInfo($this->menu_index, $purview) ) {
			$onshelf_purview = true;
		} else {
			$onshelf_purview = false;
		}
		
		if ($this->menu_index)
			$data['breadcrumbs'] = $this->config->item('breadcrumbs');
		
		$data ['onshelf_purview'] = $onshelf_purview;

		$this->db->flush_cache();
		$this->db->select('fps.*, fps.title AS article_title, fpc.edit_user_id, fpc.lock_user_id');
		$this->db->from('facebook_post_statistics as fps');
		$this->db->where('fps.fb_group_id', $fans);
		$this->db->where('fps.create_time >=', $earlier_time);
		$this->db->where('fps.create_time <', $later_time);
		$this->db->where('fpc.language', '1');
		$this->db->join ('facebook_post_content as fpc', 
			'fpc.id = fps.id');

		if ( is_array($orderBy) ) {
			if ( !empty($orderBy) ) {
				foreach ( $orderBy as $key => $value ) {
					$key = str_replace('by-', '', $key);
					if ($value == "down") {
						$this->db->order_by('fps.'.$key . ' DESC');
					} else {
						$this->db->order_by('fps.'.$key . ' ASC');
					}
				} 
			} else {	
				$this->db->order_by('fps.create_time DESC');
			}
		} else {
			$this->db->order_by('fps.create_time DESC');
		}

		$total_query = clone $this->db;
		$page['total_page'] = $total_query->get()->num_rows;
		$this->db->limit($page_one, ($pg - 1) * $page_one);
		$query = $this->db->get();
		
		$data['title'] = $this->admin_model->getTitle($this->menu_index);
		$data['content'] = $query->result_array();

		$user_array = $this->admin_model->get_user_id_array();

		$time = time();
		$check_time = $time - ($time % 86400) - 28800;
		foreach ( $data['content'] as $key => $list ) {
			$data['content'][$key]['fb_post_content'] = json_decode($data['content'][$key]['fb_post_content'], true);
			
			if ( $data['content'][$key]['create_time'] >= $check_time) {
				$data['content'][$key]['create_time'] = $this->date2before($data['content'][$key]['create_time']);
			} else {
				$data['content'][$key]['create_time'] = date('Y-m-d H:i:s', $data['content'][$key]['create_time']);
			}
			
			if ( empty($data['content'][$key]['article_title']) || $data['content'][$key]['article_title'] == 'null') {
				if ( is_array($data['content'][$key]['fb_post_content']) ) {
					$data['content'][$key]['article_title'] = '';
					foreach ($data['content'][$key]['fb_post_content'] as $value) {
						$data['content'][$key]['article_title'] .= $value['content'];
					}
				} else {
					$data['content'][$key]['article_title'] = '來源';
				}
			}

			if ( $data['content'][$key]['edit_user_id'] != 0 ) {
				$data['content'][$key]['edit_user_id'] = $user_array[$data['content'][$key]['edit_user_id']];
			}

			if ( $data['content'][$key]['lock_user_id'] != 0 ) {
				$data['content'][$key]['lock_fb_article'] = 'off';
			} else {
				$data['content'][$key]['lock_fb_article'] = '';
			}
		}

		$page['start'] = 1;
		$page['end'] = ceil ($page['total_page'] / $page_one);
		$page['url'] = 'news/news_statistics';
		$page['page_num'] = $pg;
		$page['url_end'] = '/' . $user;	
		$data['page_list'] = $this->theme->page($page);

		$data['days'] = $days;
		$data['days_list'] = $days_list;
		$data['fans'] = $fans;
		$data['fans_list'] = $fans_list;
		$data['from_url'] = 'statistics';

		$data['main_content'] = $this->parser->parse('article/news_statistics', $data, true);
		$this->parser->parse('template', $data);
	}

	/*
	 * 英文統計
	 */
	function news_statistics_en($pg = 1, $user = '') {
		$this->menu_index = '英文統計';
		$page_one = 50;
		$fans_list = $this->news->get_fans_array(2);
		$days_list = $this->statistics->get_day_list(7);

		if ( !empty($this->input->post('fans')) ) {
			$pg = 1;
			$fans = $this->input->post('fans');
			$this->session->set_userdata('news_statistics_en-fans', $fans);
		} else {
			if ( !empty($this->session->userdata('news_statistics_en-fans')) && empty($this->input->post('days')) ) {
				$fans = $this->session->userdata('news_statistics_en-fans');
			} else {
				$fans = isset($fans_list['0']['fb_group_id']) ? $fans_list['0']['fb_group_id'] : '';
			}
		}

		if ( !empty($this->input->post('days')) ) {
			$days = $this->input->post ('days');
			$this->session->set_userdata('news_statistics_en-days', $days);
		} else {
			if ( !empty($this->session->userdata('news_statistics_en-days')) ) {
				$days = $this->session->userdata('news_statistics_en-days');
			} else {
				$days = date('Y-m-d');
			}
		}

		$earlier_time = strtotime($days);
		$later_time = strtotime($days) + 86400;

		$order_name = $this->input->post('order_by');
		$orderBy = $this->theme->order_by($order_name, 'news_statistics');
		$data = array_merge($this->data, $orderBy);
		
		
		 
		if ( false === $activestatus = $this->admin_model->checkCompetence_getMenuInfo($this->menu_index, 'V') ) {
			$this->parser->parse('template', $data);
			return false;
		}

		$defaultstatus = isset($activestatus) ? $activestatus : $this->config->item('defaultstatus');
		
		//
		$purview = 'O';
		if ( $this->admin_model->checkCompetence_getMenuInfo($this->menu_index, $purview) ) {
			$onshelf_purview = true;
		} else {
			$onshelf_purview = false;
		}
		
		if ($this->menu_index)
			$data['breadcrumbs'] = $this->config->item('breadcrumbs');
		
		$data ['onshelf_purview'] = $onshelf_purview;

		$this->db->flush_cache();
		$this->db->select('fps.*, fps.title AS article_title, fpc.edit_user_id, fpc.lock_user_id');
		$this->db->from('facebook_post_statistics as fps');
		$this->db->where('fps.fb_group_id', $fans);
		$this->db->where('fps.create_time >=', $earlier_time);
		$this->db->where('fps.create_time <', $later_time);
		$this->db->where('fpc.language', '2');
		$this->db->join ('facebook_post_content as fpc', 
			'fpc.id = fps.id');

		if ( is_array($orderBy) ) {
			if ( !empty($orderBy) ) {
				foreach ( $orderBy as $key => $value ) {
					$key = str_replace('by-', '', $key);
					if ($value == "down") {
						$this->db->order_by('fps.'.$key . ' DESC');
					} else {
						$this->db->order_by('fps.'.$key . ' ASC');
					}
				} 
			} else {	
				$this->db->order_by('fps.create_time DESC');
			}
		} else {
			$this->db->order_by('fps.create_time DESC');
		}

		$total_query = clone $this->db;
		$page['total_page'] = $total_query->get()->num_rows;
		$this->db->limit($page_one, ($pg - 1) * $page_one);
		$query = $this->db->get();
		
		$data['title'] = $this->admin_model->getTitle($this->menu_index);
		$data['content'] = $query->result_array();

		$user_array = $this->admin_model->get_user_id_array();

		$time = time();
		$check_time = $time - ($time % 86400) - 28800;
		foreach ( $data['content'] as $key => $list ) {
			$data['content'][$key]['fb_post_content'] = json_decode($data['content'][$key]['fb_post_content'], true);
			
			if ( $data['content'][$key]['create_time'] >= $check_time) {
				$data['content'][$key]['create_time'] = $this->date2before($data['content'][$key]['create_time']);
			} else {
				$data['content'][$key]['create_time'] = date('Y-m-d H:i:s', $data['content'][$key]['create_time']);
			}
			
			if ( empty($data['content'][$key]['article_title']) || $data['content'][$key]['article_title'] == 'null') {
				if ( is_array($data['content'][$key]['fb_post_content']) ) {
					$data['content'][$key]['article_title'] = '';
					foreach ($data['content'][$key]['fb_post_content'] as $value) {
						$data['content'][$key]['article_title'] .= $value['content'];
					}
				} else {
					$data['content'][$key]['article_title'] = '來源';
				}
			}

			if ( $data['content'][$key]['edit_user_id'] != 0 ) {
				$data['content'][$key]['edit_user_id'] = $user_array[$data['content'][$key]['edit_user_id']];
			}

			if ( $data['content'][$key]['lock_user_id'] != 0 ) {
				$data['content'][$key]['lock_fb_article'] = 'off';
			} else {
				$data['content'][$key]['lock_fb_article'] = '';
			}
		}

		$page['start'] = 1;
		$page['end'] = ceil ($page['total_page'] / $page_one);
		$page['url'] = 'news/news_statistics';
		$page['page_num'] = $pg;
		$page['url_end'] = '/' . $user;	
		$data['page_list'] = $this->theme->page($page);

		$data['days'] = $days;
		$data['days_list'] = $days_list;
		$data['fans'] = $fans;
		$data['fans_list'] = $fans_list;
		$data['from_url'] = 'statistics_en';

		$data['main_content'] = $this->parser->parse('article/news_statistics', $data, true);
		$this->parser->parse('template', $data);
	}

	/*
	 * 新聞監控
	 */
	function news_hot($pg = 1, $user = '') {
		$this->menu_index = '新聞監控';
		$page_one = 100;

		if ( !empty($this->input->post('days')) ) {
			$days = $this->input->post ('days');
			$this->session->set_userdata('news_hot-days', $days);
		} else {
			if ( !empty($this->session->userdata('news_hot-days')) ) {
				$days = $this->session->userdata('news_hot-days');
			} else {
				$days = date('Y-m-d');
			}
		}
		$earlier_time = strtotime($days);
		$later_time = strtotime($days) + 86400;
		$days_list = $this->statistics->get_day_list(7);
		$fans_name_list = $this->news->get_fans_name_array();

		$order_name = $this->input->post('order_by');
		$orderBy = $this->theme->order_by($order_name ,'news_hot');
		$data = array_merge($this->data, $orderBy);
		
		if ( false === $activestatus = $this->admin_model->checkCompetence_getMenuInfo($this->menu_index, 'V') ) {
			$this->parser->parse('template', $data);
			return false;
		}

		$defaultstatus = isset($activestatus) ? $activestatus : $this->config->item('defaultstatus');
		
		$purview = 'O';
		if ( $this->admin_model->checkCompetence_getMenuInfo($this->menu_index, $purview) ) {
			$onshelf_purview = true;
		} else {
			$onshelf_purview = false;
		}
		
		if ( $this->menu_index )
			$data['breadcrumbs'] = $this->config->item('breadcrumbs');
		
		$data['onshelf_purview'] = $onshelf_purview;
		$data['article_edited'] = 0;
		$data['article_total'] = 0;

		$this->db->flush_cache();
		$this->db->select('id');
		$this->db->from('hot_news');
		$this->db->where('create_time >=', $earlier_time);
		$this->db->where('create_time <', $later_time);
		$query = $this->db->get();
		$hot_array = $query->result_array();
		
		$id_array = array();
		foreach ( $hot_array as $key => $value ) {
			$id_array[] = $value['id'];
		}
		$hot_news = implode(',', $id_array);

		if ( !empty($hot_news) ) {
			$this->db->flush_cache();
			$this->db->select('fpc.*, fpc.title AS article_title, fps.most_likes AS fps_most_likes');
			$this->db->from('facebook_post_content as fpc');
			$this->db->where('fpc.fb_post_url !=', '');
			$this->db->where('fpc.language', '1');
			$this->db->where_in('fpc.id', $id_array);
			$this->db->join ('facebook_post_statistics as fps', 
			'fps.id = fpc.id');

			if ( is_array($orderBy) && !empty($orderBy) ) {
				foreach ( $orderBy as $key => $value ) {
					if (!$key)
						continue;

					$key = str_replace ('by-', '', $key);
					// if ( $this->news->check_field('facebook_post_content', $key) == 1 ) {
						if ($value == "down")
							$this->db->order_by('fpc.'.$key . ' DESC');
						else {
							$this->db->order_by('fpc.'.$key . ' ASC');
						}
					// } else {
					// 	$this->db->order_by('facebook_post_content.create_time DESC');
					// }
				}
			} else {
				$this->db->order_by('fpc.create_time DESC');
			}

			$total_query = clone $this->db;
			$page['total_page'] = $total_query->get()->num_rows;
			$this->db->limit($page_one, ($pg - 1) * $page_one);
			$query = $this->db->get();
			
			$data['title'] = $this->admin_model->getTitle($this->menu_index);
			$data['content'] = $query->result_array();

			$user_array = $this->admin_model->get_user_id_array();

			$time = time();
			$check_time = $time - ($time % 86400);
			foreach ( $data['content'] as $key => $list ) {
				$data['content'][$key]['fb_post_content'] = json_decode($data['content'][$key]['fb_post_content'], true);
				
				if ( $data['content'][$key]['create_time'] >= $check_time) {
					$data['content'][$key]['create_time'] = $this->date2before($data['content'][$key]['create_time']);
				} else {
					$data['content'][$key]['create_time'] = date('Y-m-d H:i:s', $data['content'][$key]['create_time']);
				}

				$data['content'][$key]['fb_post_type'] = $this->get_type_ch($data['content'][$key]['fb_post_type']);
				$data['content'][$key]['name'] = $fans_name_list[$data['content'][$key]['fb_group_id']];
				$data['content'][$key]['most_likes'] = '區段' . $data['content'][$key]['most_likes'];

				if ( empty($data['content'][$key]['article_title']) || $data['content'][$key]['article_title'] == 'null') {
					if ( is_array($data['content'][$key]['fb_post_content']) ) {
						$data['content'][$key]['article_title'] = '';
						foreach ($data['content'][$key]['fb_post_content'] as $value) {
							$data['content'][$key]['article_title'] .= $value['content'];
						}
					} else {
						$data['content'][$key]['article_title'] = '來源';
					}
				}

				if ( $data['content'][$key]['edit_user_id'] != 0 ) {
					$data['content'][$key]['edit_user_id'] = $user_array[$data['content'][$key]['edit_user_id']];
				}

				if ( $data['content'][$key]['article_id'] != 0 ) {
					$data['article_edited'] += 1;
				}

				if ( $data['content'][$key]['lock_user_id'] != 0 ) {
					$data['content'][$key]['lock_fb_article'] = 'off';
				} else {
					$data['content'][$key]['lock_fb_article'] = '';
				}
			}
			$data['article_total'] = !isset($key) ? 0 : $key+1;
		} else {
			$page['total_page'] = 1;
			$data['content'] = array();
		}
		
		

		$page['start'] = 1;
		$page['end'] = ceil($page['total_page'] / $page_one);
		$page['url'] = 'news/news_hot';
		$page['page_num'] = $pg;
		$page['url_end'] = '/' . $user;	
		$data['page_list'] = $this->theme->page($page);
		$data['days'] = $days;
		$data['days_list'] = $days_list;
		$data['from_url'] = 'hot';

		$data['main_content'] = $this->parser->parse('article/news_hot', $data, true);
		$this->parser->parse('template', $data);
	}

	/*
	 * 英文監控
	 */
	function news_hot_en($pg = 1, $user = '') {
		$this->menu_index = '英文監控';
		$page_one = 100;

		if ( !empty($this->input->post('days')) ) {
			$days = $this->input->post ('days');
			$this->session->set_userdata('news_hot-days_en', $days);
		} else {
			if ( !empty($this->session->userdata('news_hot_en-days')) ) {
				$days = $this->session->userdata('news_hot_en-days');
			} else {
				$days = date('Y-m-d');
			}
		}
		$earlier_time = strtotime($days);
		$later_time = strtotime($days) + 86400;
		$days_list = $this->statistics->get_day_list(7);
		$fans_name_list = $this->news->get_fans_name_array();

		$order_name = $this->input->post('order_by');
		$orderBy = $this->theme->order_by($order_name ,'news_hot');
		$data = array_merge($this->data, $orderBy);
		
		if ( false === $activestatus = $this->admin_model->checkCompetence_getMenuInfo($this->menu_index, 'V') ) {
			$this->parser->parse('template', $data);
			return false;
		}

		$defaultstatus = isset($activestatus) ? $activestatus : $this->config->item('defaultstatus');
		
		$purview = 'O';
		if ( $this->admin_model->checkCompetence_getMenuInfo($this->menu_index, $purview) ) {
			$onshelf_purview = true;
		} else {
			$onshelf_purview = false;
		}
		
		if ( $this->menu_index )
			$data['breadcrumbs'] = $this->config->item('breadcrumbs');
		
		$data['onshelf_purview'] = $onshelf_purview;
		$data['article_edited'] = 0;
		$data['article_total'] = 0;

		$this->db->flush_cache();
		$this->db->select('id');
		$this->db->from('hot_news');
		$this->db->where('create_time >=', $earlier_time);
		$this->db->where('create_time <', $later_time);
		$query = $this->db->get();
		$hot_array = $query->result_array();
		
		$id_array = array();
		foreach ( $hot_array as $key => $value ) {
			$id_array[] = $value['id'];
		}
		$hot_news = implode(',', $id_array);

		if ( !empty($hot_news) ) {
			$this->db->flush_cache();
			$this->db->select('fpc.*, fpc.title AS article_title, fps.most_likes AS fps_most_likes');
			$this->db->from('facebook_post_content as fpc');
			$this->db->where('fpc.fb_post_url !=', '');
			$this->db->where('fpc.language', '2');
			$this->db->where_in('fpc.id', $id_array);
			$this->db->join ('facebook_post_statistics as fps', 
			'fps.id = fpc.id');

			if ( is_array($orderBy) && !empty($orderBy) ) {
				foreach ( $orderBy as $key => $value ) {
					if (!$key)
						continue;

					$key = str_replace ('by-', '', $key);
					// if ( $this->news->check_field('facebook_post_content', $key) == 1 ) {
						if ($value == "down")
							$this->db->order_by('fpc.'.$key . ' DESC');
						else {
							$this->db->order_by('fpc.'.$key . ' ASC');
						}
					// } else {
					// 	$this->db->order_by('facebook_post_content.create_time DESC');
					// }
				}
			} else {
				$this->db->order_by('fpc.create_time DESC');
			}

			$total_query = clone $this->db;
			$page['total_page'] = $total_query->get()->num_rows;
			$this->db->limit($page_one, ($pg - 1) * $page_one);
			$query = $this->db->get();
			
			$data['title'] = $this->admin_model->getTitle($this->menu_index);
			$data['content'] = $query->result_array();

			$user_array = $this->admin_model->get_user_id_array();

			$time = time();
			$check_time = $time - ($time % 86400);
			foreach ( $data['content'] as $key => $list ) {
				$data['content'][$key]['fb_post_content'] = json_decode($data['content'][$key]['fb_post_content'], true);
				
				if ( $data['content'][$key]['create_time'] >= $check_time) {
					$data['content'][$key]['create_time'] = $this->date2before($data['content'][$key]['create_time']);
				} else {
					$data['content'][$key]['create_time'] = date('Y-m-d H:i:s', $data['content'][$key]['create_time']);
				}

				$data['content'][$key]['fb_post_type'] = $this->get_type_ch($data['content'][$key]['fb_post_type']);
				$data['content'][$key]['name'] = $fans_name_list[$data['content'][$key]['fb_group_id']];
				$data['content'][$key]['most_likes'] = '區段' . $data['content'][$key]['most_likes'];

				if ( empty($data['content'][$key]['article_title']) || $data['content'][$key]['article_title'] == 'null') {
					if ( is_array($data['content'][$key]['fb_post_content']) ) {
						$data['content'][$key]['article_title'] = '';
						foreach ($data['content'][$key]['fb_post_content'] as $value) {
							$data['content'][$key]['article_title'] .= $value['content'];
						}
					} else {
						$data['content'][$key]['article_title'] = '來源';
					}
				}

				if ( $data['content'][$key]['edit_user_id'] != 0 ) {
					$data['content'][$key]['edit_user_id'] = $user_array[$data['content'][$key]['edit_user_id']];
				}

				if ( $data['content'][$key]['article_id'] != 0 ) {
					$data['article_edited'] += 1;
				}

				if ( $data['content'][$key]['lock_user_id'] != 0 ) {
					$data['content'][$key]['lock_fb_article'] = 'off';
				} else {
					$data['content'][$key]['lock_fb_article'] = '';
				}
			}
			$data['article_total'] = !isset($key) ? 0 : $key+1;
		} else {
			$page['total_page'] = 1;
			$data['content'] = array();
		}
		
		

		$page['start'] = 1;
		$page['end'] = ceil($page['total_page'] / $page_one);
		$page['url'] = 'news/news_hot';
		$page['page_num'] = $pg;
		$page['url_end'] = '/' . $user;	
		$data['page_list'] = $this->theme->page($page);
		$data['days'] = $days;
		$data['days_list'] = $days_list;
		$data['from_url'] = 'hot_en';

		$data['main_content'] = $this->parser->parse('article/news_hot', $data, true);
		$this->parser->parse('template', $data);
	}

	/*
	 * 轉換時間顯示方式
	 */
	function date2before($val) {
		$diff = time() - $val;

		if ($diff < 0) {
			return '不久的將來';
		} elseif ($diff < 60) {
			return $diff . '秒前';
		} elseif ($diff < 3600) {
			return floor($diff/60) . '分鐘前';
		} elseif ($diff < 86400) {
			return floor($diff/3600) . '小時前';
		} elseif ($diff < 604800) {
			return floor($diff/86400) . '天前';
		} else {
			return floor($diff/604800) . '週前';
		}
	}

	/*
	 * 轉換發文類型顯示文字
	 */
	function get_type_ch($type) {
		if ( $type == '0' ) {
	        return '文字';
	   	} elseif ( $type == '1' ) {
	        return '圖文';
	    } elseif ( $type == '2' ) {
	        return '連結';
	    } elseif ( $type == '3' ) {
	        return '影片';
	    } else {
	    	return '';
	    }
	}

	/*
	 * 新增、編輯文章
	 */
	function actions($ac) {
		switch ($ac) {
		//新增文章
		case 'add_news' :	
			$index = $this->input->post('index');
			$from = $this->input->post('from');
			
			$account_id = $this->config->item('admin_accountIndex');
			if ( !$index ) {
				$purview = 'A';
			}else{
				$purview = 'E';
			}
		
			$this->menu_index = '新增文章'; 
			
			if ( $this->admin_model->checkCompetence_getMenuInfo($this->menu_index, $purview) ) {
				$onshelf_purview = true;
			} else {
				$onshelf_purview = false;
			}

			if ( $from == 'write' ) {
				//預設值
				$data['id'] = '0';
				$data['title'] = '';
				$data['fb_id'] = 0;
				$data['pofans'] = '';
				$data['summary'] = '';
				$data['article_text'] = '';
				$data['youtube_status'] = 'off';

				$data['article_img'] = 'images/fb.jpg';
				$data['fb_post_image_url'] = 'images/fb.jpg';

				$news_categories_list = $this->news->news_categories('',' *,"" AS checked ',false);
				foreach( $news_categories_list AS $key => $value ){
					if ( $value['parent_id'] == 0 ) {
						$data['news_categories_list'][$value['id']][$value['id']] = $value;
						$data['news_categories_list'][$value['id']][$value['id']]['checked'] = '';
					} else {
						$data['news_categories_list'][$value['parent_id']][$value['id']] = $value;
						$data['news_categories_list'][$value['parent_id']][$value['id']]['checked'] = '';
					}
				}
				$news_tags_list = $this->news->news_tags('',' *,"" AS checked ',false);
				foreach( $news_tags_list AS $key => $value ){
					$data['news_tags_list'][$value['id']] = $value;
					$data['news_tags_list'][$value['id']]['checked'] = '';
				}

				
				$data['fb_comment'] = array();
				$data['article_comment'] = array();
				$data['pofans_mode'] = '2';
				$data['source_mode'] = '3';

				$source_mode_list[] = array( 'mode_id' => '3', 'value' => '參考' );
				$source_mode_list[] = array( 'mode_id' => '2', 'value' => '翻譯' );
				$source_mode_list[] = array( 'mode_id' => '1', 'value' => '改寫' );		
				$data['source_mode_list'] = $source_mode_list;

			} else {
				$this->db->select ('*, fb_post_url AS url, id AS fb_id');
				$this->db->from ('facebook_post_content');
				$this->db->where ('facebook_post_content.id', $index);
				$query = $this->db->get();
				$result = $query->result_array();

				if ( $result[0]['action_time'] > 0 && $result[0]['edit_user_id'] != $account_id && (time()-$result[0]['action_time']<5400) ) {
					echo '文章編輯中';
					exit;
				} elseif ( $result[0]['lock_user_id'] > 0 ) {
					echo '文章上鎖中';
					exit;
				} else {
					$this->db->flush_cache();
					$this->db->where('id', $index);
					$this->db->update('facebook_post_content', array('action_time' => time(), 'edit_user_id' => $account_id));
				}

				if ( is_array($result) && count($result) ) {
					$data = array_pop($result);

					$data['fb_post_content'] = json_decode($data['fb_post_content'], true);
					$data['youtube_status'] = (!empty($data['youtube_id'])) ? 'on': 'off';
					$data['article_img'] = $data['fb_post_image_url'];

					$data['pofans'] = '';
					$data['summary'] = '';
					if ( !empty($data['fb_post_content']) ) {
						foreach ( $data['fb_post_content'] as $key => $value ) {
							$data['pofans'] .= $value['content'];
							$data['pofans'] .= $value['url'];
						}
					}
					
					$data['from_table'] = 'news_statistics';
					$data['from_index'] = '';

					$news_categories_list = $this->news->news_categories('',' *,"" AS checked ',false);
					foreach( $news_categories_list AS $key => $value ){
						if ( $value['parent_id'] == 0 ) {
							$data['news_categories_list'][$value['id']][$value['id']] = $value;
							$data['news_categories_list'][$value['id']][$value['id']]['checked'] = '';
						} else {
							$data['news_categories_list'][$value['parent_id']][$value['id']] = $value;
							$data['news_categories_list'][$value['parent_id']][$value['id']]['checked'] = '';
						}
					}

					$news_tags_list = $this->news->news_tags('',' *,"" AS checked ',false);
					foreach( $news_tags_list AS $key => $value ){
						$data['news_tags_list'][$value['id']] = $value;
						$data['news_tags_list'][$value['id']]['checked'] = '';
					}
					
					$fb_comment = $this->news->get_comments_list($index, 0);
					$article_comment = $this->news->get_comments_list($index, 1);
					$data['fb_comment'] = $fb_comment;
					$data['article_comment'] = $article_comment;

					$data['pofans_mode'] = '2';
					$data['source_mode'] = (preg_match('/_en/', $from)) ? 2 : 1;
				}
			}

			$pofans_mode_list[] = array( 'mode_id' => '2', 'value' => '連結' );
			$pofans_mode_list[] = array( 'mode_id' => '1', 'value' => '圖文' );		
			$data['pofans_mode_list'] = $pofans_mode_list;

			$data['from_table'] = $from;
			$data['from_index'] = '';
			$data['google_status'] = '';
			$data['news_tags_name'] = '';
			$data['category_name'] = '';
			$data['content'] = '';
			$data['like_name'] = '頂';
			$data['unlike_name'] = '噓';
			$data['article_source'] = '';
			$data['pofans_mode_photo_img'] = '';
			
			$this->parser->parse('article/news_edit', $data);
		break;
		//修改文章
		case 'edit_news_article' :
			$index = $this->input->post('index');
			if ( !$index ) {
				$purview = 'A';
			}else{
				$purview = 'E';
			}

			$account_id = $this->config->item('admin_accountIndex');
			$active = $this->input->post('active');
			if ( $active == 'edit' ) {
				$group = $this->admin_model->get_groupIndex($account_id);

				$this->db->flush_cache();
				$this->db->select('nas.action_time, nas.edit_user_id, nas.online, na.create_user_id');
				$this->db->from('news_article_state as nas');
				$this->db->where('nas.id', $index);
				$this->db->join ('news_article as na', 
				'nas.id = na.id');
				$query = $this->db->get();
				$temp = $query->row_array();

				if ( $group == 3 && $temp['create_user_id'] != $account_id ) {
					echo '<div style="font-size: 48px; text-align: center; margin-top: 100px;">權限不足</div>';
					exit;
				} elseif ( $temp['action_time'] > 0 && $temp['edit_user_id'] != $account_id && (time()-$temp['action_time']<5400) ) {
					echo '<div style="font-size: 48px; text-align: center; margin-top: 100px;">文章編輯中</div>';
					exit;
				} elseif ( $temp['online'] == 1 ) {
					echo '<div style="font-size: 48px; text-align: center; margin-top: 100px;">文章上架中</div>';
					exit;
				}else {
					$this->db->flush_cache();
					$this->db->where('id', $index);
					$this->db->update('news_article_state', array('action_time' => time(), 'edit_user_id' => $account_id));
				}
			}
				
			$this->menu_index = '編輯文章'; 
			
			if ( $this->admin_model->checkCompetence_getMenuInfo($this->menu_index, $purview) ) {
				$onshelf_purview = true;
			} else {
				$onshelf_purview = false;
			}
		
			/*default value*/
			$data['onshelf_purview'] = $onshelf_purview;
			$data['news_categories_list'] = $this->news->news_categories('',' *,"" AS checked ',false);
			$data['article_imgurl'] = 'http://goquiz88.com/img/goquiz88_facebook_share.jpg';
			$data['auto_index'] = 0;
			$data['online'] = 0;
			$data['article_title'] = '';
			$data['article_optiontitle'] = '';
			$data['article_text'] = '';
			$data['article_options'][] = array('choose'=>'','title'=>'','text'=>'');
			$data['from_table'] = '';
			$data['from_index'] = '';

			$this->db->flush_cache();
			$this->db->select('*, fb_post_content AS pofans, fb_img AS fb_post_image_url, pofans_photo AS pofans_mode_photo_img');
			$this->db->from('news_article');
			$this->db->where('id', $index);
			$query = $this->db->get();
			$result = $query->result_array();

			$this->db->flush_cache();
			$this->db->select('*');
			$this->db->from('news_article_state');
			$this->db->where('id', $index);
			$query = $this->db->get();
			$article_state = $query->result_array();

			if ( is_array($result) && count($result)) {
				$data = array_pop($result);
				
				$data['from_table'] = 'news_article';
				$data['from_index'] = '';
				$data['active'] = $active;

				$data['youtube_status'] = (!empty($data['youtube_id'])) ? 'on': 'off';
				$data['google_status'] = ($article_state[0]['google_ad'] == 1) ? 'on': 'off';
				$data['fb_id'] = $article_state[0]['fb_id'];
				$data['source_mode'] = $article_state[0]['source_mode'];
				$data['pofans_mode'] = $article_state[0]['pofans_mode'];
				
				//類別處理
				$news_categories_list = $this->news->news_categories('',' *,"" AS checked ',false);
				foreach( $news_categories_list AS $key => $value ) {
					if ( $value['parent_id'] == 0 ) {
						$data['news_categories_list'][$value['id']][$value['id']] = $value;
						$data['news_categories_list'][$value['id']][$value['id']]['checked'] = in_array($value['id'], json_decode($data['category'])) ? 'checked' : '';
					} else {
						$data['news_categories_list'][$value['parent_id']][$value['id']] = $value;
						$data['news_categories_list'][$value['parent_id']][$value['id']]['checked'] = in_array($value['id'], json_decode($data['category'])) ? 'checked' : '';
					}
				}

				//標籤處理
				$tag_array = is_array( json_decode($data['tag']) ) ? json_decode($data['tag']) : array();
				$news_tags_list = $this->news->news_tags('',' *,"" AS checked ',false);
				$tag_name_array = array();
				foreach( $news_tags_list AS $key => $value ){
					$data['news_tags_list'][$value['id']] = $value;
					if ( in_array($value['id'], $tag_array) ) {
						$data['news_tags_list'][$value['id']]['checked'] = 'checked';
						$tag_name_array[] = $value['name'];
					} else {
						$data['news_tags_list'][$value['id']]['checked'] = '';
					}
				}
				$data['news_tags_name'] = implode(',', $tag_name_array);

				//留言處理
				$fb_comment = $this->news->get_comments_list($article_state[0]['fb_id'], 0);
				$article_comment = $this->news->get_comments_list($article_state[0]['fb_id'], 1);
				$data['fb_comment'] = $fb_comment;
				$data['article_comment'] = $article_comment;

				//標題雙引號處理
				$data['title'] = str_replace("\"", "&#34;", $data['title']);
				$data['title'] = str_replace("'", "&#39;", $data['title']);
			}

			$pofans_mode_list[] = array( 'mode_id' => '2', 'value' => '連結' );
			$pofans_mode_list[] = array( 'mode_id' => '1', 'value' => '圖文' );		
			$data['pofans_mode_list'] = $pofans_mode_list;

			$this->parser->parse ( 'article/news_edit', $data );
			break;

		//編輯留言
		case 'edit_news_comment' :
			$article_id = $this->input->post('article_id');
			$fb_id = $this->input->post('fb_id');

			$this->db->flush_cache();
			$this->db->select ('like_name, unlike_name');
			$this->db->from ('news_article');
			$this->db->where ('id', $article_id);
			$query = $this->db->get();
			$result = $query->result_array();

			if ( is_array($result) && count($result)) {
				$data = array_pop($result);

				//留言處理
				$data['article_id'] = $article_id;
				$data['fb_id'] = $fb_id;

				$fb_comment = $this->news->get_comments_list($fb_id, 0);
				$article_comment = $this->news->get_comments_list($fb_id, 1);
				$data['fb_comment'] = $fb_comment;
				$data['article_comment'] = $article_comment;
			}

			$this->parser->parse ( 'article/news_comments_edit', $data );
			break;
		}
	}

	/*
	 * 圖片取代網址
	 */
	public function move_article_photo() {
		$img = $this->input->post('img');
		
		$check_dir = '../file/check/';
		$check_url = 'check'.((int)100*microtime(true)).$this->input->post('index').'.jpg';
		$check_img =  $this->news->move_article_photo($img, $check_url, $check_dir);
		
		
		if ( $this->news->server_name() == '127.0.0.1' ) {
			echo ( preg_match("/http|https|data\:/", $check_img) ) ? $check_img : 'http://127.0.0.1/news/file/check/'.$check_img;
		}else{
			echo ( preg_match("/http|https|data\:/", $check_img) ) ? $check_img : 'http://file.meoments.com/check/'.$check_img;
		}
	}

	/*
	 * 刪除圖片
	 */
	public function remove_check_photo() {
		$img = $this->input->post('img');
		if ( $this->news->server_name() == '127.0.0.1' ) {
			$img = str_replace('http://127.0.0.1/news/file/', '../file/', $img);
			if( preg_match('/news\/file\/n/', $img) ) return;
		}else{
			$img = str_replace('http://file.meoments.com/', '../file/', $img);
			if( preg_match('/meoments\.com\/n/', $img) ) return;
		}

		$this->news->remove_check_photo($img);
	}

	/*
	 * 裁圖
	 */
	function cut_img(){
		$img = $this->input->post('img');
		$cut_info = $this->input->post('cut_info');
		$img_type = $this->input->post('img_type');

		$base64img = $this->admin_model->cut_base64img($img, $cut_info);

		if ( $img_type == 'fb_img' ) {
			echo $base64img;
		} else {
			$check_dir = '../file/check/';
			$check_url = 'check'.((int)100*microtime(true)).'.jpg';
			$check_img =  $this->news->move_article_photo($base64img, $check_url, $check_dir);
			
			if ( $this->news->server_name() == '127.0.0.1' ) {
				echo ( preg_match("/http|https|data\:/", $check_img) ) ? $check_img : 'http://127.0.0.1/news/file/check/'.$check_img;
			}else{
				echo ( preg_match("/http|https|data\:/", $check_img) ) ? $check_img : 'http://file.meoments.com/check/'.$check_img;
			}
		}
	}

	function watermark(){
		$img = $this->input->post('img');
		$check_img = $this->news->watermark($img);

		if ( $this->news->server_name() == '127.0.0.1' ) {
			echo ( preg_match("/http|https|data\:/", $check_img) ) ? $check_img : 'http://127.0.0.1/news/file/check/'.$check_img;
		}else{
			echo ( preg_match("/http|https|data\:/", $check_img) ) ? $check_img : 'http://file.meoments.com/check/'.$check_img;
		}
	}

	/*
	 * 建立新文章
	 */
	function create_article() {
		$topic_array = $this->input->post('topic_array');
		$from_table = $this->input->post('from_table');
		$from_index = $this->input->post('from_index');
		$auto_index = $this->input->post('auto_index');
		$fb_id = $this->input->post('fb_id');
		$source_id = $this->input->post('source_id');

		$date = time();
		$account_id = $this->config->item('admin_accountIndex');
		
		$article_group = (isset($topic_array['article_group']) && is_array($topic_array['article_group'])) ? json_encode($topic_array['article_group']) : '';
		$article_tag = (isset($topic_array['article_tag']) && is_array($topic_array['article_tag'])) ? json_encode($topic_array['article_tag']) : '';
		$article_tag = $this->news->check_tag_exist_and_insert($topic_array['article_tag']);

		$add_input = array (
				'title' => $topic_array['article_title'],                           //標題
				'fb_post_content' => $topic_array['article_optiontitle'],           //pofans內容
				'summary' => $topic_array['summary'],           					//文章簡介
				'pofans_photo' => $topic_array['pofans_mode_imgurl'],			    //圖文方式圖片
				'fb_img' => $topic_array['fb_imgurl'],					            //fb封面圖
				'article_img' => $topic_array['article_imgurl'],                    //網站封面圖
				'content' => $this->remove_img_style($topic_array['article_text']), //文章內容   
				'category' => $article_group,                                       //分類
				'tag' => $article_tag,                                           	//標籤
				'like_name' => trim($topic_array['like_name']),
				'unlike_name' => trim($topic_array['unlike_name']),
				'update_time' => $date,
				'update_user_id' => $account_id,
				'youtube_id' => $topic_array['youtube_id'],
				'article_source' => trim($topic_array['article_source'])
		);

		$google_ad = $topic_array['google_ad'];
		$pofans_mode = $topic_array['pofans_mode'];
		$source_mode = $topic_array['source_mode'];

		$comment_good = !empty($topic_array['comment_good']) ? $topic_array['comment_good'] : array();
		$comment_bad = !empty($topic_array['comment_bad']) ? $topic_array['comment_bad'] : array();
		
		if ( $from_table == 'news_article' ) {
			$result = $this->news->check_article_exist_and_update($add_input, $auto_index, $google_ad, $comment_good, $comment_bad, $fb_id, $pofans_mode, $source_mode);
		} else {
			$add_input['create_time'] = $date;
			$add_input['create_user_id'] = $account_id;
			$result = $this->news->check_article_exist_and_insert($add_input, $fb_id, $source_id, $google_ad, $comment_good, $comment_bad, $pofans_mode, $source_mode);
		}

		//觀察圖片換位
		if( isset($result['index']) ) {
			$test_input = array(
				'news_article_id' => $result['index'],
				'content' => $add_input['content'],
				'article_img' => json_encode($topic_array['article_img']),
				'time' => date('Y-m-d H:i:s'),
				'user_id' => $account_id
			);
			$this->db->flush_cache();
			$this->db->insert('test_news_article', $test_input);
			$test_index = $this->db->insert_id();
		}

		//將圖片取代成自己的網址
		if( isset($result['index']) && $result['index'] ) {
			$article_text = $add_input['content'];
			//是否建立目錄
			$newDir = '../file/n'.$result['index'].'/';
			if( false !== $newDir && false === is_dir( $newDir ) ) {
				mkdir($newDir);
				chmod($newDir, 0777);
			}

			if ( isset($topic_array['article_img']) && is_array($topic_array['article_img']) ) {
				$cloudflare_array = array();
				foreach( $topic_array['article_img'] AS $key =>$img ) {
					$dir = '../file/';
					
					if ( $this->news->server_name() == '127.0.0.1' ) {
						$from = str_replace('http://127.0.0.1/news/file/', $dir, $img);
					}else{
						$from = str_replace('http://file.meoments.com/', $dir, $img);
					}
					$to = $newDir.$key.'.jpg';
					rename($from, $to); 
				 
					if ( $this->news->server_name() == '127.0.0.1' ) {
						$replace = 'http://127.0.0.1/news/file/n'.$result['index'].'/'.$key.'.jpg';
					} else {
						$replace = 'http://file.meoments.com/n'.$result['index'].'/'.$key.'.jpg';
					}
					$article_text = str_replace($img, $replace, $article_text);
					$cloudflare_array[] = $replace;

					/*$to = $newDir.$key.'.jpg';*/
					$this->news->adjust_content_img_size($to, $to, 600);
					//自動加浮水印 
					// $this->news->watermark_auto($to);
					$this->news->adjust_content_img_size($to, $newDir.$key.'_m'.'.jpg', 300);
				}

				$this->news->cloudflare($cloudflare_array);
			}
			
			// echo $fb_img = $this->news->move_article_photo($topic_array['fb_imgurl'], 'fb.jpg','../file/n'.$result['index'].'/');
			// echo $article_img = $this->news->move_article_photo($topic_array['article_imgurl'], 'article.jpg','../file/n'.$result['index'].'/');
			// $pofans_photo = $this->news->adjust_content_img_size($topic_array['pofans_mode_imgurl'], '../file/n' . $result['index'] . '/p.jpg', 600);
			$pofans_photo = file_put_contents('../file/n' . $result['index'] . '/p.jpg', file_get_contents($topic_array['pofans_mode_imgurl']));
			$fb_img = $this->news->adjust_img_size($topic_array['fb_imgurl'], $result['index'], 'f');
			$article_img = $this->news->adjust_img_size($topic_array['article_imgurl'], $result['index'], 't', true);
			if ( $this->news->server_name() == '127.0.0.1' ) {
				$pofans_photo = ( $pofans_photo != 'no file' ) ? 'http://127.0.0.1/news/file/n'.$result['index'].'/'.'p.jpg' : $topic_array['pofans_mode_imgurl'];
				$fb_img = ( $fb_img != 'no file' ) ? 'http://127.0.0.1/news/file/n'.$result['index'].'/'.'f.jpg' : $topic_array['fb_imgurl'];
				$article_img = ( $article_img != 'no file' ) ? 'http://127.0.0.1/news/file/n'.$result['index'].'/'.'t.jpg' : $topic_array['article_imgurl'];
			} else {
				$pofans_photo = ( $pofans_photo != 'no file' ) ? 'http://file.meoments.com/n'.$result['index'].'/'.'p.jpg' : $topic_array['pofans_mode_imgurl'];
				$fb_img = ( $fb_img != 'no file' ) ? 'http://file.meoments.com/n'.$result['index'].'/'.'f.jpg' : $topic_array['fb_imgurl'];
				$article_img = ( $article_img != 'no file' ) ? 'http://file.meoments.com/n'.$result['index'].'/'.'t.jpg' : $topic_array['article_imgurl'];
			}

			$cloudflare_cover_array = array($fb_img, $article_img);
			$this->news->cloudflare($cloudflare_cover_array);

			$news = array ( 'content'     => $article_text,
							'pofans_photo'=> $pofans_photo,
							'fb_img'      => $fb_img,
							'article_img' => $article_img
			);

			$this->db->flush_cache();
			$this->db->where('id', $result['index']);
			$this->db->update('news_article', $news);

			$this->db->flush_cache();
			$this->db->where('id', $test_index);
			$this->db->update('test_news_article', array('replace_content' => $article_text));
		}

		$this->db->flush_cache();
		$this->db->where('id', $result['index']);
		$this->db->update('news_article_state', array('action' => 0));

		$this->load->driver('cache');
		$this->cache->memcached->delArticleMemcached($result['index']);
	}

	/*
	 * 編輯文章的關鍵字查詢
	 */
	function search_keyword() {
		$keyword = $this->input->post('keyword');
		
		// echo $orderby = $this->input->post('orderby');
		// echo $order = $this->input->post('order');
		// $source_id = $this->input->post('source_id');
		$data = array();
		$account_id = $this->config->item('admin_accountIndex');
		$data['reference_news'] = $this->news->get_reference_article_array($keyword, $account_id);

		// $sitename_array = $this->news->get_sitename_array();
		// $data['reference_news'] = $this->news->get_reference_news_array($keyword, $orderby, $order, $source_id);
		
		// foreach ($data['reference_news'] as $key => $value) {
		// 	$data['reference_news'][$key]['site_id'] = $sitename_array[$value['site_id']];
		// 	$data['reference_news'][$key]['publish_time'] = date('Y-m-d', $value['publish_time']);
		// 	$data['reference_news'][$key]['lock'] = ($data['reference_news'][$key]['lock'] == 1 ) ? 'lock' : '';
		// }
		
		echo $this->parser->parse('article/reference_news', $data, true);
	}

	/*
	 * 上下架文章
	 */
	function news_article_online() {
		$index = $this->input->post('index');
		$online = $this->input->post('online');
		$account_id = $this->config->item('admin_accountIndex');
		$group = $this->admin_model->get_groupIndex($account_id);
		$result['success'] = 'N';
		$result['msg'] = '';

		if ( $group == 3 ) {
			$this->db->flush_cache();
			$query = $this->db->select("create_user_id")->from("news_article")->where("id", $index)->get();
			$temp = $query->row_array();
			if ( $temp['create_user_id'] != $account_id ) {
				// echo '權限不足';
				$result['msg'] = '權限不足';
				
				echo json_encode($result);
				return;
			}
		}
		
		if ( empty($index) || !is_numeric($index) ) return;

		//設定為上架狀態
		if ( $online == 'true' ) {
			$result = $this->news->onshelf_article_by_index($index);
			if ( $result['success'] == 'N' ) {
				$result['msg'] = '[ 編號：'.$index.' ] '.$result['msg'];
				
				echo json_encode($result);
				return;
			}

			if ( $this->news->server_name() != '127.0.0.1' ) {
				$this->news->pofans2($index);
				$this->cache->memcached->delArticleMemcached($index);
			}

			$this->news->fbcache('http://meoments.com/' . $index . '/');

			echo json_encode($result);
			return;
		} else {
			//設定為下架狀態
			$this->db->flush_cache();
			$topic = array('online' => '0');
			$this->db->where('id', $index);
			$this->db->update('news_article_state', $topic);

			$this->db->flush_cache();
			$this->db->where('id', $index);
			$this->db->delete('news_article_onshelf');
			$result['success'] = 'Y';
			$result['msg'] = '下架成功';
			
			echo json_encode($result);
			return;
		}
	}

	function adonis() {
		// $dir = '../file/check';
	 //    if (!defined('ROOT_PATH')) {
	 //        define('ROOT_PATH', dirname(dirname(__FILE__)) );
	 //    }
	    
	 //    // echo $rp = realpath('C:\xampp\htdocs\news\admin/'.$dir);
	 //    // echo dirname($rp);
	 //    echo $_SERVER["SCRIPT_NAME"];

		$data = $this->data;

		$data['main_content'] = $this->parser->parse('article/test', $data, true);
		$this->parser->parse ('template', $data);
	}

	//鎖住來源文章
	public function lock_article() {
		$id = $this->input->post('id');
		$status = $this->input->post('status');
		
		$this->db->flush_cache();
		$topic = array('lock' => $status);
		$this->db->where('id', $id);
		$this->db->update('source_news_content', $topic);
	}

	//鎖住FB文章
	public function lock_fb_article() {
		$id = $this->input->post('id');
		$action = $this->input->post('action');

		if ( $action == 'lock' ) {
			$account_id = $this->config->item('admin_accountIndex');
		
			$this->db->flush_cache();
			$topic = array('lock_user_id' => $account_id);
			$this->db->where('id', $id);
			$this->db->update('facebook_post_content', $topic);
		} else {
			$this->db->flush_cache();
			$topic = array('lock_user_id' => 0);
			$this->db->where('id', $id);
			$this->db->update('facebook_post_content', $topic);
		}
		
	}

	//去除img中的style
	public function remove_img_style($article) {
		preg_match_all('/(?P<img><img.*(?P<style>style=".*?").*>)/', $article, $matches);
		if ( isset($matches['img']) ) {
			foreach ($matches['img'] as $key => $value) {
				$replace = str_replace($matches['style'][$key], '', $value);

				$article = str_replace($value, $replace, $article);
			}
		}

		return $article;
	}

	public function update_comment() {
		$fid = $this->input->post('id');
		// $sid = $this->input->post('sid');
		$like_name = $this->input->post('like_name');
		$unlike_name = $this->input->post('unlike_name');

		$this->db->flush_cache();
		$this->db->select('id, fb_id, source_id, url');
		$this->db->from('facebook_comment_schedule');
		$this->db->where('fb_id', $fid);
		// $this->db->where("(source_id='0' OR source_id='$sid')", NULL, FALSE);
		$query = $this->db->get();
		$schedule = $query->result_array();

		require_once('../get_fb_article/facebook-php-sdk-master/src/facebook.php');
		$facebook = new Facebook(array( 
		    'appId'  => '792264297536375', 
		    'secret' => 'c6c8fe7316e88b4b16e7d987664485e8', 
		    'cookie' => true, // enable optional cookie support 
		));

		foreach ($schedule as $key => $value) {
			$id = $value['id'];
	    	$fb_id = $value['fb_id'];
	    	$source_id = $value['source_id'];
	    	$article_id = $value['url'];

	    	if( $source_id != 0 && preg_match('/http/', $article_id) ) {
	    		$article_id = $this->news->get_news_fbid($article_id, $facebook);
	    		if ( empty($article_id) ) continue;
				$this->db->flush_cache();
				$this->db->where('id', $id);
				$this->db->update('facebook_comment_schedule', array('url' => $article_id));
	    	}
		
		    $this->news->get_fb_comments($fb_id, $source_id, $article_id, $facebook);
    	}

    	$fb_comment = $this->news->get_comments_list($fid, 0);
		$article_comment = $this->news->get_comments_list($fid, 1);
		
		$data['fb_comment'] = $fb_comment;
		$data['article_comment'] = $article_comment;
		$data['like_name'] = $like_name;
		$data['unlike_name'] = $unlike_name;

    	echo $this->parser->parse('article/news_comments', $data, true);
	}

	public function update_action() {
		$index = $this->input->post('index');
		$table = $this->input->post('table');
		$this->db->flush_cache();
		$this->db->where('id', $index);
		$this->db->update($table, array('action_time' => 0));
	}

	function update_article_comment() {
		$topic_array = $this->input->post('topic_array');
		$index = $this->input->post('auto_index');
		$fb_id = $this->input->post('fb_id');

		$this->news->clear_comments_onshelf($index, $fb_id);

		if ( !empty($topic_array['comment_good']) ) {
			$this->news->insert_facebook_comments_onshelf($topic_array['comment_good'], 2, $index);
		}
		if ( !empty($topic_array['comment_bad']) ) {
			$this->news->insert_facebook_comments_onshelf($topic_array['comment_bad'], 1, $index);
		}

		$this->news->caculate_comments($index);

		$this->db->flush_cache();
		$this->db->where('id', $index);
		$this->db->update('news_article', array('like_name' => $topic_array['like_name'], 'unlike_name' => $topic_array['unlike_name']));

		$this->db->flush_cache();
		$this->db->where('id', $index);
		$this->db->update('news_article_onshelf', array('like_name' => $topic_array['like_name'], 'unlike_name' => $topic_array['unlike_name']));

		$this->cache->memcached->delArticleMemcached($index);
	}

	function change_google() {
		$index = $this->input->post('index');
		$status = $this->input->post('status');

		$this->db->flush_cache();
		$this->db->where('id', $index);
		$this->db->update('news_article_state', array('google_ad' => $status));
	}

	function change_article_id() {
		$index = $this->input->post('index');

		$account_id = $this->config->item('admin_accountIndex');
		$group = $this->admin_model->get_groupIndex($account_id);

		$result['success'] = 'N';
		$result['msg'] = '';

		if ( $group != 1 ) {
			$result['msg'] = '權限不足';
			echo json_encode($result);

			return;
		}

		$this->news->change_article_id($index);
	}

}
