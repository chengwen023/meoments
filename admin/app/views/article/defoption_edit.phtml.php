<link href="css/iframeEDT.css" rel="stylesheet" type="text/css">
<script src="js/tool.js"></script>
<script src="js/span_editor.js"></script>
<style>

#img_list IMG[data-check="check_ok"] {
	background: url(images/check_green.png) right bottom no-repeat;
	padding-right: 30px;
}

#img_selectbox {
	display: none;
	width: 550px;
	height: 420px;
	text-align: center;
	background-color: #94b0dc;
}

.fb_view_control {
	background-color: #336699;
	color: #fff;
	height: 20px;
}

.fb_view_control SPAN {
	margin-right: 10px;
	padding: 2px 5px;
}

.fb_view_control SPAN:HOVER {
	cursor: pointer;
	color: #3366cc;
	background-color: #fff;
}

.fb_view {
	width: 500px;
	height: 375px;
	margin: auto;
	border: 1px solid red;
}

.fb_link {
	width: 500px;
	height: 260px;
}

.fb_link_s {
	width: 375px;
	height: 375px;
}

.topic_photo {
	border: 1px solid;
	box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3);
	padding: 7px 5px 7px 7px;
	margin-right: 15px;
	margin-bottom:15px;
	float: left;
	margin-top: 25px;
	position: relative;
}
.topic_photo:before{
	content: attr(data-title);
	position: absolute;
	top: -20px;
}
.topic_group {
	width: 340px;
	float: left;
	margin: 10px;
}

.topic_group STRONG {
	color: #000;
	float: right;
	cursor: pointer;
	border-bottom: 1px solid #999;
}

.topic_group UL {
	margin: 0;
	display: none;
}

.topic_group.editable UL.options_select {
	position: absolute;
	display: inline-block;
	margin-top: 30px;
	background-color: #fff;
	border: 1px solid #D6D6D6;
	border-radius: 10px;
	padding: 5px 5px;
	box-shadow: 3px 3px 4px rgba(0, 0, 0, 0.2);
	z-index: 10;
}

.topic_group UL LI {
	float: left;
	margin-right: 10px;
	cursor: pointer;
}

.topic_group UL LI:AFTER {
	content: ' ';
	padding: 10px 0px 0px 30px;
}

.topic_group UL LI:HOVER {
	color: #585858;
}

.topic_group UL LI:HOVER:AFTER {
	background: url(images/check_green.png) no-repeat left;
}

.topic_group #topic_group_name UL {
	display: block;
	float: left;
}

.topic_group #topic_group_name UL LI.checked:AFTER {
	background: url(images/check_green.png) no-repeat left;
}

.audit_left.not_pass:BEFORE {
	content: ' ';
	padding: 10px 0px 10px 30px;
	background: url(images/not_pass.gif) no-repeat left;
	position: absolute;
	margin-left: -30px;
}

.audit_right.not_pass:AFTER {
	content: ' ';
	padding: 10px 0px 10px 30px;
	background: url(images/not_pass.gif) no-repeat left;
}
.topic_photo.not_pass:BEFORE {
	content: attr(data-title);
	padding: 0px 0px 00px 30px;
	background: url(images/not_pass.gif) no-repeat left;
}
.topic_photo.not_pass:AFTER{
	display: none;;
}
.data_title:BEFORE {
	content: attr(data-title);
	position: absolute;
	color: #000;
	margin-left: -40px;
	font-weight: bolder;
}

input[type="text"] {
	width: 100%;
	font-size: 18px;
	height: 24px;
	margin-bottom: 5px;
}

input[type="text"]:HOVER {
	background-color: rgba(33, 153, 233, 0.3);
}

input[type="text"].empty {
	border-color: rgba(241, 193, 154, 1);
}
IMG.errorsize{
	/*border-color: rgba(230, 128, 45, 1); */
	border:3px solid red; 
	 

}

.topic_photo.audit_right.dropping:AFTER {
	content: ' ';
	background-color: rgba(33, 33, 33, 0.3);
	width: 114px;
	position: absolute;
	height: 91px;
	border: 1px solid red;
	margin-top: -83px;
	margin-left: -9px;
}

.topic_photo.audit_left.dropping:AFTER {
	content: ' ';
	background-color: rgba(33, 33, 33, 0.3);
	width: 114px;
	position: absolute;
	height: 91px;
	border: 1px solid red;
	margin-top: -83px;
	margin-left: -9px;
}

.topic_option_list {
	counter-reset: option_div;
}

.topic_option_list .page-content.dropimg-content:BEFORE {
	counter-increment: option_div;
	content: counter(option_div);
	font-size: 135px;
	position: absolute;
	margin-top: 60px;
	color: #fff;
	text-shadow: 1px 2px 4px rgba(199, 199, 199, 1);
	font-family: sans-serif;
}

.dropimg-content.dropping:AFTER {
	content: ' ';
	background-color: rgba(33, 33, 33, 0.3);
	width: 100%;
	position: absolute;
	height: 180px;
	border: 1px solid red;
}

#img_control {
	background: url(images/arrow_right.png) no-repeat center;
	padding: 40px 100px;
	width: 735px;
	/* height: 200px; */
	position: absolute;
	z-index: 1000;
	background-color: rgba(33, 33, 33, 0.3);
}

.topic_photo #img_control {
	margin-left: -90px;
	left: 0;
}
.audit_right #img_control {	
	left: -483px;
}
#img_control #old_img {
	float: left;
	padding: 8px;
	background-color: #f6f6f6;
	border: 1px dashed #b2b2b2;
	-webkit-box-shadow: 3px 3px 4px rgba(0, 0, 0, 0.2);
	-moz-box-shadow: 3px 3px 4px rgba(0, 0, 0, 0.2);
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
}

#img_control #new_img {
	float: right;
	padding: 8px;
	background-color: #f6f6f6;
	border: 1px dashed #b2b2b2;
	-webkit-box-shadow: 3px 3px 4px rgba(0, 0, 0, 0.2);
	-moz-box-shadow: 3px 3px 4px rgba(0, 0, 0, 0.2);
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
}

#img_control DIV#control_btn {
	top: 0;
	right: 0;
	position: absolute;
	color: #fff;
	font-size: 24px;
	overflow: hidden;
	width: 50px;
	height: 100%;
}

SPAN.text_editable{
	padding-right: 20px;
}
SPAN.text_editable:HOVER,SPAN.text_editable:FOCUS{
	border-bottom: 1px solid rgb(102,153,204);
}

.ie-icon:BEFORE {
	width: 32px;
	height: 32px;
}

.ie-icon:HOVER:BEFORE {
	margin-left: -5px;
	padding: 5px;
	border-radius: 5px;
	background: rgba(0, 0, 0, 0.5);
}


#upload_file::-webkit-file-upload-button {
  visibility: hidden;
}
#upload_file::before {
	content: ' ';
	display: inline-block;
	background: url("images/uploadfile.png") no-repeat left center;
	padding: 30px;
	cursor:pointer;
    position: absolute;
}

.topic_insert{
	font-size: 28px;
	cursor: pointer;
	color:rgb(35, 35, 35);
}
.topic_insert:HOVER{	
	color:rgb(255, 113, 9);
}
</style>

<input type="hidden" name="auto_index" value="<?php echo $content['auto_index'];?>">
<div class="plugin_box" style="position: fixed; right: 40px;z-index: 100;">
	<div id="img_selectbox">
		<div class="fb_view_control">
			<span data-fb="fb_view">原始圖片</span> <span data-fb="fb_link">連結圖片(大)</span>
			<span data-fb="fb_link_s">連結圖片(小)</span> <span data-fb="close"
				style="float: right;">關閉</span>
		</div>
		<div style="margin: auto; height: 25px;">
			<span style="float: left;"><input id="upload_file" type="file" onchange="upload_img();"/></span>
		</div>
		<div class="fb_view"></div>
	</div>
</div>


<div class="fancybox-content" >
	<div class="fancybox-content-bg-bottom">
		<div class="fancybox-header">
			<ul>		
			<li class="<?php echo $content['active'];?>">						
				<span class="icon-publish topic_insert" onclick="topic_insert('<?php echo $content['auto_index'];?>');" title="新增" alt="新增"></span>				
			</ul>
			
			<br class="clear">
			<div class="fancybox-logo" style="color: #919191;">
				
				<div style="float: left; width: 350px; margin-right: 10px;">
					<h2>
						<span contenteditable="true" class="text_editable audit_right " data-name="defoptions_group_name" data-title="預設選項名稱"><?php echo $content['defoptions_group_name'];?></span>
					</h2>					
				</div>
				<br class="clear">
			</div>
		</div>

		<div class="fancybox-inside">
			
			<div class="fancybox-inside-bg-top">
				<div class="fancybox-inside-bg-bottom">
					<div class="resume slide">

						<div class="topic_option_list" style="min-height: 550px;">
							<?php if(isset($content['article_options'])):?>
							<?php foreach ($content['article_options'] AS $list):?>
							<div  class="page-content dropimg-content" >								
									<div class="entry audit_left "style="font-size: 14px;" >										
										<span class="icon-cancel topic_option_remove" style="float: right;cursor: pointer;" alt="刪除選項" title="刪除選項"></span>
										<span class="icon-plus_alt add_topic_option" style="float: right;cursor: pointer;" alt="複製選項" title="複製選項"></span>
										<strong style="color: #000; display: block;"> 
											<span contenteditable="true" class="text_editable data_title" data-title="選項" data-type="title" value="<?php echo $list['choose'];?>" data-name="choose"><?php echo $list['choose'];?></span>
										</strong> 
										<strong style="color: #000; display: block;"> 
											<span contenteditable="true" class="text_editable data_title" data-title="標題" data-type="title" value="<?php echo $list['title'];?>" data-name="title"><?php echo $list['title'];?></span>
										</strong> 
											
									</div>
									<h2>
										<div class="gallery-item audit_left ">
											<img class="editable" data-name="pic_url" src="<?php echo $list['pic_url'];?>" alt="" width="75px" />
										</div>
									</h2>
								</div>
							<?php endforeach;?>
							<?php endif;?>
						</div>
						
					</div>
					
				</div>
			</div>
		</div>
	</div>
	<br class="clear">
</div>


<script>


$(function() {
	set_span_editor();
	
	
	$("SPAN.text_editable").attr('contenteditable',true).each(function(){
			if(!$(this).html() || $(this).html() == '') $(this).html( $(this).attr('data-title')||'請輸入資料' );

		}).live("focus",function(){
			if( $(this).html() == ($(this).attr('data-title')||'請輸入資料') ) $(this).html('');
			
		}).live("blur",function(){
			if(!$(this).html() || $(this).html() == '') $(this).html($(this).attr('data-title')||'請輸入資料');
		}).live("keyup",function(e){
			
	});

	
	
});


function del_options(el){
	var temp = $(el).parents('DIV.page-content.dropimg-content');
	if($('DIV.page-content.dropimg-content').length == 1) {
		alert('你把選項都刪光了! 笨蛋!');		
	    $(temp).hide().show(1000);
	    return false;		
	}
	
	if($(temp)) $(temp).remove();
	
}
function add_options(el){
	var temp = $(el).parents('DIV.page-content.dropimg-content');
	var temp_clone = $(temp).clone();
	$(temp).after(temp_clone);

	$(temp_clone).each(function(){
		this.addEventListener("dragenter", dragEnter, false);
		this.addEventListener("dragexit", dragExit, false);
		this.addEventListener("dragover", dragOver, false);
		this.addEventListener("dragleave", dragLeave, false);
		this.addEventListener("drop", drop, false);	
	});
	
}


$('.add_topic_option').live('click',function(){
	add_options(this);
})

$('.topic_option_remove').live('click',function(){
	del_options(this);
});

function topic_insert(auto_index){
	CKEDITOR_destroy();

	
	var defoption = get_post_val();

	var from_table = $('#from_index').attr('data-table');
	var from_index = $('#from_index').val();
	
	if(defoption == false || defoption['options'].length < 1) {

		alert('輸入的資料有誤');
		
		return false;
	}
	
	
	
	if(confirm("是否確定新增資料? ")){
	
		if(defoption && defoption.error_msg == ''){
			url = 'article/actions/validate_defoptions'; 
			
			$.fancybox.showLoading();
			$.ajax({
				url: url,
			  cache: false,
				dataType: 'html',
			  type: 'POST',
			  data:{
				  defoption:defoption,
				  auto_index:auto_index
			  },
			  success: function(data){
				  $.fancybox.hideLoading();
				 if(data) console.info(data);
				 else {
			  		location.reload();
				 }
			  }
			});
		}else{
			alert(defoption.error_msg);
		}
	}
	
	
}


function get_post_val(){
	
	var post_id = new Array('defoptions_group_name');
	
	var defoption = {			 
			  "defoptions_group_name": '',
			  "error_msg":''	 
			}
	for( var key in post_id){		

		var temp = $('[data-name="'+post_id[key]+'"').attr('data-title');
		
		if(typeof(temp) != 'undefined' && $('[data-name="'+post_id[key]+'"').html() == temp) {
			console.info(post_id[key]);	
				return false;
		}
		
		if($('[data-name="'+post_id[key]+'"').get(0).tagName.toLowerCase() == "span" ) defoption[post_id[key]] = $('[data-name="'+post_id[key]+'"').html();
		if($('[data-name="'+post_id[key]+'"').get(0).tagName.toLowerCase() == "textarea" ) defoption[post_id[key]] = $('[data-name="'+post_id[key]+'"').html();

	}
	

	defoption['options'] = new Array();
	$(".topic_option_list .page-content.dropimg-content").each(function(){
			var option = {"choose": '',"title": '',"pic_url": ''	};


			for( var key in option){	
				var temp = $(this).find('SPAN[data-name="'+key+'"]').attr('data-title');
				
				if(typeof(temp) != 'undefined' && $(this).find('SPAN[data-name="'+key+'"]').html() == temp) return false;
			}
			
			option['choose'] = $(this).find('SPAN[data-name="choose"]').html();
			option['title']  = $(this).find('SPAN[data-name="title"]').html();


			if(!$(this).find('IMG.editable').hasClass("errorsize")){				
				option['pic_url'] = $(this).find('IMG.editable').attr("src");
			}
			else{
				option['pic_url'] = '';
			}
			console.info(option);
			defoption['options'].push(option);
		});


	
	return defoption;
	
}


/**/

//FB圖片預覽
$("DIV.fb_view_control SPAN").on('click', function() {
	if( $(this).attr('data-fb') == "close"){
		$('#img_selectbox').hide();	
		
	}else{		
		$("DIV.fb_view").attr('class','fb_view');	
		$("DIV.fb_view").addClass( $(this).attr('data-fb'));
	}
});
$("IMG.editable").live('click', function() {
	$("IMG.editable").removeClass("focus");
	$(this).addClass("focus");
	
	$('#img_selectbox').show(500);
	 
	$('#img_selectbox .fb_view').attr('class','fb_view').css('background', 'url("'+$(this).attr("src")+'") center center no-repeat');
});


//檔案拖拉

//檔案拖拉

$("DIV.dropimg-content, SPAN.topic_photo").each(function(){
	this.addEventListener("dragenter", dragEnter, false);
	this.addEventListener("dragexit", dragExit, false);
	this.addEventListener("dragover", dragOver, false);
	this.addEventListener("dragleave", dragLeave, false);
	this.addEventListener("drop", drop, false);	
});

function dragEnter(evt) {
	$('#img_control').remove(); 
	$(".dropping").removeClass("dropping");
	evt.stopPropagation();
	evt.preventDefault();
	$(this).addClass("dropping");
}

function dragExit(evt) {
	evt.stopPropagation();
	evt.preventDefault();
}

function dragLeave(evt) {
	evt.stopPropagation();
	evt.preventDefault();	
	if( $(this).hasClass("dropping")) $(this).removeClass("dropping");
}
function dragOver(evt) {
	evt.stopPropagation();
	evt.preventDefault();
	if(!$(this).hasClass("dropping")) $(this).addClass("dropping");	
}

function drop(evt) {

	if(( typeof( $(this).attr('data-src')) == 'undefined')) 
		$(this).attr('data-src',$(this).attr('src'));

	
	if(( typeof( $(this).attr('data-width')) == 'undefined')) 
		$(this).attr('data-width',this.naturalWidth);
	if( typeof( $(this).attr('data-height')) == 'undefined') 
		$(this).attr('data-height',this.naturalHeight);
	

	
	$(this).removeClass("dropping");
	evt.stopPropagation();
	evt.preventDefault();

	imgreplacebox(this);
	
	var dat = $(evt.dataTransfer.getData('text/html'));
    var img = dat.attr('src');
    if (!img) img = dat.find("img").attr('src');
    
	var files = evt.dataTransfer.files;
	var count = files.length;

	
	// Only call the handler if 1 or more files was dropped.
	if (count > 0 && handleFiles(files)){
		$(this).addClass('Upload_img');		
	}
	else if(img){		
		$("#new_img").attr('src',img);		
	}else{
		init_img_controllor(true);
	}
}

//檔案讀取

function handleFiles(files) {
	
	var file = files[0];
	var check_file  = null;
	if(typeof(file) != "undefined")
		check_file = file.type.match('image');

	if(check_file == null) {
		alert('上傳檔案需為圖檔');			
		return false;
	}
	var reader = new FileReader();

	reader.onloadend = handleReaderLoadEnd;

	reader.readAsDataURL(file);
	return true;
}

function handleReaderLoadEnd(evt) {
	$("#new_img").attr('src',evt.target.result);

}




function init_img_controllor(inputfile){
	//初始化
	$("#img_control").remove();
	$(".Upload_img").removeClass("Upload_img");
	if(inputfile) $("#upload_file").val('');
	console.info('init');
}
function save_img(){

	$("img_control").find("IMG.editable").attr("src");
	
	var old_img = $("#img_control #old_img").attr("src");
	var new_img = $("#img_control #new_img").attr("src");

	$("#img_control").parent().find("IMG.editable").attr("src",new_img);
	
	$("#img_control").remove();
	$('#img_selectbox').hide();	
}

function upload_img(){
	
	var div_el = $('IMG.editable.focus').parents('DIV.page-content.dropimg-content');
	var span_el = $('IMG.editable.focus').parents('SPAN.topic_photo');

	imgreplacebox((span_el.length)?span_el:div_el);


	var fileInput = document.getElementById('upload_file');
	 
	if( handleFiles(fileInput.files) ){
		if(span_el.length) span_el.addClass('Upload_img');
		else div_el.addClass('Upload_img');

	}else {
		init_img_controllor(true);
		return false;

	}
	/**/
	
}



function imgreplacebox(el){

	init_img_controllor(false);
	
	var old_img = $(el).find("IMG.editable").attr("src");

	var img_control = document.createElement('div');
	$(img_control).attr("id","img_control");
	
	$(el).prepend(img_control);
	
	
	$("#img_control").prepend('<img id="new_img"  width="200px"  />')
	$("#img_control").prepend('<img id="old_img"  src ="'+old_img+'" width="200px"  />')
	
	
	
	
	var control_btn = document.createElement('div');
	$(control_btn).attr("id","control_btn");
	$("#img_control").append(control_btn);

	
	
	var control_bar = new Tool(canvasToolConfig);
	
	$("#new_img").load(function() {

		$('#img_selectbox').hide();	
		if( typeof( $(this).attr("data-src") ) == "undefined" || 
			typeof( $(this).attr("data-width") ) == "undefined" ||
			typeof( $(this).attr("data-height") ) == "undefined")
		{				
			return false;
		}
			
		
		var width = this.naturalWidth;
		var height = this.naturalHeight;

		
		if(width == $(this).attr("data-width") && height == $(this).attr("data-height")) 
		{
			if( false == confirm("是否變更圖片") ){
				reduction_img(this);
			}else if(false) {
				save_img(this);
			}
		}	
		else {
			alert('圖片規格不符');
			reduction_img(this);		
		}	
		
	})
	
	$("#new_img,#old_img").on("click",function(){		

		
		$('#img_selectbox').show(500);
		$('#img_selectbox .fb_view').attr('class','fb_view').css('background', 'url("'+$(this).attr("src")+'") center center no-repeat');

		});
	$("#new_img").error(function(){
		if( typeof($(this).attr("data-src")) != 'undefined' ){
			alert('圖片讀取有誤!');
			reduction_img(this);	
		}
		
	});
}
</script>