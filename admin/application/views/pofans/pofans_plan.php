
<script src="js/tips.js"></script>
<style>

/*
* Tips CSS
*/
.tips_box{	
	font-size: 16px;	
	padding: 5px 10px;
	background: #fff;
	box-shadow: 1px 1px 2px 1px rgba(33,66,99,.5);
	border-radius:5px;
	min-width: 120px;
}


.arrow_box {	
	background: #ffffff;
	border: 4px solid #eb9c8d;
}
.arrow_box:after, .arrow_box:before {	
	left: 50%;
	border: solid transparent;
	content: " ";
	height: 0;
	width: 0;
	position: absolute;
	pointer-events: none;
}

.arrow_box:after {
	border-color: rgba(255, 255, 255, 0);	
	border-width: 10px;
	margin-left: -10px;
}
.arrow_box:before {
	border-color: rgba(194, 225, 245, 0);	
	border-width: 16px;
	margin-left: -16px;
}

.arrow_box.arrow_right:after, .arrow_box.arrow_right:before{
	left: 95%;
}
.arrow_box.arrow_left:after, .arrow_box.arrow_left:before{
	left: 5%;
}
.arrow_box.top:before {
	border-top-color: #eb9c8d;
}
.arrow_box.top:after{
	border-top-color: #ffffff;
}
.arrow_box.top:after, .arrow_box.top:before {
	top: 100%;	
}
.arrow_box.bottom:after{
	border-bottom-color: #ffffff;
}
.arrow_box.bottom:before {
	border-bottom-color: #eb9c8d;
}
.arrow_box.bottom:after, .arrow_box.bottom:before {
	bottom: 100%;	
}

/*
* Tips CSS END
*/

.dropping{
	background-color:rgba(0,0,0,.3) 
}
.user_row TD:BEFORE {
	display: block;
	content: attr(data-time);
	
}
.plan_item{
	width: 80px;
	padding: 10px;
	margin-right:10px;
	border: 1px solid;
	float: left;
	color:#111;
	font-size: 14px;
}
.plan_item.article{
	color:rgba(99,22,66,1);
	border-color:rgba(99,22,66,0.8);
	background:  rgba(99,22,66,0.3);
}
.plan_item.topic{
	color:rgba(22, 40, 99, 1);
	border-color:rgba(22, 40, 99, 0.8);
	background: rgba(22, 40, 99, 0.3);
}
.plan_item.question{
	color:rgba(99, 62, 22, 1);
	border-color:rgba(99, 62, 22, 0.8);
	background: rgba(99, 62, 22, 0.3);
}
.plan_item:BEFORE{
	display:block;
	border-radius: 3px;
	text-align: center;
	font-size: 12px;
}


.plan_item.message:BEFORE{	
	content: '純文字';
	background: #fff;

}
.plan_item.link:BEFORE{	
	content: '連結';
	background: #337E99;
	color: #eee;
	

}
.plan_item.photo:BEFORE{	
	content: '圖片';
	background: rgb(43, 145, 106);
	color: #fff;
}

.plan_item SPAN.del_btn{
	display: none;	
	position: absolute;
	right: 0;
	top: 0;
}
.plan_item[data-plan="plan"] SPAN.del_btn{
	display: block;
}
</style>
<div class="widget">

	<div id="plan_list" style="margin: 20px;">
	{data_table}
		{post_type}
	<div  id="{table_name}_{value}" class="plan_item {table_name} {value}" draggable="true" ondragstart="drag(event)" style="position: relative;">
		<span class="del_btn icon-close" onclick="remove_plan(this);"></span>
		{table_label}
	</div>
		{/post_type}
	{/data_table}
	<div class="clear"></div>
	</div>

	<table cellpadding="0" cellspacing="0" class="tDefault tLight">		
		<tbody>
			
			{time_part}
			<tr class="user_row" >
				{time_list}
				<td tips="{plan_time}" data-time="{plan_time}">
				{plan_item}
					<plan_item replace="{plan_name}"></plan_item>
				{/plan_item}
				</td>				
				{/time_list}
			</tr>
			{/time_part}
			
		</tbody>
	</table>
	
</div>
<script type="text/javascript">

//拖拉
$("TD").each(function(){
	this.addEventListener("dragenter", dragEnter, false);
	this.addEventListener("dragexit", dragExit, false);
	this.addEventListener("dragover", dragOver, false);
	this.addEventListener("dragleave", dragLeave, false);
	this.addEventListener("drop", drop, false);
});

//刪除排程
function remove_plan(el){
	var td_el = $(el).parents('td');	
	$(el).parent('DIV').remove();
	update_plan(td_el);
}

function dragEnter(evt) {	
	$(".dropping").removeClass("dropping");
	evt.stopPropagation();
	evt.preventDefault();
	$(this).addClass("dropping");
}
function dragExit(evt) {
	evt.stopPropagation();
	evt.preventDefault();
}
function dragLeave(evt) {
	evt.stopPropagation();
	evt.preventDefault();	
	if( $(this).hasClass("dropping")) $(this).removeClass("dropping");
}
function dragOver(evt) {
	evt.stopPropagation();
	evt.preventDefault();
	if(!$(this).hasClass("dropping")) $(this).addClass("dropping");
}
//drap_el:被拖拉的物件; drap_td:目標表格
var drap_el,drap_td;

//將表格中的plan_item換成圖文格
$(function() {
	var config = {			
			filed_name : ['data-title','tips'],	
			class_Name : 'tips_box',
			tipbox_name: "tip_box",		
	}

	tips = new Tips(config);
	console.info(tips);

	$('plan_item').each(function(){

			var p_td = $(this).parent('td');
			$('#plan_list #'+$(this).attr('replace')).clone().attr('data-plan','plan').appendTo(p_td);
			$(this).remove();
			
		});
});


function drag(evt) {
	$('#OnceTips').remove();
	oncetips = tips.onceTips($(evt.target),'你正在移動此排程',false,'tips_box arrow_box' );
	evt.dataTransfer.setData("text/html", evt.target.id);
	console.info(evt.target);
	drap_el = evt.target;
	drap_td = ($(evt.target).get(0).tagName.toLowerCase() == 'td')?$(evt.target):$(evt.target).parent('td');
}

function drop(evt) {
	evt.preventDefault();
	$('#OnceTips').remove();
    $(".dropping").removeClass("dropping");
    
    //drap_el(evt)的id
    var data = evt.dataTransfer.getData("text/html");

   	//td物件
    var td_el = ($(evt.target).get(0).tagName.toLowerCase() == 'td')?$(evt.target):$(evt.target).parent('td');

    if($(td_el).find('#'+data).length) {
        tips.onceTips($(td_el),'該排程已存在',1500,'tips_box arrow_box' );
        return false;
    }
    else if( $(drap_el).is('[data-plan="plan"]') )  $(drap_el).appendTo(td_el);    
    else $(document.getElementById(data)).clone().attr('data-plan','plan').appendTo(td_el);
	
    update_plan(td_el);
    update_plan(drap_td);
    drap_td = '';
}

//更新排程
function update_plan(el){
	if(!el) return false;
	var time = $(el).attr('data-time');
	var plan_list = new Array();		
	
	$(el).find('div').each(function(){
		plan_list.push($(this).attr('id'));
	})

	url = 'pofans/update_plan';
	 
	$.fancybox.showLoading();
	$.ajax({
		url: url,
	  cache: false,
		dataType: 'html',
	  type: 'POST',
	  data:{
		  plan_time:time,
		  plan_list:plan_list
	  },
	  success: function(data){
		  $.fancybox.hideLoading();
		  if(data) console.info(data);
	  }
	});
	
}
</script>