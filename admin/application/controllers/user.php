<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	
	
	public function __construct(){
		parent::__construct();
		$this->load->helper('cookie');
		$this->userInfo = $this->session->userdata('userInfo');
		
		
	}
	/*
	* index
	*/
	public function index(){
		
		$this->login();
	}
	
	function login($ac = 'form'){
		
		if(!empty($this->userInfo['auto_index'])){
			redirect(base_url('account'), 'refresh');
		}else{
			if($ac == 'form'){
				$data['username'] = '';
				if(isset($_COOKIE['username'])) $data['username'] = $this->input->cookie('username');
				else $data['flipped'] = 'flipped';
				$this->parser->parse('login', $data);
			}elseif($ac == 'validate'){
		
				$username = $this->input->post("username");
		        $password = $this->input->post("password");
		        
		        $this->db->flush_cache();
		        $this->db->select('*')
		               	 ->from('admin_account')
		                 ->where('id', $username);
		        $query = $this->db->get();
		        $result_arr = $query->row_array();
		        
		        if(count($result_arr) > 0) {
		        	setcookie('username','',time()-3600*24);
		        	setcookie('username',$username,time()+3600*24);
			        if($result_arr['pw'] == MD5($password)) {
			        	$this->session->set_userdata('userInfo', $result_arr);
			        	
			        	
		            	redirect(base_url('news'), 'refresh');
			        }else{
			        	redirect(base_url('user/login?msg='.urlencode('帳號密碼錯誤!!')), 'refresh');
			        } 
		        }else{
		        	redirect(base_url('user/login?msg='.urlencode('帳號密碼錯誤!!')), 'refresh');
		        }
			}
		}
	}
	/**
	 *	logout
	 */
	function logout(){
		
		$userInfo = array();
		$this->session->set_userdata('userInfo',$userInfo);
		redirect(base_url('user'), 'refresh');
	}
	
}