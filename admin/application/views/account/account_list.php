<STYLE>
tbody IMG {
	display: inline-block;
}
tbody IMG.focus {
	width:150%;
	border:2px dotted red;
}
</STYLE>
<div class="widget">
	<table cellpadding="0" cellspacing="0" width="100%" class="tLight">
		<form id="list_orderby" method="post">
		<input id="order_by" type="hidden" name="order_by" value="">
		<thead>
			<tr>
				<td title="帳號" class="order_control" data-orderby = "id">帳號<i class="icon-chevron-{by-id}"></i></td>
				<td title="群組名稱" class="order_control" data-orderby = "admingroup_index">群組名稱<i class="icon-chevron-{by-admingroup_index}"></i></td>
				<td title="使用者名稱" class="order_control" data-orderby = "memo">使用者名稱<i class="icon-chevron-{by-memo}"></i></td> 
				<td title="顯示圖片" >顯示圖片</td>
				<td title="新增資料" width="150px;" style="cursor: pointer;" onclick="account.edit_account('');">新增資料<i class="icon-plus-sign"></i></td>
			</tr>
		</thead>
		</form>
		<tbody>
			{content}
			<tr id="user_{auto_index}" class="user_row">
				<td>{id}</td>
				<td>{group_name}</td>
				<td>{memo}</td>
				<td>				
					<img src="http://i-gotest.com/ci_gamesapp/uploads/temp/20131206/20131206_52a1447bd91a6.jpg" style="width: 60px;" />
				</td>
				<td>
				<a href="javascript:void(0);" class="tablectrl_small bGreen" original-title="Edit" title="修改" alt="修改" onclick="account.edit_account({auto_index});"><span class="iconb" data-icon="" ></span></a>
				<a href="javascript:void(0);" class="tablectrl_small bGreen" original-title="Options" title="密碼修改" alt="密碼修改" onclick="account.edit_password({auto_index});"><span class="iconb" data-icon="" ></span></a>
				<a href="javascript:void(0);" class="tablectrl_small {lock}" original-title="Lock" title="帳號上鎖" alt="帳號上鎖" onclick="account.lock({auto_index}, {online});"><span class="iconb" data-icon="" ></span></a>
				<a href="javascript:void(0);" class="tablectrl_small bRed" original-title="Del" title="刪除" alt="刪除" onclick="account.del({auto_index});">
					<span class="iconb" data-icon="" ></span></a>		
				</td>
			</tr>
			{/content}
		</tbody>
	</table>
</div>

<script src="js/account.js"></script>
<script>
$(document).ready(function() {
	$("IMG").each(function(){
		this.addEventListener("dragenter", dragEnter, false);
		this.addEventListener("dragexit", dragExit, false);
		this.addEventListener("dragover", dragOver, false);
		this.addEventListener("dragleave", dragLeave, false);
		this.addEventListener("drop", drop, false);
	});
	$("IMG").on('click', function() {	
		var img_box = $(this).clone();	
		$.fancybox.open(img_box.width(this.naturalWidth));
		$(img_box).bind("dragenter", dragEnter);
		$(img_box).bind("dragexit", dragExit);
		$(img_box).bind("dragover", dragOver);
		$(img_box).bind("dragleave", dragLeave);
		$(img_box).bind("drop", function(evt){
			evt.stopPropagation();
			evt.preventDefault();

			var dat = $(evt.originalEvent.dataTransfer.getData('text/html'));
		    var img = dat.attr('src');
		    if (!img) img = dat.find("img").attr('src');
		    
			console.info(img);
			var files = evt.originalEvent.dataTransfer.files;
			var count = files.length;

			// Only call the handler if 1 or more files was dropped.
			if (count > 0)
				handleFiles(files);
			else if(img){		
				$(this).attr('src',img);		
			}

		});

		$(img_box).load(function(){
			var width = this.naturalWidth;
			var height = this.naturalHeight;
			
			if(width<500 || height<375) {
				alert('圖片檔案需等於500x375');		
			}	
			else if(width>500 || height>375) {

			$(this).width(this.naturalWidth);
				
				if(width*3/4 == height) alert('圖片檔案需等於500x375，將自動縮放圖片');
				else {
					$(this).Jcrop({			            
			            bgColor:     'black',
			            bgOpacity:   .4,
			            setSelect:   [ 0, 0, 500, 375 ],
			            minSize: [500, 375],
			            aspectRatio: 4 / 3
			        });
				}
			}

		});
	});

	
});

function dragEnter(evt) {
	evt.stopPropagation();
	evt.preventDefault();
	$(this).addClass("focus");
}

function dragExit(evt) {
	evt.stopPropagation();
	evt.preventDefault();
	console.info(333);
}

function dragLeave(evt) {
	evt.stopPropagation();
	evt.preventDefault();
	$(this).removeClass("focus");
}
function dragOver(evt) {
	evt.stopPropagation();
	evt.preventDefault();
	console.info(111);
}

function drop(evt) {
	evt.stopPropagation();
	evt.preventDefault();

	var dat = $(evt.dataTransfer.getData('text/html'));
    var img = dat.attr('src');
    if (!img) img = dat.find("img").attr('src');
    
	console.info(img);
	var files = evt.dataTransfer.files;
	var count = files.length;

	// Only call the handler if 1 or more files was dropped.
	if (count > 0)
		handleFiles(files);
	else if(img){		
		$(this).attr('src',img);		
	}
}


function handleFiles(files) {
	var file = files[0];

	//document.getElementById("droplabel").innerHTML = "Processing " + file.name;

	var reader = new FileReader();

	// init the reader event handlers
	reader.onprogress = handleReaderProgress;
	reader.onloadend = handleReaderLoadEnd;

	// begin the read operation
	reader.readAsDataURL(file);
}

function handleReaderProgress(evt) {
	if (evt.lengthComputable) {
		var loaded = (evt.loaded / evt.total);

		//$("#progressbar").progressbar({ value: loaded * 100 });
	}
}

function handleReaderLoadEnd(evt) {
	//$("#progressbar").progressbar({ value: 100 });

	var img = document.getElementById("preview");
	img.src = evt.target.result;
}
</script>
<script>
function order_by(this_td){

	$('#order_by').val( $(this_td).attr('data-orderby'));

	$('#list_orderby').submit();

}

$(".order_control").live( "click", function() {
	var order_name = $(this).attr('data-orderby');
	if(order_name) order_by(this);
	
	return false;
});	

</script>

