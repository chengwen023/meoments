<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Knoledge extends CI_Controller {
	
	var $menu_index;
	
	
	public function __construct(){
		parent::__construct();
		
		$this->menu_index = 7;//
		$this->load->helper('url');
		$this->userInfo = $this->session->userdata('userInfo');
		$this->load->model('admin_model', 'admin');
		$this->load->model('topic_model', 'topic');
		$this->page_obj->collapse = 'user';
		
		
		$this->data['admin_menu'] = $this->config->item('admin_menu');
		$this->data['admin_menuGroup'] = $this->config->item('admin_menuGroup');
		$this->data['userInfo'] = $this->userInfo;
		
		// css & js
		$this->page_obj->css[] = 'js/jquery.treeview/jquery.treeview';
		$this->page_obj->css[] = 'js/Jcrop-v0.9.12/css/jquery.Jcrop.min';
		$this->page_obj->js[] = 'js/jquery.treeview/jquery.treeview';
		$this->page_obj->js[] = 'js/Jcrop-v0.9.12/js/jquery.Jcrop.min';
		$this->page_obj->js[] = 'js/img_box';
		$this->page_obj->js[] = 'js/google_img';
		$this->page_obj->js[] = 'js/horoscope';
		$this->page_obj->js[] = 'js/choose';
		
		
	}
	/*
	* index
	*/
	public function index(){
		$this->lists();
	}
	
	/**
	 *	list
	 */
	function lists(){
		
		
		
		$data = $this->data;
		$this->menu_index = 'Ikuso生活知識';//
		if(false === $activestatus = $this->admin->checkCompetence_getMenuInfo( $this->menu_index, 'V')){
			$this->load->view('admin/template', $data);
			return false;
		}
		
		$defaultstatus = (isset($activestatus))?$activestatus:$this->config->item('defaultstatus');
		
		
		$data['title'] = $this->admin->getTitle($this->menu_index);
		//讀取以建立的題目INDEX
		$this->db->flush_cache();
		$this->db->select("*");
		$this->db->from("knoledge_temp");
		$this->db->where("knoledge_temp.online",'1');
		
		
		switch($this->userInfo['id']){
			case 'user11':
				$this->db->where('knoledge_temp.article_id >', 101); 
				$this->db->where('knoledge_temp.article_id <=', 201); break;
			case 'user12':
				$this->db->where('knoledge_temp.article_id >', 201); 
				$this->db->where('knoledge_temp.article_id <=', 301); break;
			case 'user13':
				$this->db->where('knoledge_temp.article_id >', 301); 
				$this->db->where('knoledge_temp.article_id <=', 401); break;
			case 'user14':
				$this->db->where('knoledge_temp.article_id >', 401); 
				$this->db->where('knoledge_temp.article_id <=', 501); break;
			case 'user15':
				$this->db->where('knoledge_temp.article_id >', 501); 
				$this->db->where('knoledge_temp.article_id <=', 601); break;
			case 'user16':
				$this->db->where('knoledge_temp.article_id >', 601); 
				$this->db->where('knoledge_temp.article_id <=', 701); break;
			case 'user17':
				$this->db->where('knoledge_temp.article_id >', 701); 
				$this->db->where('knoledge_temp.article_id <=', 801); break;
			case 'user18':
				$this->db->where('knoledge_temp.article_id >', 801); 
				$this->db->where('knoledge_temp.article_id <=', 901); break;
			case 'user19':
				$this->db->where('knoledge_temp.article_id >', 901); 
				$this->db->where('knoledge_temp.article_id <=', 1001); break;
			case 'user20':
				$this->db->where('knoledge_temp.article_id >', 1001); 
				$this->db->where('knoledge_temp.article_id <=', 1101); break;
					
			default:
				$this->db->where('knoledge_temp.article_id <=', 100);
				break;
		}
		
		$query = $this->db->get();
		
		
		
		$data['content'] = $query->result_array();
		
		

		$this->page_obj->main_content = $this->load->view('admin/knoledge_list', $data, true);
		$this->load->view('admin/template', $data);
	}
	
	function remove_data(){
		
		$result = array('success' => 'Y', 'msg' => '');
		
		$horoscope_fk = $this->input->post('index');
		$this->db->flush_cache();
		$data = array('online'=>'0');
		$this->db->where('knoledge_temp.article_id', $horoscope_fk);
		$this->db->update('knoledge_temp', $data);
		echo json_encode($result);
	}
}