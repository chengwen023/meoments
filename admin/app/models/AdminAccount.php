<?php 
class AdminAccount extends \Phalcon\Mvc\Model
{
	
	
	public function initialize()
	{
		
		$this->hasOne("admingroup_index", "AdminGroup", "auto_index");
		$this->hasMany("auto_index", "PofansFanslist", "create_account_index");
		$this->hasMany("auto_index", "PofansFanslist", "update_account_index");
		
		$this->hasMany("auto_index", "Article", "create_account_index");
		$this->hasMany("auto_index", "Article", "update_account_index");
	}
}

?>