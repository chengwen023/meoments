<?php 
	$start = microtime(true);
	session_start();
	header("Content-type: text/html; charset=utf-8");

	include_once("include/db.php");
	include_once("include/htmltpl.php");
	include_once("include/function.php");

	$article_id = $_GET['id'];
	$curPageURL	=  curPageURL();
	$curServerURL = curPageURL(true);
	$article = array();
	$category_tables = array();
	$category_name = array();
	$account = array();
	
// 	ArticleOnlineCkeck($article_id,$curServerURL);
	
	
	include_once('global_load.php');
		
	
	$URL = $curPageURL;
	$_SESSION ['redirect'] = $URL;
	include_once('facebook_load.php');

	setcookie('compose'.$article_id, '');
	setcookie('composestatus'.$article_id, '');
	//點擊紀錄
	$views_cookie = $_COOKIE['views_cookie'];
	$views_cookie = ($views_cookie)?JSON_decode($views_cookie):array();
	
	
	
	
	$ArticleKey = "Article[".$article_id."]";
	$memcached->set_prefix($ArticleKey);
// 	$memcached->deleteMulti ( $memcached->getAllKeys () );
	
		
	//留言
	$sql = "SELECT * FROM `facebook_comments_onshelf` WHERE news_article_id = $article_id ORDER BY update_time DESC";	
	$keyValues[] = [
			'key' => 'comment_list' ,
			'func' => $memcached->SQLfunc( $sql )
	];
	
	//留言數
	$sql = " SELECT `google_ad`,`views`,`like_click_count`,`unlike_click_count`,(like_click_count+unlike_click_count) AS total_click ,`like_message_count`,`unlike_message_count`,(like_message_count+unlike_message_count) AS total_message_count FROM `news_article_state` WHERE id = $article_id LIMIT 1";
	$keyValues[] = [
			'key' => 'news_article_state' ,
			'func' => $memcached->SQLfunc( $sql )
	];


	
	$keyValues = $memcached->getMultiWithSet($keyValues,86400);

	$google_check = ($keyValues['news_article_state'][0]['google_ad']);

	$img_t = "CONCAT('$ImgServerUrl/n', `news_article`.id, '/t.jpg')";
	//文章資訊
	$sql = "SELECT `news_article`.*, $img_t AS img, `admin_account`.memo AS create_user_id FROM `news_article`
	LEFT JOIN `admin_account` ON (`admin_account`.auto_index = `news_article`.create_user_id )
	INNER JOIN (
	SELECT DISTINCT `id`
	FROM  `news_article_state`
	WHERE  `id`= $article_id LIMIT 1
	) AS nas ON nas.id =  `news_article`.`id` ";
	
	$result = mysql_query($sql);
	$article = mysql_fetch_assoc($result);
	
	//TPL資料處理
	if( count($article) == 1) $article = array_pop($article);
	
	
	

	foreach ( $global['ad_code'] AS $row ) {
		$DM["DM_{$row['width']}x{$row['height']}"] = ($google_check)?$row['google_code']:$row['other_code'];
		$article['content'] = str_replace("<div><dm{$row['width']}>{$row['width']}x{$row['height']}</dm{$row['width']}></div>", '<div class="centerBlock">'.$DM["DM_{$row['width']}x{$row['height']}"].'</div>', $article['content']);
	}	
	
	
	
	
	if($article['category'])
	$article['category'] = array_values(array_intersect_key( $category_name,	array_flip( json_decode($article['category'])) ));
	$article['comment_count'] = $keyValues['news_article_state'];
	
	
	
	$total_click = $keyValues['news_article_state'][0]['total_click'];
	$like_percent = ($total_click)?((int) (100* $keyValues['news_article_state'][0]['like_click_count'] / $total_click)):100;
	
	$article['like_percent'] = $like_percent;
	$article['unlike_percent'] = (int) (100-$like_percent);
	
	$comment_list = array();
	if(is_array($keyValues['comment_list']) && count($keyValues['comment_list'])){
		foreach ( $keyValues['comment_list'] AS $row ) {
			$comment_item = $row;
		
			$comment_item['comment_sort'] = ($comment_item['status'] == 2)?'頂':'噓';
			$comment_item['comment_class'] = ($comment_item['status'] == 2)?'like':'unlike';
				
			$comment_item['update_time'] = date('Y-m-d', $row['update_time']);
			$comment_list[] = $comment_item;
		}
	
		$article['comment_list'] = $comment_list;
	}
	
	$article['create_time'] = date('Y-m-d', $article['create_time']);

	$head = array(
			'img'		=> $article['img'],
			'img_s'		=> $ImgServerUrl.'/n'.$article['id'].'/t_s.jpg',
			'url'		=> $curPageURL,
			'baseUrl'   => $baseUrl,
			'title'		=> $article['title'],
			'link:img_src'	=> $ImgServerUrl.'/n'.$article['id'].'/f.jpg',
			'meta' => array(
					['name' => 'keywords' 			,'content'=> $article['title'] ],
					['name' => 'description'		,'content'=> $article['fb_post_content'] ],
					['property' => 'og:title'			,'content'=> $article['title'] ],
					['property' => 'og:image'			,'content'=> $ImgServerUrl.'/n'.$article['id'].'/f.jpg'],
					['property' => 'og:type'			,'content'=> "website" ],
					['property' => 'og:url'				,'content'=> $curServerURL."/{$article['id']}/" ],
					['property' => 'og:site_name'		,'content'=> "Toments" ],
	
// 					['property' => 'fb:admins' 			,'content'=> '588005798009398' ],
// 					['property' => 'fb:app_id' 			,'content'=> '588005798009398' ],
			)
	);

	
	
	$article['recommend_list'] = $global['article_list'];
	$article['composebox_url'] = $FB_URL;
	
	$data['article'][] = $article;
	$data['article_list'] = $global['article_list'];
	$data['daily_views_list'] = $global['daily_views_list'];
	
		
	$data['HEAD'][] = $head;
	$data['MENU'] = $category_name;
	$data['mobile_MENU'] = $category_name;
	
	$data['DM728x90'] = $DM['DM_728x90'];
	$data['DM300x250'] = $DM['DM_300x250'];
	$data['DM334x280'] = $DM['DM_334x280'];
	$data['curPageURL'] = $curPageURL;
	$data['curServerURL'] = $curServerURL;
	
	$HTML['HTML'][] = $data;
// 	echo microtime(true) - $start ;
	echo tpl_get_html("./tpl/","news_article.tpl.html",$HTML);
?>