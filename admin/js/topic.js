var topic = new Object;
topic.insert = function(index){
	
	url = 'topic/insert_topic';
	 
	$.fancybox.showLoading();
	$.ajax({
		url: url,
	  cache: false,
		dataType: 'html',
	  type: 'POST',
	  data:{
		  index:index
	  },
	  success: function(data){
	  	$.fancybox.hideLoading();
	  	$.fancybox.open(data,{	  
	  		wrapCSS : 'fancybox-background',
	  		maxHeight: '800',
	  		autoCenter   : true,
		  	helpers : {
		        overlay : {
		        	closeClick: false
		        }
	        },
	        afterShow : function(){
	        	//$('.fancybox-inner').css('width', '620px');
	        },
	        beforeClose : function(){
	        	$('.add_topic_option').die('click');
	        	$('.topic_option_remove').die('click');
	        	$("DIV.fb_view_control SPAN").die('click');
	        	$("IMG.editable").die('click');
	        	CKEDITOR_destroy();
	        	return(confirm("是否關閉 新增/編輯 資料視窗!?"));	        	
	        }
		  	});
	  }
	});
}


topic.del = function(index){
	
	url = 'topic/actions/del';
	 
	$.fancybox.showLoading();
	$.ajax({
		url: url,
	  cache: false,
	  dataType: 'json',
	  type: 'POST',
	  data:{
		  index:index
	  },
	  success: function(data){
	  	$.fancybox.hideLoading();
	  	if(data.success == 'Y'){
	  		$.fancybox.close(true);
	  		location.reload();
	  	}else{
	  		$.fancybox.close(true);
	  		alert(data.msg);
	  	}
	  }
	});
}

topic.add_edit = function(index){
	
	url = 'topic/actions/add_topic';
	 
	$.fancybox.showLoading();
	$.ajax({
		url: url,
	  cache: false,
		dataType: 'html',
	  type: 'POST',
	  data:{
		  index:index
	  },
	  success: function(data){
	  	$.fancybox.hideLoading();
	  	$.fancybox.open(data,{	  
	  		wrapCSS : 'fancybox-background',
	  		autoCenter   : true,
		  	helpers : {
		        overlay : {
		        	closeClick: false
		        }
	        },
	        afterShow : function(){
	        	//$('.fancybox-inner').css('width', '620px');
	        },
	        beforeClose : function(){
	        	CKEDITOR_destroy();
	        	return(confirm("是否關閉 新增/編輯 資料視窗!?"));	        	
	        }
		  	});
	  }
	});
}

topic.reviseview = function(index){
	
	url = 'topic/actions/revise_topic';
	console.info('adonis');
	$.fancybox.showLoading();
	$.ajax({
		url: url,
	  cache: false,
		dataType: 'html',
	  type: 'POST',
	  data:{
		  index:index
	  },
	  success: function(data){
	  	$.fancybox.hideLoading();
	  	$.fancybox.open(data,{	  
	  		wrapCSS : 'fancybox-background',
	  		autoCenter   : true,
		  	helpers : {
		        overlay : {
		        	closeClick: false
		        }
	        },
	        afterShow : function(){
	        	//$('.fancybox-inner').css('width', '620px');
	        },
	        beforeClose : function(){
	        	//CKEDITOR_destroy();
	        	return(confirm("是否關閉 新增/編輯 資料視窗!?"));	        	
	        }
		  	});
	  }
	});
}

topic.view = function(index){
	
	url = 'topic/actions/onshelf_topic';
	 
	
	$.fancybox.showLoading();
	$.ajax({
		url: url,
	  cache: false,
		dataType: 'html',
	  type: 'POST',
	  data:{
		  index:index
	  },
	  success: function(data){
	  	$.fancybox.hideLoading();
	  	$.fancybox.open(data,{	  
	  		wrapCSS : 'fancybox-background',
	  		autoCenter   : true,
		  	helpers : {
		        overlay : {
		        	closeClick: false
		        }
	        },
	        afterShow : function(){
	        	//$('.fancybox-inner').css('width', '620px');
	        },
	        beforeClose : function(){
	        	return(confirm("是否關閉 新增/編輯 資料視窗!?"));	        	
	        }
		  	});
	  }
	});
}

topic.pofans = function(this_span,index){
	
	url = 'http://192.168.1.122/pofans/aip_rec_article.php';
	//url = 'http://i-kuso.com/pofans/aip_rec_article.php';
	 
	$('.pofans').removeClass('focus');
	$(this_span).next('.pofans').addClass('focus');
}

topic.ifunso_tests = function(index){
	
	
	url = 'ifunso_tests/insert_topic'; 
	
	$.fancybox.showLoading();
	$.ajax({
		url: url,
	  cache: false,
		dataType: 'html',
	  type: 'POST',
	  data:{
		  index:index
	  },
	  success: function(data){
	  	$.fancybox.hideLoading();
	  	$.fancybox.open(data,{	  
	  		autoCenter   : true,
		  	helpers : {
		        overlay : {
		        	closeClick: false
		        }
	        },
	        afterShow : function(){
	        	//$('.fancybox-inner').css('width', '620px');
	        },
	        beforeClose : function(){
	        	return(confirm("是否關閉 新增/編輯 資料視窗!?"));	        	
	        }
		  	});
	  }
	});
}

topic.knoledge = function(index){	
	
	url = 'knoledge/insert_topic'; 
	$.ajax({
		url: url,
	  cache: false,
		dataType: 'html',
	  type: 'POST',
	  data:{
		  index:index
	  },
	  success: function(data){
	  
	  	$.fancybox.open(data,{	  
	  		wrapCSS : 'fancybox-background',
	  		autoCenter   : true,
		  	helpers : {
		        overlay : {
		        	closeClick: false
		        }
	        },
	        afterShow : function(){
	        	//$('.fancybox-inner').css('width', '620px');
	        },
	        beforeClose : function(){
	        	return(confirm("是否關閉 新增/編輯 資料視窗!?"));	        	
	        }
		  	});
	  }
	});
	
}
topic.knoledge_remove = function(index){	
	
	url = 'knoledge/remove_data'; 
	
	$.fancybox.showLoading();
	$.ajax({
		url: url,
	  cache: false,
		dataType: 'json',
	  type: 'POST',
	  data:{
		  index:index
	  },
	  success: function(data){
		  if(data.success == 'Y'){
		  		$.fancybox.close(true);
		  		location.reload();
		  	}else{
		  		$.fancybox.close(true);
		  		alert(data.msg);
		  	}
	  }
	});
}
topic.horoscope = function(index){	
	
	url = 'horoscope/insert_topic'; 
	$.ajax({
		url: url,
	  cache: false,
		dataType: 'html',
	  type: 'POST',
	  data:{
		  index:index
	  },
	  success: function(data){
	  
	  	$.fancybox.open(data,{	  
	  		wrapCSS : 'fancybox-background',
	  		autoCenter   : true,
		  	helpers : {
		        overlay : {
		        	closeClick: false
		        }
	        },
	        afterShow : function(){
	        	//$('.fancybox-inner').css('width', '620px');
	        },
	        beforeClose : function(){
	        	return(confirm("是否關閉 新增/編輯 資料視窗!?"));	        	
	        }
		  	});
	  }
	});
}

topic.horoscope_remove = function(index){	
	
	url = 'horoscope/remove_data'; 
	
	$.fancybox.showLoading();
	$.ajax({
		url: url,
	  cache: false,
		dataType: 'json',
	  type: 'POST',
	  data:{
		  index:index
	  },
	  success: function(data){
		  if(data.success == 'Y'){
		  		$.fancybox.close(true);
		  		location.reload();
		  	}else{
		  		$.fancybox.close(true);
		  		alert(data.msg);
		  	}
	  }
	});
}