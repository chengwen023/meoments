<style type="text/css">
.author_info {
	font-size:18px; 
	font-weight:bold;
	text-align: center;
	margin:23px 0;
}
</style>

<div class="author_info">{author_name}</div>
<div>
	<table cellpadding="0" cellspacing="0" width="100%" class="tDefault tLight">
		<thead>
			<tr>
				<td>編號</td>
				<td>標題</td>
				<td>來源</td>
				<td>次數</td>
				<!-- <td title="作者" tips ="使用滑鼠點選 可以排序該欄位" class="order_control" data-orderby = "id">編號<i class="icon-chevron-{by-id}"></i></td>
				<td title="發文數" tips ="使用滑鼠點選 可以排序該欄位" class="order_control" data-orderby = "count">次數<i class="icon-chevron-{by-count}"></i></td> -->
			</tr>			
		</thead>
		
		<tbody class="statistics_tbody">
			{content}
			<tr id="{auto_index}">
				<td>{id}</td>
				<td><a href="http://toments.com/{id}" target="_blank">{title}</a></td>
				<td><a href="{source_url}" target="_blank">{fb_id} / {source_mode}</a></td>
				<td>{count}</td>											
			</tr>			
			{/content}
		</tbody>
	</table>
</div>