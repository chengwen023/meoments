<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Topic extends CI_Controller {
	var $menu_index;
	public function __construct() {
		parent::__construct ();
		
		$this->menu_index = 6; //
		$this->load->helper ( 'url' );
		$this->userInfo = $this->session->userdata ( 'userInfo' );
		$this->load->model ( 'topic_model', 'topic' );
		$this->load->model ( 'theme_model', 'theme' );
		
		$this->data ['admin_menu'] = $this->config->item ( 'admin_menu' );
		$this->data ['admin_menuGroup'] = $this->config->item ( 'admin_menuGroup' );
		$this->data ['userInfo'] = $this->userInfo;
		$this->data ['menu'] = $this->config->item ( 'admin_accountMenu' );
		$this->data ['breadcrumbs'] = $this->config->item ( 'breadcrumbs' );
		$this->data ['title'] = $this->config->item ( 'admin_title' );
		
		$this->data ['topbar_control'] = array ();
		$this->data ['select_result'] = array ();
	}
	/*
	 * index
	 */
	public function index($pg = 1) {
		$this->lists ( $pg );
	}
	
	/**
	 * list
	 */
	
	function ajax_iplayfun($pg = 1){
		
		//{--changePage--}
		//{--summary:start--}
		//{--content:start--}
		$get = array('ai'=>'3000','limit'=>1);
		header ( 'Content-type: text/plain; charset=utf-8' );
		
		$value = $this->curl->simple_get('http://i-playfun.com/ajax/article_ajax.php',$get);
		
		$result = json_decode($value);
		if(is_array($result)) var_dump( $result );
		else echo json_decode($result);
		
	}
	
	function lists($pg = 1, $user = '') {
		$page_one = 10;
		$topSearch = $this->input->post ( 'topSearch' );
		$order_name = $this->input->post ( 'order_by' );
		$orderBy = $this->theme->order_by ( $order_name ,'topic_list');
		$data = array_merge ( $this->data, $orderBy );
		
		if ($pg || $user)
			$this->menu_index = '測驗列表'; //
		
		if (false === $activestatus = $this->admin_model->checkCompetence_getMenuInfo ( $this->menu_index, 'V' )) {
			$this->parser->parse ( 'template', $data );
			return false;
		}
		
		$defaultstatus = (isset ( $activestatus )) ? $activestatus : $this->config->item ( 'defaultstatus' );
		
		$purview = 'O';
		if ($this->admin_model->checkCompetence_getMenuInfo ( $this->menu_index, $purview )) {
			$onshelf_purview = true;
		} else {
			$onshelf_purview = false;
		}
		
		if ($this->menu_index)
			$data ['breadcrumbs'] = $this->config->item ( 'breadcrumbs' );
		
		$data ['onshelf_purview'] = $onshelf_purview;
		
		// 讀取 題目 使用者資訊
		$sql = "select admin_account.id AS value,CONCAT(admin_account.id,'(' ,sum(case when topic.online = '0' then 0 else 1 end),'/',count(admin_account.auto_index) ,')'  ) AS label 
				FROM admin_account 
				INNER JOIN topic ON admin_account.auto_index = topic.create_account_index
				GROUP BY admin_account.auto_index 
				";
		$this->db->flush_cache ();
		$query = $this->db->query ( $sql );
		
		preg_match ( '/select_(?P<user_id>[0-9]+)/', $user, $user_id );
		if ($user_id)
			$temp_userid ['select_id'] = $user_id ['user_id'];
		
		preg_match ( '/^(?P<group_id>[0-9]+)/', $user, $group_id );
		if ($group_id)
			$temp_userid ['group_id'] = $group_id ['group_id'];
		
		$temp_selector ['current_url'] = base_url ( 'topic/lists/' . $pg ) . "/";
		
		$temp_selector ['select_name'] = (! isset ( $temp_userid ) && ! isset ( $temp_userid ) && $user) ? $user : 'All User';
		$temp_selector ['select_option'] = $query->result_array ();
		$data ['select_result'] [] = $temp_selector;
		
		if (isset ( $temp_userid ['group_id'] ))
			$temp_name = $this->topic->topic_group ( $temp_userid ['group_id'], 'name AS group_name' );
		$temp_selector ['select_name'] = (isset ( $temp_name ) && isset ( $temp_name ['group_name'] )) ? $temp_name ['group_name'] : 'All Group';
		$temp_selector ['select_option'] = $this->topic->topic_group ( '', 'name AS label, auto_index AS value' );
		$data ['select_result'] [] = $temp_selector;
		
		if (isset ( $temp_userid ['select_id'] ))
			$temp_name = $this->topic->topic_select ( $temp_userid ['select_id'], 'select_name' );
		$temp_selector ['select_name'] = (isset ( $temp_name ) && isset ( $temp_name ['select_name'] )) ? $temp_name ['select_name'] : 'All Select';
		$temp_selector ['select_option'] = $this->topic->topic_select ( '', 'select_name AS label, CONCAT("select_",topic_select.auto_index ) AS value' );
		$data ['select_result'] [] = $temp_selector;
		
		$this->db->flush_cache ();
		$this->db->select ( "topic.*,statistics_onshelf.fail_state AS fail_state,admin_account.id" );
		$this->db->from ( "topic" );
		// $this->db->where("topic.online",'0');
		$this->db->join ( 'admin_account', 'admin_account.auto_index = topic.create_account_index' );
		$this->db->join ( "statistics_onshelf", 'statistics_onshelf.topic_index = topic.auto_index', 'left' );
		/*
		 * if($defaultstatus == 'S'){ $this->db->where('create_account_index',$this->config->item('admin_accountIndex')); }else if($defaultstatus == 'G'){ $this->db->where_in('create_account_index',$this->config->item('admin_accountGroupIndexArray')); } /*
		 */
		
		if ($topSearch) {
			$this->db->where ( "topic.topic like '%" . $topSearch . "%'" );
			$this->db->or_where ( "topic.auto_index", $topSearch );
		}
		
		if ($user) {
			
			if (isset ( $temp_userid ['select_id'] )) {
				$this->db->where ( 'topic.options_select', $temp_userid ['select_id'] );
			} else if (isset ( $temp_userid ['group_id'] )) {
				$this->db->where ( "topic.topic_group like '%\"" . $temp_userid ['group_id'] . "\"%'" );
			} else {
				$this->db->where ( 'admin_account.id', $user );
			}
		}
		if (is_array ( $orderBy )) {
			foreach ( $orderBy as $key => $value ) {
				if (! $key)
					continue;
				if ($value == "down")
					$this->db->order_by ( 'topic.' . str_replace ( 'by-', '', $key ) . ' DESC' );
				else
					$this->db->order_by ( 'topic.' . str_replace ( 'by-', '', $key ) . ' ASC' );
			}
		} else
			$this->db->order_by ( 'topic.online ASC, topic.create_account_index ASC' );
		
		$total_query = clone $this->db;
		
		$page ['total_page'] = $total_query->get ()->num_rows;
		
		$this->db->limit ( $page_one, ($pg - 1) * $page_one );
		$query = $this->db->get ();
		
		$data ['title'] = $this->admin_model->getTitle ( $this->menu_index );
		
		$data ['content'] = $query->result_array ();
		foreach ( $data ['content'] as $key => $list ) {
			if ($data ['content'] [$key] ['online'] < 1)
				$data ['content'] [$key] ['edit_control'] = '';
			else
				$data ['content'] [$key] ['edit_control'] = 'hide';
			$temp_group = $this->topic->topic_group ();
			$fans_groupname = array ();
			$temp_groupindex = json_decode ( $list ['topic_group'] );
			
			$data ['content'] [$key] ['fail_state'] = (false !== strpos ( $list ['fail_state'], 'true' )) ? '請修正問題' : '';
			
			
			
			$data ['content'] [$key] ['topic_imgurl'] = 
			(preg_match("/^http/", $list['topic_imgurl']))?$list['topic_imgurl']:(((preg_match("/data\:/", $list['topic_imgurl']))?'':'../uploads/q'.$list['auto_index'].'/').$list['topic_imgurl']);
			$data ['content'] [$key] ['options_imgurl'] = 
			(preg_match("/^http/", $list['options_imgurl']))?$list['options_imgurl']:(((preg_match("/data\:/", $list['options_imgurl']))?'':'../uploads/q'.$list['auto_index'].'/').$list['options_imgurl']);
			
			
			foreach ( $temp_group as $value ) {
				if (is_array ( $temp_groupindex ) && in_array ( $value ['auto_index'], $temp_groupindex ))
					$fans_groupname [] = $value ['name'];
			}
			$select_option = $this->topic->topic_select ( $list ['options_select'] );
			$data ['content'] [$key] ['group_names'] = ($temp_group) ? implode ( ',', $fans_groupname ) : '無';
			$data ['content'] [$key] ['select_optionName'] = (isset ( $select_option ['select_name'] )) ? $select_option ['select_name'] : '無';
		}
		
		$page ['start'] = 1;
		$page ['end'] = ceil ( $page ['total_page'] / $page_one );
		$page ['url'] = 'topic/lists';
		$page ['page_num'] = $pg;
		$page ['url_end'] = '/' . $user;
		
		$data ['page_list'] = $this->theme->page ( $page );
		
		$data ['main_content'] = $this->parser->parse ( 'topic/topic_list', $data, true );
		$this->parser->parse ( 'template', $data );
	}
	function onshelf_lists($pg = 1, $user = '') {
		$page_one = 10;
		
		$topSearch = $this->input->post ( 'topSearch' );
		
		$order_name = $this->input->post ( 'order_by' );
		$orderBy = $this->theme->order_by ( $order_name ,'topic_onshelflist');
		$data = array_merge ( $this->data, $orderBy );
		
		if ($pg || $user)
			$this->menu_index = '測驗上線列表'; //
		
		if (false === $activestatus = $this->admin_model->checkCompetence_getMenuInfo ( $this->menu_index, 'V' )) {
			$this->parser->parse ( 'template', $data );
			return false;
		}
		
		$defaultstatus = (isset ( $activestatus )) ? $activestatus : $this->config->item ( 'defaultstatus' );
		
		if ($this->menu_index)
			$data ['breadcrumbs'] = $this->config->item ( 'breadcrumbs' );
			
			// 讀取 題目 使用者資訊
		$sql = "select admin_account.id AS value,CONCAT(admin_account.id,'(' ,sum(case when topic.online = '0' then 0 else 1 end),'/',count(admin_account.auto_index) ,')'  ) AS label 
				FROM admin_account 
				INNER JOIN topic ON admin_account.auto_index = topic.create_account_index
				GROUP BY admin_account.auto_index 
				";
		$this->db->flush_cache ();
		$query = $this->db->query ( $sql );
		
		$temp_selector ['current_url'] = base_url ( 'topic/onshelf_lists/' . $pg ) . "/";
		$temp_selector ['select_name'] = (false === is_numeric ( $user ) && $user) ? $user : 'All User';
		$temp_selector ['select_option'] = $query->result_array ();
		$data ['select_result'] [] = $temp_selector;
		
		if (true === is_numeric ( $user ) && $user > 0)
			$temp_name = $this->topic->topic_group ( $user, 'name AS select_name' );
		$temp_selector ['select_name'] = (isset ( $temp_name ) && isset ( $temp_name ['select_name'] )) ? $temp_name ['select_name'] : 'All Group';
		$temp_selector ['select_option'] = $this->topic->topic_group ( '', 'name AS label, auto_index AS value' );
		$data ['select_result'] [] = $temp_selector;
		
		$data ['postfb_type'] = $this->topic->topic_post_type ();
		$data ['topic_select'] = $this->topic->topic_select ();
		
		$this->db->flush_cache ();
		$this->db->select ( "topic.*,admin_account.id, statistics_onshelf.plus_state AS plus_state, topic_onshelf.auto_index AS onshelf_index, topic_onshelf.daily_click AS daily_click, topic_onshelf.total_click AS total_click, topic_onshelf.create_time AS create_time, topic_onshelf.fb_like AS fb_like, topic_onshelf.fb_recommend AS fb_recommend, topic_onshelf.wall_status_count AS wall_status_count,topic_onshelf.autopost AS autopost, topic_onshelf.pofans_total AS pofans_total" );
		$this->db->from ( "topic" );
		$this->db->where ( "topic.online", '1' );
		$this->db->join ( 'admin_account', 'admin_account.auto_index = topic.create_account_index' );
		$this->db->join ( 'topic_onshelf', 'topic_onshelf.topic_fromindex = topic.auto_index', 'inner' );
		$this->db->join ( 'statistics_onshelf', 'statistics_onshelf.topic_index = topic.auto_index', 'inner' );
		
		/*
		 * if($defaultstatus == 'S'){ $this->db->where('create_account_index',$this->config->item('admin_accountIndex')); }else if($defaultstatus == 'G'){ $this->db->where_in('create_account_index',$this->config->item('admin_accountGroupIndexArray')); } /*
		 */
		
		if ($topSearch) {
			$this->db->where ( "topic.topic like '%" . $topSearch . "%'" );
			$this->db->or_where ( "topic.auto_index", $topSearch );
		}
		
		if ($user) {
			$this->db->where ( 'admin_account.id', $user );
			$this->db->or_where ( "topic.topic_group like '%\"" . $user . "\"%'" );
		}
		if (is_array ( $orderBy )) {
			foreach ( $orderBy as $key => $value ) {
				if (! $key)
					continue;
				if ($value == "down")
					$this->db->order_by ( 'topic_onshelf.' . str_replace ( 'by-', '', $key ) . ' DESC' );
				else
					$this->db->order_by ( 'topic_onshelf.' . str_replace ( 'by-', '', $key ) . ' ASC' );
			}
		} else
			$this->db->order_by ( 'topic.update_time DESC' );
		
		$total_query = clone $this->db;
		
		$page ['total_page'] = $total_query->get ()->num_rows;
		
		$this->db->limit ( $page_one, ($pg - 1) * $page_one );
		$query = $this->db->get ();
		
		$data ['title'] = $this->admin_model->getTitle ( $this->menu_index );
		$data ['total_state'] = 0;
		$data ['content'] = $query->result_array ();
		foreach ( $data ['content'] as $key => $list ) {
			
			$data ['content'] [$key] ['plus_state'] = '';
			$temp_state = json_decode ( $list ['plus_state'], true );
			if (is_array ( $temp_state )) {
				$data ['total_state'] += count ( array_diff ( $temp_state, array (
						1 
				) ) );
				$data ['content'] [$key] ['plus_state'] = count ( array_diff ( $temp_state, array (
						1 
				) ) ) . " :[" . implode ( ',', $temp_state ) . "] ";
			}
			$temp_group = $this->topic->topic_group ();
			$fans_groupname = array ();
			$temp_groupindex = json_decode ( $list ['topic_group'] );
			foreach ( $temp_group as $value ) {
				if (is_array ( $temp_groupindex ) && in_array ( $value ['auto_index'], $temp_groupindex ))
					$fans_groupname [] = $value ['name'];
			}
			
			if ($data ['content'] [$key] ['autopost'] == 0)
				$data ['content'] [$key] ['autopost'] = '關閉';
			foreach ( $data ['postfb_type'] as $value ) {
				if ($value ['auto_index'] == $data ['content'] [$key] ['autopost'])
					$data ['content'] [$key] ['autopost'] = $value ['type_name'];
			}
			
			$select_option = $this->topic->topic_select ( $list ['options_select'] );
			$data ['content'] [$key] ['group_names'] = ($temp_group) ? implode ( ',', $fans_groupname ) : '無';
			$data ['content'] [$key] ['select_optionName'] = (isset ( $select_option ['select_name'] )) ? $select_option ['select_name'] : '無';
			
			$data ['content'] [$key] ['pofans_text'] = Null;
			$data ['content'] [$key] ['optionList'] = $this->topic->pofans_options ( $list ['auto_index'], ($list ['options_select'] == 3) ? true : false );
			
			$optionIndex = 1;
			$select_list = '';
			foreach ( $data ['content'] [$key] ['optionList'] as $okey => $ovalue ) {
				if ($list ['options_select'] == 4) {
					$select_list = $this->topic->cutText_for_pofans ( $ovalue ['text'] );
				} else {
					if ($list ['options_select'] == 3 || ((isset ( $ovalue ['horo'] ) && $ovalue ['horo']))) {
						$select_list = $select_list . $ovalue ['choose'] . ": " . $ovalue ['title'] . "\r\n";
					} elseif ($list ['options_select'] == 1 || ((isset ( $ovalue ['horo'] ) && $ovalue ['horo']))) {
						$select_list = $select_list . ($optionIndex ++) . ". " . $ovalue ['title'] . "\r\n";
					} else {
						$select_list = $select_list . ($optionIndex ++) . ". " . $ovalue ['choose'] . "\r\n";
					}
				}
			}
			$pofans_title = (($list ['options_select'] == 4) ? '閱讀更多' : '分析你的結果') . '：http://i-gotest.com/q' . $list ['auto_index'] . '.html';
			$data ['content'] [$key] ['pofans_img'] = $list ['topic'] . "\r\n\r\n" . $pofans_title;
			$data ['content'] [$key] ['pofans_link'] = $list ['topic'] . "\r\n\r\n" . (($list ['topic_optiontitle']) ? $list ['topic_optiontitle'] . "\r\n\r\n" : '') . $select_list;
			$data ['content'] [$key] ['pofans_text'] = $list ['topic'] . "\r\n\r\n" . (($list ['topic_optiontitle']) ? $list ['topic_optiontitle'] . "\r\n\r\n" : '') . $select_list . "\r\n" . $pofans_title;
		}
		
		$page ['start'] = 1;
		$page ['end'] = ceil ( $page ['total_page'] / $page_one );
		$page ['url'] = 'topic/onshelf_lists';
		$page ['page_num'] = $pg;
		$page ['url_end'] = '/' . $user;
		$data ['current_url'] = base_url ( 'topic/onshelf_lists/' . $pg ) . "/";
		$data ['page_list'] = $this->theme->page ( $page ); 
		
		$data ['main_content'] = $this->parser->parse ( 'topic/topic_onshelflist', $data, true );
		$this->parser->parse ( 'template', $data );
	}
	function onshelf_timesadd() {
		$index = $this->input->post ( 'onshelf_index' );
		
		$this->db->set ( 'pofans_total', 'pofans_total+1', FALSE );
		$this->db->where ( 'auto_index', $index );
		$this->db->update ( 'topic_onshelf' );
	}
	function pofans_log() {
		$index = $this->input->post ( 'onshelf_index' );
		$pofans_index = $this->input->post ( 'pofans_index' );
		
		$date = date ( 'Y-m-d H:i:s' );
		$add_input = array (
				'topiconshelf_index' => $index,
				'pofans_index' => $pofans_index,
				'createtime' => $date 
		);
		$this->db->set ( $add_input );
		$this->db->insert ( 'topic_pofans' );
	}
	function choose_data() {
		$type = $this->input->post ( 'type' );
		
		if ($type == '1') {
			echo '<li><div class="del_btn">移除該選項</div><div class="imgurl_box" ><span><img src="uploads/temp/default_pic.jpg" width="70px" /></span><input type="hidden" value="uploads/temp/default_pic.jpg" name="options[text_imgurl][]"/></div><div>標題<input type="text" name="options[title][]" value="" /><br/>內容<textarea name="options[text][]"></textarea><input type="hidden" name="options[choose][]" value="hide"/></div></li>';
		} else if ($type == '2') {
			echo '<li><div class="del_btn">移除該選項</div><div class="imgurl_box" ><span><img src="uploads/temp/default_pic.jpg" width="70px" /></span><input type="hidden" value="uploads/temp/default_pic.jpg" name="options[text_imgurl][]"/></div><div>選項<input type="text" name="options[choose][]" value=""/><br/>標題<input type="text" name="options[title][]" value=""/><br/>內容<textarea name="options[text][]"></textarea></div></li>';
		} else {
			$this->db->select ( "*" )->from ( "topic_defaultoptions" )->where ( "select_index", $type );
			$query = $this->db->get ();
			$data = $query->result_array ();
			foreach ( $data as $item ) {
				echo '<li><div class="imgurl_box" ><span><img src="uploads/temp/default_pic.jpg" width="70px" /></span><input type="hidden" value="uploads/temp/default_pic.jpg" name="options[text_imgurl][]"/></div><div>選項:' . $item ['option_name'] . '<br/>標題<input type="text" name="options[title][]" value="' . $item ['option_name'] . '" /><br/>內容<textarea name="options[text][]"></textarea><input type="hidden" name="options[choose][]" value="' . $item ['option_name'] . '"/></div></li>';
			}
		}
	}
	function defoption_select() {
		$index = $this->input->post ( 'index' );
		$topic_index = $this->input->post ( 'topic_index' );
		
		
		$defoption_pic = ($index > 0)?$this->topic->topic_defaultoption_pic ( $index ):false;
		
		//判斷是否可套圖
		$query = $this->db->select ( "auto_index" )->from ( "topic_options" )->where ( "topic_index", $topic_index )->get ()->num_rows;
		if ($index && false !== $defoption_pic && $query == 12){
			$this->topic->topic_set_defoption_select ( $index, $topic_index );				
		}
		
		echo json_encode ( $defoption_pic );
		
		
			
	}
	
	
	function change_options_select() {
		$topic_index = $this->input->post ( 'topic_index' );
		$select_index = $this->input->post ( 'select_index' );
		
		$query = $this->db->select ( "auto_index" )->from ( "topic_options" )->where ( "topic_index", $topic_index )->get ()->num_rows;
		
		if($query == 12 && ( $select_index == 2 || $select_index == 3) ) {
			//只有選項12個 才可切換星座-N選1
			$add_input = array('options_select' => $select_index);
			$this->db->where ( 'auto_index', $topic_index );
			$this->db->update ( 'topic', $add_input );
		}else{			
			echo json_encode("選項規則不符合變更條件");
		}
		
		
		exit;
		
	}
	
	
	function update_topic(){
		$index = ( int ) $this->input->post ( 'index' );
		$filed = $this->input->post ( 'filed' );
		$value = $this->input->post ( 'value' );
		$type = $this->input->post ( 'type' );
	
		$update_input = array ($filed => trim($value) );
		$date = date ( "Y-m-d H:i:s" );
		$account_id = $this->config->item ( 'admin_accountIndex' );
	
		if (isset ( $index ) && $index > 0) {
				
			$this->db->where ( 'auto_index', $index );
			if($type) $this->db->update ( $type, $update_input );
			
		}else{
			echo '資料有誤';
		}
	}
	
	function group_topic(){
		$index = ( int ) $this->input->post ( 'index' );
		$group_list = json_encode ( $this->input->post ( 'group_list' ) );
	
	
		$add_input = array ('topic_group' => $group_list );
		$date = date ( "Y-m-d H:i:s" );
		$account_id = $this->config->item ( 'admin_accountIndex' );
	
		if (isset ( $index ) && $index > 0) {
				
			$add_input ['update_time'] = $date;
			$add_input ['update_account_index'] = $account_id;
				
			$this->db->where ( 'auto_index', $index );
			$this->db->update ( 'topic', $add_input );
		} else{
			echo '資料有誤';
		}
	}
	
	function audit_topic(){
		$index = ( int ) $this->input->post ( 'index' );
		$fail = json_encode ( $this->input->post ( 'audit_list' ) );
		
		$this->db->flush_cache ();
		$query = $this->db->select ( "auto_index" )->from ( "statistics_onshelf" )->where ( "topic_index", $index )->where ( "online", '0' )->get ();
		$temp = $query->row_array ();
		
		$add_input = array (
				'topic_index' => $index,
				'fail_state' => $fail 
		);
		$date = date ( "Y-m-d H:i:s" );
		$account_id = $this->config->item ( 'admin_accountIndex' );
		
		if (isset ( $temp ['auto_index'] ) && $temp ['auto_index'] > 0) {
			
			$add_input ['update_time'] = $date;
			$add_input ['update_account_index'] = $account_id;
			
			$this->db->where ( 'auto_index', $temp ['auto_index'] );
			$this->db->update ( 'statistics_onshelf', $add_input );
		} else {
			
			$add_input ['create_time'] = $date;
			$add_input ['update_time'] = $date;
			$add_input ['create_account_index'] = $account_id;
			$add_input ['update_account_index'] = $account_id;
			
			$this->db->insert ( 'statistics_onshelf', $add_input );
		}
	}
	
	function not_pass() {
		$index = ( int ) $this->input->post ( 'index' );
		$fail = json_encode ( $this->input->post ( 'fail' ) );
		
		$this->db->flush_cache ();
		$query = $this->db->select ( "auto_index" )->from ( "statistics_onshelf" )->where ( "topic_index", $index )->where ( "online", '0' )->get ();
		$temp = $query->row_array ();
		
		$add_input = array (
				'topic_index' => $index,
				'fail_state' => $fail 
		);
		$date = date ( "Y-m-d H:i:s" );
		$account_id = $this->config->item ( 'admin_accountIndex' );
		
		if (isset ( $temp ['auto_index'] ) && $temp ['auto_index'] > 0) {
			
			$add_input ['update_time'] = $date;
			$add_input ['update_account_index'] = $account_id;
			
			$this->db->where ( 'auto_index', $temp ['auto_index'] );
			$this->db->update ( 'statistics_onshelf', $add_input );
		} else {
			
			$add_input ['create_time'] = $date;
			$add_input ['update_time'] = $date;
			$add_input ['create_account_index'] = $account_id;
			$add_input ['update_account_index'] = $account_id;
			
			$this->db->insert ( 'statistics_onshelf', $add_input );
		}
	}
	
	function change_text() {
		$id = $this->input->post ( 'id' );		
		$text = $this->input->post ( 'text' );
		$type = $this->input->post ( 'type' );
		
		if(strtolower($type) != "html")
			$text =  $this->topic->cutText_for_pofans ( $text );
		
		preg_match ( '/(?P<field_name>[a-zA-Z]+)_(?P<field_index>[0-9]+)/', $id, $field_id );
		
		if(!$field_id || !is_array($field_id) || $field_id ['field_index'] <1 ) exit;
		
		$table_name = '';
		switch($field_id ['field_name']){
			default:break;
			case 'topic':
			case 'topic_optiontitle':
				$table_name = "topic";
				break;
			case 'choose':
			case 'title':
			case 'text':
				$table_name = "topic_options";
				break;
		}
		
		if($table_name && $field_id ['field_name']){
			
			$this->db->flush_cache ();
			$this->db->where ( $table_name.'.auto_index', $field_id ['field_index'] );
			$adonis = $this->db->update ( $table_name, array($field_id ['field_name'] => trim($text) ) );
			
		}
	}
	
	function insert_topic(){
		
			$index = $this->input->post ( 'index' );
			
		
			if($index<1) $index = 2;
			
			$data['defoption_select'] = $this->topic->topic_defaultselect_pic();
	
			
			$data ['topic_group_list'] = $this->topic->topic_group ();
			$topic_select = $this->topic->topic_select($index);
			$data ['options_select_name'] = $topic_select['select_name'];
			$data ['options_select_index'] = $topic_select['auto_index'];
			
			$data ['topic_select'] = $this->topic->topic_select ();
			foreach($data['topic_select'] AS $key => $value){
				if( $value['auto_index'] == $index) $data['topic_select'][$key]['active'] = 'active';
			}
			foreach($data['topic_group_list'] AS $key => $value){
				$data['topic_group_list'][$key]['group_index'] = $value['auto_index'];					
			}
				
			
			if($index == 3){
				$this->db->flush_cache();
				$this->db->select("*")->from("topic_defaultoptions")->where("select_index",3);
				$query = $this->db->get();
				
				$data['topic_options'] = $query->result_array();
				$temp_i = 0;
				foreach ( $data ['topic_options'] as $key => $i ) {					
					$data ['topic_options'] [$key] ['choose'] = $i ['option_name'];
					$data ['topic_options'] [$key] ['title'] = $i ['option_name'];
					$data ['topic_options'] [$key] ['text'] = $i ['option_name'];
						
				}
			}else {
// 				
				
// 				for($i = 0;$i<(($index == 4)?1:4) ;$i++)
					$data['topic_options'][] = array('choose'=>'','title'=>'','text'=>'');
						
			}
			
			
				
			if($index == 4) $this->parser->parse ( 'topic/topic_insert_article', $data );
			else $this->parser->parse ( 'topic/topic_insert', $data );
			
		
	}
	
	function create_topic(){
		$topic_array = $this->input->post ( 'topic_array' );
		$options_select_index = $this->input->post ( 'options_select_index' );
		$date = date ( "Y-m-d H:i:s" );
		$account_id = $this->config->item ( 'admin_accountIndex' );
		
		$topic_group = (isset($topic_array['topic_group']) && is_array($topic_array['topic_group']))?json_encode($topic_array['topic_group']):'';
		
		$add_input = array (
				'topic' => $topic_array['topic'],
				'topic_optiontitle' => $topic_array['topic_optiontitle'],
				'facebook_imgurl' => $topic_array['facebook_imgurl'],
				'topic_imgurl' => $topic_array['topic_imgurl'],
				'options_imgurl' => $topic_array['options_imgurl'],
				'topic_group'=> $topic_group,
				'options_select'=> $options_select_index,
		);
		$add_options = $topic_array['options'];
		
		$add_input ['create_time'] = $date;
		$add_input ['create_account_index'] = $account_id;
		$add_input ['update_time'] = $date;
		$add_input ['update_account_index'] = $account_id;
		
		$result = $this->topic->check_topic_exist_and_insert($add_input,$add_options);
		
		
	}
	
	
	/**
	 * actions
	 */
	function actions($ac) {
		switch ($ac) {
			case 'edit_pofans' :
				
				$index = $this->input->post ( 'index' );
				$index = 4;
				$data = $this->data;
				
				$this->db->select ( '*' )->from ( 'topic_onshelf' );
				$this->db->where ( 'topic_onshelf.auto_index', $index );
				
				$query = $this->db->get ();
				$result = $query->result_array ();
				if (is_array ( $result )) {
					$data = array_pop ( $result );
					$data ['topic_img'] = substr ( $data ['topic_imgurl'], - 4 );
					$data ['select_img'] = substr ( $data ['options_imgurl'], - 4 );
					
					$this->db->select ( '*' )->from ( 'topic_options' )->where ( 'topic_index', $data ['topic_fromindex'] );
					$query = $this->db->get ();
					
					$data ['topic_options'] = array ();
					$data ['topic_options'] = $query->result_array ();
					$temp_i = 0;
					foreach ( $data ['topic_options'] as $key => $i ) {
						$data ['topic_options'] [$key] ['optionIndex'] = (++ $temp_i);
						$data ['topic_options'] [$key] ['optionImg'] = $data ['topic_options'] [$key] ['optionIndex'] . substr ( $i ['text_imgurl'], - 4 );
					}
				}
				$data ['postfb_group'] = $this->topic->topic_post_group ();
				$data ['postfb_type'] = $this->topic->topic_post_type ();
				
				$postfb_text = $this->topic->topic_post_fb ( $index );
				
				//
				$data ['postfb_list'] = array ();
				$replace_value ['title'] = "<!--[title]-->";
				$replace_value ['link'] = "<!--[href_link]-->";
				
				if (isset ( $postfb_text ) && is_array ( $postfb_text ))
					foreach ( $postfb_text as $key => $value ) {
						
						$postfb ['post_img_choose'] = $value ['post_img_choose'];
						$postfb ['post_index'] = $value ['auto_index'];
						foreach ( $replace_value as $rkey => $rvalue ) {
							$postfb [$rkey] = $this->topic->checkreplace_for_pofans ( $rvalue, $value ['post_text'] );
						}
						$postfb ['post_text'] = $this->topic->getText_for_pofans ( $value ['post_text'], $replace_value );
						$data ['postfb_list'] [( int ) $value ['post_group']] [( int ) $value ['post_type']] = $postfb;
					}
				
				$this->parser->parse ( 'topic/edit_pofans', $data );
				break;
			case 'save_pofans' :
				$date = date ( "Y-m-d H:i:s" );
				$account_id = $this->config->item ( 'admin_accountIndex' );
				
				$index = $this->input->post ( 'index' );
				$autopost = $this->input->post ( 'autopost' );
				$feedback = $this->input->post ( 'feedback' );
				$postfb = $this->input->post ( 'postfb' );
				
				$update_input = array (
						"autopost" => ( int ) $autopost,
						"feedback" => ( int ) $feedback 
				);
				$this->db->flush_cache ();
				$this->db->where ( 'topic_onshelf.auto_index', $index );
				$this->db->update ( 'topic_onshelf', $update_input );
				
				$data ['index'] = $index;
				$data ['postfb'] = $postfb;
				
				$replace_value ['title'] = "<!--[title]-->";
				$replace_value ['link'] = "<!--[href_link]-->";
				
				if (isset ( $postfb ) && is_array ( $postfb ))
					foreach ( $postfb as $key => $value ) {
						
						if (isset ( $value ['title'] ) && $value ['title'] && $value ['text']) {
							$value ['text'] = $replace_value ['title'] . $value ['text'];
						}
						if (isset ( $value ['link'] ) && $value ['link'] && $value ['text']) {
							$value ['text'] = $value ['text'] . $replace_value ['link'];
						}
						
						$add_input = array (
								"auto_index" => ( int ) $value ['index'],
								"topic_onshelfindex" => ( int ) $index,
								"post_group" => ( int ) $value ['group'],
								'post_type' => ( int ) $value ['type'],
								'post_text' => $value ['text'] 
						);
						$add_input ['post_img_choose'] = (isset ( $value ['img_choose'] )) ? ( int ) $value ['img_choose'] : 0;
						
						$add_input ['create_time'] = $date;
						$add_input ['update_time'] = $date;
						$add_input ['create_account_index'] = ( int ) $account_id;
						$add_input ['update_account_index'] = ( int ) $account_id;
						
						if ($value ['index']) {
							unset ( $add_input ['create_time'] );
							unset ( $add_input ['create_account_index'] );
						}
						
						$ppstfb_index = $this->topic->check_postfb_exist_and_insert ( $add_input );
					}
				$data = array (
						'success' => 'Y',
						'msg' => '修改完成' 
				);
				echo json_encode ( $data );
				
				break;
			
			case 'revise_topic' :
				
				$index = $this->input->post ( 'index' );
				if (! $index) {
					echo $this->load->view ( 'purview_error', '' );
					return false;
				}
				
				$this->menu_index = '測驗列表'; //
				$purview = 'O';
				if ($this->admin_model->checkCompetence_getMenuInfo ( $this->menu_index, $purview )) {
					$onshelf_purview = true;
				} else {
					$onshelf_purview = false;
				}
				
				$this->db->select ( 'topic.*,statistics_onshelf.fail_state AS fail_state,topic_select.defaultoptions AS defaultoptions,topic_select.auto_index AS options_select_hide,topic_select.select_name AS options_select_name' )->from ( 'topic' );
				$this->db->join ( "topic_select", 'topic_select.auto_index = topic.options_select' );
				$this->db->join ( "statistics_onshelf", 'statistics_onshelf.topic_index = topic.auto_index', 'left' );
				$this->db->where ( 'topic.auto_index', $index );
				
				$query = $this->db->get ();
				$result = $query->result_array ();
				if (is_array ( $result )) {
					$data = array_pop ( $result );
					
					$data['topic_imgurl'] = 
					(preg_match("/^http/", $data['topic_imgurl']))?$data['topic_imgurl']:(((preg_match("/[0-9]+\/[0-9]+_\S+/", $data['topic_imgurl']))?'../ci_gamesapp/uploads/temp/':'').$data['topic_imgurl']);
					
					$data['options_imgurl'] = 
					(preg_match("/^http/", $data['options_imgurl']))?$data['options_imgurl']:(((preg_match("/[0-9]+\/[0-9]+_\S+/", $data['options_imgurl']))?'../ci_gamesapp/uploads/temp/':'').$data['options_imgurl']);


					
					$data ['onshelf_purview'] = $onshelf_purview;
					$data ['defoption_select'] = $this->topic->topic_defaultselect_pic();
					$data ['defoption_select_list'] = $this->topic->topic_defaultoption_pic();
					$data ['topic_group_list'] = $this->topic->topic_group ();
					$data ['topic_img'] = substr ( $data ['topic_imgurl'], - 4 );
					$data ['select_img'] = substr ( $data ['options_imgurl'], - 4 );
					$data ['topic_select'] = $this->topic->topic_select ();
					$data ['topic_options'] = array ();
					
					$fail_state = json_decode ( $data ['fail_state'], true );
					if (is_array ( $fail_state )) {
						$data ['fail_state'] = array ();
						foreach ( $fail_state as $temp ) {
							foreach ( $temp as $key => $value ) {
								$data ['fail_state'] [$key] = ($value == 'true') ? true : false;
							}
						}
					}
					
					$data ['topic_group_name'] = array ();
					$fans = json_decode ( $data ['topic_group'] );
					
					foreach($data['topic_group_list'] AS $key => $value){
						$data['topic_group_list'][$key]['group_index'] = $value['auto_index'];
						$data['topic_group_list'][$key]['checked'] = ((is_array($fans)) && in_array($value['auto_index'], $fans))?'checked':'';
					}
					
					$this->db->select ( 'auto_index AS st_AI , state_name' )->from ( 'statistics_state' );
					$query = $this->db->get ();
					$data ['statistics_state'] = $query->result_array ();
					
					$this->db->select ( '*' )->from ( 'topic_options' )->where ( 'topic_index', $index );
					$query = $this->db->get ();
					$data ['topic_options'] = $query->result_array ();
					$temp_i = 0;
					foreach ( $data ['topic_options'] as $key => $i ) {
						$data ['topic_options'] [$key] ['optionIndex'] = (++ $temp_i);
						$data ['topic_options'] [$key] ['optionImg'] = $data ['topic_options'] [$key] ['optionIndex'] . substr ( $i ['text_imgurl'], - 4 );
						$data ['topic_options'] [$key] ['AI'] = $i ['auto_index'];
						$data ['topic_options'] [$key] ['text_imgurl'] =
						(preg_match("/^http/", $i['text_imgurl']))?$i['text_imgurl']:(((preg_match("/[0-9]+\/[0-9]+_\S+/", $i['text_imgurl']))?'../ci_gamesapp/uploads/temp/':'').$i['text_imgurl']);
						
						/**/
						$data ['topic_options'] [$key] ['fail_options_p'] = (isset ( $data ['fail_state'] ['fail_options' . $i ['auto_index'] . '_p'] ) && $data ['fail_state'] ['fail_options' . $i ['auto_index'] . '_p']) ? ' not_pass' : '';
						$data ['topic_options'] [$key] ['fail_options_t'] = (isset ( $data ['fail_state'] ['fail_options' . $i ['auto_index'] . '_t'] ) && $data ['fail_state'] ['fail_options' . $i ['auto_index'] . '_t']) ? ' not_pass' : '';
					}
				}
				
				$this->parser->parse ( 'topic/topic_resive', $data );
				
				break;
			
			case 'onshelf_topic' :
				
				$index = $this->input->post ( 'index' );
				if (! $index) {
					echo $this->load->view ( 'purview_error', '' );
					return false;
				}
				
				$this->menu_index = '測驗列表'; //
				$purview = 'O';
				if ($this->admin_model->checkCompetence_getMenuInfo ( $this->menu_index, $purview )) {
					$onshelf_purview = true;
				} else {
					$onshelf_purview = false;
				}
				
				$this->db->select ( 'topic_onshelf.*,statistics_onshelf.fail_state AS fail_state,topic_select.defaultoptions AS defaultoptions,topic_select.auto_index AS options_select_hide,topic_select.select_name AS options_select_name' )->from ( 'topic_onshelf' );
				$this->db->join ( "topic_select", 'topic_select.auto_index = topic_onshelf.options_select' );
				$this->db->join ( "statistics_onshelf", 'statistics_onshelf.topic_index = topic_onshelf.topic_fromindex', 'left' );
				$this->db->where ( 'topic_onshelf.topic_fromindex', $index );
				
				$query = $this->db->get ();
				$result = $query->result_array ();
				if (is_array ( $result )) {
					$data = array_pop ( $result );
					
					$data ['topic_img'] = substr ( $data ['topic_imgurl'], - 4 );
					$data ['select_img'] = substr ( $data ['options_imgurl'], - 4 );
					
					$data ['topic_options'] = array ();
					
					$fans = json_decode ( $data ['topic_group'] );
					
					foreach ( $this->topic->topic_group () as $key => $value ) {
						if ((is_array ( $fans )) && in_array ( $value ['auto_index'], $fans ))
							$data ['topic_group_name'] [] ['name'] = $value ['name'];
					}
					
					$this->db->select ( '*' )->from ( 'topic_options' )->where ( 'topic_index', $index );
					$query = $this->db->get ();
					$data ['topic_options'] = $query->result_array ();
					$temp_i = 0;
					foreach ( $data ['topic_options'] as $key => $i ) {
						$data ['topic_options'] [$key] ['optionIndex'] = (++ $temp_i);
						$data ['topic_options'] [$key] ['optionImg'] = $data ['topic_options'] [$key] ['optionIndex'] . substr ( $i ['text_imgurl'], - 4 );
					}
				}
				
				$this->parser->parse ( 'topic/topic_view', $data );				
				break;
			
			case 'add_topic' :
				
				$index = $this->input->post ( 'index' );
				if (! $index) {
					echo $this->load->view ( 'purview_error', '' );
					return false;
				}
				
				$this->menu_index = '測驗列表'; //
				$purview = 'O';
				if ($this->admin_model->checkCompetence_getMenuInfo ( $this->menu_index, $purview )) {
					$onshelf_purview = true;
				} else {
					$onshelf_purview = false;
				}
				
				$this->db->select ( 'topic.*,statistics_onshelf.fail_state AS fail_state,topic_select.defaultoptions AS defaultoptions,topic_select.auto_index AS options_select_hide,topic_select.select_name AS options_select_name' )->from ( 'topic' );
				$this->db->join ( "topic_select", 'topic_select.auto_index = topic.options_select' );
				$this->db->join ( "statistics_onshelf", 'statistics_onshelf.topic_index = topic.auto_index', 'left' );
				$this->db->where ( 'topic.auto_index', $index );
				
				$query = $this->db->get ();
				$result = $query->result_array ();
				if (is_array ( $result )) {
					$data = array_pop ( $result );
						
					$data ['onshelf_purview'] = $onshelf_purview;
					$data['defoption_select'] = $this->topic->topic_defaultselect_pic();
						
					$data['topic_imgurl'] =
					(preg_match("/^http/", $data['topic_imgurl']))?$data['topic_imgurl']:(((preg_match("/[0-9]+\/[0-9]+_\S+/", $data['topic_imgurl']))?'../ci_gamesapp/uploads/temp/':'').$data['topic_imgurl']);
						
					$data['options_imgurl'] =
					(preg_match("/^http/", $data['options_imgurl']))?$data['options_imgurl']:(((preg_match("/[0-9]+\/[0-9]+_\S+/", $data['options_imgurl']))?'../ci_gamesapp/uploads/temp/':'').$data['options_imgurl']);
						
						
					$data ['topic_group_list'] = $this->topic->topic_group ();
					
					$data ['topic_select'] = $this->topic->topic_select ();
					$data ['topic_options'] = array ();
						
					$fail_state = json_decode ( $data ['fail_state'], true );
					if (is_array ( $fail_state )) {
						$data ['fail_state'] = array ();
						foreach ( $fail_state as $temp ) {
							foreach ( $temp as $key => $value ) {
								$data ['fail_state'] [$key] = ($value == 'true') ? true : false;
							}
						}
					}
						
					$data ['topic_group_name'] = array ();
					$fans = json_decode ( $data ['topic_group'] );
					
					foreach($data['topic_group_list'] AS $key => $value){
						$data['topic_group_list'][$key]['group_index'] = $value['auto_index'];
						$data['topic_group_list'][$key]['checked'] = ((is_array($fans)) && in_array($value['auto_index'], $fans))?'checked':'';
					}
					
					$this->db->select ( 'auto_index AS st_AI , state_name' )->from ( 'statistics_state' );
					$query = $this->db->get ();
					$data ['statistics_state'] = $query->result_array ();
						
					$this->db->select ( '*' )->from ( 'topic_options' )->where ( 'topic_index', $index );
					$query = $this->db->get ();
					$data ['topic_options'] = $query->result_array ();
					$temp_i = 0;
					foreach ( $data ['topic_options'] as $key => $i ) {
						$data ['topic_options'] [$key] ['optionIndex'] = (++ $temp_i);
						$data ['topic_options'] [$key] ['optionImg'] = $data ['topic_options'] [$key] ['optionIndex'] . substr ( $i ['text_imgurl'], - 4 );
						$data ['topic_options'] [$key] ['AI'] = $i ['auto_index'];
						$data ['topic_options'] [$key] ['text_imgurl'] =
						(preg_match("/^http/", $i['text_imgurl']))?$i['text_imgurl']:(((preg_match("/[0-9]+\/[0-9]+_\S+/", $i['text_imgurl']))?'../ci_gamesapp/uploads/temp/':'').$i['text_imgurl']);
						$data ['topic_options'] [$key] ['fail_options_p'] = (isset ( $data ['fail_state'] ['fail_options' . $i ['auto_index'] . '_p'] ) && $data ['fail_state'] ['fail_options' . $i ['auto_index'] . '_p']) ? ' not_pass' : '';
						$data ['topic_options'] [$key] ['fail_options_t'] = (isset ( $data ['fail_state'] ['fail_options' . $i ['auto_index'] . '_t'] ) && $data ['fail_state'] ['fail_options' . $i ['auto_index'] . '_t']) ? ' not_pass' : '';
					}
				}
				
				$this->parser->parse ( 'topic/topic_edit', $data );
				break;
			case 'add_group' :
				
				$data = $this->data;
				
				$data ['competence'] = $this->config->item ( 'admin_group' );
				$data ['admin_competence'] = $this->config->item ( 'admin_competence' );
				$data ['fans_group'] = $this->admin_model->get_fansgroupdefault ();
				
				$data ['group_defaultstatus'] = $this->config->item ( 'admin_competencestatus' );
				
				echo $this->load->view ( 'admin/fans_groupedit', $data, true );
				break;
			case 'validate' :
				$this->menu_index = '測驗列表'; //
				if (! $this->admin_model->checkCompetence_getMenuInfo ( $this->menu_index, 'E' )) {
					$result = array (
							'success' => 'N',
							'msg' => '權限不足' 
					);
					echo json_encode ( $result );
					break;
				}
				
				$index = ( int ) $this->input->post ( 'auto_index' );
				
				$date = date ( "Y-m-d H:i:s" );
				$account_id = $this->config->item ( 'admin_accountIndex' );
				
				$add_options = $this->input->post ( "options" );
				
				$add_input = array (
						"topic" => '',
						'topic_imgurl' => '',
						'options_imgurl' => '',
						'topic_optiontitle' => '',
						'topic_group' => '',
						'options_select' => 0 
				);
				
				foreach ( $add_input as $key => $value ) {
					if (! $value) {
						switch ($key) {
							case "topic_group" :
								$add_input [$key] = json_encode ( $this->input->post ( "topic_group" ), true );
								break;
							default :
								$add_input [$key] = $this->input->post ( $key );
								break;
						}
					}
				}
				
				if ($index) {
					$add_input ['update_time'] = $date;
					$add_input ['update_account_index'] = $account_id;
					$result = $this->topic->check_topic_exist_and_update ( $add_input, $add_options, $index );
				} else {
					$ifunso_test_fk = ( int ) $this->input->post ( 'ifunso_test_fk' );
					$horoscope_fk = ( int ) $this->input->post ( 'horoscope_fk' );
					$knoledge_fk = ( int ) $this->input->post ( 'knoledge_fk' );
					
					$add_input ['create_time'] = $date;
					$add_input ['update_time'] = $date;
					$add_input ['create_account_index'] = $account_id;
					$add_input ['update_account_index'] = $account_id;
					
					$result = $this->topic->check_topic_exist_and_insert ( $add_input, $add_options );
					if ($result ['success'] == 'Y') {
						if ($ifunso_test_fk) {
							$ifunso_test = array (
									'tests_question_fk' => $ifunso_test_fk,
									'topic_index' => $result ['msg'] 
							);
							$this->db->insert ( 'ifunso_test', $ifunso_test );
						}
						if ($horoscope_fk) {
							$data = array (
									'online' => '2',
									'topic_index' => $result ['msg'] 
							);
							$this->db->where ( 'horoscope.auto_index', $horoscope_fk );
							$this->db->update ( 'horoscope', $data );
						}
						if ($knoledge_fk) {
							$data = array (
									'online' => '2',
									'topic_index' => $result ['msg'] 
							);
							$this->db->where ( 'knoledge_temp.article_id', $knoledge_fk );
							$this->db->update ( 'knoledge_temp', $data );
						}
						$result ['msg'] = '新增成功';
					}
				}
				header ( 'Content-type: text/plain; charset=utf-8' );
				echo json_encode ( $result );
				
				break;
			
			case 'validate_onshelf' :
				$this->menu_index = '測驗列表'; //
				if (! $this->admin_model->checkCompetence_getMenuInfo ( $this->menu_index, 'O' )) {
					$result = array (
							'success' => 'N',
							'msg' => '權限不足' 
					);
					echo json_encode ( $result );
					break;
				}
				
				$index = $this->input->post ( 'auto_index' );
				$plus_state = json_encode ( $this->input->post ( "plus_state" ), true );				
				
				
				$result = $this->topic->onshelf_topic_by_index ( $index );
				
				
				
				$this->db->flush_cache ();
				$query = $this->db->select ( "auto_index" )->from ( "statistics_onshelf" )->where ( "topic_index", $index )->where ( "online", '0' )->get ();
				$temp = $query->row_array ();
				
				$add_input = array (
						'topic_index' => $index,
						'online' => '1',
						'plus_state' => $plus_state,
						'fail_state' => Null 
				);
				$date = date ( "Y-m-d H:i:s" );
				$account_id = $this->config->item ( 'admin_accountIndex' );
				
				if (isset ( $temp ['auto_index'] ) && $temp ['auto_index'] > 0) {
					
					$add_input ['update_time'] = $date;
					$add_input ['update_account_index'] = $account_id;
					
					$this->db->where ( 'auto_index', $temp ['auto_index'] );
					$this->db->update ( 'statistics_onshelf', $add_input );
					$onshelf_index = $temp ['auto_index'];
				} else {
					
					$add_input ['create_time'] = $date;
					$add_input ['update_time'] = $date;
					$add_input ['create_account_index'] = $account_id;
					$add_input ['update_account_index'] = $account_id;
					
					$this->db->insert ( 'statistics_onshelf', $add_input );
					$onshelf_index = $this->db->insert_id ();
				}
				
				
				
				echo json_encode ( $result );
				break;
			case 'del' :
				$this->menu_index = '測驗列表'; //
				if (! $this->admin_model->checkCompetence_getMenuInfo ( $this->menu_index, 'D' )) {
					$result = array (
							'success' => 'N',
							'msg' => '權限不足' 
					);
					echo json_encode ( $result );
					break;
				}
				
				$index = $this->input->post ( 'index' );
				$result = $this->topic->del_topic_by_index ( $index );
				
				header ( 'Content-type: text/plain; charset=utf-8' );
				echo json_encode ( $result );
				break;
		}
	}
}