	/*	
	window.fbAsyncInit = function() {
		    FB.init({
		      appId      : '588005798009398',
		      xfbml      : true,
		      version    : 'v2.3'
		    });
		    
		    
		  };

		  (function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/zh_TW/all.js#xfbml=1&appId=588005798009398";
			  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
/**/

window.fbAsyncInit = function() {
	FB.Event.subscribe('edge.create', function(response,fb_el) {	
		FB.getLoginStatus(function(response){
	       if (response.status === 'connected')
	       {
	           var userId =  response.authResponse.userID;	           
	           if(typeof(userId) !='undefined') $.cookie("likes"+userId, 'done', { path:'/', expires: 1 });
	           console.info(userId);
	       }
	    })
		
		if($(fb_el).attr("id") == 'lightbox_fb' ) {							
			//$('.scroll-lightbox2, .lightbox-backdrop').hide();
		}
			return false;					
												     
	});
	FB.Event.subscribe('edge.remove', function(response) {														
			return false; 													     
	    });
	
	FB.Event.subscribe('comment.create', function(response) {
		
		var commentID = response['commentID'];
		var id = $('#comments').attr('data-id');
		
		if ( !isNaN(response.commentID) ) {
			console.info(id);
			$.cookie('compose'+id, commentID, { path:'/', expires: 1 });
			location.reload();
			console.info(commentID);
		}
		
    });
};
	
/* 抓fb資料用 */
  function get_fb_public_data(fb_id, field, callback){
	$.getJSON('https://graph.facebook.com/' + fb_id + '?callback=?', function(r,s){
		var return_value = "";
		if(typeof field == "undefined"){
			return_value ="未指定目標值";
		} else {
			if (s == "success" && !r.error) {
				for (p in r) {
					if(p == field) 
					return_value = r[p];
				} 
			} else {
				return_value = "error";
			}
		}
			
		 if(typeof(callback)=='function'){   
			callback(return_value);   
		} else {
			return return_value;
		}
	});
  }     
  
  $(document).ready(function() {

	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-63740052-1', 'auto');
		  ga('send', 'pageview');
	});