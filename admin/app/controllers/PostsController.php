<?php
class PostsController extends \Phalcon\Mvc\Controller
{

	public $settings;
	
	public function initialize()
	{
		$this->settings = array(
				"mySetting" => "value"
		);
	}
	
	public function onConstruct()
	{
		
	}
	

	public function notFoundAction()
	{
		// Send a HTTP 404 response header
		$this->response->setStatusCode(404, "Not Found");
	}
	
	public function beforeExecuteRoute($dispatcher)
	{
		
		
		// This is executed before every found action
		if ($dispatcher->getActionName() == 'save') {
	
			$this->flash->error("You don't have permission to save posts");
	
			$this->dispatcher->forward(array(
					'controller' => 'home',
					'action' => 'index'
			));
	
			return false;
		}
	}
	
	public function afterExecuteRoute($dispatcher)
	{
		// Executed after every found action
	}
	

	public function indexAction()
	{
		
		$AdminAccount = AdminAccount::find();
// 		// Traversing with a foreach
// 		foreach ($AdminAccount as $robot) {
// 			echo $robot->id, " - ";
// 			echo $robot->memo, " - ";
// 			echo $robot->AdminGroup->group_name, " - ";
// 			echo $robot->AdminGroup->group_defaultstatus, "<br/>";
// 		}
		
		
        $this->view->post = $AdminAccount;
        
        
	}
}
?>