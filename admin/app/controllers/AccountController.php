<?php
class AccountController extends BaseController
{

	public $settings;
	
	public function initialize()
	{
	
		if ($this->request->isPost()) {
		
			$this->order_by = $this->request->getPost('order_by');
			
		}
	
	}
	
	
	public function notFoundAction()
	{
		
	}
	
	public function indexAction()
	{
		$this->action = 'list';
		$this->listAction();
		return;
	}
	
	function listAction($viewType = false){
		$orderBy = $this->order_by($this->order_by,'AdminAccount');
		
		
		$content = AdminAccount::find();
		
		if($content != false) $content = $content->toArray();
		
		$viewContent['content'] = $content;
		if($viewType == 'json') return json_encode($content,true);
		
		$view = new contentView();
		$main_content = $view->getRender($this->controller, $this->action,$viewContent);
		
		$this->template = array(
				"main_content" => $main_content,
		);
	
	}
	function listjsonAction($index = false){
		echo $this->listAction('json',$index);
		exit;
	}
	
	function groupjsonAction($index = false){
		echo $this->groupAction('json',$index);
		exit;
	}
	
	
	
	function groupdefaultjsonAction($index = false){
		
		$group = $this->groupAction('array',$index);		
		echo $group['group_default'];
		exit;
	}
	
	function grouplistAction(){	
		$this->action = 'group';
		$this->groupAction();
		
	}
	
	function groupAction($viewType = false,$index = false){
		
		if($index) $content = AdminGroup::findFirst("auto_index=".$index);
		else $content = AdminGroup::find();
		$viewContent['content'] = $content->toArray();
		
		if($viewType == 'json') return json_encode($viewContent['content'],true);
		if($viewType == 'array') return $viewContent['content'];
		$view = new contentView('./app/views');
		$main_content = $view->getRender($this->controller, $this->action,$viewContent);
		

		$this->template = array(
			"main_content" => $main_content,
		);
		
	}
	
	function actionsAction($action = false) {
		
		if ($this->request->isPost()) {
			$Login = $this->session->get('userInfo')->toArray();
				
			$date = date("Y-m-d H:i:s");
			$account_id = $Login['auto_index'];
			
			
			
			if($action == 'edit_password'){
				$index = $this->request->getPost('index');
				$content = AdminAccount::findFirst("auto_index = '".$index."'");
				
				if($content != false){
						
						
					$content =$content->toArray();
						
					$this->view->account_info = $content;
						
				}
				
				echo  $this->view->start()->render('account', 'editPw')->finish()->getContent();
				
			}

			if($action == 'add_account') {
				
				$index = $this->request->getPost('index');
				
				$AdminMenu = AdminMenu::find()->toArray();
				$competence = AdminGroup::find()->toArray();
					
				$this->view->competence = $competence;
				$this->view->admin_competence = array('A'=>'新增','D'=>'刪除','E'=>'修改','V'=>'觀看','O'=>'上下架');
				$this->view->AdminMenu = $AdminMenu;
				
				
				$content = AdminAccount::findFirst("auto_index = '".$index."'");
				
				if($content != false){
					
					
					$content =$content->toArray();
					$content['account_competence'] = json_decode($content['competence'],true);					
					
					$this->view->account_info = $content;
					
				}
				
				echo  $this->view->start()->render('account', 'edit')->finish()->getContent();
					
			}
			
			if($action == 'validate_password'){
				
				$index = $this->request->getPost('auto_index');
				$pw =  $this->request->getPost('pw');
				$confim_pw =  $this->request->getPost('confim_pw');
				$content = AdminAccount::findFirst("auto_index = '".$index."'");
				
				if($content != false && $pw == $confim_pw) {
					$add_input['pw'] = md5($pw);
					$add_input['create_time'] = $date;
					$add_input['update_time'] = $date;
					$add_input['create_account_index'] = $account_id;
					$add_input['update_account_index'] = $account_id;
					
					$content->update($add_input);
				}
			}
			
			if($action == 'validate') {
				
				$index = $this->request->getPost('auto_index');
				
				$online = '1';
				$add_input = array("id"=>'','pw'=>'','admingroup_index'=>0,'competence'=>'','online'=>$online,'memo'=>'');
					
				foreach ($add_input AS $key => $value){
					if(!$value){
						switch ($key){
							case "competence":
								$add_input[$key] = json_encode( $this->request->getPost('competence'),true);
								break;
							default:
								$add_input[$key] = $this->request->getPost($key);
								break;
						}
					}
				}
				
				$content = AdminAccount::findFirst("auto_index = '".$index."'");
			
				if($content != false){
					unset($add_input['pw']);
					
					$add_input['update_time'] = $date;
					$add_input['update_account_index'] = $account_id;
					
					$content->update($add_input);
				}else{
					$add_input['pw'] = md5($add_input['pw']);
					$add_input['create_time'] = $date;
					$add_input['update_time'] = $date;
					$add_input['create_account_index'] = $account_id;
					$add_input['update_account_index'] = $account_id;					
					
					$content = new AdminAccount();					
					$insert_check = $content->create($add_input);
					
					if(!$insert_check) echo 'Error Insert';
					
				}
					
			}
				
		}
		
		
		
		

	}
	
}
?>