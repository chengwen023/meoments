<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Horoscope extends CI_Controller {
	
	var $menu_index;
	
	
	public function __construct() {
		parent::__construct ();
		
		
		$this->load->helper ( 'url' );
		$this->userInfo = $this->session->userdata ( 'userInfo' );
		$this->load->model ( 'topic_model', 'topic' );
		$this->load->model ( 'theme_model', 'theme' );
		
		$this->data ['admin_menu'] = $this->config->item ( 'admin_menu' );
		$this->data ['admin_menuGroup'] = $this->config->item ( 'admin_menuGroup' );
		$this->data ['userInfo'] = $this->userInfo;
		$this->data ['menu'] = $this->config->item ( 'admin_accountMenu' );
		$this->data ['breadcrumbs'] = $this->config->item ( 'breadcrumbs' );
		$this->data ['title'] = $this->config->item ( 'admin_title' );
		
		$this->data ['topbar_control'] = array ();
		$this->data ['select_result'] = array ();
	}
	/*
	* index
	*/
	public function index(){		
		$this->lists();
	}
	
	/**
	 *	list
	 */
	function lists($pg = 1){
		
		$page_one = 20;
		$topSearch = $this->input->post ( 'topSearch' );
		$order_name = $this->input->post ( 'order_by' );
		$orderBy = $this->theme->order_by ( $order_name ,'topic_list');
		$data = array_merge ( $this->data, $orderBy );
		
		if ($pg || $user)
			$this->menu_index = '測驗列表'; //

		$data = $this->data;
		$this->menu_index = '星座運勢題目'; //
		if (false === $activestatus = $this->admin_model->checkCompetence_getMenuInfo ( $this->menu_index, 'V' )) {
			$this->parser->parse ( 'template', $data );
			return false;
		}
		
		$defaultstatus = (isset ( $activestatus )) ? $activestatus : $this->config->item ( 'defaultstatus' );
		
		if ($this->menu_index)
			$data ['breadcrumbs'] = $this->config->item ( 'breadcrumbs' );
		
		$data['title'] = $this->admin_model->getTitle($this->menu_index);
		
		
		$this->db->flush_cache();
		$this->db->select("*");
		$this->db->from("horoscope");
		$this->db->where("horoscope.online",'1');
		$this->db->order_by('auto_index','ASC');
		
		switch($this->userInfo['id']){
			case 'user22':
				$this->db->where('horoscope.auto_index >', 300);
				$this->db->where('horoscope.auto_index <=', 1107); break;
			case 'user23':
				$this->db->where('horoscope.auto_index >', 1107);
				$this->db->where('horoscope.auto_index <=', 1221); break;			
			case 'user_star':
				$this->db->where('horoscope.auto_index >', 1221);
				break;
			default:
				$this->db->where('horoscope.auto_index <=', 300);
				break;
		}
		
		$total_query = clone $this->db;
		
		$page ['total_page'] = $total_query->get ()->num_rows;
		
		$this->db->limit ( $page_one, ($pg - 1) * $page_one );
		$query = $this->db->get();
		
		$data['content'] = $query->result_array();
		foreach ($data['content'] AS $key => $val){
		
			$data['content'][$key]['id'] = (int) $val['auto_index'];
			$data['content'][$key]['horo_title'] = $val['title'];
		}
		$page ['start'] = 1;
		$page ['end'] = ceil ( $page ['total_page'] / $page_one );
		$page ['url'] = 'horoscope/lists';
		$page ['page_num'] = $pg;
		$page ['url_end'] = '';
		
		$data ['page_list'] = $this->theme->page ( $page );
		

		
		$data['main_content'] = $this->parser->parse('horoscope/horoscope_list', $data, true);
		$this->parser->parse('template', $data);
		
	}
	

	//嵌入
	function insert_topic(){
		$index = $this->input->post ( 'index' );
	
		if (! $index) {
			echo $this->load->view ( 'purview_error', '' );
			return false;
		}
	
		$this->menu_index = '測驗列表'; //
		$purview = 'A';
		if ($this->admin_model->checkCompetence_getMenuInfo ( $this->menu_index, $purview )) {
			$onshelf_purview = true;
		} else {
			$onshelf_purview = false;
		}
	
		$this->db->select ( 'auto_index,title as horo_title, text as horo_temp' )->from ( 'horoscope' );
		$this->db->where ( 'horoscope.auto_index', $index );
		$this->db->where ( 'horoscope.topic_index', 0 );
	
		$query = $this->db->get ();
		$result = $query->result_array ();
		if (is_array ( $result )) {
			$data = array_pop ( $result );
	
	
			$data['defoption_select'] = $this->topic->topic_defaultselect_pic();
	
			$data ['auto_index'] = (int) $data ['auto_index'];
			$data ['topic'] = $data ['horo_title'];
			$data ['topic_optiontitle'] = '';
			$data ['topic_img'] = '';
			$data ['select_img'] = '';
			$topic_select = $this->topic->topic_select(3);
				
			$data ['options_select_defname'] = $topic_select['select_name'];
			$data ['options_select_defindex'] = $topic_select['auto_index'];
			
			
			$topic_select = $this->topic->topic_select(2);
			$data ['options_select_name'] = $topic_select['select_name'];
			$data ['options_select_index'] = $topic_select['auto_index'];
			$data ['topic_group_list'] = $this->topic->topic_group ();
	
			$data ['topic_select'] = $this->topic->topic_select ();
			$data ['topic_options'] = array ();
			$data ['horoscope_index'] = $index;
	
	
			$data ['topic_group_name'] = array ();
	
	
			foreach($data['topic_group_list'] AS $key => $value){
				$data['topic_group_list'][$key]['group_index'] = $value['auto_index'];
	
			}
	
			$this->db->select ( 'auto_index AS st_AI , state_name' )->from ( 'statistics_state' );
			$query = $this->db->get ();
			$data ['statistics_state'] = $query->result_array ();
	
			$this->db->flush_cache();
			$this->db->select("*")->from("topic_defaultoptions")->where("select_index",3);
			$query = $this->db->get();
			
			$data['topic_options'] = $query->result_array();
			$temp_i = 0;
			foreach ( $data ['topic_options'] as $key => $i ) {
				$data ['topic_options'] [$key] ['optionIndex'] = (++ $temp_i);
				$data ['topic_options'] [$key] ['text_imgurl'] = '';
				$data ['topic_options'] [$key] ['AI'] = $data ['auto_index'];
				$data ['topic_options'] [$key] ['choose'] = $i ['option_name'];;
				$data ['topic_options'] [$key] ['title'] = $i ['option_name'];
				$data ['topic_options'] [$key] ['text'] = $data ['horo_temp'];
	
			}
		}
	
		$this->parser->parse ( 'horoscope/topic_horoscope', $data );
	
	}
	
	
	
	//新增
	function add_topic(){
		$topic_array = $this->input->post ( 'topic_array' );
		$horoscope_index = $this->input->post ( 'horoscope_index' );
		$date = date ( "Y-m-d H:i:s" );
		$account_id = $this->config->item ( 'admin_accountIndex' );
	
		$topic_group = (isset($topic_array['topic_group']) && is_array($topic_array['topic_group']))?json_encode($topic_array['topic_group']):'';
	
		$add_input = array (
				'topic' => $topic_array['topic'],
				'topic_optiontitle' => $topic_array['topic_optiontitle'],
				'facebook_imgurl' => $topic_array['facebook_imgurl'],
				'topic_imgurl' => $topic_array['topic_imgurl'],
				'options_imgurl' => $topic_array['options_imgurl'],
				'topic_group'=> $topic_group,
				'options_select'=> $topic_array['options_select'],
		);
		$add_options = $topic_array['options'];
	
		$add_input ['create_time'] = $date;
		$add_input ['create_account_index'] = $account_id;
		$add_input ['update_time'] = $date;
		$add_input ['update_account_index'] = $account_id;
	
	
		$this->load->model ( 'topic_model', 'topic' );
		$result = $this->topic->check_topic_exist_and_insert($add_input,$add_options);
	
		var_dump($result);
		if($result['msg'] > 0){
			$update_input = array('topic_index' =>$result['msg'],'online' =>'2');
			$this->db->where('auto_index', $horoscope_index);
			$this->db->where('topic_index = 0');
			$this->db->update('horoscope', $update_input);
				
		}
	}
	
	function remove_data(){
	
		$result = array('success' => 'Y', 'msg' => '');
	
		$horoscope_fk = $this->input->post('index');
		$this->db->flush_cache();
		$data = array('online'=>'0');
		$this->db->where('horoscope.auto_index', $horoscope_fk);
		$this->db->update('horoscope', $data);
		echo json_encode($result);
	}
}