<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/zh_TW/all.js#xfbml=1&version=v2.1&appId=806536466070254";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));	
	</script>
<script src="js/tips.js"></script>
<style>

/*
* Tips CSS
*/
.tips_box{	
	font-size: 16px;	
	padding: 5px 10px;
	background: #fff;
	box-shadow: 1px 1px 2px 1px rgba(33,66,99,.5);
	border-radius:5px;
	min-width: 350px;
}


.arrow_box {	
	background: #ffffff;
	border: 4px solid #eb9c8d;
}
.arrow_box:after, .arrow_box:before {	
	left: 50%;
	border: solid transparent;
	content: " ";
	height: 0;
	width: 0;
	position: absolute;
	pointer-events: none;
}

.arrow_box:after {
	border-color: rgba(255, 255, 255, 0);	
	border-width: 10px;
	margin-left: -10px;
}
.arrow_box:before {
	border-color: rgba(194, 225, 245, 0);	
	border-width: 16px;
	margin-left: -16px;
}

.arrow_box.arrow_right:after, .arrow_box.arrow_right:before{
	left: 95%;
}
.arrow_box.arrow_left:after, .arrow_box.arrow_left:before{
	left: 5%;
}
.arrow_box.top:before {
	border-top-color: #eb9c8d;
}
.arrow_box.top:after{
	border-top-color: #ffffff;
}
.arrow_box.top:after, .arrow_box.top:before {
	top: 100%;	
}
.arrow_box.bottom:after{
	border-bottom-color: #ffffff;
}
.arrow_box.bottom:before {
	border-bottom-color: #eb9c8d;
}
.arrow_box.bottom:after, .arrow_box.bottom:before {
	bottom: 100%;	
}


</style>
<div class="widget">
	<table cellpadding="0" cellspacing="0" width="100%" class="tDefault tLight" style="table-layout: fixed;">
		<form id="list_orderby" method="post">
			<input id="order_by" type="hidden" name="order_by" value="">
			<thead>
				<tr>
					<td title="索引" class="order_control" data-orderby="auto_index">索引<i class="icon-chevron-{by-auto_index}"></i></td>
					<td title="粉絲團" class="order_control" data-orderby="fans_id">粉絲團<i class="icon-chevron-{by-fans_id}"></i></td>
					<td title="發佈類型" class="order_control" data-orderby="post_type">發佈類型<i class="icon-chevron-{by-post_type}"></i></td>
					<td width="320px" title="訊息" class="overhide_td">訊息</td>					
					<td title="發佈時間" >發佈時間</td>
					<td title="發佈截止時間" >發佈截止時間</td>
					<td title="刪除" >刪除</td>
				</tr>
			</thead>
		</form>
		<tbody>
			{content}
			<tr class="user_row" >
				<td title="索引">{auto_index}</td>
				<td title="粉絲團" style="white-space:nowrap;"><a href="http://www.facebook.com/{page_id}" target="_blank">{page_id}</a></td>
				<td title="發佈類型">{post_type}</td>
				<td title="訊息" >
					<a  href="http://www.facebook.com/{post_fb_id}" target="_blank"><span class="overhide">{message}</span></a>
					<span class="tips_fb" data-fb_id="{post_fb_id}">預覽</span>
				</td>
				<td title="發佈時間" >{post_time}</td>
				<td title="發佈截止時間" >{published_time}</td>
				<td>
					<a href="javascript:void(0);" class="tablectrl_small bRed " original-title="Del" title="刪除" alt="刪除" onclick="fans.del_published({auto_index});">
					<span class="iconb" data-icon="" ></span></a>	
				</td>
			</tr>
			{/content}
		</tbody>
	</table>
</div>
{page_list}

<script src="js/pofans_fans.js"></script>
<style>

.tips_fb{
	float:right;
}


.tLight tbody td{
	padding: 0px 16px;
}
SPAN.overhide{
	display: inline-block;
	max-width: 300px;
	width: 300px;
  overflow : hidden;
  text-overflow : ellipsis;
  white-space : nowrap;
  max-height: 50px;
  

}
#pofans_batlog li.sending:AFTER{
		content:'Sending';
		float: right;
}
#pofans_batlog li.error:AFTER{
	content:'Error';
	float: right;
}
.pofans {
	position: absolute;
	display: none;
	background-color: rgba(21, 21, 21, 0.6);
	padding: 10px;
	height: 430px;
	border-radius: 10px;
}

.pofans.focus {
	display: block;
	right: 250px;
	margin-top: -20px;
	color: #fff;
}

.pofans TEXTAREA {
	width: 400px;
	overflow: auto;
}

.pofans .send_btn {
	float: right;
	margin-bottom: 10px;
}

.external_link {
	background: none;
}

.more_page {
	text-align: center;
}

.more_page SPAN,.pofans .send_btn {
	-moz-box-shadow: inset 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow: inset 0px 1px 0px 0px #ffffff;
	box-shadow: inset 0px 1px 0px 0px #ffffff;
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ededed
		), color-stop(1, #dfdfdf));
	background: -moz-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
	background: -webkit-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
	background: -o-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
	background: -ms-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
	background: linear-gradient(to bottom, #ededed 5%, #dfdfdf 100%);
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed',
		endColorstr='#dfdfdf', GradientType=0);
	background-color: #ededed;
	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;
	border-radius: 6px;
	border: 1px solid #dcdcdc;
	display: inline-block;
	cursor: pointer;
	color: #777777;
	font-family: arial;
	font-size: 15px;
	font-weight: bold;
	padding: 6px 14px;
	text-decoration: none;
	text-shadow: 0px 1px 0px #ffffff;
}

.more_page SPAN.page_selected {
	color: #c92200;
	text-shadow: 0px 1px 0px #ded17c;
}

.more_page SPAN:hover {
	color: #ffffff;
	text-shadow: 0px 1px 0px #528ecc;
	-moz-box-shadow: inset 0px 1px 0px 0px #bbdaf7;
	-webkit-box-shadow: inset 0px 1px 0px 0px #bbdaf7;
	box-shadow: inset 0px 1px 0px 0px #bbdaf7;
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #378de5
		), color-stop(1, #79bbff));
	background: -moz-linear-gradient(top, #378de5 5%, #79bbff 100%);
	background: -webkit-linear-gradient(top, #378de5 5%, #79bbff 100%);
	background: -o-linear-gradient(top, #378de5 5%, #79bbff 100%);
	background: -ms-linear-gradient(top, #378de5 5%, #79bbff 100%);
	background: linear-gradient(to bottom, #378de5 5%, #79bbff 100%);
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#378de5',
		endColorstr='#79bbff', GradientType=0);
	background-color: #378de5;
	border: 1px solid #84bbf3;
}

.more_page SPAN:active,.pofans .send_btn {
	position: relative;
	top: 1px;
}
</style>

<script>
$(function() {
	var config = {			
			filed_name : ['data-title','tips'],	
			class_Name : 'tips_box',
			tipbox_name: "tip_box",		
	}

	tips = new Tips(config);
	
});
$('span.tips_fb').hover(function(){
	$('#OnceTips').remove();
	var id = $(this).attr('data-fb_id');
	var focus_el = $(this);
	
	FB.api('/'+id, function(response) {
		console.info(response);
		var name = response.name||response.message;
		var description = response.description||response.message;
		var link =response.link;			
		var view = name+'<br/>'+description+'<br/>'+link;

		var img = response.source||response.picture;
		img = '<img src="'+img+'"/>';
		
		view = '<div>'+img+view+'</div>'
		
		oncetips = tips.onceTips(focus_el,view,5000,'tips_box arrow_box');
	});
},function(){
	$(oncetips).remove();
})



function order_by(this_th){

	$('#order_by').val( $(this_th).attr('data-orderby'));

	$('#list_orderby').submit();

}

$(".order_control").live( "click", function() {
	var order_name = $(this).attr('data-orderby');
	if(order_name) order_by(this);
	
	return false;
});	
</script>