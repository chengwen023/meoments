<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *	Adonis
 *	後台預設資料讀取
 */

class News_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}

	function get_fans_array($language) {
		$this->db->flush_cache ();
		$this->db->select ("fb_group_id, fb_sitename, avg_likes_first_day, avg_likes_second_day, avg_likes, avg_likes_0, avg_likes_1, avg_likes_2, avg_likes_3, avg_likes_4, avg_likes_5");
		$this->db->from ("facebook_post_source_list");
		$this->db->where('is_enable', 1);
		$this->db->where('language', $language);
		$query = $this->db->get();
		$result = $query->result_array();
	
		return $result;
	}

	function get_fans_name_array() {
		$this->db->flush_cache ();
		$this->db->select ("fb_group_id, fb_sitename");
		$this->db->from ("facebook_post_source_list");
		$query = $this->db->get();
		$result = $query->result_array();

		foreach ($result as $key => $value) {
			$name_array[ $value["fb_group_id"] ] = $value["fb_sitename"];
		}
	
		return $name_array;
	}

	function get_categories_array() {
		$this->db->flush_cache();
		$this->db->select('id, name')->from('categories');
		$query = $this->db->get();
		$result = $query->result_array();

		foreach ($result as $key => $value) {
			$name_array[ $value["id"] ] = $value["name"];
		}
	
		return $name_array;
	}
	
	function order_by($order_name,$order_page = '') {
		$order_by = $this->session->userdata($order_page.'-order_by');
		
		if(isset($order_name) && $order_name){
			if(isset($order_by['by-'.$order_name]) && $order_by['by-'.$order_name] != 'down') $order_by['by-'.$order_name] = "down";
			else {
				unset($order_by);
				$order_by['by-'.$order_name] = "up";
			}
			$this->session->set_userdata($order_page.'-order_by', $order_by);
		}
	
		return (is_array($order_by))?$order_by:array();
	}

	function news_categories($index='', $field='*', $select_type=true) {
		$this->db->flush_cache();
		$this->db->select($field, $select_type)->from('categories')->where('online', '1');
		if($index){
			$this->db->where('auto_index',$index);
		}
	
		$query = $this->db->get();
		$data = $query->result_array();
	
		if($index && is_array($data)) $data = array_pop($data);
		return $data;
	}

	function news_tags($index='', $field='*', $select_type=true) {
		$this->db->flush_cache();
		$this->db->select($field, $select_type)->from('news_tags')->where('online', '1');
		if($index){
			$this->db->where('auto_index', $index);
		}
	
		$query = $this->db->get();
		$data = $query->result_array();
	
		if($index && is_array($data)) $data = array_pop($data);
		return $data;
	}

	function check_field($table_name, $field_name) {
		$query = $this->db->query("select count(b.column_name)
							from information_schema.columns as b 
							where b.table_schema='toments_news'
							and b.table_name='".$table_name.
							"' and b.column_name='".$field_name."'");
		
		return $query->result_array()[0]["count(b.column_name)"]; 
	}
	
	function check_article_exist_and_insert($add_input, $fb_id, $source_id, $google_ad, $comment_good, $comment_bad, $pofans_mode, $source_mode) {
		$result = array('success' => 'N', 'msg' => '');
		
		//檢查題目是否重複
		$this->db->flush_cache();
		$query = $this->db->select("COUNT(*) AS qun")
		->from("news_article")
		->where("title", $add_input['title'])
		->get();
		$temp = $query->row_array();
	
		if($temp['qun'] == 0){
			$this->db->flush_cache();
			$this->db->insert('news_article', $add_input);
			$article_index = $this->db->insert_id();

			if ( !empty($fb_id) ) {
				$this->db->flush_cache();
				$this->db->where('id', $fb_id);
				$this->db->update('facebook_post_content', array('article_id' => $article_index));

				$this->db->flush_cache();
				$this->db->where('id', $fb_id);
				$this->db->update('facebook_post_statistics', array('article_id' => $article_index));
			}
			
			// $this->db->flush_cache();
			// $this->db->where('id', $source_id);
			// $this->db->update('source_news_content', array('lock' => 1));

			$add_input_state = array(
						'id'          => $article_index,
						'fb_id'       => $fb_id,
						//'source_id' => $source_id,
						'pofans_mode' => $pofans_mode,
						'source_mode' => $source_mode,
						'google_ad'   => $google_ad,
						'category'    => $add_input['category'],
						'tag'         => $add_input['tag'],
						'author_id'   => $add_input['create_user_id']
			);
			$this->db->flush_cache();
			$this->db->insert('news_article_state', $add_input_state);

			if ( !empty($comment_good) ) {
				$this->insert_facebook_comments_onshelf($comment_good, 2, $article_index);
			}
			
			if ( !empty($comment_bad) ) {
				$this->insert_facebook_comments_onshelf($comment_bad, 1, $article_index);
			}

			$this->caculate_comments($article_index);

			$result['success'] = 'Y';
			$result['index'] = $article_index;
		}else{
			$result['msg'] = '此題目已經有了哦!!!';
		}

		return $result;
	}

	function check_article_exist_and_update($add_input, $index, $google_ad, $comment_good, $comment_bad, $fb_id, $pofans_mode, $source_mode){
		$result = array('success' => 'N', 'msg' => '');

		$this->db->flush_cache();
		$this->db->where('id', $index);
		$this->db->update('news_article_state', array('action' => 4, 'action_time' => 0));
		
		$this->db->flush_cache();
		$query = $this->db->select("COUNT(*) AS qun")
		->from("news_article")
		->where('id', $index)
		->get();
		$temp = $query->row_array();
	
		if($temp['qun'] == 0){
			$result['msg'] = '無該筆資料';
		}else{
		
			$this->db->flush_cache();
			$this->db->where('id', $index);
			$this->db->update('news_article', $add_input);
					
			// $article_index = $this->db->insert_id();
			$this->db->flush_cache();
			$this->db->where('id', $index);
			$this->db->update('news_article_state', 
				array( 'google_ad'   => $google_ad,
					   'pofans_mode' => $pofans_mode,
					   'source_mode' => $source_mode,
				       'category'    => $add_input['category'],
					   'tag'         => $add_input['tag']
				));

			$this->db->flush_cache();
			$query = $this->db->select("COUNT(*) AS qun")
			->from("news_article_onshelf")
			->where('id', $index)
			->get();
			$temp = $query->row_array();
		
			if( $temp['qun'] != 0 ) {
				$this->db->flush_cache();
				$this->db->where('id', $index);
				$this->db->update('news_article_onshelf', $add_input);
			}

			$this->clear_comments_onshelf($index, $fb_id);  
			// $this->db->delete('facebook_comments_onshelf', array('news_article_id' => $index));
			if ( !empty($comment_good) ) {
				$this->insert_facebook_comments_onshelf($comment_good, 2, $index);
			}
			if ( !empty($comment_bad) ) {
				$this->insert_facebook_comments_onshelf($comment_bad, 1, $index);
			}

			$this->caculate_comments($index);

			$result['success'] = 'Y';
			$result['index'] = $index;
		}

		return $result;
	}

	// //搜尋相關來源文章
	// function get_reference_news_array($keyword, $orderby, $order, $source_id) {
	// 	$this->db->flush_cache();
	// 	$this->db->select ('id, title, content, site_id, publish_time, lock');
	// 	$this->db->from ('source_news_content');
	// 	$this->db->like('title', $keyword);
	// 	$this->db->where('status', 2);
	// 	$this->db->where('id !=', $source_id);
	// 	$this->db->order_by($orderby, $order);
	// 	$query = $this->db->get();
	// 	$result = $query->result_array();
	
	// 	return $result;
	// }

	//搜尋相關本站文章
	function get_reference_article_array($keyword, $account_id) {
		$like_query = '';
		if ( preg_match('/,/', $keyword) ) {
			$like_query1 = '';
			$like_query2 = '';
			$keyword_array = explode(',', $keyword);
			foreach ($keyword_array as $key => $value) {
				if ( $key != 0 ) {
					$like_query1 .= ' OR ';
					$like_query2 .= ' OR ';
				}
				$like_query1 .= "`title` LIKE '%" . $value . "%'";
				$like_query2 .= "`content` LIKE '%" . $value . "%'";
			}
			$like_query = '(' . $like_query1 . ') OR (' . $like_query2 . ')';

		} elseif ( preg_match('/@/', $keyword) ){
			$like_query1 = '';
			$like_query2 = '';
			$keyword_array = explode('@', $keyword);
			foreach ($keyword_array as $key => $value) {
				if ( $key != 0 ) {
					$like_query1 .= ' AND ';
					$like_query2 .= ' AND ';
				}
				$like_query1 .= "`title` LIKE '%" . $value . "%'";
				$like_query2 .= "`content` LIKE '%" . $value . "%'";
			}
			$like_query = '(' . $like_query1 . ') OR (' . $like_query2 . ')';

		} else {
			$like_query = "`title` LIKE '%" . $keyword . "%' OR `content`  LIKE '%". $keyword . "%'";
		}
		
		// $this->db->flush_cache();
		// $this->db->select ('*');
		// $this->db->from ('news_article');

		// $this->db->order_by('create_time', 'DESC');
		// $query = $this->db->get();
		// $result = $query->result_array();

		$sql = "SELECT `na`.*,`nas`.`online` FROM `news_article` AS na JOIN `news_article_state` AS nas ON `nas`.`id` = `na`.`id` WHERE (`nas`.`online`=1) AND create_user_id=$account_id AND ($like_query) ORDER BY `create_time` DESC";
		
		$query = $this->db->query($sql);
		$result = $query->result_array();
		// echo $this->db->last_query();

		return $result;
	}

	function get_sitename_array() {
		$this->db->flush_cache ();
		$this->db->select ("id, site_name");
		$this->db->from ("source_news_site");
		$query = $this->db->get();
		$result = $query->result_array();

		foreach ($result as $key => $value) {
			$name_array[ $value["id"] ] = $value["site_name"];
		}
	
		return $name_array;
	}

	function move_article_photo($img, $newUrl, $newDir){
		//是否建立目錄
		if(false !== $newDir && false === is_dir($newDir) ) {
			mkdir($newDir);
			chmod($newDir, 0777);
		}
	
		$filename = $newDir.$newUrl;
	
		if ( preg_match("/^data:image\/(png|jpeg|gif);base64,/", $img) ) {
			//delete前置data
			$img = preg_replace("/^data:image\/(png|jpeg|gif);base64,/", '', $img);
	
			$img = str_replace(' ', '+', $img);
			//轉換base64 data
			$data = base64_decode($img);
			file_put_contents($filename, $data);
		} else {
			file_put_contents($filename, file_get_contents($img));
		}
	
		if( is_file($filename) === false ) return 'no file';
	
		return $newUrl;
	}

	// function check_article_from_index($add_input) {
	// 	$result = array('success' => 'N', 'msg' => '');
	
	// 	$this->db->flush_cache();
	// 	$query = $this->db->select("COUNT(*) AS qun")
	// 	->from('article_from_index')
	// 	->where("article_index", $add_input['article_index'])
	// 	->get();
	// 	$temp = $query->row_array();
	
	// 	if( $temp['qun'] < 1 ) {		
	// 		$this->db->delete('article_from_index', array('article_index' => $add_input['article_index']));			
	// 	}
	// 	$this->db->insert('article_from_index', $add_input);	
	// }

	function remove_check_photo($img){
		if( is_file($img) === false ) return 'no file';
		else{
			unlink($img);
		}
	}

	function check_tag_exist_and_insert($tags) {
		$id_array = array();
		if ( is_array($tags) ) {
			foreach ($tags as $key => $value) {
				$value = trim($value);
				if ( empty($value) ) continue;
				$this->db->flush_cache();
				$query = $this->db->select('id')
				->from('news_tags')
				->where('name', $value)
				->get();

				if ( $query->num_rows ){
					$temp = $query->row_array();
					$id_array[] = $temp['id'];

					$id = $temp['id'];
					$sql="UPDATE `news_tags` AS nt,  
		   					(SELECT DISTINCT count(id) as total
							FROM  `news_article_state` 
							WHERE `online` =  '1' AND `tag` LIKE  '%\"$id\"%') AS nas  
						SET nt.`article_num`= nas.`total`+1
						WHERE nt.`id`= $id";
					$query = $this->db->query($sql);

				} else {
					$date = date('Y-m-d H:i:s');
					$add_input = array(
						'name' => $value,
						'article_num' => 1,
						'create_time' => $date,
						'update_time' => $date
					);
					$this->db->insert('news_tags', $add_input);
					$id_array[] = $this->db->insert_id();

					$this->load->driver('cache');
					$this->cache->memcached->delGlobalMemcached('news_tags');
				}
			}
		}
		
		return json_encode($id_array);
	}

	function onshelf_article_by_index($index) {
		$result = array('success' => 'N', 'msg' => '');

		$this->db->flush_cache();
		$query = $this->db->select("action, action_time")
		->from("news_article_state")
		->where("id", $index)
		->get();

		$temp = $query->result_array();
		if ( $temp['0']['action_time'] > 0 && time()-$temp['0']['action_time']<5400 ) {
			$result['msg'] = '編輯中，無法上架!';
			return $result;
		} elseif ( $temp['0']['action'] != 0 ) {
			$result['msg'] = '儲存中，請檢查文章是否修改成功!';
			return $result;
		}

		$this->db->flush_cache();
		$query = $this->db->select("*")
		->from("news_article_onshelf")
		->where("id", $index)
		->get();

		$temp = $query->result_array();
		$data = array_pop($temp);
		
		if (is_array($data)) {
			//設定為上架狀態
			$this->db->flush_cache();
			$topic = array('online' => '1', 'online_time' => time());
			$this->db->where('id', $index);
			$this->db->update('news_article_state', $topic);
		} else {
			$date = time();
			$account_id = $this->config->item('admin_accountIndex');

			$this->db->flush_cache();
			$query = $this->db->select("*")
			->from("news_article")
			->where("id", $index)
			->get();

			$temp = $query->result_array();
			$data = array_pop($temp);

			//轉至上架資料表
			$topic_shelf = array();
			$topic_shelf['id'] = $data['id'];
			$topic_shelf['title'] = $data['title'];
			$topic_shelf['fb_post_content'] = $data['fb_post_content'];
			$topic_shelf['summary'] = $data['summary'];
			$topic_shelf['fb_img'] = $data['fb_img'];
			$topic_shelf['article_img'] = $data['article_img'];
			$topic_shelf['category'] = $data['category'];
			$topic_shelf['tag'] = $data['tag'];
			$topic_shelf['content'] = $data['content'];
			$topic_shelf['like_name'] = $data['like_name'];
			$topic_shelf['unlike_name'] = $data['unlike_name'];
			
			$topic_shelf['create_time'] = $date;
			$topic_shelf['update_time'] = $date;
			$topic_shelf['create_user_id'] = $data['create_user_id'];
			$topic_shelf['update_user_id'] = $data['update_user_id'];
			$topic_shelf['youtube_id'] = $data['youtube_id'];

			$this->db->insert('news_article_onshelf', $topic_shelf);				
			$onshelf_index = $this->db->insert_id();
			
			// $newFile = $this->adjust_img_size($data['fb_img'], $data['id']);
			
			// if( $newFile == 'success'){
			// 	$this->db->flush_cache();
			// 	$article = array('fb_img'=>$newFile,'article_img'=>$newFile);
			// 	$this->db->where('id', $onshelf_index);
			// 	$this->db->update('news_article_onshelf', $article);
			// }
			
			//設定為上架狀態
			$this->db->flush_cache();
			$topic = array('online' => '1', 'online_time' => time());
			$this->db->where('id', $index);
			$this->db->update('news_article_state', $topic);
		}

		$query = $this->db->query("UPDATE `news_article_state` AS nas,  
		   (SELECT `news_article_id`, COUNT(*) AS `total` FROM `facebook_comments_onshelf` WHERE `status`=2 GROUP BY `news_article_id`) AS fco  
		SET nas.`like_message_count`=fco.`total`, nas.`like_click_count`=fco.`total`
		WHERE nas.`id`=fco.`news_article_id`");

		$query = $this->db->query("UPDATE `news_article_state` AS nas,  
		   (SELECT `news_article_id`, COUNT(*) AS `total` FROM `facebook_comments_onshelf` WHERE `status`=1 GROUP BY `news_article_id`) AS fco  
		SET nas.`unlike_message_count`=fco.`total`, nas.`unlike_click_count`=fco.`total`
		WHERE nas.`id`=fco.`news_article_id`");
		
		$result['success'] = 'Y';
		$result['msg'] = '上架完成';
		return $result;
	}

	//製作內文圖片縮圖
	function adjust_content_img_size($basefile, $targetfile, $max_width){
		if( is_file($basefile) === true ) {
			chmod($basefile, 0777);
			//製作縮圖
			$src_info = getimagesize($basefile);
			if ( $src_info[0] <= $max_width && $basefile == $targetfile ) return false;

			if($src_info["mime"] == "image/png"){
				$src_im = imagecreatefrompng($basefile);
			}else if($src_info["mime"] == "image/gif"){
				$src_im = imagecreatefromgif($basefile);
			}else {
				$src_im = imagecreatefromjpeg($basefile);
			}

			$alpha = 100;
			$a_height = $max_width * ($src_info[1] / $src_info[0]);

			$resize = imagecreatetruecolor($max_width, $a_height) or die("error4!\n");
			imagecopyresampled($resize, $src_im, 0, 0, 0, 0, $max_width, $a_height, $src_info[0], $src_info[1]);
			imagejpeg($resize, $targetfile, $alpha);
		}
		else return false;
	}

	//製作封面圖縮圖
	function adjust_img_size($basefile, $index, $new_name, $img_m=false){
		if ( empty($basefile) ) return '';
		$filename = 'before_cut';
		$img_type = '';
		
		if(preg_match("/^data:image\/(?P<type>png|jpeg|gif);base64,/", $basefile,$match)){
			//delet前置data
			$img_type = 'image/'.$match['type'];
			$basefile = preg_replace("/^data:image\/(png|jpeg|gif);base64,/", '', $basefile);
			$basefile = str_replace(' ', '+', $basefile);
			
			//轉換base64 data
			$data = base64_decode($basefile);
			file_put_contents($filename, $data);
		}else{
			file_put_contents($filename, file_get_contents($basefile));
		}
		
		if( is_file($filename) === true ) {
			chmod($filename, 0777);
			//製作縮圖

			$src_info = getimagesize($filename);
			if($src_info["mime"] == "image/png"){
				$src_im = imagecreatefrompng($filename);
			}else if($src_info["mime"] == "image/gif"){
				$src_im = imagecreatefromgif($filename);
			}else {
				$src_im = imagecreatefromjpeg($filename);
			}
			if(!$img_type) $img_type = $src_info["mime"];
			$alpha = 100;
			
			preg_match("/\/(?P<type>png|jpeg|gif)/", $src_info["mime"], $match);
			
			if ( empty($match['type']) ) return false;
			elseif ( $match['type'] == 'jpeg' ) $match['type'] = 'jpg';

			$filename = '../file/n' . $index . '/' . $new_name . '.jpg';
			// $a_height = $a_width * ($src_info[1] / $src_info[0]);
			if ( $img_m ) {
				$a_width = 500;
				$a_height = 375;
			} else {
				$a_width = 600;
				$a_height = 315;
			}

			$resize = imagecreatetruecolor($a_width, $a_height) or die("error4!\n");
			imagecopyresampled($resize, $src_im, 0, 0, 0, 0, $a_width, $a_height, $src_info[0], $src_info[1]);
			imagejpeg($resize, $filename, $alpha);
			
			//自動加浮水印 
			// $this->news->watermark_auto($filename);
			$src_info = getimagesize($filename);
			$src_im = imagecreatefromjpeg($filename);

			if ( $img_m ) {
				$filename1 = '../file/n' . $index . '/' . $new_name . '_m.jpg';
				$a_width = 300;
				// $a_height = $a_width * ($src_info[1] / $src_info[0]);
				$a_height = 225;

				$resize = imagecreatetruecolor($a_width, $a_height) or die("error4!\n");
				imagecopyresampled($resize, $src_im, 0, 0, 0, 0, $a_width, $a_height, $src_info[0], $src_info[1]);
				imagejpeg($resize, $filename1, $alpha);
			}
			

			return $match['type'];
		}
		else return 'no file';
	}

	//產生浮水印
	function watermark($basefile){
		$temp = 'images/onshelf-icon.png';
		$src_temp = imagecreatefrompng($temp);
		$imagesize_temp = getimagesize($temp);
		$imgname = ((int)100*microtime(true));//'water';
		$filename = '../file/check/' . $imgname;
		$img_type = '';
		if ( preg_match("/^data:image\/(?P<type>png|jpeg|gif);base64,/", $basefile,$match) ) {
			//delet前置data
		
			$img_type = 'image/'.$match['type'];
			
			$basefile = preg_replace("/^data:image\/(png|jpeg|gif);base64,/", '', $basefile);
			
		
			$basefile = str_replace(' ', '+', $basefile);
			//轉換base64 data
			$data = base64_decode($basefile);
			file_put_contents($filename, $data);
			
		} else {
			file_put_contents($filename, file_get_contents($basefile));
		}
		
		if ( is_file($filename) === true ) {
			chmod($filename, 0777);
			//製作縮圖
			$src_info = getimagesize($filename);
			if($src_info["mime"] == "image/png"){
				$src_im = imagecreatefrompng($filename);
			}else if($src_info["mime"] == "image/gif"){
				$src_im = imagecreatefromgif($filename);
			}else {
				$src_im = imagecreatefromjpeg($filename);
			}
			if(!$img_type) $img_type = $src_info["mime"];
			$alpha = 100;

			preg_match("/\/(?P<type>png|jpeg|gif)/", $src_info["mime"], $match);
			if ( empty($match['type']) ) return false;
			elseif ( $match['type'] == 'jpeg' ) $match['type'] = 'jpg';

			// $a_width = 300;
			// $a_height = $a_width * ($src_info[1] / $src_info[0]);
			$a_width = $src_info[0];
			$a_height = $src_info[1];
			
			unlink($filename);
			$imgname .= '.'. $match['type'];
			$filename .= '.'. $match['type'];
			
			$resize = imagecreatetruecolor($a_width, $a_height) or die("error4!\n");
			imagecopyresampled($resize, $src_im, 0, 0, 0, 0, $a_width, $a_height, $src_info[0], $src_info[1]);

			imagecopyresampled($resize, $src_temp, 0, $a_height-$imagesize_temp[1], 0, 0, $imagesize_temp[0], $imagesize_temp[1], $imagesize_temp[0], $imagesize_temp[1]);
			imagejpeg($resize, $filename, $alpha);

			return $imgname;
		}
		else return false;
	}

	//自動加浮水印
	function watermark_auto($filename, $position='bottom_right', $markfile='images/url-icon.png'){
		$src_temp = imagecreatefrompng($markfile);
		$imagesize_temp = getimagesize($markfile);

		if( is_file($filename) === true ) {
			chmod($filename, 0777);
			//製作縮圖
			$src_info = getimagesize($filename);
			if($src_info["mime"] == "image/png"){
				$src_im = imagecreatefrompng($filename);
			}else if($src_info["mime"] == "image/gif"){
				$src_im = imagecreatefromgif($filename);
			}else {
				$src_im = imagecreatefromjpeg($filename);
			}
			$img_type = $src_info["mime"];
			$alpha = 100;

			preg_match("/\/(?P<type>png|jpeg|gif)/", $src_info["mime"], $match);
			if ( empty($match['type']) ) return false;
			elseif ( $match['type'] == 'jpeg' ) $match['type'] = 'jpg';

			$a_width = $src_info[0];
			$a_height = $src_info[1];
			
			// unlink($filename);
			// $filename .= '.'. $match['type'];
			
			$resize = imagecreatetruecolor($a_width, $a_height) or die("error4!\n");
			imagecopyresampled($resize, $src_im, 0, 0, 0, 0, $a_width, $a_height, $src_info[0], $src_info[1]);
			switch ($position) {
			    case 'top_right':
			        imagecopyresampled($resize, $src_temp, $a_width-$imagesize_temp[0], 0, 0, 0, $imagesize_temp[0], $imagesize_temp[1], $imagesize_temp[0], $imagesize_temp[1]);
			        break;
			    case 'top_left':
			        imagecopyresampled($resize, $src_temp, 0, 0, 0, 0, $imagesize_temp[0], $imagesize_temp[1], $imagesize_temp[0], $imagesize_temp[1]);
			        break;
			    case 'bottom_right':
			        imagecopyresampled($resize, $src_temp, $a_width-$imagesize_temp[0], $a_height-$imagesize_temp[1], 0, 0, $imagesize_temp[0], $imagesize_temp[1], $imagesize_temp[0], $imagesize_temp[1]);
			        break;
			    case 'bottom_left':
			    	imagecopyresampled($resize, $src_temp, 0, $a_height-$imagesize_temp[1], 0, 0, $imagesize_temp[0], $imagesize_temp[1], $imagesize_temp[0], $imagesize_temp[1]);
			        break;
			    default:
			    	imagecopyresampled($resize, $src_temp, 0, $a_height-$imagesize_temp[1], 0, 0, $imagesize_temp[0], $imagesize_temp[1], $imagesize_temp[0], $imagesize_temp[1]);
			}
			
			imagejpeg($resize, $filename, $alpha);
		}
		else return false;
	}

	function server_name(){
		return	$_SERVER["SERVER_NAME"];
	}

	function insert_facebook_comments_onshelf($array, $status, $news_article_id) {
		$this->db->flush_cache();
		$query = $this->db->select('fb_post_id, fb_comment_id, author_id, author_name, message')
			->from('facebook_comments')
			->where_in('fb_comment_id', $array)
			->get();
		$temp = $query->result_array();
		foreach ($temp as $key => $value) {
			$value['create_time'] = time();
			$value['update_time'] = time();
			$value['status'] = $status;
			$value['news_article_id'] = $news_article_id;

			$this->db->insert('facebook_comments_onshelf', $value);
		}

		$this->db->flush_cache();
		$this->db->where_in('fb_comment_id', $array);
		$this->db->update('facebook_comments', array('status' => $status));
	}

	function clear_comments_onshelf($index, $fb_id) {
		$this->db->flush_cache();
		$this->db->where('news_article_id', $index);
		$this->db->where('fb_post_id !=', 0);
		$this->db->delete('facebook_comments_onshelf');

		$this->db->flush_cache();
		$this->db->where('fb_id', $fb_id);
		$this->db->update('facebook_comments', array('status' => 0));
	}

	
	function caculate_comments($id) {
		$this->db->flush_cache();
		$query = $this->db->select('COUNT(*) AS qun')
		->from('facebook_comments_onshelf')
		->where('news_article_id', $id)
		->where('status', 2)
		->get();
		$temp = $query->row_array();
		$this->db->flush_cache();
		$this->db->where('id', $id);
		$this->db->update('news_article_state', array('like_message_count' => $temp['qun'], 'like_click_count' => $temp['qun']));

		$this->db->flush_cache();
		$query = $this->db->select('COUNT(*) AS qun')
		->from('facebook_comments_onshelf')
		->where('news_article_id', $id)
		->where('status', 1)
		->get();
		$temp = $query->row_array();
		$this->db->flush_cache();
		$this->db->where('id', $id);
		$this->db->update('news_article_state', array('unlike_message_count' => $temp['qun'], 'unlike_click_count' => $temp['qun']));
	}

	function get_fb_comments($fb_id, $source_id, $article_id, $facebook) {
		try {
			//取得文章留言
			$fbQueryTemp = $article_id . '/comments?fields=id,from,message,created_time,like_count,comment_count&limit=10';

		    $aryTemp = $facebook->api($fbQueryTemp);

		    if ( !empty($aryTemp['data']) ) {
		    	$time = time();
		    	foreach ($aryTemp['data'] as $key => $value) {
			    	$explode_id = explode('_', $value['id']);
			        $fb_post_id = $explode_id[0];
			        $fb_comment_id = $explode_id[1];
			    	$author_id = $value['from']['id'];
			    	$author_name = $value['from']['name'];
			    	$message = trim($value['message']);  //FB已加反斜線
			    	$fb_create_time = strtotime($value['created_time']);
			    	$like_count = $value['like_count'];
			    	$comment_count = $value['comment_count'];

			    	$this->db->flush_cache();
					$query = $this->db->select("id")
					->from("facebook_comments")
					->where('fb_comment_id', $fb_comment_id)
					->get();
					$temp = $query->row_array();
				
					if ( !isset($temp['id']) ) {
						$add_input = array(
			    					'fb_id'          => $fb_id, 
			    					'source_id'      => $source_id, 
			    					'fb_post_id'     => $fb_post_id, 
			    					'fb_comment_id'  => $fb_comment_id, 
			    					'author_id'      => $author_id, 
			    					'author_name'    => $author_name, 
			    					'message'        => $message, 
			    					'like_count'     => $like_count, 
			    					'comment_count'  => $comment_count, 
			    					'fb_create_time' => $fb_create_time, 
			    					'create_time'    => $time, 
			    					'update_time'    => $time
			    				);
					    $this->db->flush_cache();
						$this->db->insert('facebook_comments', $add_input);
					} else {
						$add_input = array(
			    					'author_name'    => $author_name, 
			    					'like_count'     => $like_count, 
			    					'comment_count'  => $comment_count, 
			    					'update_time'    => $time
			    				);
						$this->db->flush_cache();
						$this->db->where('id', $temp['id']);
						$this->db->update('facebook_comments', $add_input);
					}
			    }
		    }
			
		} catch(Exception $ex) {
			echo 'error!';
			echo "\r\n";

			return array($ex);
		}
	}

	function get_news_fbid($url, $facebook) {
		try{
			//取得網頁ID
		    $aryTemp = $facebook->api($url);
		    $id = isset($aryTemp['og_object']['id']) ? $aryTemp['og_object']['id'] : '';

		    return $id;
		}catch(Exception $ex) {
			echo 'error!';
			echo "\r\n";

			return '';
		}
	}

	function get_comments_list($fid, $sid) {
		$this->db->select('*, id AS cid');
		$this->db->from('facebook_comments');
		$this->db->where('fb_id', $fid);
		$this->db->where('source_id', $sid);
		//$this->db->where("(source_id='0' OR source_id='$sid')", NULL, FALSE);
		$this->db->order_by('like_count', 'DESC');
		$query = $this->db->get();
		
		$comment_result = $query->result_array();
		foreach ($comment_result as $key => $value) {
			if ( $value['status'] == '1' ) {
				$comment_result[$key]['bad'] = 'checked';
				$comment_result[$key]['good'] = '';
			} elseif ( $value['status'] == '2' ) {
				$comment_result[$key]['bad'] = '';
				$comment_result[$key]['good'] = 'checked';
			} else {
				$comment_result[$key]['bad'] = '';
				$comment_result[$key]['good'] = '';
			}
		}

		return $comment_result;
	}

	// function get_category_name($id) {
	// 	$this->db->select ('name');
	// 	$this->db->from ('source_news_category');
	// 	$this->db->where('id', $id);
	// 	$query = $this->db->get();
	// 	$result = $query->result_array();

	// 	return $result['0']['name'];
	// }

	function pofans2($id) {
		$this->db->select('na.title, na.fb_post_content, na.category, nas.pofans2_id');
		$this->db->from('news_article as na');
		$this->db->where('na.id', $id);
		$this->db->join('news_article_state as nas', 
					'nas.id = na.id');
		$query = $this->db->get();
		$result = $query->result_array();

		if ( is_array($result) && count($result) ) {
			$data = array_pop($result);
			if ( $data['pofans2_id'] > 0 ) return '文章已發過';
			$category = $this->get_pofans_category(json_decode($data['category']));
			
			$input = array(
						'message' => $data['fb_post_content'],
						'url'     => 'http://meoments.com/'.$id.'/',
						'img_url' => '',
						'test'    => false,
						'gid'     => $category,
						'key'     => 'toments'
						//'back_door'=> true //走後門
					);
			
			$response = $this->pofans_send($input);
			$response = json_decode($response, true);

			if ( $response['state'] ) {
				$this->db->flush_cache();
				$this->db->where('id', $id);
				$this->db->update('news_article_state', array('pofans2_id' => $response['index']));

				return $response['index'];
			} else {
				return $response['error'];
			}
		}

		return '無此文章';
	}

	function get_pofans_category($category_toments) {
		$this->db->select('id, pofans2_category');
		$this->db->from('categories');
		$this->db->where_in('id', $category_toments);
		$query = $this->db->get();
		$result = $query->result_array();

		$category_array = array();
		foreach ($result as $key => $value) {
			if ( !empty($value['pofans2_category']) ) {
				$category = json_decode($value['pofans2_category'], true);
				
				foreach ($category as $key2 => $value2) {
					if ( !in_array($value2, $category_array) ) {
						$category_array[] = $value2;
					}
				}
			} 
		}
		if ( !in_array(36, $category_array) ) {
			$category_array[] = 36;
		}
		
		$category = implode(',', $category_array);

		return $category;
	}


	function pofans_send($input, $url = 'http://i-kuso.com/pofans2/api/post_message.php') {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $input);

		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

		return $response = curl_exec($ch);  
	}

	function cloudflare($cache_url) {
		$domain = 'meoments.com';
		$post['tkn'] = '3b9c3aabb8fc2f00e471f0126e55a59df3994';
		$post['email'] = 'ohtea1116@gmail.com';
		$post['z'] = $domain; //沒http://開頭
		$url = 'https://www.cloudflare.com/api_json.html';

		if( is_array($cache_url) && !empty($cache_url) ){
			/* 清除快取，單一網址*/
		 	$post['a'] = 'zone_file_purge';
			foreach ($cache_url as $key => $value) {
				$post['url'] = $value;
				$response = $this->news->pofans_send($post, $url);
				var_dump($response);
			}
		} else {
		  	/* 清除快取，全部網域*/
			$post['a'] = 'fpurge_ts';
		 	$post['v'] = '1';
		 	$response = $this->news->pofans_send($post, $url);
		}
	}

	function fbcache($url) {
		include_once('_fb_autoload.php');
		include_once('class_curl.php');
		include_once('class_pofans_fb.php');

		$pofans = new Pofans;
		$url = '/' . $url;
		$count = 0;
		do {
			$fbObj = $pofans->graph_api($url);
			if ( !empty($fbObj) ) {
				echo $id = $fbObj->getProperty('og_object')->getProperty('id');
				$id = '/' . $id;
				$pofans->graph_api($id, 'POST', array('scrape' => true));

				return true;
			} else {
				$count+=1;
				sleep( rand(1,3) );
			}
		} while ( $count < 5 );
		
		return false;
	}

	function change_article_id($id) {
		
		$this->db->select('na.id, nas.pofans2_id');
		$this->db->from('news_article as na');
		$this->db->where('na.id', $id);
		$this->db->join('news_article_state as nas', 
					'nas.id = na.id');
		$query = $this->db->get();
		$result = $query->result_array();

		if ( is_array($result) && count($result) ) {
			$data = array_pop($result);
			$new_id = $id+10000;

			if ( $data['pofans2_id'] != 0 ) {
				$input = array(
					'key' => 'toments',
					'id'  => $data['pofans2_id']
				);
				$response = $this->pofans_send($input, 'http://i-kuso.com/pofans2/api/del_message.php');
				$response = json_decode($response);
			}
			

			$this->copy_dir('../file/n'.$id.'/', '../file/n'.$new_id.'/');


			$this->db->flush_cache();
			$this->db->where('id', $id);
			$this->db->update('news_article', array('id' => $new_id));

			$this->db->flush_cache();
			$this->db->where('id', $id);
			$this->db->update('news_article_state', array('id' => $new_id, 'pofans2_id' => 0));

			$this->db->flush_cache();
			$this->db->where('id', $id);
			$this->db->update('day_article_details', array('id' => $new_id));

			$this->db->flush_cache();
			$this->db->where('news_article_id', $id);
			$this->db->update('facebook_comments_onshelf', array('news_article_id' => $new_id));

			$this->db->flush_cache();
			$this->db->where('article_id', $id);
			$this->db->update('facebook_post_content', array('article_id' => $new_id));

			$this->db->flush_cache();
			$this->db->where('article_id', $id);
			$this->db->update('facebook_post_statistics', array('article_id' => $new_id));

			$this->db->flush_cache();
			$this->db->where('article_id', $id);
			$this->db->update('daily_like_click_count', array('article_id' => $new_id));

			$this->db->flush_cache();
			$this->db->where('article_id', $id);
			$this->db->update('daily_like_message_count', array('article_id' => $new_id));

			$this->db->flush_cache();
			$this->db->where('article_id', $id);
			$this->db->update('daily_unlike_click_count', array('article_id' => $new_id));

			$this->db->flush_cache();
			$this->db->where('article_id', $id);
			$this->db->update('daily_unlike_message_count', array('article_id' => $new_id));

			$this->db->flush_cache();
			$this->db->where('article_id', $id);
			$this->db->update('daily_views', array('article_id' => $new_id));



			$this->load->driver('cache');
			$this->cache->memcached->delArticleMemcached($id);
			$this->cache->memcached->delGlobalMemcached();

			// return $response['state'];
		}
	}

	function copy_dir($from_dir,$to_dir){  
        if(!is_dir($from_dir)){  
            return false ;  
        } 
        
        echo "<br>From:",$from_dir,' --- To:',$to_dir;  
        $from_files = scandir($from_dir);  

        if(!file_exists($to_dir)){  
            mkdir($to_dir);
        }  
        if( ! empty($from_files)){  
            foreach ($from_files as $file){  
                if($file == '.' || $file == '..' ){  
                    continue;  
                }  
                  
                if(is_dir($from_dir.'/'.$file)){
                    copy_dir($from_dir.'/'.$file,$to_dir.'/'.$file);  
                }else{
                    copy($from_dir.'/'.$file,$to_dir.'/'.$file);  
                }  
            }  
        }
        return true ;
    }
}	