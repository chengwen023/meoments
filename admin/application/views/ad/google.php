<div class="widget">
	<table cellpadding="0" cellspacing="0" width="100%" class="tLight">
		<form id="list_orderby" method="post">
			<input id="order_by" type="hidden" name="order_by" value="">
			<thead>
				<tr>
					<td title="名稱">廣告名稱</td>
					<td title="程式碼" width:"350px">code</td>
					<td title="廣告商" width:"350px">廣告商</td>
					<td title="動作" class="order_control" style="width: 120px;" data-orderby = "online"><span style="width:60px;">動作<i class="icon-chevron-{by-online}"></i></span><i class="icon-plus-sign"></i></td>					
				</tr>
			</thead>
		</form>
		<tbody>
			{content}
			<tr id="user_{pk}" class="user_row">
				<td>{name}</td>
				<td>
					<input class="" style="width:350px; height:150px;"type="text" name="" value="	" />
				</td>
				<td >{type}</td>
				<td><a href="javascript:void(0);" class="tablectrl_small bGreen" original-title="Edit" title="修改" alt="修改"><span class="iconb"						data-icon="" onclick="fans.group_edit({id});"></span></a>
					<span class="online {off}" data-index="{id}"> 
						<a href="javascript:void(0);" class="tablectrl_small bBlue" original-title="Edit" title="打開" alt="打開">打開<span class="iconb" data-icon=""></span></a>
						<a href="javascript:void(0);" class="tablectrl_small bGold" original-title="Edit" title="關閉" alt="關閉">關閉<span class="iconb" data-icon="">
					</span></a> 							
				</span></td>
			</tr>
			{/content}
		</tbody>
	</table>
</div>
<style>
.online A {
	position: relative;
}

.online A:first-child {
	z-index: 100;
}

.online A:not(:first-child ) {
	display:none;
	!left: -53px;
	z-index: 1;
	opacity: 0;
	filter: alpha(opacity = 0);
	-moz-transform: rotateY(180deg);
	-webkit-transform: rotateY(180deg);
	transform: rotateY(180deg);
}

.online.off A:first-child {
	display: none;
	opacity: 0;
	filter: alpha(opacity = 0);
	-moz-transform: rotateY(-180deg);
	-webkit-transform: rotateY(-180deg);
	transform: rotateY(-180deg);
}

.online.off A:not(:first-child ) {
	display:inline;
	opacity: 1;
	filter: alpha(opacity = 100);
	-moz-transform: rotateY(0deg);
	-webkit-transform: rotateY(0deg);
	transform: rotateY(0deg);
}
</style>

<script src="js/fans.js"></script>

<script>
$(".online").click(function() {
	var index= $(this).attr('data-index'); 
	var online = $(this).hasClass("off")
	
	if(online) $(this).removeClass("off");
	else $(this).addClass("off");

	if(index){
		$.ajax({
		  //url: 'fb_fans/fansid_online/',
		  dataType: 'html',
		  type: "POST",
		  data: {
			  online:online,
			  index:index
		  },
		  success: function(data){
			  console.info(data);
		  	/**/
		  }
		});
	}
});
$(".order_control i").click(function() {
	//url = 'source/actions/edit_fansid';
	 
	//$.fancybox.showLoading();
	$.ajax({
		url: url,
	  cache: false,
		dataType: 'html',
	  type: 'POST',
	  data:{
		
	  },
	  success: function(data){
	  	$.fancybox.hideLoading();
	  	$.fancybox.open(data);
	  }
	});
	
});
function order_by(this_td){

	$('#order_by').val( $(this_td).attr('data-orderby'));

	$('#list_orderby').submit();

}

$(".order_control span").live( "click", function() {	
	var order_name = $(this).parents().attr('data-orderby');
	if(order_name) order_by($(this).parents());
	return false;
});	

</script>