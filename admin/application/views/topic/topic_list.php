
<div class="widget">
	<div class="list_controllers"
		style="position: absolute; text-align: right; right: 0; margin-top: -25px;">
		<a href="javascript:void(0);" onclick="topic.insert(4);" title="新增">新增資料<i class="icon-plus-sign"></i></a> 
		
		
	</div>
	<table cellpadding="0" cellspacing="0" width="100%"
		class="tDefault tLight">
		<form id="list_orderby" method="post">
			<input id="order_by" type="hidden" name="order_by" value="">
			<thead>
				<tr>
					<td title="題目" class="order_control" data-orderby="topic">題目<i
						class="icon-chevron-{by-topic}"></i></td>
					<td title="群組名稱">群組名稱</td>
					<td title="選項類型" class="order_control"
						data-orderby="options_select">選項類型<i
						class="icon-chevron-{by-options_select}"></i></td>
					<td title="題目圖片">題目圖片</td>
					<td title="選項圖片">選項圖片</td>
					<td title="建立人員" class="order_control"
						data-orderby="create_account_index">建立人員<i
						class="icon-chevron-{by-create_account_index}"></i></td>
					<td title="建立日期" class="order_control" data-orderby="create_time"
						style="width: 80px">建立日期<i class="icon-chevron-{by-create_time}"></i></td>
					<td title="修改日期" class="order_control" data-orderby="update_time"
						style="width: 80px">修改日期<i class="icon-chevron-{by-update_time}"></i></td>
					<td title="審核">審核</td>
					<td title="上線排序" class="order_control" data-orderby="online">上線排序<i
						class="icon-chevron-{by-online}"></i></td>

				</tr>
			</thead>
		</form>
		<tbody>
			{content}
			<tr id="user_{auto_index}" class="user_row">
				<td style="white-space: nowrap;">{topic}</td>
				<td>{group_names}</td>
				<td>{select_optionName}</td>
				<td><img src="{topic_imgurl}" style="width: 60px;" /></td>
				<td><img src="{options_imgurl}" style="width: 60px;" /></td>
				<td class="create_id">{id}</td>
				<td style="white-space: nowrap;">{create_time}</td>
				<td style="white-space: nowrap;">{update_time}</td>
				<td style="white-space: nowrap;">{fail_state}</td>
				<td style="white-space: nowrap;"><a href="javascript:void(0);"
					class="tablectrl_small bBlue topicview_{edit_control}"
					original-title="View" title="觀看" alt="觀看"
					onclick="topic.view({auto_index});"> <span class="iconb"
						data-icon=""></span></a> <a href="javascript:void(0);"
					class="tablectrl_small bBlue {edit_control}" original-title="View"
					title="觀看" alt="觀看" onclick="topic.reviseview({auto_index});"> <span
						class="iconb" data-icon=""></span></a> <a
					href="javascript:void(0);"
					class="tablectrl_small bGreen {edit_control}" original-title="Edit"
					title="編輯" alt="編輯" onclick="topic.add_edit({auto_index});"> <span
						class="iconb" data-icon=""></span></a> <a
					href="javascript:void(0);"
					class="tablectrl_small bRed {edit_control}" original-title="Del"
					title="刪除" alt="刪除" onclick="topic.del({auto_index});"> <span
						class="iconb" data-icon=""></span></a></td>
			</tr>
			{/content}
		</tbody>
	</table>

</div>
<div class="widget" style="width: 100%; overflow: scroll;">{page_list}</div>
<script src="js/topic.js"></script>
<script src="js/span_editor.js"></script>
<style>
thead TD {
	white-space: nowrap;
}

.hide,.topicview_ {
	display: none;
}

.topicview_hide {
	display: initial;
}

.tLight tbody td {
	padding: 0px 16px;
}

.external_link {
	background: none;
}

.more_page,.page_num {
	float: left;
	padding: 5px;
	display: inline-flex;
	line-height: 0px;
	cursor: pointer;
	margin: 0 5px;
}

.page_num:HOVER,.page_num.selected {
	border-bottom: 1px solid #000;
}
</style>

<script>

function order_by(this_td){

	$('#order_by').val( $(this_td).attr('data-orderby'));

	$('#list_orderby').submit();

}

$(".order_control").live( "click", function() {
	var order_name = $(this).attr('data-orderby');
	if(order_name) order_by(this);
	
	return false;
});	

</script>