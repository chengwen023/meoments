<?php
class TopicController extends BaseController
{

	public $settings;
	
	public function initialize()
	{
		if ($this->request->isPost()) {		
			$this->order_by = $this->request->getPost('order_by');				
		}
		
	}
	
	
	public function indexAction()
	{
		
		$this->action = 'list';
		$this->listAction();
		return;
	}

	
	public function listAction($viewType = false)
	{
		$orderBy = $this->order_by($this->order_by,'Topic');
		
		
		$ArticleList = array();
		$content = Topic::find();
		
		$content->rewind();
		while ($content->valid()) {
		    $Article = $content->current();
		    $ArticleItem = $Article->toArray();
		    $ArticleItem['id'] = $Article->get_AdminAccount_field('id');
		    $ArticleItem['select_name'] = $Article->get_TopicSelect_field('select_name');
		    $ArticleItem['edit_control'] = ($ArticleItem['online'])?'hide':'';		    		    
		    if($ArticleItem['topic_group'] && false != $group_array = json_decode($ArticleItem['topic_group'],true) )
		    {
		    	$group_array = ArticleGroup::find(array(" auto_index in (".implode(',', $group_array).") AND online = '1'",));
		    
		    	if($group_array != false ) {
		    		$ArticleItem['topic_group']= implode(',', array_column($group_array->toArray(), 'name'));
		    	}
		    }
		    $ArticleList[] = $ArticleItem; 
		    $content->next();
		}
		
		
		
		$viewContent['content'] = $ArticleList;
		if($viewType == 'json') return json_encode($viewContent,true);
		
		$view = new contentView();
		$main_content = $view->getRender($this->controller, $this->action,$viewContent);
		
		$this->template = array(
				"main_content" => $main_content,
		);
			
	}

	
}
?>