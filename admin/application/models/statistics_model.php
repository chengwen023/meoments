<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Statistics_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}


	function get_day_list($num=7) {
		$days_time = time();
		for($i=0; $i<$num; $i++) {
			$days_list[$i]['day'] = date('Y-m-d', $days_time);
			$days_time -= 86400;
		}

		return $days_list;
	}

	/*function join_daily($days, $sql_order) {
		$day_array = explode('-', $days);
		$day = array_pop($day_array);
		$day = ( preg_match('/0[0-9]/', $day) ) ? str_replace('0', '', $day) : $day;
		$day_field = 'day_' . $day;
		$month = implode('-', $day_array);

		$earlier_time = strtotime($days);
		$later_time = strtotime($days) + 86400;

		$sql = 
			"SELECT auto_index, id, total, DV, DLC, DUC
			FROM (
			        SELECT  `na`.`create_user_id` , COUNT( na.id ) AS total
			        FROM ( `news_article` AS na )
			        JOIN  `news_article_state` AS nas ON  `nas`.`id` =  `na`.`id` 
			        WHERE  `na`.`create_time` >= $earlier_time
			        AND  `na`.`create_time` < $later_time
			        AND  `nas`.`online` = 1
			        GROUP BY  `na`.`create_user_id`
			    ) AS NA_total 
			RIGHT JOIN admin_account AS AA ON AA.auto_index = NA_total.create_user_id

			LEFT JOIN (SELECT create_user_id, SUM(`$day_field`) AS DV FROM `daily_views` WHERE `datetime` LIKE '%$month%' GROUP BY create_user_id ORDER BY DV DESC) AS DV
			ON AA.auto_index = DV.create_user_id
		
			LEFT JOIN (SELECT create_user_id, SUM(`$day_field`) AS DLC FROM `daily_like_click_count` WHERE `datetime` LIKE '%$month%' GROUP BY create_user_id ORDER BY DLC DESC) AS DLC
			ON AA.auto_index = DLC.create_user_id
	
			LEFT JOIN (SELECT create_user_id, SUM(`$day_field`) AS DUC FROM `daily_unlike_click_count` WHERE `datetime` LIKE '%$month%' GROUP BY create_user_id ORDER BY DUC DESC) AS DUC
			ON AA.auto_index = DUC.create_user_id
	
			-- LEFT JOIN (SELECT create_user_id, SUM(`$day_field`) AS DLM FROM `daily_like_message_count` WHERE `datetime` LIKE '%$month%' GROUP BY create_user_id ORDER BY DLM DESC) AS DLM
			-- ON AA.auto_index = DLM.create_user_id

			-- LEFT JOIN (SELECT create_user_id, SUM(`$day_field`) AS DUM FROM `daily_like_message_count` WHERE `datetime` LIKE '%$month%' GROUP BY create_user_id ORDER BY DUM DESC) AS DUM
			-- 	ON AA.auto_index = DUM.create_user_id

			WHERE AA.admingroup_index = 3".$sql_order;

		$query = $this->db->query($sql);
		$result = $query->result_array();

		// echo '<pre>';
		// var_dump($result);
		return $result;
	}*/

	//查詢一日流量
	function one_daily($author_id, $table, $days) {
		$day_array = explode('-', $days);
		$day = array_pop($day_array);
		$day = ( preg_match('/0[0-9]/', $day) ) ? str_replace('0', '', $day) : $day;
		$day_field = 'day_' . $day;
		$month = implode('-', $day_array);
		
		$earlier_time = strtotime($days);
		$later_time = strtotime($days) + 86400;

		// $sql = "SELECT na.id, na.title, dv.$day_field AS count
		// 		FROM  `$table` AS dv
		// 		INNER JOIN  `news_article` AS na ON na.id = dv.article_id
		// 		WHERE dv.create_user_id =$author_id
		// 		AND dv.$day_field >0
		// 		AND dv.datetime LIKE  '%$month%'
		// 		ORDER BY $day_field DESC";
		// $sql = "SELECT id, title, DV.count
		// 		FROM  `news_article` AS na
		// 		INNER JOIN (

		// 		SELECT article_id, $day_field AS count
		// 		FROM  `$table`
		// 		WHERE $day_field >0
		// 		AND create_user_id =$author_id
		// 		AND datetime LIKE  '%$month%'
		// 		ORDER BY $day_field DESC
		// 		) AS DV ON na.id = DV.article_id";

		$sql = "SELECT id, title, article_source, fb_id, source_mode, DV.count, fb_group_id, fb_post_id
		FROM  (                   
		    SELECT NA.id, NA.title, NA.article_source, NAS.source_mode, NAS.fb_id
		    FROM news_article_state AS NAS, news_article AS NA
		    WHERE NA.create_user_id =$author_id
		    AND NAS.id = NA.id
		    ) AS na
		    			
		INNER JOIN (
		    SELECT article_id, $day_field AS count
		    FROM  `$table`
		    WHERE $day_field >0
		    AND create_user_id =$author_id
		    AND datetime LIKE  '%$month%'
		) AS DV ON na.id = DV.article_id

		LEFT JOIN (SELECT fb_group_id, fb_post_id, id as fpc_id FROM `facebook_post_content`) AS fpc
		ON fpc.fpc_id = na.fb_id
		
		ORDER BY DV.count DESC";

		$query = $this->db->query($sql);
		$result = $query->result_array();

		return $result;
	}

	/*function daily_source_mode($days, $operator) {
		$day_array = explode('-', $days);
		$day = array_pop($day_array);
		$day = ( preg_match('/0[0-9]/', $day) ) ? str_replace('0', '', $day) : $day;
		$day_field = 'day_' . $day;
		$month = implode('-', $day_array);
		
		$earlier_time = strtotime($days);
		$later_time = strtotime($days) + 86400;

		$sql = "SELECT COUNT( NAS.source_mode ) AS source_mode_count , NAS.source_mode, NA.create_user_id
				FROM news_article_state AS NAS, news_article AS NA
				WHERE  `NA`.`create_time` >= $earlier_time
				AND  `NA`.`create_time` < $later_time
				AND NAS.id = NA.id
				AND fb_id $operator 0
				GROUP BY NA.create_user_id, NAS.source_mode";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		
		$source_inner = array();
		foreach ($result as $key => $value) {
			$source_inner[$value['create_user_id']][$value['source_mode']] = $value['source_mode_count'];
		}
		
		return $source_inner;
	}*/

	function get_section_statistics($start_date, $end_date, $sql_order) {
		$sql = "SELECT aa.id, account_id, SUM(article_count) as article_count, SUM(inner_1) as inner_1, SUM(inner_2) as inner_2, 
			SUM(outer_1) as outer_1, SUM(outer_2) as outer_2, SUM(outer_3) as outer_3, (SUM(like_click_count)+SUM(unlike_click_count)) as like_unlike, 
			SUM(views) as views, SUM(inner_1+inner_2+outer_1+outer_2)*40+SUM(outer_3)*20 as article_time
			FROM `daily_statistics` as ds INNER JOIN admin_account as aa ON aa.auto_index = ds.account_id AND aa.online='1'
			WHERE date >= '$start_date' AND  date <= '$end_date' GROUP BY account_id $sql_order";

		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	function get_section_author_statistics($start_date, $end_date, $author_id) {
		$sql = "SELECT aa.id, account_id, article_count, inner_1, inner_2, outer_1, outer_2, outer_3, 
		like_click_count+unlike_click_count as like_unlike, views, (inner_1+inner_2+outer_1+outer_2)*40+outer_3*20 as article_time
		FROM `daily_statistics` as ds LEFT JOIN admin_account as aa ON aa.auto_index = ds.account_id 
		WHERE date >= '$start_date' AND date <= '$end_date' AND account_id=$author_id";

		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	function get_section_day_statistics($start_date, $end_date) {
		$sql = "SELECT `date`, sum(views) as day_total_views FROM `daily_statistics` WHERE `date` >= '$start_date' AND `date` <= '$end_date' GROUP BY `date`";

		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	function get_chinese_weekday($datetime) {
	    $weekday = date('w', strtotime($datetime));
	    return ' (' . ['日', '一', '二', '三', '四', '五', '六'][$weekday] . ')';
	}
}