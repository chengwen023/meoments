
var category_list = new Object;

category_list.add_edit = function(index){
	url = 'news_category/actions/add_category';
	
	$.fancybox.showLoading();
	$.ajax({
		url: url,
	  cache: false,
		dataType: 'html',
	  type: 'POST',
	  data:{
		  index:index
	  },
	  success: function(data){
	  	$.fancybox.hideLoading();
	  	$.fancybox.open(data,{	  
	  		autoCenter   : true,
		  	helpers : {
		        overlay : {
		        	closeClick: false
		        }
	        },
	        afterShow : function(){
	        	//$('.fancybox-inner').css('width', '620px');
	        },
	        beforeClose : function(){
	        	var ask = confirm("是否關閉 新增/編輯 資料視窗!?");
	        	
	        	if(ask) {
	        		$('.add_topic_option').die('click');
		        	$("DIV.fb_view_control SPAN").die('click');
		        	$("IMG.editable").die('click');
	        		
	        	}
	        	return(ask);	
	        }
		  });
	  }
	});
}

category_list.del= function(index){
	console.info(123);
	$.ajax({
	  url: 'news_category/actions/del',
	  cache: false,
	  dataType: 'json',
	  async : false,
	  type: 'POST',
	  data: 'index='+index,
	  success: function(data){		
		  console.info(data);
		  	if(data.success == 'Y'){
		  		$.fancybox.close(true);
		  		location.reload();
		  	}else{
		  		$.fancybox.close(true);
		  		alert(data.msg);
		  	}
	  }
	});
}