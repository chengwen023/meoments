<style>
IMG.editable{
	background-color: #94b0dc;
}

#choose_list LI INPUT[type="text"],#choose_list LI TEXTAREA{
	width:350px;
}
#choose_list LI TEXTAREA{
	height:80px;
}

#choose_list LI{
	clear:both;
	
	font-size: 24px;
	list-style-position: inside;	
}

#choose_list LI DIV:not([class="imgurl_box"], [class="imgurl_box imgedit_focus"]){
	width:400px;
	
}
#choose_list LI .list_info{
	float: right;
	display: inline-block;
}
#choose_list LI .list_info > DIV{
	float: right;
	font-size: 14px;
}

.del_btn{
	margin: 5px 20px;
	
	position: relative;
	cursor: pointer;
	text-align: center;
	width:80px;	
	border: 1px solid #D17A1C; 
	color:#D17A1C;
}


#choose_list LI DIV.del_btn{
	float:none;
	margin-left: 415px;
	margin-bottom: 10px;
}
.del_btn:HOVER{
	background-color:#D17A1C;
	color: #fff;
}


#choose_list LI DIV SPAN.data_title:before{
	content:attr(data-title);
	color:#D17A1C;
	line-height: 25px;
	margin-right: 10px;
}
#choose_list LI DIV SPAN.data_title:first-child{
	background: rgba(255,153,102,0.5);
	color: #993300;
	border-radius: 0 10px 10px 0;
}


#choose_list LI DIV SPAN.data_title{
	display: block;
	margin-right: 20px;
	border-bottom: 1px solid #666;
}

.submit_btn{	
	color: #66a3d3;	
	border-color: #66a3d3;
}
.submit_btn:HOVER{	
	color: #fff;
	background-color: #678197;
	border-color: #678197;
}
.postfb_group li{	
	float:left;
	list-style-position:inside;
	margin-right: 15px;
	padding: 1px 3px;
}
.postfb_group li.li_pofans:HOVER,.postfb_group li.li_pofans.focus {
	background-color: #66a3d3;
	color:#FFF;
	cursor: pointer;
}
.postfb_group li.li_autopost:HOVER,.postfb_group li.li_autopost.focus {
	background-color: #CF9822;
	color:#FFF;
	cursor: pointer;
}
.postfb_group li.li_feedback:HOVER,.postfb_group li.li_feedback.focus {
	background-color: #63835D;
	color:#FFF;
	cursor: pointer;
}
.edit_feedback, .edit_autopost{
	display: none;
}
.hide_textarea{
	float:right;
	cursor: pointer;
}

.edit_pofans.edit_item:HOVER{
	background-color: #CEE2F0;
}
.edit_autopost.edit_item:HOVER{
	background-color: #E9D2A2;
}
.edit_feedback.edit_item:HOVER{
	background-color: #B4CCB0;
}
.img_choose{
	display: inline-block;
	color:#2B9227;
}
</style>
	<div id="topic_info" style="width:1100px;">
		<form id="form_folder" onsubmit="return false">		
			
			<div class="tb-green" style="width:550px;height:700px;float: left;">
			<table class="tLight" style="width:100%;">
				<tr>
					<td width="66">題目</td>
					<td>{topic_title}</td>
					<td rowspan="2">						
						<div class="imgurl_box" >
							<img class="editable" data-width="500" data-height="375" src="../uploads/q{topic_fromindex}/t{topic_img}" width="80px" />
						</div>
					</td>				
				</tr>
				<tr>
					<td width="66">網址</td>
					<td>http://i-gotest.com/q{topic_fromindex}.html</td>					
				</tr>		
				<tr>
					<td width="66"  colspan="3">					
							<ol id="choose_list">
								{topic_options}
									<li>
										<div class="list_info">
											<div class="imgurl_box" data-type="title"><span><img class="editable" data-width="500" data-height="375" src="../uploads/q{topic_fromindex}/{optionImg}" width="80px" /></span></div>
											<div style="width:400px;margin-bottom: 20px;">
												<span class="data_title" data-title="選項" data-type="title">{choose}</span>
												<span class="data_title" data-title="標題" data-type="title">{title}</span>
												<span class="data_title editable" data-title="內容" data-type="info">{text}</span>										
											</div>
										</div>
									</li>
								{/topic_options}
							</ol>							
						</td>						
					</tr>				
				</table>
			</div>
			<div class="tb-green"style="width:500px;height:600px;float: left;">			
			<table class="tLight" style="width:100%;">			
				<thead>
					<tr class="column1">
						<th >自動分享
							<SELECT name="autopost" style="width:auto;">
								<option value="0" >關閉</option>
								{postfb_type}
								<option value="{auto_index}" >{type_name}</option>
								{/postfb_type}
							</SELECT>	
							看完覺得			
							<SELECT  name="feedback" style="width:auto;">
								<option value="0" >預設</option>
								{postfb_type}
								<option value="{auto_index}" >{type_name}</option>
								{/postfb_type}
							</SELECT>
						</th>						
						<th scope="col" >
							<input type="hidden" name="index" value="{auto_index}">
							<input type="submit" class="submit_btn" value="修改完成" style="float:right ;padding:3px;font-size:14px;" />						
						</th>
					</tr>
					<tr class="column1">
						<th colspan="2">
						<ol class="postfb_group">							
							{postfb_group}
									<li class="li_{en_name}" data-class="edit_{en_name}" onclick="change_editor(this);">{name}</li>
							{/postfb_group}
						</ol>						
						</th>
					</tr>
				</thead>
				<tbody>
				<?php $postAI = 0;?>
				<?php foreach($postfb_group AS $group_value):?>
					
					<?php foreach($postfb_type AS $type_key => $type_value):?>
					<?php $postAI++; ?>
					<?php $postfb_check = (isset($postfb_list[$group_value['auto_index']]) && isset($postfb_list[$group_value['auto_index']][$type_value['auto_index']]) );?>
					
					<tr class="edit_<?php echo $group_value['en_name']?> edit_item">
						<td colspan="2"><?php echo $type_value['type_name']?> 
						<input name="postfb[<?php echo $postAI;?>][title]" value="1" type="checkbox" 
							<?php if( !$postfb_check|| $postfb_list[$group_value['auto_index']][$type_value['auto_index']]['title'] ):?>
							checked="checked" <?php endif;?>>標題
						
						<?php if($type_value['type_enname'] != 'link'):?>	
						<input name="postfb[<?php echo $postAI;?>][link]" value="1" type="checkbox" 
							<?php if( !$postfb_check|| $postfb_list[$group_value['auto_index']][$type_value['auto_index']]['link'] ):?>
							checked="checked" <?php endif;?>>連結 
						<?php endif;?>
							
							
							<span class="hide_textarea icon-chevron-down"></span>
							<span style="float:right;"><?php echo $group_value['name']?></span>
							
							<?php if($type_value['type_enname'] != 'text' && $group_value['en_name'] != 'pofans'):?>		
							<div class="img_choose">				
							<input type="radio" name="postfb[<?php echo $postAI;?>][img_choose]" value="0" <?php if($postfb_check &&  $postfb_list[$group_value['auto_index']][$type_value['auto_index']]['post_img_choose'] != 1): ?>checked="checked" <?php endif?>>題目圖片
							<input type="radio" name="postfb[<?php echo $postAI;?>][img_choose]" value="1" <?php if($postfb_check &&  $postfb_list[$group_value['auto_index']][$type_value['auto_index']]['post_img_choose'] == 1): ?>checked="checked" <?php endif?>>選項圖片
							</div>
						<?php endif;?>
						</td>
					</tr>		
					<tr class="edit_<?php echo $group_value['en_name']?> edit_item">
					<td  colspan="2">
						<textarea class="editor" data-extraPlugins = "replaceValue<?php if($group_value['en_name'] != 'pofans') echo ',replaceFacebook';?>" id="edit_<?php echo $group_value['en_name'];?>_<?php echo $type_value['type_enname']?>" 
							name="postfb[<?php echo $postAI;?>][text]" ><?php if($postfb_check) echo $postfb_list[$group_value['auto_index']][$type_value['auto_index']]['post_text']; ?></textarea>
						</td>
						<input type="hidden" name="postfb[<?php echo $postAI;?>][index]"  value="<?php if($postfb_check) echo $postfb_list[$group_value['auto_index']][$type_value['auto_index']]['post_index']?>" />
						<input type="hidden" name="postfb[<?php echo $postAI;?>][group]"  value="<?php echo $group_value['auto_index'];?>" />
						<input type="hidden" name="postfb[<?php echo $postAI;?>][type]"  value="<?php echo $type_value['auto_index'];?>" />						
					</tr>				
					<?php endforeach;?>
						
				<?php endforeach;?>
					
					
				</tbody>		
				</table>
			</div>	
			</form>			
		</div><!-- / tb-green-->


<script>
$('TEXTAREA[class="editor"]').each(function(){
	var id = $(this).attr("id");
	var extraPlugins = $(this).attr("data-extraPlugins");

	
	 CKEDITOR.replace( id,{
		 	customConfig: 'ckeditor_config.js',
 			extraPlugins: extraPlugins,
		    width:520,
		    height:200 
		});
	
});
					            
$(".hide_textarea").on("click",function(){
	if( $(this).hasClass("icon-chevron-down")){
		$(this).parents("tr").next().show();
		$(this).removeClass("icon-chevron-down").addClass("icon-chevron-up");
	}else{
		$(this).parents("tr").next().hide();
		$(this).addClass("icon-chevron-down").removeClass("icon-chevron-up");
	}	
})
					            
function change_editor(this_li){
	var display_class = $(this_li).attr("data-class");
	$(this_li).parent().find('li').removeClass('focus');
	$(this_li).addClass('focus');
	$(".edit_item").hide();
	$("."+display_class).show();
	
	$('.icon-chevron-down').each(function(){
		$(this).parents("tr").next().hide();
	});
}
function CKupdate(){
    for ( instance in CKEDITOR.instances )
        CKEDITOR.instances[instance].updateElement();
}
	$(function(){

		
		//預設關閉編輯是窗
		$('.icon-chevron-down').each(function(){
			$(this).parents("tr").next().hide();
		});	
			
		
		// 註冊驗證
		$("#form_folder").validate({	
			rules: {
				pofans_text: {
					required: true
				}			
			},
			messages: {
				pofans_text: "資料不得為空"					
			},		
			submitHandler: function(form) {		
				
				CKupdate();
				
				// 透過 Ajax 驗證是否註冊
				$.ajax({
				  url: 'pofans/actions/save_pofans',
				  cache: false,
				  dataType: 'json',
				  type: "POST",
				  data: $("#form_folder").serialize(),
				  success: function(data){
					  
				  	if(data.success == 'Y' ){
					  	alert(data.msg);
					  	console.info(data.adonis);
				  		//location.reload();
				  	}else{				  		
				  		alert('資料有誤');			  		
				  		return false;
				  	}
				  	/**/
				  },
				  error: function(data,msg){
					  console.info('error');
					  console.info(msg);			
				  }
				});
				//form.submit();
				return false;
			}
		});
		// end of $("#signupForm").validate
	});
</script>