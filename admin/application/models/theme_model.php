<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *	Adonis
 *	後台預設資料讀取
 */

class Theme_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
		
	}
	

	function order_by($order_name,$order_page = ''){
			
		$order_by = $this->session->userdata($order_page.'-order_by');
		
		if(isset($order_name) && $order_name){
			if(isset($order_by['by-'.$order_name]) && $order_by['by-'.$order_name] != 'down') $order_by['by-'.$order_name] = "down";
			else {
				unset($order_by);
				$order_by['by-'.$order_name] = "up";
			}
			$this->session->set_userdata($order_page.'-order_by', $order_by);
		}
	
		return (is_array($order_by))?$order_by:array();
	}
	
	function page($page){
		  
		$pagelists['pagenum'] = $page['start'];

		do{	
			if ( $pagelists['pagenum'] < $page["page_num"]-9 || $pagelists['pagenum'] > $page["page_num"]+9 ) {
				continue;
			}
			$pagelists['url'] = base_url($page['url']."/".$pagelists['pagenum']).$page['url_end'];
			$page['pagelists'][] = $pagelists;

		}while($pagelists['pagenum']++ < $page['end']);
		
		$page['prev'] = base_url($page['url']."/".(($page['page_num']-1<$page['start'])?$page['start']:$page['page_num']-1) ).$page['url_end'];
		$page['next'] = base_url($page['url']."/".(($page['page_num']+1>$page['end'])?$page['end']:$page['page_num']+1) ).$page['url_end'];
		
		return $this->load->view('page_list', $page, true);
	}
	
	
}