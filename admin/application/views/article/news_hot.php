<style>
#tips_box{
	position: fixed;
	font-size: 16px;
	display: none;
	padding: 5px 10px;
	background: #fff;
	box-shadow: 1px 1px 2px 1px rgba(33,66,99,.5);
	border-radius:5px;
	z-index: 10000; 
}

</style>

<div class="widget adonis">
	<form id="list_orderby" method="post">
	<input id="order_by" type="hidden" name="order_by" value="">
	<input id="days_selected" type="hidden" name="days_selected" value="{days}">

	<select id="days" name="days">
	{days_list}
　		<option id="{day}" value="{day}">{day}</option>
	{/days_list}
	</select>
	</form>

	<div class="article_total">文章總數：<span>{article_total}</span> &nbsp;&nbsp; 已編輯：<span>{article_edited}</span></div>

	<a href="javascript:void(0);" class="tablectrl_small bGreen {edit_control} write_article" original-title="Edit" title="新增文章" alt="新增文章" onclick="article.add_edit_news('0', 'write');">
	新增文章</a>

	<table cellpadding="0" cellspacing="0" width="100%" class="tDefault tLight">
		<thead>
			<tr >
				<td title="編號" tips ="使用滑鼠點選 可以排序該欄位" class="order_control" data-orderby = "id">編號<i class="icon-chevron-{by-id}"></i></td>
				<td title="標題">標題</td>
				<td title="類型" class="order_control" data-orderby="most_likes">熱門區段(讚數)<i class="icon-chevron-{by-most_likes}"></i></td>
				<td title="建立時間" class="order_control" data-orderby="create_time">建立時間<i class="icon-chevron-{by-create_time}"></i></td>
				<td title="類型" class="order_control" data-orderby="fb_post_type">類型<i class="icon-chevron-{by-fb_post_type}"></i></td>
				<td title="來源" class="order_control" data-orderby="fb_group_id">來源<i class="icon-chevron-{by-fb_group_id}"></i></td>
				<td></td>	
			</tr>
		</thead>
		
		<tbody class="hot_tbody">
			{content}
			<tr id="user_{id}" class="user_row article_{article_id} {lock_fb_article}">
				<td style="white-space:nowrap;">{id}</td>
				<td><a href="https://www.facebook.com/{fb_group_id}/posts/{fb_post_id}" target="_blank">
					{article_title}
				</a></td>
				<td>{most_likes} <span class="most_likes">({fps_most_likes})</span></td>
				<td>{create_time}</td>
				<td>{fb_post_type}</td>
				<td><a href="https://www.facebook.com/{fb_group_id}" target="_blank">{name}</a></td>

				<td style="white-space:nowrap;">
					<!-- <a href="javascript:void(0);" class="tablectrl_small bBlue {edit_control}" original-title="View" title="觀看" alt="觀看" onclick="article.add_edit_news({id});">
					<span class="iconb" data-icon="" ></span></a> -->								
					<a href="javascript:void(0);" class="tablectrl_small bGreen {edit_control}" original-title="Edit" title="編輯" alt="編輯" onclick="article.add_edit_news({id}, '{from_url}');">
					<span class="iconb" data-icon="" ></span></a>
					<!-- <a href="javascript:void(0);" class="tablectrl_small bRed {edit_control}" original-title="Del" title="刪除" alt="刪除" onclick="article.del({id});">
					<span class="iconb" data-icon="" ></span></a>		 -->	
					<a href="http://toments.com/{article_id}" target="_blank" class="article_link">{edit_user_id}</a>

					<span class="online {lock_fb_article}" data-index="{id}"> 
						<a href="javascript:void(0);" class="tablectrl_small bRed" original-title="Del" title="上鎖" alt="上鎖" onclick="article.lock_fb_article({id}, 'lock');">
						<span class="iconb" data-icon="" ></span></a>
						<a href="javascript:void(0);" class="tablectrl_small bBlue" original-title="Del" title="解鎖" alt="解鎖" onclick="article.lock_fb_article({id}, 'unlock');">
						<span class="iconb" data-icon="" ></span></a>
					</span>			
				</td>														
			</tr>			
			{/content}
		</tbody>
	</table>
	
</div>
{page_list}
<script src="js/article.js"></script>
<script src="js/span_editor.js"></script>
<style>

thead TD{
	white-space: nowrap;
}
.hide, .topicview_{
	display: none;
}
.topicview_hide{
	display: initial;
}
/*.tLight tbody td{
	padding: 0px 16px;
}*/
.external_link{
	background: none;
}
.more_page{
	text-align: center;
}

.more_page SPAN {
	-moz-box-shadow:inset 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;
	box-shadow:inset 0px 1px 0px 0px #ffffff;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #dfdfdf));
	background:-moz-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
	background:-webkit-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
	background:-o-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
	background:-ms-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
	background:linear-gradient(to bottom, #ededed 5%, #dfdfdf 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#dfdfdf',GradientType=0);
	background-color:#ededed;
	-moz-border-radius:6px;
	-webkit-border-radius:6px;
	border-radius:6px;
	border:1px solid #dcdcdc;
	display:inline-block;
	cursor:pointer;
	color:#777777;
	font-family:arial;
	font-size:15px;
	font-weight:bold;
	padding:6px 14px;
	text-decoration:none;
	text-shadow:0px 1px 0px #ffffff;
}
.more_page SPAN.page_selected {		
	color:#c92200;
	text-shadow:0px 1px 0px #ded17c;
}

.more_page SPAN:hover {
	color:#ffffff;
	text-shadow:0px 1px 0px #528ecc;
	-moz-box-shadow:inset 0px 1px 0px 0px #bbdaf7;
    -webkit-box-shadow:inset 0px 1px 0px 0px #bbdaf7;
    box-shadow:inset 0px 1px 0px 0px #bbdaf7;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #378de5), color-stop(1, #79bbff));
	background:-moz-linear-gradient(top, #378de5 5%, #79bbff 100%);
	background:-webkit-linear-gradient(top, #378de5 5%, #79bbff 100%);
	background:-o-linear-gradient(top, #378de5 5%, #79bbff 100%);
	background:-ms-linear-gradient(top, #378de5 5%, #79bbff 100%);
	background:linear-gradient(to bottom, #378de5 5%, #79bbff 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#378de5', endColorstr='#79bbff',GradientType=0);
	background-color:#378de5;
	
	border:1px solid #84bbf3;
}
.more_page SPAN:active {
	position:relative;
	top:1px;
}
form#list_orderby {
	float: left;
	margin: 20px 10px;
}
.article_total {
	float: left;
	margin: 25px 10px 20px 10px;
	font-size: 16px;
	font-weight: bold;
}
.article_total span{
	color: rgb(0,0,255);
}
.tLight tbody td.red_string{
	color: red; 
}
.tLight tbody td.blue_string{
	color: blue; 
}
.tLight tbody td.green_string{
	color: green; 
}

.write_article {
	float: right;
	margin: 15px;
}

.hot_tbody tr.user_row:not(.article_0) {
	background: rgba(255,106,106,.4);
}

.hot_tbody tr.user_row:not(.article_0) .tablectrl_small {
	display: none;
}

.hot_tbody tr.user_row.article_0 .article_link {
	display: none;
}

.most_likes {
	color: rgb(0,0,215);
	font-weight: bold;
}

.hot_tbody tr.user_row.off {
	background: rgba(255,106,106,.2);
}

.online, .online A {
	position: relative;
}

.online A:first-child {
	z-index: 100;
}

.online A:not(:first-child ) {
	left: -26px;
	z-index: 1;
	opacity: 0;
	filter: alpha(opacity = 0);
	-moz-transform: rotateY(180deg);
	-webkit-transform: rotateY(180deg);
	transform: rotateY(180deg);
}

.online.off A:first-child {
	/*opacity: 0;
	filter: alpha(opacity = 0);
	-moz-transform: rotateY(-180deg);
	-webkit-transform: rotateY(-180deg);
	transform: rotateY(-180deg);*/
	display: none;
}

.online.off A:not(:first-child ) {
	opacity: 1;
	filter: alpha(opacity = 100);
	-moz-transform: rotateY(0deg);
	-webkit-transform: rotateY(0deg);
	transform: rotateY(0deg);
}
</style>

<script>
$(document).ready(function() {
    var day = $('#days_selected').val();
    $('#'+day).attr("selected", "selected");
	
    $('#days').on("change", function() {
		$('#list_orderby').submit();
	});	
});

function order_by(this_td){

	$('#order_by').val( $(this_td).attr('data-orderby'));

	$('#list_orderby').submit();

}

$(".order_control").live( "click", function() {
	var order_name = $(this).attr('data-orderby');
	console.info(this);
	if(order_name) order_by(this);
	
	return false;
});	

$('[tips]').hover(
	function() {
	    var x = $(this).offset().left;
	    var y = $(this).offset().top;
		var tips_box = document.createElement('div');
	    $(tips_box).attr("id","tips_box").html($(this).attr('tips')).appendTo('body');

	    var tips_width = $('#tips_box').width()+40;
	    var tips_height = $('#tips_box').height();
	  
	    var this_width = $(this).width();
	    var this_height = $(this).height();
	 
	    var diff_x = (tips_width + x > $(window).width())?$(window).width()-(tips_width + x):((this_width-$('#tips_box').width())/2);
	    var c_x = x + diff_x ;

	    var diff_y = (tips_height + y > $(window).height())?$(window).height()-(tips_height + y):(this_height+(tips_height)/2);
	    var c_y = y + diff_y ;
		 
	    $(tips_box).css('left',c_x).css('top',c_y).show();
	  			   
	}, function() {
		$('#tips_box').remove();
	}
);
</script>