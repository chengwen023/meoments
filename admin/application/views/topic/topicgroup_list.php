<STYLE>
tbody IMG {
	display: inline-block;
}

tbody IMG.focus {
	width: 150%;
	border: 2px dotted red;
}

.group_match {
	float: right;
	width: 550px;
	height: 600px;
	overflow: scroll;
	position: relative;
}

.group_match:BEFORE {
	content: attr(data-title);
	display: block;
	font-size: 22px;
	border-bottom: 1px solid #333;
	margin-bottom: 5px;
}

.group_match LI {
	float: left;
	width: 120px;
	border-bottom: 1px solid rgba(0,0,0,.1);
	line-height: 24px;
	cursor: pointer;
}
.group_match LI.selected{
	background: rgba(33,66,99,0.2);
border-radius: 5px;
}
.group_match LI:HOVER {
	border-bottom: 1px solid rgba(0,0,0,1);
}
.group_match LI:BEFORE {
	content: attr(data-index);
	float: left;
	margin-right: 5px;
	padding: 0 2px;
	border-right: 1px solid #369;
	width: 20px;
	text-align: right;
	color: #369;
}
.group_match_title{
	position: absolute;
	right: 0;
	top: 0;
	margin-right: 10px;
	color: #369;
	font-size: 20px;
}
</STYLE>
<div class="widget">
	<table cellpadding="0" cellspacing="0" class="tLight"
		style="float: left;">
		<form id="list_orderby" method="post">
			<input id="order_by" type="hidden" name="order_by" value="">
			<thead>
				<tr>
					<td title="群組名稱" class="order_control" data-orderby="name">帳號<i
						class="icon-chevron-{by-name}"></i></td>

					<td title="新增資料" width="150px;" style="cursor: pointer;"
						onclick="account.edit_account('');">新增資料<i class="icon-plus-sign"></i></td>
				</tr>
			</thead>
		</form>
		<tbody>
			{content}
			<tr class="select_tr" data-index="{auto_index}"  data-title="{name}" class="user_row">
				<td>{name}</td>
				<td></td>
			</tr>
			{/content}
		</tbody>
	</table>
	<div class="group_match" data-title="I-Kuso">
		<div class="group_match_title" data-index="">益置</div>
		<ul>
			{ikuso_group}
			<li data-index="{gid}">{name}</li> {/ikuso_group}
		</ul>
	</div>
</div>



<script>
$(".group_match LI").on( "click", function() {
	if($(this).hasClass("selected") )$(this).removeClass("selected");
	else $(this).addClass("selected");

	var index = $(".group_match_title").attr("data-index");

	
	$.ajax({
		url: 'topic_group/set_pofans_group',
	  cache: false,
	  dataType: 'html',
	  type: 'POST',
	  data:{
		index:index,
		fans_gids:get_group_selected()  
	  },
	  success: function(data){
	  	
	  }
	});
	
});	

$(".select_tr").on("click",function(){
	var index = $(this).attr("data-index");
	
	if(index < 1) return false;
	
	$(".group_match_title").attr("data-index",index).html($(this).attr("data-title"))
	
	$.ajax({
		url: 'topic_group/get_pofans_group',
	  cache: false,
		dataType: 'json',
	  type: 'POST',
	  data:{
		index:index  
	  },
	  success: function(data){
		  $(".group_match LI").removeClass("selected");
		  if(data){
				for(var k in data){
					$('.group_match LI[data-index="'+data[k]+'"]').addClass("selected");
				}
			  	}
	  }
	});
	
});

function get_group_selected(){

	var group_arr = new Array();
	$(".group_match LI.selected").each(function(){
		group_arr.push($(this).attr('data-index'))

		});
	return group_arr;
}

function order_by(this_td){

	$('#order_by').val( $(this_td).attr('data-orderby'));

	$('#list_orderby').submit();

}

$(".order_control").live( "click", function() {
	var order_name = $(this).attr('data-orderby');
	if(order_name) order_by(this);
	
	return false;
});	

</script>

