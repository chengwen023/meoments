<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ifunso_tests extends CI_Controller {
	
	var $menu_index;
	
	
	public function __construct(){
		parent::__construct();
		
		$this->menu_index = 7;//
		$this->load->helper('url');
		$this->userInfo = $this->session->userdata('userInfo');
		$this->load->model('admin_model', 'admin');
		$this->load->model('topic_model', 'topic');
		
		
		
		$this->data['admin_menu'] = $this->config->item('admin_menu');
		$this->data['admin_menuGroup'] = $this->config->item('admin_menuGroup');
		$this->data['userInfo'] = $this->userInfo;
		
		// css & js
		$this->page_obj->css[] = 'js/jquery.treeview/jquery.treeview';
		$this->page_obj->css[] = 'js/Jcrop-v0.9.12/css/jquery.Jcrop.min';
		$this->page_obj->js[] = 'js/jquery.treeview/jquery.treeview';
		$this->page_obj->js[] = 'js/Jcrop-v0.9.12/js/jquery.Jcrop.min';
		$this->page_obj->js[] = 'js/img_box';
		$this->page_obj->js[] = 'js/google_img';
		$this->page_obj->js[] = 'js/horoscope';
		$this->page_obj->js[] = 'js/choose';
		
		
	}
	/*
	* index
	*/
	public function index(){
		$this->lists();
	}
	
	/**
	 *	list
	 */
	function lists(){
		
		
		
		$data = $this->data;
		
		if(false === $activestatus = $this->admin->checkCompetence_getMenuInfo( $this->menu_index, 'V')){
			$this->parser->parse('template', $data);
			return false;
		}
		
		$defaultstatus = (isset($activestatus))?$activestatus:$this->config->item('defaultstatus');
		
		
		$data['title'] = $this->admin->getTitle($this->menu_index);
		//讀取以建立的題目INDEX
		$this->db->flush_cache();
		$this->db->select("tests_question_fk");
		$this->db->from("ifunso_test");
		$query = $this->db->get();
		
		$ifunso_test = $query->result_array();
		foreach($ifunso_test AS $value){
			$ifunso_test_fk[] = $value['tests_question_fk'];			
		}
		
		$ifunso_tests = $this->load->database('ifunso_tests',true);
		
		$ifunso_tests->flush_cache();
		$ifunso_tests->select("question.*,img_upload.file_path");
		$ifunso_tests->from("question");
		$ifunso_tests->join("img_upload", 'img_upload.pk_img_upload = question.fk_img');
		$ifunso_tests->group_by("question.pk_question");
		if(isset($ifunso_test_fk) && is_array($ifunso_test_fk)){
			$ifunso_tests->where_not_in('question.pk_question', $ifunso_test_fk);
		}
		switch($this->userInfo['id']){
			case 'user01': 
				$ifunso_tests->where('question.pk_question <=', 113);				
				break;
			case 'user02': 
				$ifunso_tests->where('question.pk_question >', 113); 
				$ifunso_tests->where('question.pk_question <=', 214); break;
			case 'user03':
				$ifunso_tests->where('question.pk_question >', 214);
				$ifunso_tests->where('question.pk_question <=', 314); break;
			case 'user04':
				$ifunso_tests->where('question.pk_question >', 314);
				$ifunso_tests->where('question.pk_question <=', 414); break;					
			case 'user05':
				$ifunso_tests->where('question.pk_question >', 414);
				$ifunso_tests->where('question.pk_question <=', 514); break;
			case 'user06':
				$ifunso_tests->where('question.pk_question >', 514);
				$ifunso_tests->where('question.pk_question <=', 614); break;
			case 'user07':
				$ifunso_tests->where('question.pk_question >', 614);
				$ifunso_tests->where('question.pk_question <=', 714); break;
			case 'user08':
				$ifunso_tests->where('question.pk_question >', 714);
				$ifunso_tests->where('question.pk_question <=', 815); break;
			case 'user09':
				$ifunso_tests->where('question.pk_question >', 815);
				$ifunso_tests->where('question.pk_question <=', 915); break;
			case 'user10':
				$ifunso_tests->where('question.pk_question >', 915); break;
			
			default:$ifunso_tests->limit(10);break;
		}
		
		$query = $ifunso_tests->get();
		
		$data['content'] = $query->result_array();
		
		foreach($data['content'] AS $key => $value){
			$dir_path = explode('_', $value['file_path']);
			
			$img_path = "http://tests.ifunso.com/uploads/test/result/".$dir_path[0]."/".$value['file_path'];
			$data['content'][$key]['file_path'] = $img_path;
		}
		

		
		$data['main_content'] = $this->parser->parse('ifunso_test/ifunso_test_list', $data, true);
		$this->parser->parse('template', $data);
		
	}
	
	
}