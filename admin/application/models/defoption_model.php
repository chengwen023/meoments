<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *	Adonis
 *	後台預設資料讀取
 */

class Defoption_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
		
		
	}
	
	
	function check_defoption_exist_and_insert($add_input,$add_options){
		$result = array('success' => 'N', 'msg' => '');
		
		$this->db->flush_cache();
		$query = $this->db->select("COUNT(*) AS qun")
		->from("article_defoption_group")
		->where("defoptions_group_name", $add_input['defoptions_group_name'])
		->where("online", '1')
		->get();
		$temp = $query->row_array();
	
		if($temp['qun'] == 0){
	
			$this->db->insert('article_defoption_group', $add_input);		

			//重新建立選項
			$defoptions_select_index = $this->db->insert_id();
			if($defoptions_select_index){
				$this->db->delete('article_defoptions', array('defoptions_group_index' => $defoptions_select_index));				
				foreach($add_options AS $key => $url){
					$url['defoptions_group_index'] = $defoptions_select_index;						
					$this->db->insert('article_defoptions', $url);
				}
			}
			
			$result['success'] = 'Y';
			$result['msg'] = $defoptions_select_index;
		}else{
			$result['msg'] = '此題目已經有了哦!!!';
		}
		return $result;
	}
	
	function check_defoption_exist_and_update($add_input,$add_options,$index){
		$result = array('success' => 'N', 'msg' => '');
	
		$this->db->flush_cache();
		$query = $this->db->select("COUNT(*) AS qun")
		->from("article_defoption_group")
		->where("auto_index", $index)
		->get();
		$temp = $query->row_array();
	
		if($temp['qun'] == 0){
			$result['msg'] = '無該筆資料';
		}else{
	
			$this->db->where('auto_index', $index);
			$this->db->update('article_defoption_group', $add_input);
			
			//重新建立選項
			$defoptions_select_index = $index;
			if($defoptions_select_index){
				$this->db->delete('article_defoptions', array('defoptions_group_index' => $defoptions_select_index));				
				foreach($add_options AS $key => $url){
					$url['defoptions_group_index'] = $defoptions_select_index;						
					$this->db->insert('article_defoptions', $url);
				}
			}
			
			$result['success'] = 'Y';
			$result['msg'] = '修改成功';
		}
		return $result;
	}
	
	function del_defoption_by_index($index){
		$this->db->flush_cache();
		$this->db->delete('article_defoption_group', array('auto_index' => $index));
		$this->db->delete('article_defoptions', array('defoptions_group_index' => $index));
	
		$tmp = array('success' => 'Y', 'msg' => '已刪除', 'index' => $index);
		return $tmp;
	
	}
}