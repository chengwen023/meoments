
var account = new Object;

account.edit_account = function(index){
	
	$.fancybox.showLoading();
	$.ajax({
	  url: 'account/actions/add_account',
	  cache: false,
	  dataType: 'html',
	  type: 'POST',
	  data:{
		  index:index
	  },
	  success: function(data){
	  	$.fancybox.hideLoading();
	  	$.fancybox.open(data);
	  }
	});
}

account.edit_password = function(index){
	
	$.fancybox.showLoading();
	$.ajax({
	  url: 'account/actions/edit_password',
	  cache: false,
	  dataType: 'html',
	  type: 'POST',
	  data:{
		  index:index
	  },
	  success: function(data){
	  	$.fancybox.hideLoading();
	  	$.fancybox.open(data);
	  }
	});
}

account.edit_group = function(index){
	
	$.fancybox.showLoading();
	$.ajax({
	  url: 'account/actions/add_group',
	  cache: false,
	  dataType: 'html',
	  type: 'POST',
	  data:{
		  index:index
	  },
	  success: function(data){
	  	$.fancybox.hideLoading();
	  	$.fancybox.open(data);
	  }
	});
}

account.competence = function(index){
	
	var url = 'account/actions/competence';
	 
	$.fancybox.showLoading();
	$.ajax({
		url: url,
	  cache: false,
		dataType: 'html',
	  type: 'POST',
	  data: {
		  index:index
	  },
	  success: function(data){
	  	$.fancybox.hideLoading();
	  	$.fancybox.open(data);
	  }
	});
}

account.del = function(index){
	if( confirm("將會刪除此作者所有文章，是否確定刪除帳號?") ) {
		var url = 'account/actions/del';
		 
		$.ajax({
		  url: url,
		  cache: false,
		  dataType: 'html',
		  type: 'POST',
		  data: {
			  index: index
		  },
		  success: function(data){
		  	location.reload();
		  },error: function(data){
		  	console.info(data);
		  }
		});
	}
}

account.lock = function(index, status){
	var url = 'account/actions/lock';
	
	$.ajax({
	  url: url,
	  cache: false,
	  dataType: 'html',
	  type: 'POST',
	  data: {
		  index: index,
		  status: status
	  },
	  success: function(data){
	  	location.reload();
	  },error: function(data){
	  	console.info(data);
	  }
	});
}