<div class="widget">
	<div class="list_controllers" style="position: absolute;text-align: right;right: 0;margin-top: -25px;">		
		<a  href="http://i-gotest.com/code/fb_info.php" target="_blank" title="重整FB資料">重整FB資料<i class="icon-refresh"></i></a>
		<a  href="http://i-gotest.com/code/wall_status.php" target="_blank" title="重整分享資料">重整分享資料<i class="icon-refresh"></i></a>
		<a  href="http://i-gotest.com/code/remove.php" target="_blank" title="產生規格圖片">產生規格圖片<i class="icon-refresh"></i></a>
	</div>

	<table cellpadding="0" cellspacing="0" width="100%" class="tDefault tLight">
		<form id="list_orderby" method="post">
			<input id="order_by" type="hidden" name="order_by" value="">
			<thead>
				<tr>
					<td title="索引" 		class="order_control" data-orderby="topic_fromindex">索引<i class="icon-chevron-{by-topic_fromindex}"></i></td>
					<td title="題目" 		class="order_control" data-orderby="topic_title">題目<i class="icon-chevron-{by-topic_title}"></i></td>
					<td title="群組名稱" >群組名稱</td>
					<td title="選項類型" 		class="order_control" data-orderby="options_select">選項類型<i class="icon-chevron-{by-options_select}"></i></td>
					<td title="題目圖片" >題目圖片</td>
					<td title="選項圖片" 		>選項圖片</td>
					<td title="建立人員" >建立人員</td>
					<td title="最後修改日期" 	>最後修改日期</td>
					<td title="審核" 		>審核{total_state}</td>
					<td title="觀看" >觀看</td>
					<td title="上架日期" 		class="order_control" data-orderby="create_time">上架日期<i class="icon-chevron-{by-create_time}"></i></td>
					
				</tr>
			</thead>
		</form>
		<tbody>
			{content}
			<tr id="user_{auto_index}" class="user_row">
				<td>{auto_index}</td>
				<td style="white-space:nowrap;"><a href="http://i-gotest.com/q{auto_index}.html"
					target="_blank">{topic}</a></td>
				<td>{group_names}</td>
				<td>{select_optionName}</td>
				<td><img class="editable" src="../uploads/q{auto_index}/t_m.jpg" style="width: 60px;" />
				<td><img src="../uploads/q{auto_index}/s_m.jpg" style="width: 60px;" />
				<td>{id}</td>
				<td>{update_time}</td>
				<td>{plus_state}</td>
				<td><a href="javascript:void(0);" class="tablectrl_small bBlue" original-title="View" title="觀看" alt="觀看" onclick="topic.view({auto_index});"><span class="iconb" data-icon="" ></span></a></td>
				<td>{create_time}</td>
				
			</tr>
			{/content}
		</tbody>
	</table>
</div>
<div class="widget" style="width: 100%;overflow: scroll;">{page_list}</div>

<script src="js/topic.js"></script>
<style>


.order_control:HOVER {
	cursor: pointer;
}
.tLight tbody td{
	padding: 0px 16px;
}

#pofans_batlog li.sending:AFTER{
		content:'Sending';
		float: right;
}
#pofans_batlog li.error:AFTER{
	content:'Error';
	float: right;
}
.pofans {
	position: absolute;
	display: none;
	background-color: rgba(21, 21, 21, 0.6);
	padding: 10px;
	height: 430px;
	border-radius: 10px;
}

.pofans.focus {
	display: block;
	right: 250px;
	margin-top: -20px;
	color: #fff;
}

.pofans TEXTAREA {
	width: 400px;
	overflow: auto;
}

.pofans .send_btn {
	float: right;
	margin-bottom: 10px;
}

.external_link {
	background: none;
}
	.more_page, .page_num{
	float: left;
	padding: 5px;
	display: inline-flex;
	line-height: 0px;
	cursor: pointer;
	margin: 0 5px;
}
.page_num:HOVER,.page_num.selected{
	border-bottom: 1px solid #000;
}
</style>

<script>




function order_by(this_th){

	$('#order_by').val( $(this_th).attr('data-orderby'));

	$('#list_orderby').submit();

}

$(".order_control").live( "click", function() {
	var order_name = $(this).attr('data-orderby');
	if(order_name) order_by(this);
	
	return false;
});	
</script>