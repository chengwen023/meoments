﻿var canvasToolConfig = {	'parent_id':'#control_btn',
	'bar': ['close', 'save','imgcut', 'water'],
	'evt': {
		'close':function(html,parent_object){						
			var parent_box = parent_object.parent_id;			
			$(html).click(function(e){								 
				$("#img_control").remove();
				init_img_controllor(true);
				$("#temp_div IMG.editable").unwrap();
			});
			
		},
		'save': function(html) {
			$(html).click(function(e) {
				save_img();
				// img_check($("#temp_div IMG.editable"));
				init_img_controllor(true);
				$("#temp_div IMG.editable").unwrap();	
			})
		},
		'imgcut': function(html) {
			$(html).click(function(e) {
				cut_jropimg();				
			})
		},
		'water': function(html) {
			$(html).click(function(e) {
				watermark();			
			})
		}
	},
	'icon': {
		'close': 'icon-close', 'save': 'icon-archive', 'imgcut': 'icon-images', 'water': 'icon-copy'
	},
	'config': {
		'close': {'title': '關閉','imgcut': '裁切','water': '浮水印'}
	}
}
var defaultToolConfig = {		'parent_id':'.ie-tool-box',		 'bar': ['Config', 'Search', '-', 'Img', 'Crop', 'Upload', '-', 'Trash', 'Save'],		 'icon': {		  'config': [3, 3],		  'crop': [9, 10],		  'search': [9, 1],		  'menu': [6, 9],		  'img': [1, 13],		  'trash': [14, 2],		  'upload': [6, 3],		  'save': [8, 10],		  'car': [12, 2],		  'close': [7,1]		 },		 'config': {		  'config': {		   'title': '設定'		  },		  'crop': {		   'title': '裁剪'		  },		  'search': {		   'title': '搜尋'		  },		  'menu': {		   'title': '選單'		  },		  'img': {		   'title': '圖片庫'		  },		  'trash': {		   'title': '垃圾桶'		  },		  'upload': {		   'title': '上傳'		  },		  'save': {		   'title': '儲存'		  },		  'car': {		   'title': '移動'		  },		  'close':{		   'title': '關閉'		  }		 },		 'evt': {		  'close': function(html){		   $(html).click(function(e){		    window.iframeEDT.close(); 		   });		  },		  'config': function(html) {		   $(html).click(function() {		    console.log('config');		   });		  },		  'crop': function(html) {		   $(html).click(function() {		    var canvas = iframeEDT.current_panel;		    if (canvas) canvas.crop();		   });		  },		  'search': function(html) {		   $(html).click(function() {		    console.log('search');		   });		  },		  'trash': function(html) {		   $(html).click(function() {		    iframeEDT.empty().		    default ()		   });		  }		 }		}


var Tool = function(customToolConfig) {
	var dtc = defaultToolConfig;
	var ctc = customToolConfig;
	if (typeof ctc == 'object') {		
		for (var k in defaultToolConfig) {
			ctc[k] =  ctc[k] || dtc[k];
		}
	} else {		console.info('def');
		ctc = dtc;
	}
	var config = ctc;
	var bar = config.bar.slice(0);
	this.icon = config.icon;
	this.config = config.config;
	this.evt = config.evt;
			this.parent_id = config.parent_id;
	var toolBox = $('.ie-tool-box');		var new_toolBox = document.createElement('div');	var new_toolBody = document.createElement('div');	toolBox = this[0] = (toolBox.length != 0) ? toolBox[0] : $(new_toolBox).addClass('ie-tool-box').appendTo(this.parent_id)[0];	var toolBody = $('.ie-tool-body.tra_right');	toolBody = (toolBody.length != 0) ? toolBody : $(new_toolBody).addClass('ie-tool-body').addClass('tra_right').appendTo(toolBox);	toolBody.empty();

	

	this.items = {};
	while (bar.length) this.addItem(bar.shift());
}
Tool.prototype.addItem = function(itemName) {		
	if (typeof itemName == 'object') {
		var itemCofig = itemName
	} else if (typeof itemName == 'string') {
		itemName = itemName.toLowerCase();
		var itemCofig = {
			name: itemName,
			icon: (typeof this.icon[itemName] != 'undefined') ? this.icon[itemName] : defaultToolConfig.icon[itemName],
			config: (typeof this.config[itemName] != 'undefined') ? this.config[itemName] : defaultToolConfig.config[itemName],
			evt: (typeof this.evt[itemName] != 'undefined') ? this.evt[itemName] : defaultToolConfig.evt[itemName]
		};
	}	itemCofig.toolBox = this;
	var item = new toolItem(itemCofig).appendTo($(this[0]).find('.ie-tool-body'));
	this.items[itemCofig.name] = item;
	return item;
}


var toolItem = function(itemConfig) {
	var div = document.createElement('div');
	var icon = itemConfig.icon;
	var name = itemConfig.name;
	var config = itemConfig.config;
	var evt = itemConfig.evt;

	if (name == '-') {
		name = 'hr';
		this[0] = $(div).addClass('ie-tool-' + name)[0];
		return this;
	}

	var html = this[0] = $(document.createElement('div')).addClass('ie-tool-' + name)[0];
	this.useIcon(icon);

	/* set config */
	for (var k in config) {
		var v = config[k];
		html.setAttribute(k, v);
	}

	if (typeof evt == 'function') evt(html,itemConfig.toolBox);

}
toolItem.prototype.useIcon = function(class_name) {	var img_control = document.createElement('div');	$(img_control).addClass("ie-icon").addClass(class_name).appendTo($(this[0]));
	return this;
}
toolItem.prototype.appendTo = function(parent) {
	$(this[0]).appendTo(parent);
	return this;
}