

	$("IMG.editable").each(function(){
		this.addEventListener("dragenter", dragEnter, false);
		this.addEventListener("dragexit", dragExit, false);
		this.addEventListener("dragover", dragOver, false);
		this.addEventListener("dragleave", dragLeave, false);
		this.addEventListener("drop", drop, false);
		$(this).mouseenter(
				function(){	
					
					if($(this).parent().hasClass('temp_bar') || typeof( $(this).attr("data-src") ) == "undefined") return false;
					else var This_Img = this;
					
					$(this).wrap('<div class="temp_bar" style="text-align: center;"></div>').width($(this).width());
					
					var img_control = document.createElement('div');
					$(img_control).attr("id","img_control");
					
					$(this).parent().append(img_control);
					
					$('#img_control').append('<span class="icon-repeat"  style="position: relative;top: -40px;background-color: #fff;cursor: pointer;"></span>');
					$('#img_control').append('<span class="icon-ok"  style="position: relative;top: -40px;background-color: #fff;cursor: pointer;"></span>');
					
					$('#img_control SPAN').on("click",function(){
						console.info($(this).attr("class"));
						if( $(this).attr("class") == 'icon-ok') save_img(This_Img);
						if( $(this).attr("class") == 'icon-repeat') reduction_img(This_Img);
					})
					
					$(this).parent().mouseleave(
							function(){
								$('#img_control').remove(); 
								$(this).children("IMG.editable").unwrap();
							});
					
				});
		
		$(this).load(function() {

			if( typeof( $(this).attr("data-src") ) == "undefined" || 
				typeof( $(this).attr("data-width") ) == "undefined" ||
				typeof( $(this).attr("data-height") ) == "undefined")
			{				
				return false;
			}
				
			
			var width = this.naturalWidth;
			var height = this.naturalHeight;

			
			if(width == $(this).attr("data-width") && height == $(this).attr("data-height")) 
			{
				if( false == confirm("是否變更圖片") ){
					reduction_img(this);
				}else if(false) {
					save_img(this);
				}
			}	
			else {
				alert('圖片規格不符');
				reduction_img(this);		
			}	
			
		});
		$(this).error(function(){
			if( typeof($(this).attr("data-src")) != 'undefined' ){
				alert('圖片讀取有誤!');
				reduction_img(this);	
			}
			
		});
		
		
	});
	function reduction_img(evt){
		var reduction =  $(evt).attr("data-src");
		$(evt).removeAttr( "data-src" );
		$(evt).attr('src',reduction);
		$(evt).removeClass('Upload_img');
		
		$('#img_control').remove(); 
	}

	function save_img(evt){
		$(evt).removeClass('Upload_img');
		 var file = $(evt).attr("data-src");
		 var img = $(evt).attr('src');
		 
		 var regex=/\?randload=\d+/;  

		 if (regex.exec(file)) {
			 file =	 file.replace(regex,"");
		 }
		$.ajax({
			  url: '/uploads/upload.php',
			  cache: false,
			  dataType: 'json',
			  type: "POST",
			  data: {
					'file':file,
					'img':img
				},
			  success: function(data){
				  if(data){
					  $('#img_control').remove(); 
					  $(evt).removeAttr("data-src").attr( 'src', data + '?randload=' +Math.ceil(Math.random()*10000000));
					  $(evt).unwrap();
					  console.info(data);  
				  }else{
					  alert('圖檔格式不符');
					  reduction_img(evt);
				  }
				  
			  },
			  error: function(data){
				  console.info('error');
			  }
			});
	
	}


	function dragEnter(evt) {
		evt.stopPropagation();
		evt.preventDefault();
		$(this).addClass("focus");
	}

	function dragExit(evt) {
		evt.stopPropagation();
		evt.preventDefault();	
		$(this).removeClass("focus");
	}

	function dragLeave(evt) {
		evt.stopPropagation();
		evt.preventDefault();
		$(this).removeClass("focus");
	}
	function dragOver(evt) {
		evt.stopPropagation();
		evt.preventDefault();	
	}

	function drop(evt) {

		if(( typeof( $(this).attr('data-src')) == 'undefined')) 
			$(this).attr('data-src',$(this).attr('src'));

		
		if(( typeof( $(this).attr('data-width')) == 'undefined')) 
			$(this).attr('data-width',this.naturalWidth);
		if( typeof( $(this).attr('data-height')) == 'undefined') 
			$(this).attr('data-height',this.naturalHeight);
		

		
		$(this).removeClass("focus");
		evt.stopPropagation();
		evt.preventDefault();

		var dat = $(evt.dataTransfer.getData('text/html'));
	    var img = dat.attr('src');
	    if (!img) img = dat.find("img").attr('src');
	    
		var files = evt.dataTransfer.files;
		var count = files.length;

		
		// Only call the handler if 1 or more files was dropped.
		if (count > 0){
			$(this).addClass('Upload_img');
			handleFiles(files);
		}
		else if(img){		
			$(this).attr('src',img);		
		}
	}


	
	function handleFiles(files) {
		var file = files[0];

		//document.getElementById("droplabel").innerHTML = "Processing " + file.name;

		var reader = new FileReader();

		// init the reader event handlers
//	 	reader.onprogress = handleReaderProgress;
		reader.onloadend = handleReaderLoadEnd;

		// begin the read operation
		reader.readAsDataURL(file);
	}

	function handleReaderLoadEnd(evt) {
		$(".Upload_img").attr('src',evt.target.result);

	}
	