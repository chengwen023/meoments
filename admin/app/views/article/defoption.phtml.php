<STYLE>
tbody IMG {
	display: inline-block;
}
tbody IMG.focus {
	border:2px dotted red;
}

</STYLE>




<div class="widget" >
	<table cellpadding="0" cellspacing="0"  class="tLight">
		<form id="list_orderby" method="post">
		<input id="order_by" type="hidden" name="order_by" value="">
		<thead>
			<tr>				
				<td title="群組名稱">名稱</td>				
				<td title="新增資料" width="150px;" style="cursor: pointer;" onclick="defoption_list.add_edit(0);">新增資料<i class="icon-plus-sign"></i></td>
			</tr>			
		</thead>
		</form>
		<tbody>
			<?php if(isset($content)):?>
			<?php foreach ($content AS $list):?>
			<tr class="user_row">
				<td><?php echo $list['defoptions_group_name'];?></td>				
				<td>	
					<a href="javascript:void(0);" class="tablectrl_small bGreen" original-title="Edit" title="修改" alt="修改" onclick="defoption_list.add_edit('<?php echo $list['auto_index'];?>');">
					<span class="iconb" data-icon="" ></span></a>
					<a href="javascript:void(0);" class="tablectrl_small bRed " original-title="Del" title="刪除" alt="刪除" onclick="defoption_list.del('<?php echo $list['auto_index'];?>');">
					<span class="iconb" data-icon="" ></span></a>					
				</td>				
			</tr>
			<?php endforeach;?>
			<?php endif;?>
		</tbody>
	</table>
</div>
<script src="js/defoption_list.js"></script>