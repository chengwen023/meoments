<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ad extends CI_Controller {
	
	
	var $fans_info;
	
	
	public function __construct(){
		parent::__construct();
		
		$this->load->driver('cache');
		$this->userInfo = $this->session->userdata('userInfo');
		$this->load->model('topic_model', 'topic');
		$this->load->model('theme_model', 'theme');
		
		
		$this->data['admin_menu'] = $this->config->item('admin_menu');
		$this->data['admin_menuGroup'] = $this->config->item('admin_menuGroup');
		$this->data['userInfo'] = $this->userInfo;
		$this->data['menu'] = $this->config->item('admin_accountMenu');
		$this->data['breadcrumbs'] = $this->config->item('breadcrumbs');
		$this->data['title'] =$this->config->item('admin_title');
		
		$this->data['topbar_control']= array();
		$this->data['select_result'] = array();
		
		
	}
	function add_row(){
	
		$add_input = $this->input->post('add_input');	
		$num = $this->db->select("auto_index")->from("ad_code") ->where("name", $add_input['name'])->get ()->num_rows;
		
		if($num) echo '名稱已重複!';
		else $this->db->insert('ad_code', $add_input);
		$this->cleanMemcached();
	}
	function del_row(){
	
		$index = $this->input->post('id');
		
		$this->db->delete('ad_code',array('auto_index' => $index));
		$this->cleanMemcached();
	}
	function update_code(){
		
		$index = $this->input->post('id');
		$code = $this->input->post('code');
		$filed = $this->input->post('filed');
		
		$add_input = array($filed =>$code);
		
		
		$this->db->where('auto_index', $index);
		$this->db->update('ad_code', $add_input);
		
		$this->cleanMemcached();
	}
	
	
	function cleanMemcached(){
		
		$this->load->driver('cache');		
		$this->cache->memcached->delGlobalMemcached('ad_code');
	}
	/*
	* index
	*/
	public function index(){
		$this->lists();
	}
	
	function lists(){
		
		$order_name = $this->input->post('order_by');	
		$orderBy = $this->theme->order_by($order_name);
		$data = array_merge($this->data, $orderBy);
		
		/*
		if(false === $activestatus = $this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, 'V')){
			$this->parser->parse('template', $data);
			return false;
		} 
		/**/
		$defaultstatus = (isset($activestatus))?$activestatus:$this->config->item('defaultstatus');
		
		
		
		$this->db->flush_cache();
		$this->db->select('*');
		$this->db->from('ad_code');
		
		$query = $this->db->get ();
		
		$data ['title'] = $this->admin_model->getTitle ( $this->menu_index );
		
		$data ['content'] = $query->result_array ();
		
		$data['main_content'] = $this->parser->parse('ad/ad_list', $data, true);
		
		$this->parser->parse('template', $data);
	
	}
	
	function yahoo(){
		$defaultstatus= 'S';
		$activestatus = 'A';
		
		$order_name = $this->input->post('order_by');	
		$orderBy = $this->theme->order_by($order_name);
		$data = array_merge($this->data, $orderBy);

		$this->menu_index = 'I-Kuso 粉絲團管理';//
		if(!$this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, 'V')){
			$this->load->view('template', $data);
				
			return false;
		}
		$query = $this->db->select('*')->from('ad_switch')->where('id', 1)->get();		
		if(empty($query->result_array())){
			$data = array('type' => 'yahoo' ,  'remark' => 'yahoo 機率');
			$this->db->insert('ad_switch', $data); 
		}

		$yahoo = $this->db->select('*')->from('ad_switch')->where('id', 1)->get()->result_array();
		
		$data['main_content'] = $this->parser->parse('ad/yahoo', $data, true);
		$this->parser->parse('template', $data);
	
	}
	
		/**
	 *	actions
	 */
	function actions($ac){
		switch($ac){	
			case 'yahoo_db':
				$sql = "SELECT * FROM `ad_switch`";
				$result = mysql_query($sql) ;
				while($temp = mysql_fetch_assoc($result)){
					$row[] = $temp;
				}
				echo json_encode($row);
				break;
			break;
			case 'yahoo_add_probability':
				$postdata = file_get_contents("php://input");  
				pre($postdata);
				//$this->request = json_decode($postdata,true);
				/*
				$data = array(
				   'title' => 'My title' ,
				   'name' => 'My Name' ,
				   'date' => 'My date'
				);

				$this->db->insert('mytable', $data); 
				*/
				break;
			break;
			
		}
	}
	
	
}