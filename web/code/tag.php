<?php 
	$start = microtime(true);
	session_start();
	header("Content-type: text/html; charset=utf-8");

	include_once("include/db.php");
	include_once("include/htmltpl.php");
	include_once("include/function.php");

	
	$curPageURL	=  curPageURL();
	$curServerURL = curPageURL(true);	
	$tagID = $_GET['tag'];
	$tag = explode("/", $_GET['tag']);	
	$tag = array_filter($tag);
	$tag_name = array();
	
	
	include_once('global_load.php');
	
	
	//判斷文章是否上架，導回首頁
	if(is_numeric($tagID) ) {
		$tag_total = TagOnlineCkeck($tagID,$curServerURL);
	}else{
		//熱門分類
		$tag_total = GetArticleTotal();
	}
	
	//點擊+1
	$SQL = "UPDATE news_tags SET daily_click = daily_click +1,total_click = total_click +1 WHERE  id = $tagID LIMIT 1";
	mysql_query($SQL);
	
	
	$tag_title = $tag_name[$tagID]['name'];
	
	if(!$tag_title) header("Location: $curServerURL");
	$tag_url = $curServerURL."/tag/$tag_title/";
	
	$pageNum = (int)((isset($_GET['page']) && $_GET['page'] > 0)?$_GET['page']:1);
	$pageRow = 18;
	$pageTotal = ceil($tag_total / $pageRow);
	if($pageNum > $pageTotal ) $pageNum = $pageTotal;
	  
	$limit = (($pageNum - 1)*$pageRow).", $pageRow";
	
	
	
	
	
	
	if(is_numeric($tagID)) $where = "AND `tag` LIKE  '%\"$tagID\"%' "; 	
	
	$order = ' `online_time` DESC ';
	
	$sql = " SELECT `news_article_onshelf`.title , $img_tm AS img, $listUrl AS url , nas.*
	FROM  news_article_onshelf  
	INNER JOIN (
		SELECT DISTINCT `id` , `tag` , `online_time` , `views`,`daily_views`
		FROM  `news_article_state` 
		WHERE `online` =  '1' $where  ORDER BY $order LIMIT $limit 
	) AS nas ON nas.id =  `news_article_onshelf`.id ";
	
	$memcached->set_prefix("tag[$tagID]");
	$listKey = 'pagelist_'.$pageNum;
	$keyValues[] = [
			'key' => $listKey ,
			'func' => $memcached->SQLfunc( $sql ),
			'expire' => 7200
	];

	$keyValues  = $memcached->getMultiWithSet($keyValues,86400);
	
	
	foreach($keyValues[$listKey] AS $row){
		if($row['tag']) {			
			$row['tag'] = array_values(array_intersect_key( $tag_name,	array_flip( json_decode($row['tag'])) ));			
		}
		
		$row['online_time'] = date('Y-m-d', $row['online_time']);		
		$data['article_list'][] = $row;
		
	}
	
	$head = array(   
   'url'  => $curPageURL,
   'baseUrl'   => $baseUrl,
   'title'  => $tag_title,
   'link:img_src' => $article['img'],
   'meta' => array(
            
     ['name' => 'name="keywords"'     ,'content'=> $tag_title ],
     ['name' => 'name="description"'    ,'content'=> $tag_title ],
     ['name' => 'property="og:description"'    ,'content'=> $tag_title ],
     
     ['name' => 'property="og:title"'   ,'content'=> $tag_title ],
     ['name' => 'property="og:image"'   ,'content'=> $ImgServerUrl.'/n'.$article['id'].'/f.jpg'],
     ['name' => 'property="og:type"'   ,'content'=> "article" ],
     ['name' => 'property="og:url"'   ,'content'=> $curPageURL ],
   	 ['name' => 'property="fb:admins"'    ,'content'=> $fb_admin ],
   	 ['name' => 'property="fb:app_id"'    ,'content'=> $fb_app_id ],
   )
 );
	
	
	
	
	
	$data['HEAD'][] = $head;
	$data['MENU'] = $category_name;
	$data['mobile_MENU'] = $tag_name;
	$data['curServerURL'] = $curServerURL;
	
	$data['pageUrl'] = $tag_url;
	$data['pageNum'] = $pageNum;
	$data['pageTotal'] = $pageTotal;
	
	$HTML['HTML'][] = $data;
	
	echo tpl_get_html("./tpl/","tag.tpl.html",$HTML);
	
	
?>