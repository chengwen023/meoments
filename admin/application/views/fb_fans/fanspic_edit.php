<style>

.imgurl_box,.imgurl_list{
	width:80px;
	height: 60px;
	border-style: dotted;
	text-align: center;
	line-height: 60px;	
	cursor: pointer;
	margin-left: 20px;
	float:left;
}

.imgurl_box.imgedit_focus{
	color:#D17A1C;
}

</style>
<div id="topic_info">
	<form id="form_folder">
		<div class="tb-green">
			<table>
				<thead>
					<tr class="column1">
						<th scope="col" colspan="2">
							<?php echo (!empty($topic_group['auto_index']) && $topic_group['auto_index'] > 0)? '編輯':'新增';?>管理員
							<input type="hidden" name="auto_index" value="<?php echo (!empty($topic_group['auto_index'])?$topic_group['auto_index']:'');?>">
						</th>
					</tr>
				</thead>
				<tr>
					<td width="66">
						分類名稱
					</td>
					<td>
						<?php echo empty($topic_group['name'])? '':$topic_group['name'];?>
					</td>
				</tr>
				<tr>
					<td width="66">
						上傳圖片
					</td>
					<td>
						<div class="imgurl_box" ><span><img src="uploads/temp/default_pic.jpg" width="70px" /></span>
							<input type="hidden" name="pic_url[]" value="" />
						</div>
						<div class="imgurl_box" ><span><img src="uploads/temp/default_pic.jpg" width="70px" /></span>
							<input type="hidden" name="pic_url[]" value="" />
						</div>
						<div class="imgurl_box" ><span><img src="uploads/temp/default_pic.jpg" width="70px" /></span>
							<input type="hidden" name="pic_url[]" value="" />
						</div>
						<div class="imgurl_box" ><span><img src="uploads/temp/default_pic.jpg" width="70px" /></span>
							<input type="hidden" name="pic_url[]" value="" />
						</div>
						<div class="imgurl_box" ><span><img src="uploads/temp/default_pic.jpg" width="70px" /></span>
							<input type="hidden" name="pic_url[]" value="" />
						</div>
						<div class="clear"></div>		
						<div id="img_selectbox" style="margin-top: 15px;">
							<div id="upload_img" class="control_imgtype" style="width:200px;margin:0px auto;display: none;">
								<input type="file" name="file_upload" id="img" />
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td width="66">
						圖片列表
					</td>
					<td >
						<ul style="width: 500px;list-style: none;">
						<?php foreach($group_pic AS $value => $group):?>
						<li style="float: left;width:100px;margin-right: 10px;">
							<div class="imgurl_list" ><span>
								<img src="uploads/temp/<?php echo $group['pic_url'];?>" width="70px" alt="<?php echo $group['auto_index'];?>"/>
							</span></div>						
						</li>
						<?php endforeach;?>
						</ul>
					</td>
				</tr>
				
				<tr>
					<td>
					</td>
					<td colspan="2">
						<input type="submit" value="送出" style="padding:3px;font-size:14px;" />
					</td>
				</tr>
			</table>
		</div><!-- / tb-green-->
	</form>
</div>
<div id="img_clipbox" style="padding-top:15px;width:500px;float: left;overflow:visible;"></div>
<script>
var site = '<?php echo $this->config->item('base_url'); ?>';
upload_clip(site, 'img');
set_imgurlbox();

function set_imgurlbox(){
	$(".imgurl_box").live( "click", function() {
		$(".imgurl_box").removeClass("imgedit_focus");
		$(this).addClass("imgedit_focus");
		$(this).children('INPUT[type="hidden"]').val('');
		img_select(this);
		return false;
	});	
	
}

function img_select(this_div){
	$("#upload_img").show();
}

function upload_clip(site, target_box){
	// 上傳圖片
	$("#"+target_box).uploadify({
		'buttonCursor'		: 'hand',
		'fileTypeDesc'		: 'Image Files',
    'fileTypeExts'		: '*.gif; *.jpg; *.png',
    'buttonText' 			: '選擇上傳',
    width							: 68,
    height						: 24,
		'multi'    				: false,
	  'swf'        			: site+'js/libs/uploadify/uploadify.swf',
	  'uploader'			  : site+'_admin/uploads/queImg',
    'onUploadSuccess' : function(file, data, response){
    	tmp = data.split(",");
    	if(tmp[0] != 'N'){
        	
	    	item = site+'uploads/source/'+tmp[0];
	  		
				$.ajax({
					url: '_admin/choose/actions/upload_img',
				  cache: false,
				  dataType: 'html',
				  async : false,
				  type: 'POST',
				  data : 'img='+item+'&width='+tmp[2]+'&height='+tmp[3],
				  success: function(data){
					  $('#topic_info').hide();
				  	  $('#img_clipbox').html(data);
				  }
				});
			}else{
				alert('上傳圖片最小為 500 x 375 !!');
			}
    }
	});
}
//確認選好圖片，進入裁圖畫面
function img_check(){
	if($('.img_select').length < 1){
		alert('圖片最少選擇一張!!');
		return false;
	}else if($('.img_select').length > 1){
		alert('圖片最多只能選擇一張!!');
		return false;
	}
	
	$.ajax({
		url: '_admin/choose/actions/check_img',
	  cache: false,
	  dataType: 'html',
	  async : false,
	  type: 'POST',
	  data : 'img='+$('.img_select').attr('rel')+'&alt='+$('.img_select').attr('alt')+'&item='+$('input[name="search_item"]').val()+'&pg='+$('.img_pg').attr('rel'),
	  success: function(data){		  
	  	tmp = data.split(',');
	  	if(tmp[0] == 'N'){
	  		alert(tmp[1]);	  		
	  	}else{
	  		$('#topic_info').hide();
	  		$('#img_clipbox').html(data);
	  		
		  }
	  }
	});
}



	$(function(){
		// 註冊驗證
		$("#form_folder").validate({			
			submitHandler: function(form) {
				
				$.fancybox.showLoading();
				// 透過 Ajax 驗證是否註冊
				$.ajax({
				  url: '_admin/fb_fans/actions/validate_pic',
				  cache: false,
				  dataType: 'json',
				  type: "POST",
				  data: $("#form_folder").serialize(),
				  success: function(data){
					
				  	$.fancybox.hideLoading();
				  	
				  	if(data.success == 'Y' ){
				  		alert(data.msg);
				  		location.reload();
				  	}else{
				  		alert(data.msg);
				  		$('input[name="id"]').focus();
				  		return false;
				  	}
				  	/**/
				  }
				});
				//form.submit();
				return false;
			}
		});
		// end of $("#signupForm").validate
	});
</script>