<link href="css/iframeEDT.css" rel="stylesheet" type="text/css">
<script src="js/libs/jquery.Jcrop.min.js"></script>
<script src="js/tool.js"></script>
<script src="js/span_editor.js"></script>
<link rel="stylesheet" href="css/jquery.Jcrop.min.css" type="text/css" />
<script src="js/jquery.tagsinput.js"></script>

<style>
.drag {
    line-height: 70px;
    cursor: pointer;
    border: 1px solid #fff;
    float: left;
    margin-left: 5px;
    text-align: center;
    font-size: 12px;
}

#dragbasic {
    width: 500px;
    height: 375px;
    border: 1px solid gray;
    background-color: #E0F0FF;
    float: left;
}
/*.default_options{
	float: left;
	width: 400px;
	margin-top: 38px;
	overflow-y: scroll;
	height: 240px;
	border: 1px dashed #b2b2b2;
}
.default_options UL{
	margin:0;
	width:100%;
	display: block;
	cursor: pointer;
}*/
/*.default_options UL:BEFORE{
	content: attr(data-title);
	display: block;
	color: #034A0E;	
	margin: 0 10px;
	font-size: 14px;
}
.default_options UL:HOVER{
	border-bottom: 1px solid #034A0E;
}
.default_options UL LI{
	margin-left: 10px;
}
.default_options UL:HOVER:AFTER,.default_options UL.checked:AFTER {
	content: ' ';
	padding: 10px 0px 0px 30px;
	background: url(images/check_green.png) no-repeat left;
}*/
#img_list IMG:not([data-check]){
	margin-bottom: 20px;
	border: 2px dotted #369;
	padding: 3px;
}
#img_list IMG{
	width: 120px;
	display:block;
}
#img_list IMG[data-check="check_ok"] {
	background: url(images/check_green.png) right bottom no-repeat;
	padding-right: 30px;
}

#img_selectbox,
#category_selectbox,
#tag_selectbox {
	display: none;
	width: 550px;
	height: 420px;
	text-align: center;
	background-color: #94b0dc;
}

.fb_view_control,
.category_control {
	background-color: #336699;
	color: #fff;
	height: 30px;
}

.fb_view_control SPAN,
.category_control SPAN {
	margin-right: 10px;
	padding: 2px 5px;
	font-size: 16px;
}

.fb_view_control SPAN:HOVER, 
.category_control SPAN:HOVER {
	cursor: pointer;
	color: #3366cc;
	background-color: #fff;
}

.fb_view {
	width: 500px;
	height: 375px;
	margin: auto;
	border: 1px solid red;
}

.fb_link {
	width: 500px;
	height: 260px;
}

.fb_link_s {
	width: 375px;
	height: 375px;
}

.topic_photo {
	border: 1px solid;
	box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3);
	padding: 7px 5px 7px 7px;
	margin-right: 15px;
	margin-bottom:15px;
	float: left;
	margin-top: 25px;
	position: relative;
}
.topic_photo:before{
	content: attr(data-title);
	position: absolute;
	top: -20px;
}
.topic_group {
	width: 340px;
	float: left;
	margin: 10px;
}

.topic_group STRONG {
	color: #000;
	float: right;
	cursor: pointer;
	border-bottom: 1px solid #999;
}

.topic_group UL {
	margin: 0;
	display: none;
}

.topic_group.editable UL.options_select {
	position: absolute;
	display: inline-block;
	margin-top: 30px;
	background-color: #fff;
	border: 1px solid #D6D6D6;
	border-radius: 10px;
	padding: 5px 5px;
	box-shadow: 3px 3px 4px rgba(0, 0, 0, 0.2);
	z-index: 10;
}

.topic_group UL LI {
	float: left;
	margin-right: 10px;
	cursor: pointer;
}

.topic_group UL LI:AFTER {
	content: ' ';
	padding: 10px 0px 0px 30px;
}

.topic_group UL LI:HOVER {
	color: #585858;
}

.topic_group UL LI:HOVER:AFTER {
	background: url(images/check_green.png) no-repeat left;
}

.topic_group #topic_group_name UL,
.topic_group #tag_name UL {
	display: block;
	float: left;
}

.topic_group #topic_group_name UL LI.checked:AFTER,
.topic_group #tag_name UL LI.checked:AFTER {
	background: url(images/check_green.png) no-repeat left;
}

.audit_left.not_pass:BEFORE {
	content: ' ';
	padding: 10px 0px 10px 30px;
	background: url(images/not_pass.gif) no-repeat left;
	position: absolute;
	margin-left: -30px;
}

.audit_right.not_pass:AFTER {
	content: ' ';
	padding: 10px 0px 10px 30px;
	background: url(images/not_pass.gif) no-repeat left;
}
.topic_photo.not_pass:BEFORE {
	content: attr(data-title);
	padding: 0px 0px 00px 30px;
	background: url(images/not_pass.gif) no-repeat left;
}
.topic_photo.not_pass:AFTER{
	display: none;;
}
.data_title:BEFORE {
	content: attr(data-title);
	position: absolute;
	color: #000;
	margin-left: -40px;
	font-weight: bolder;
	display: none;
}

input[type="text"] {
	width: 100%;
	font-size: 18px;
	height: 24px;
	margin-bottom: 5px;
}

input[type="text"]:HOVER {
	background-color: rgba(33, 153, 233, 0.3);
}

input[type="text"].empty {
	border-color: rgba(241, 193, 154, 1);
}
IMG.errorsize{
	/*border-color: rgba(230, 128, 45, 1); */
	border:3px solid red; 
	 

}

.topic_photo.audit_right.dropping:AFTER {
	content: ' ';
	background-color: rgba(33, 33, 33, 0.3);
	width: 114px;
	position: absolute;
	height: 91px;
	border: 1px solid red;
	margin-top: -83px;
	margin-left: -9px;
}

.topic_photo.audit_left.dropping:AFTER {
	content: ' ';
	background-color: rgba(33, 33, 33, 0.3);
	width: 114px;
	position: absolute;
	height: 91px;
	border: 1px solid red;
	margin-top: -83px;
	margin-left: -9px;
}

.topic_option_list {
	counter-reset: option_div;
}



.dropimg-content.dropping:AFTER {
	content: ' ';
	background-color: rgba(33, 33, 33, 0.3);
	width: 100%;
	position: absolute;
	height: 180px;
	border: 1px solid red;
}

#img_control {
	background: url(images/arrow_right.png) no-repeat center;
	padding: 40px 100px;
	width: 1150px;
	top: 100px;
	z-index: 1000;
	background-color: rgba(33, 33, 33, 0.3);
}
#img_control.cutstyle IMG#old_img,#img_control.cutstyle IMG#new_img{
	display: none;
}
.topic_photo #img_control {
	/*margin-left: -90px;*/
	left: 0;
	position: fixed;
}
.audit_right #img_control {	
	left: -483px;
}
#img_control #old_img {
	float: left;
	padding: 8px;
	background-color: #f6f6f6;
	border: 1px dashed #b2b2b2;
	-webkit-box-shadow: 3px 3px 4px rgba(0, 0, 0, 0.2);
	-moz-box-shadow: 3px 3px 4px rgba(0, 0, 0, 0.2);
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
}

#img_control #new_img {
	float: right;
	padding: 8px;
	background-color: #f6f6f6;
	border: 1px dashed #b2b2b2;
	-webkit-box-shadow: 3px 3px 4px rgba(0, 0, 0, 0.2);
	-moz-box-shadow: 3px 3px 4px rgba(0, 0, 0, 0.2);
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
}

#img_control DIV#control_btn {
	top: 0;
	right: 0;
	position: absolute;
	color: #fff;
	font-size: 24px;
	overflow: hidden;
	width: 50px;
	height: 100%;
}

SPAN.text_editable{
	padding-right: 20px;
}
SPAN.text_editable:HOVER,SPAN.text_editable:FOCUS{
	border-bottom: 1px solid rgb(102,153,204);
}

.ie-icon:BEFORE {
	width: 32px;
	height: 32px;
}

.ie-icon:HOVER:BEFORE {
	margin-left: -5px;
	padding: 5px;
	border-radius: 5px;
	background: rgba(0, 0, 0, 0.5);
}


#upload_img::-webkit-file-upload-button, #upload_file::-webkit-file-upload-button {
  visibility: hidden;
}
#upload_img::before,#upload_file::before {
	content: ' ';
	display: inline-block;
	background: url("images/uploadfile.png") no-repeat left center;
	padding: 30px;
	cursor:pointer;
    position: absolute;
}
.fancybox-header {
	background: url(images/content-bglarge-top.png) no-repeat top center;
	width: 1151px;
	margin-top: -60px;
}
.fancybox-header ul li {
	margin-top: 10px;
}

.fancybox-content {
	margin: auto;
	width: 1751px;
	background-image: url(images/content-bglarge-center.png);
}
.fancybox-content-bg-bottom {
	background: url(images/content-bglarge-bottom.png) no-repeat bottom center;
	width: 1151px;
	padding-bottom: 0px; 
}
.fancybox-inside-bg-bottom {
	padding-bottom: 0px; 
}

.fancybox-logo {
	margin-left: 40px;
}

.topic_insert{
	font-size: 28px;
	cursor: pointer;
	color:rgb(35, 35, 35);
	padding-top: 10px;
}
.topic_insert:HOVER{	
	color:rgb(255, 113, 9);
}

.reference_news_box {
	height: 880px;
	overflow: scroll;
}

.button {
	background-color: rgba(139,139,122,.2);
	padding: 5px;
}

ul.orderby {
	/*display: inline;
	padding-left: 20px;*/
	margin-top: 10px;
}
.orderby li {
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
	border: 3px outset rgba(190,190,190,.6);

	background-color: rgba(190,190,190,.8);
	display: inline-block;
	padding: 5px 20px;
	cursor: pointer;
	position: relative;
}

.orderby li.selected {
	border: 3px inset rgba(190,190,190,.6);
	background-color: rgba(190,190,190,.4);
}

.orderby li.selected[order="DESC"]:AFTER{
	content:' ';
	width: 20px;
	height: 100%;		
	background: url('../gif/z_a.png') center no-repeat;
	position: absolute;
	right: 0;
	top: 0;
}

.orderby li.selected[order="ASC"]:AFTER{
	content: ' ';
	width: 20px;
	height: 100%;		
	background: url('../gif/a_z.png') center no-repeat;
	position: absolute;
	right: 0;
	top: 0;
}

#tab_edit_menu {
	background: white;
	color: rgb(255,240,245);
	padding: 5px 5px 5px 0;
	width: 250px;
	position: relative;
}

#tab_edit_menu span {
	cursor: pointer;
	padding: 2px 2px 2px 8px;
	background: rgb(30,144,255);
	margin-right: 10px;
	-webkit-border-top-left-radius: 5px;
	-moz-border-radius-topleft: 5px;
	border-top-left-radius: 5px;
}

#tab_edit_menu span:AFTER {
	content: '';
	padding: 0 8px 0 8px;
	background: rgb(30,144,255);
	position: absolute;
	height: 31px;
  	top: 0px;
  	-webkit-border-top-right-radius: 60px;
	-moz-border-radius-topright: 60px;
	border-top-right-radius: 60px;
}

#tab_edit_menu span.visible {
	background: rgb(16,78,139);
}

#tab_edit_menu span.visible:AFTER {
	background: rgb(16,78,139);
}

#tab_edit_content {
	border: 3px solid rgba(190,190,190,.6);
	position: relative;
}

#tab_edit_content > div {
	display: none;
}

#tab_edit_content div.visible {
	display: block;
}

#tab_edit_content #category_edit {
	padding: 10px;
}

#tab_edit_content #category_edit span.topic_category,
#tab_edit_content #tag_edit span.tag_edit_button {
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
	background: rgba(255,165,0,.3);
	cursor: pointer;
	font-size: 18px;
	position: absolute; 
	bottom: 10px;
	right: 10px;
	padding: 5px;
	border: 1px outset rgba(190,190,190,.5);
}

#tab_edit_content #category_edit span.list,
#tab_edit_content #tag_edit span.list {
	font-size: 16px;
	margin: 5px 10px;
}

#temp_div {
	border: 1px solid red;
}

#google, 
#youtube {
	cursor: pointer;
	color: rgb(0,0,0);
}

#google.on {
	background: url(images/check_green.png) right bottom no-repeat;
}
</style>


<input type="hidden" name="auto_index" value="{id}">
<div class="plugin_box" style="position: fixed; right: 40px;z-index: 100;">
	<div id="img_selectbox">
		<div class="fb_view_control">
			<span data-fb="fb_view">原始圖片</span> 
			<span data-fb="fb_link">連結圖片(大)</span>
			<span data-fb="fb_link_s">連結圖片(小)</span> 
			<span data-fb="close" style="float: right;">關閉</span>
		</div>
		<div style="margin: auto; height: 25px;">
			<span style="float: left;"><input id="upload_file" type="file" onchange="upload_img();"/></span>
		</div>
		<div class="fb_view"></div>
	</div>
</div>
<!--選擇類別-->
<div class="plugin_box" style="position: fixed; right: 40px;z-index: 100;">
	<div id="category_selectbox">
		<div class="category_control">
			<input id="search_match_category" style="border: 1px solid #8f8f8f;">
			<span data-fb="close" style="float: right;">關閉</span>
		</div>
		<div style="margin: auto; height: 25px;">
			<div class="topic_group">						
				<div id="topic_group_name">
					<ul>
					<?php foreach ($news_categories_list as $key1 => $value1):?>
						<?php foreach ($value1 as $key => $value):?>		
						<li class="<?php echo $value['parent_id']; echo ' '; echo $value['checked']; ?>" id="<?php echo $value['id']; ?>"><?php echo $value['name'];?></li>
						<?php endforeach;?>
						<br>
					<?php endforeach;?>
					</ul>
				</div>	
			</div>
		</div>
	</div>
</div>

<!--選擇標籤-->
<div class="plugin_box" style="position: fixed; right: 40px;z-index: 100;">
	<div id="tag_selectbox">
		<div class="category_control">
			<input id="search_match_tag" style="border: 1px solid #8f8f8f;">
			<span data-fb="close" style="float: right;">關閉</span>
		</div>
		<div style="margin: auto; height: 25px;">
			<div class="topic_group">						
				<div id="tag_name">
					<ul>
						<?php foreach ($news_tags_list as $key => $value):?>	
							<li class="<?php echo $value['checked']; ?>" id="<?php echo $value['id']; ?>"><?php echo $value['name'];?></li>
						<?php endforeach;?>
					</ul>
				</div>	
			</div>
		</div>
	</div>
</div>

<div id="add_text" style="display: none;">  
	<div id="dragbasic" >    
		<div id="drag1" class="drag"><img src="http://starlove99.com/code/text_pic.php"/></div>
	</div>  
	<div style="float:left;">
		<div>font-color<input type="text" value="000"/></div>
		<div>words<input type="text" value="adonis"/></div>
	</div>
</div>

<div class="fancybox-content">
	<div class="fancybox-content-bg-bottom" style="float: left;">
		<div class="fancybox-header">
			<ul>		
			<li class="{active}">		
				<span class="icon-publish topic_insert" onclick="topic_insert('{id}');" title="新增" alt="新增"></span>
			</ul>
			
			<input type="hidden" id="from_index" value="{from_index}" data-table="{from_table}"/>
			<br class="clear">

			<div class="fancybox-logo" style="color: #919191;">
				<div style="width: 150px;float: left;">
					<!-- <span data-title="web圖片" id="web_photo" class="topic_photo audit_left"> 
						<img data-name="article_imgurl" data-title="web圖片" class="editable" alt=""src="{article_img}" style="width: 120px;">
					</span> -->

					<span data-title="fb圖片" id="fb_photo" class="topic_photo audit_left"> 
						<img data-name="fb_imgurl" data-title="fb圖片" class="editable" alt=""src="{fb_img}" style="width: 120px;">
					</span>
				</div>

				<div style="float: left; width: 750px; margin: 10px 30px;">
					<h2>
						<span contenteditable="true" class="text_editable audit_right " data-name="article_title" data-title="文章標題">{title}</span>
					</h2>
				</div>

				<div id="tab_edit" style="float: left; width: 600px; margin: 10px 30px;">
					<div id="tab_edit_menu" style="float: left; font-size: 20px;">
						<span name="pofans_edit" class="visible">pofans</span>
						<span name="category_edit">分類</span>
						<span name="tag_edit">標籤</span>
					</div>
					<div id="tab_edit_content" style="float: left; width: 600px; height: 150px;">
						<div id="pofans_edit" class="visible">
							<textarea style="display: block; font-size: 16px;height: 150px;" class="text_editable audit_right" data-name="article_optiontitle" data-title="文章簡介" onchange="$(this).html(this.value);">{fb_post_content}</textarea>
						</div>
						<div id="category_edit">
							<!-- <span data-title="編輯類別" class="topic_category"> 
								編輯類別
							</span> -->
						</div>
						<div id="tag_edit">
							<input id="tag_edit_input" value="{news_tags_name}" style="display: block; font-size: 16px; width:600px;">
							<!-- <span data-title="編輯標籤" class="tag_edit_button"> 
								編輯標籤
							</span> -->
						</div>
					</div>
				</div>

				<div id="google_youtube" style="width: 220px;float: left;margin-left: 20px;">
					<span id="google" class="{google_status}">google</span>
					<br>
					<span style="display:none;" id="youtube" class="{youtube_status}"><a href="https://www.youtube.com/watch?v={youtube_id}" target="_blank">youtube</a></span>
					<input style="display:none;" id="youtube_id" value="{youtube_id}" style="border: 1px solid #8f8f8f;">
				</div>

				<br class="clear">
			</div>
		</div>

		<div class="fancybox-inside" style="position: relative;">
			<input id="upload_img" type="file" style="position: absolute;right: -120px;margin-top: -30px;width: 120px;" onchange="upload_article_img()"/>
			<span id="img_list" class="topic_photo" style="position: absolute;right: -135px;margin-top: 30px;width: 120px;"></span>
			<div class="fancybox-inside-bg-top">
				<div class="fancybox-inside-bg-bottom">
					<div class="resume slide">						
						<div  class="page-content" style="margin: 20px;width: 950px;min-height: 600px;">		
							<span id="text_ckeditor" class="data_title" data-title="內容" data-type="info" data-name="article_text">{content}</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class='right_box' style="width:600px;float: right;">
		<form style="margin: 20px;">
			關鍵字 : <input id="keyword" name="keyword" style="border: 1px solid #8f8f8f;">
　			<input class="button" type="button" value="查詢" onclick="search_keyword()">
			
			<ul class="orderby">
				排序方式 : 
				<li class="selected" orderby="publish_time" order="DESC">時間</li>
				<li orderby="fb_likes">讚數</li>
				<li orderby="site_id">來源網站</li>
			</ul>
		</form>
		<div class="reference_news_box">	
		</div>
	</div>

	<br class="clear">
</div>


<script>
//qqq
function showCoords(c){
	cut_info = c;
}

/*function add_text(){
	$('#img_control').addClass('cutstyle');
	$('#add_text').appendTo("#img_control").show();
	$('#dragbasic').css('background','url('+$('#new_img').attr('src')+')')
}*/

//裁圖
function cut_jropimg(img_type){
	if(typeof(cut_info) == 'object'){
		
		console.info(cut_info);

		$.fancybox.showLoading();
		var url='news/cut_img';
		var img = this;
		$.ajax({
			url: url,
		  cache: false,
			dataType: 'html',
		  type: 'POST',
		  data:{
			  img:$('#new_img').attr('src'),
			  cut_info:cut_info
		  },
		  success: function(data){
			  $.fancybox.hideLoading();
			  $('#img_control').removeClass('cutstyle');
			  $('#new_img').attr('src',data);
		 	  $('DIV.jcrop-holder').remove();
		 	  $('#cut_img').remove();
			  delete cut_info;
		  },error: function(data){
		  }
		});	
	}
	else if($('#cut_img').length <1 ){
		$('#img_control').addClass('cutstyle').prepend('<img id="cut_img" />');

		$('#cut_img').css('max-width','1000px').css('margin','auto').attr('src',$('#new_img').attr('src'));
		
		var def_1 = {
            onSelect: showCoords,
            onChange: showCoords,
            aspectRatio: 4 / 3,
            minSize:[400, 300]
        };

	    var def_2 = {
            onSelect: showCoords,
            onChange: showCoords,
            aspectRatio: 2 / 1,
            minSize:[400, 200]
        };

        if ( img_type == 'web' ) {
	        var def = def_1;
        } else {
        	var def = def_2;
        }

		jcrop_api =  $('#cut_img').Jcrop(def);
	}
	
}

//傳入textArea(CKeditor轉換後)的值，取出IMG屬性
function refresh_img_list(editorText){
	
	var check_listarr = new Array();
	
	editorText = '<div>'+editorText+'</div>';

	$(editorText).find("IMG").each(function(){
		var src = $(this).attr("src");

		check_listarr.push(src);
		//取內文img的src到右側data-src
		if($('#img_list IMG[data-src="'+src+'"]').length < 1)
		$('<img src="'+src +'" data-src="'+src+'"  >').appendTo('#img_list').load(function(){
    		if($(this).attr('data-check') != 'check_ok' && $(this).attr('data-check') != 'checking') img_check(this);
		});			
	}); 

	$('#img_list IMG[data-check]').each(function(){
		var src = $(this).attr('data-src');

		var check_listimg = $.inArray( src, check_listarr );
		if(check_listimg<0) $(this).remove();

	})

}

//刪除右側上傳圖片
function clear_img_list(){
	url = 'news/remove_check_photo'; 
	$("#img_list IMG").each(function(){
		var img = this;
		console.info($(img).attr('src'));
		
		$.ajax({
			url: url,
		  cache: false,
			dataType: 'html',
		  type: 'POST',
		  data:{
			  img:$(img).attr('src')
		  },
		  success: function(data){
			  $(img).remove();
		  }
		});
	});	
}

//儲存圖片，預覽圖取代為線上網址
function img_check(img){
	var key = '<?php echo date('YmdHis')?>';
	var check_img = img;
	$(check_img).attr('data-check','checking');
	
	$(check_img).load(function(){		
		$(this).attr('data-check','check_ok');
		return false;
	});
	
	url = 'news/move_article_photo'; 
	$.ajax({
		url: url,
	  cache: false,
		dataType: 'html',
	  type: 'POST',
	  data:{
		  img:$(check_img).attr("src"),
		  index:key+$(check_img).index()
	  },
	  success: function(data){
		 
		  $(check_img).attr("src",data) ;
	  	
	  }
	});
}

//ckeditor
$(function() {
	$("#dragbasic div[id^='drag1']").draggable({
		containment: "#dragbasic",
		stack: ".drag"
	});
	
	set_span_editor();
	refresh_img_list($('#text_ckeditor').html());
	
	element = CKEDITOR.replace("text_ckeditor",{ width:'950px',height:'600px' });
	$("SPAN.text_editable").attr('contenteditable',true).each(function(){
		if(!$(this).html() || $(this).html() == '') $(this).html( $(this).attr('data-title')||'請輸入資料' );

		}).live("focus",function(){
			if( $(this).html() == ($(this).attr('data-title')||'請輸入資料') ) $(this).html('');			
		}).live("blur",function(){
			if(!$(this).html() || $(this).html() == '') $(this).html($(this).attr('data-title')||'請輸入資料');
		}).live("keyup",function(){
		
	});
	// element.on('instanceReady', function (ev) {
	// 	ev.editor.on('paste', function (ev) {
	// 		ev.data.dataValue = 'adonis';
	// 	});
	// });
	element.on('loaded', function(e){		
		e.editor.on('contentDom', function () {
			var editable = this.editable();
			editable.attachListener( this.document, 'paste', function(evt) {
				evt.data.dataValue = 'adonis';
			});
		    editable.attachListener( this.document, 'drop', function(evt) {
            	var drop_text = evt.data.$.dataTransfer.getData('text/html');
            	$("<div>"+drop_text+"</div>").find("IMG").each(function(){
    				var src = $(this).attr("src");
					$('<img src="'+$(this).attr("src") +'" data-src="'+ src +'"  >').appendTo('#img_list').load(function(){
	            		if($(this).attr('data-check') != 'check_ok' && $(this).attr('data-check') != 'checking') img_check(this);
					});				
            	}); 
            	
		    } );
		    
		    e.editor.document.on('paste', function (evt) {
		    	evt.data.dataValue = 'adonis';
	        });

	        e.editor.document.on('keyup', function (evt) {
	        	if((evt.data.$.ctrlKey == true && evt.data.$.keyCode == 86) ||  evt.data.$.keyCode == 46||  evt.data.$.keyCode == 8){

					// element.focus();
					// element.document.$.execCommand( 'SelectAll', false, null );
					// element.execCommand( 'removeFormat', element.getSelection().getNative() );
	        		var editorText = element.getData();
	        		refresh_img_list(editorText);
	        	}
		    });		      
	    });
	});
});



id = 0;
$('#topic_group_name UL LI').each(function(){
	if ( $(this).hasClass('checked') ) {
		if( $(this).hasClass('0') ) {
			id = $(this).attr("id");
		}
		$('#category_edit').append('<span id="'+$(this).attr('id')+'" class="'+$(this).attr('class')+' list"'+'>'+$(this).html()+'</span>');
	} else if ( !$(this).hasClass('0') &&  !$(this).hasClass(id) ) {
		$(this).hide();
	}
});

// $('#tag_name UL LI').each(function(){
// 	if ( $(this).hasClass('checked') ) {
// 		$('#tag_edit').append('<span id="'+$(this).attr('id')+'" class="'+$(this).attr('class')+' list"'+'>'+$(this).html()+'</span>');
// 	}
// });

//分類選擇
$("#topic_group_name UL LI").on("click",function(){
	if($(this).hasClass("checked")) {
		$(this).removeClass("checked");
		$('#category_edit').find('#'+$(this).attr('id')).remove();
	} else {
		$('#category_edit').append('<span id="'+$(this).attr('id')+'" class="'+$(this).attr('class')+' list"'+'>'+$(this).html()+'</span>');
		$(this).addClass("checked");
	}

	if ( $(this).hasClass("0") ) {
		var id = $(this).attr("id");
		
		if ( $(this).hasClass("checked") ) {
			$("#topic_group_name UL LI."+id).show();
		} else {
			$("#topic_group_name UL LI."+id).hide();
			$("#topic_group_name UL LI."+id).removeClass("checked");
			$('#category_edit').find('.'+id).remove();
		}
	}

	var topic_index = $('INPUT[name="auto_index"]').val();
	if(topic_index < 1) return false;
});

//標籤選擇
$("#tag_name UL LI").on("click",function(){
	if($(this).hasClass("checked")) {
		$(this).removeClass("checked");
		a.removeTag($(this).html());
		// $('#tag_edit').find('#'+$(this).attr('id')).remove();

	} else {
		// $('#tag_edit').append('<span id="'+$(this).attr('id')+'" class="'+$(this).attr('class')+' list"'+'>'+$(this).html()+'</span>');
		$(this).addClass("checked");
		if ( !a.tagExist() ) {
			a.addTag($(this).html());
		}	
	}

	var topic_index = $('INPUT[name="auto_index"]').val();
	if(topic_index < 1) return false;
});

//新增類型?目前沒用到的拖拉
/*$('.add_topic_option').live('click',function(){
	var temp = $(this).parents('DIV.page-content.dropimg-content');
	var temp_clone = $(temp).clone();
	$(temp).after(temp_clone);

	$(temp_clone).each(function(){
		this.addEventListener("dragenter", dragEnter, false);
		this.addEventListener("dragexit", dragExit, false);
		this.addEventListener("dragover", dragOver, false);
		this.addEventListener("dragleave", dragLeave, false);
		this.addEventListener("drop", drop, false);	
	});
})

$('.topic_option_remove').live('click',function(){
	var temp = $(this).parents('DIV.page-content.dropimg-content');
	if($('DIV.page-content.dropimg-content').length == 1) {
		alert('你把選項都刪光了! 笨蛋!');		
	    $(temp).hide().show(1000);
	    return false;		
	}
	
	if($(temp)) $(temp).remove();
});*/

//新增文章
function topic_insert(auto_index){
	CKEDITOR_destroy();

	var topic_array = get_post_val();
	// console.info(topic_array['youtube_id']);return;
	var from_table = $('#from_index').attr('data-table');
	var from_index = $('#from_index').val();

	if(topic_array == false ) {
		alert('輸入的資料有誤');
		CKEDITOR.replace("text_ckeditor",{ width:'800px',height:'500px' });
		return false;
	}

	if(topic_array['article_group'].length < 1){
		alert('請選擇分類');
		CKEDITOR.replace("text_ckeditor",{ width:'800px',height:'500px' });
		return false;
	}

	// topic_array['article_text'] = topic_array['article_text'].replace(new RegExp('&amp;','g'),'&');
	// console.info(topic_array['article_text']);
	if(confirm("是否確定新增資料? ")){
	
		if(topic_array && topic_array.error_msg == ''){
			url = 'news/create_article'; 
			
			$.fancybox.showLoading();
			$.ajax({
				url: url,
			  cache: false,
				dataType: 'html',
			  type: 'POST',
			  data:{
				  topic_array:topic_array,
				  auto_index:auto_index,
				  from_index:from_index,
				  from_table:from_table
			  },
			  success: function(data){	
			  	$.fancybox.hideLoading();
			  	clear_img_list();
			  	location.reload();
			  },
			  error: function(data){
			  }
			});
		}else{
			alert(topic_array.error_msg);
		}
	}else{
		CKEDITOR.replace("text_ckeditor",{ width:'800px',height:'500px' });
	}
}

//取出各欄位的資料，存於物件中
function get_post_val(){
	var post_id = new Array('article_imgurl','fb_imgurl','article_title','article_optiontitle','article_text');
	var topic_array = {
			  "article_imgurl": '',
			  "fb_imgurl": '',
			  "article_title": '',
			  "article_optiontitle": '',
			  "article_text": '',
			  "error_msg":''	 
	}

	for( var key in post_id){		
		var temp = $('[data-name="'+post_id[key]+'"').attr('data-title');

		if(typeof(temp) != 'undefined' && $('[data-name="'+post_id[key]+'"').html() == temp) {
			console.info(post_id[key]);	
			return false;
		}
		
		if($('[data-name="'+post_id[key]+'"').get(0).tagName.toLowerCase() == "img") topic_array[post_id[key]] = $('[data-name="'+post_id[key]+'"').attr("src");
		if($('[data-name="'+post_id[key]+'"').get(0).tagName.toLowerCase() == "span" ) topic_array[post_id[key]] = $('[data-name="'+post_id[key]+'"').html();
		if($('[data-name="'+post_id[key]+'"').get(0).tagName.toLowerCase() == "textarea" ) topic_array[post_id[key]] = $('[data-name="'+post_id[key]+'"').html();
	}
	
	topic_array['article_group'] = new Array();
	$("#topic_group_name UL LI.checked").each(function(){
		topic_array['article_group'].push($(this).attr("id"));
	});

	topic_array['article_tag'] = new Array();
	topic_array['article_tag'] = a.getValues();
	// $("#tag_name UL LI.checked").each(function(){
	// 	topic_array['article_tag'].push($(this).attr("id"));
	// });

	topic_array['article_img'] = new Array();
	refresh_img_list($('#text_ckeditor').html());
	$('#img_list IMG').each(function(){
		var reg_str = topic_array['article_text'].replace(new RegExp('&amp;','g'),'&'); 
		var src = $(this).attr('src');
		topic_array['article_img'].push(src);

		var data_src = $(this).attr('data-src');
		var data_replace = data_src.replace(new RegExp('\\?','g'),'\\\?');
		topic_array['article_text'] = reg_str.replace(new RegExp(data_replace,'g'), $(this).attr('src'));
	});

	if ( $('#google').hasClass('off') ) {
		topic_array['google_ad'] = 0;
	} else {
		topic_array['google_ad'] = 1;
	}
	
	topic_array['youtube_id'] = $('#youtube_id').val();
	
	return topic_array;
}



//FB圖片跳出框
$("DIV.fb_view_control SPAN").on('click', function() {
	if( $(this).attr('data-fb') == "close"){
		$('#img_selectbox').hide();	
		
	}else{
		//找到class有fb_view，清除fb_view以外的class
		$("DIV.fb_view").attr('class','fb_view');	
		$("DIV.fb_view").addClass( $(this).attr('data-fb'));
	}
});
//FB圖片預覽
// $("IMG.editable").live('click', function() {
// 	$("IMG.editable").removeClass("focus");
// 	$(this).addClass("focus");
	
// 	$('#img_selectbox').show(500);
	 
// 	$('#img_selectbox .fb_view').attr('class','fb_view').css('background', 'url("'+$(this).attr("src")+'") center center no-repeat');
// });

//選擇類別跳出框
$("DIV.category_control SPAN").on('click', function() {
	if( $(this).attr('data-fb') == "close"){
		$('#category_selectbox').hide();
		$('#tag_selectbox').hide();		
	}
});
//選擇類別
$("span.topic_category").live('click', function() {
	$('#category_selectbox').show(500);
});

//選擇標籤
$("span.tag_edit_button").live('click', function() {
	$('#tag_selectbox').show(500);
});

//檔案拖拉
$("DIV.dropimg-content, SPAN.topic_photo").each(function(){
	this.addEventListener("dragenter", dragEnter, false);
	this.addEventListener("dragexit", dragExit, false);
	this.addEventListener("dragover", dragOver, false);
	this.addEventListener("dragleave", dragLeave, false);
	this.addEventListener("drop", drop, false);	
});

//dropping在CSS會有紅框效果
function dragEnter(evt) {
	$('#img_control').remove(); 
	$(".dropping").removeClass("dropping");
	evt.stopPropagation();
	evt.preventDefault();
	$(this).addClass("dropping");
}

function dragExit(evt) {
	evt.stopPropagation();
	evt.preventDefault();
}

function dragLeave(evt) {
	evt.stopPropagation();
	evt.preventDefault();	
	if( $(this).hasClass("dropping")) $(this).removeClass("dropping");
}
function dragOver(evt) {
	evt.stopPropagation();
	evt.preventDefault();
	if(!$(this).hasClass("dropping")) $(this).addClass("dropping");	
}

function drop(evt) {
	if(( typeof( $(this).attr('data-src')) == 'undefined')) 
		$(this).attr('data-src',$(this).attr('src'));
	if(( typeof( $(this).attr('data-width')) == 'undefined')) 
		$(this).attr('data-width',this.naturalWidth);
	if( typeof( $(this).attr('data-height')) == 'undefined') 
		$(this).attr('data-height',this.naturalHeight);
	
	$(this).removeClass("dropping");
	evt.stopPropagation();
	evt.preventDefault();

	imgreplacebox(this);
	
	var dat = $(evt.dataTransfer.getData('text/html'));
    var img = dat.attr('src');
    if (!img) img = dat.find("img").attr('src');
    
	var files = evt.dataTransfer.files;
	var count = files.length;

	
	// Only call the handler if 1 or more files was dropped.
	if (count > 0 && handleFiles(files)){
		$(this).addClass('Upload_img');		
	}
	else if(img){		
		$("#new_img").attr('src',img);		
	}else{
		init_img_controllor(true);
	}
}


//右側上傳圖片
function upload_article_img(){
	var fileInput = document.getElementById('upload_img');
	//沒有出現data-check文字，都用中括號
	$('#img_list IMG:not([data-check])').remove();
	
	$('#img_list').prepend('<img id="upload" src=""/>');
	
	handleFiles(fileInput.files); 
}

//檔案讀取
function handleFiles(files) {
	var file = files[0];
	var check_file  = null;
	if(typeof(file) != "undefined")
		check_file = file.type.match('image');

	if(check_file == null) {
		alert('上傳檔案需為圖檔');			
		return false;
	}
	var reader = new FileReader();

	reader.onloadend = handleReaderLoadEnd;

	reader.readAsDataURL(file);
	return true;
}

//右側儲存圖片
function handleReaderLoadEnd(evt) {
	var check_Uploadimg = $('#img_list IMG#upload');
	if(check_Uploadimg.length){
		url = 'news/move_article_photo'; 
		$.ajax({
			url: url,
		  cache: false,
			dataType: 'html',
		  type: 'POST',
		  data:{
			  img:evt.target.result,
			  index:'upload_img'
		  },
		  success: function(data){
			 
			  $("#img_list IMG#upload").attr('src',data);
			  $("IMG#upload").removeAttr('id');
		  }
		});
		
	}else{
		
		$("#new_img").attr('src',evt.target.result);
	}

}

function init_img_controllor(inputfile){
	//初始化
	delete cut_info;
	$("#img_control").remove();
	$(".Upload_img").removeClass("Upload_img");
	if(inputfile) $("#upload_file").val('');
	console.info('init');
}

function save_img(){
	var old_img = $("#img_control #old_img").attr("src");
	var new_img = $("#img_control #new_img").attr("src");

	$("#img_control").parent().find("IMG.editable").attr("src",new_img);
	
	var photo = $("#img_control").parent().find("IMG.editable");

	$("#img_control").remove();
	$('#img_selectbox').hide();

	if ( $(photo).attr('data-src') ) {
		replace_cut_img(photo);
	}
}

//上傳圖片
function upload_img(){
	var div_el = $('IMG.editable.focus').parents('DIV.page-content.dropimg-content');
	var span_el = $('IMG.editable.focus').parents('SPAN.topic_photo');

	imgreplacebox((span_el.length)?span_el:div_el);


	var fileInput = document.getElementById('upload_file');
	 
	if( handleFiles(fileInput.files) ){
		if(span_el.length) span_el.addClass('Upload_img');
		else div_el.addClass('Upload_img');

	}else {
		init_img_controllor(true);
		return false;

	}
	/**/
	
}

//跳出的轉換圖片畫面
function imgreplacebox(el){
	init_img_controllor(false);
	var photo_type = el.id;
	var old_img = $(el).find("IMG.editable").attr("src");

	//建立img_control
	var img_control = document.createElement('div');
	$(img_control).attr("id","img_control");
	$(el).prepend(img_control);
	
	
	$("#img_control").prepend('<img id="new_img"  width="200px"  />')
	$("#img_control").prepend('<img id="old_img"  src ="'+old_img+'" width="200px"  />')
	

	var control_btn = document.createElement('div');
	$(control_btn).attr("id","control_btn");
	$("#img_control").append(control_btn);

	var control_bar = new Tool(canvasToolConfig);

	// if ( photo_type == 'web_photo') {
	// 	$("#new_img").load(function() {
	// 		$('#img_selectbox').hide();	
	// 		if( typeof( $(this).attr("data-src") ) == "undefined" || 
	// 			typeof( $(this).attr("data-width") ) == "undefined" ||
	// 			typeof( $(this).attr("data-height") ) == "undefined")
	// 		{	
	// 			$(this).attr("data-width",400);
	// 			$(this).attr("data-height",300);
							
	// 			//return false;
	// 		}
				
	// 		var width = this.naturalWidth;
	// 		var height = this.naturalHeight;

	// 		if( width < $(this).attr("data-width") && height < $(this).attr("data-height") ) {
	// 			alert('圖片規格不符');	
	// 			$('#img_control').remove(); 	
	// 		} else if (width *3 != height *4 ) {
				
	// 			if( confirm('圖片規格不符,是否裁切') ) 	cut_jropimg('web');	

	// 			return false;
	// 		}	
	// 		// else {
	// 		// 	add_text();
	// 		// }
			
	// 	})
	// } else if ( photo_type == 'fb_photo') {
		$("#new_img").load(function() {
			$('#img_selectbox').hide();	
			if( typeof( $(this).attr("data-src") ) == "undefined" || 
				typeof( $(this).attr("data-width") ) == "undefined" ||
				typeof( $(this).attr("data-height") ) == "undefined")
			{	
				$(this).attr("data-width",400);
				$(this).attr("data-height",200);
							
				//return false;
			}
				
			var width = this.naturalWidth;
			var height = this.naturalHeight;

			if( width < $(this).attr("data-width") && height < $(this).attr("data-height") ) {
				alert('圖片規格不符');	
				$('#img_control').remove(); 	
			} else if (width *1 != height *2 ) {
				
				if( confirm('圖片規格不符,是否裁切') ) 	cut_jropimg('fb');	

				return false;
			}	
			
		})
	// }
	
	
	
	$("#new_img,#old_img").on("click",function(){		

		//img_selectbox是FB預覽圖
		$('#img_selectbox').show(500);
		$('#img_selectbox .fb_view').attr('class','fb_view').css('background', 'url("'+$(this).attr("src")+'") center center no-repeat');

		});
	$("#new_img").error(function(){
		if( typeof($(this).attr("data-src")) != 'undefined' ){
			alert('圖片讀取有誤!');
			$('#img_control').remove(); 
		}
		
	});
}

//相關文章搜尋
function search_keyword() {
	var keyword = $('#keyword').val();
	if ( keyword == '' ) return;
	var orderby = $('li.selected').attr('orderby');
	var order = $('li.selected').attr('order');
	var source_id = $('input[name="source_id"]').val();
	$('.reference_news_box').html('<div>查詢中</div>');
	$.ajax({
		url: 'news/search_keyword',
	  	cache: false,
	  	dataType: 'html',
		async : false,
		type: 'POST',
		data: {
			keyword:keyword,
			orderby:orderby,
			order:order,
			source_id: source_id
		},
		success: function(data) {
			console.info(1);
			$('.reference_news_box').html(data);
		},
		error: function(data) {
			console.info(0);
			$('.reference_news_box').html('<div>查詢失敗</div>');
		}
	});
}

$('#keyword').on('keyup', function() {
	var re = new RegExp($(this).val());
	$('.reference_news_box').find("div.news_title").each(function(){
		var reg_td = $(this).html();
		$(this).hide();
		if (reg_td.match(re)) {
			$(this).show();
		}				
	});		 		
});

//分類搜尋
$('#search_match_category').on('keyup', function() {
	var match = $(this).val();
	var re = new RegExp($(this).val());
	$('#topic_group_name UL LI').each(function(){
		var reg_td = $(this).html();
		$(this).hide();
		if (reg_td.match(re)) {
			$(this).show();
		}				
	});

	if ( match == '' ) {
		var parent_id = 0;
		$('#topic_group_name UL LI').each(function(){	
			if ( !$(this).hasClass('0') && !$(this).hasClass(parent_id) ) {
				$(this).hide();
				if ( $(this).hasClass('checked') ) {
					$(this).removeClass('checked')
					parent_id = $(this).attr('class');

					$('#topic_group_name UL .'+parent_id).show();
					$(this).addClass('checked')
				}
			} else if ( $(this).hasClass('0') && $(this).hasClass('checked') ) {
				parent_id = $(this).attr('id');
			}
		});	
	}	
});

//標籤搜尋
$('#search_match_tag').on('keyup', function() {
	var match = $(this).val();
	var re = new RegExp($(this).val());
	$('#tag_name UL LI').each(function(){
		var reg_td = $(this).html();
		$(this).hide();
		if (reg_td.match(re)) {
			$(this).show();
		}				
	});
});

$('.orderby li').on('click',function(){
	if ( $(this).hasClass("selected") ) {
		if ( $(this).attr('order') == 'DESC' ) {
			$(this).attr('order', 'ASC');
		} else {
			$(this).attr('order', 'DESC');
		}
		
	} else {
		// $('.selected').attr('order', '');
		$('.selected').removeClass("selected");
		$(this).addClass("selected");
		$(this).attr('order', 'DESC');
	}
	
	search_keyword();
});

$('#tab_edit_menu span').on('click',function(){
	var re = /_edit/g;
	var name = $('#tab_edit_menu .visible').attr('name').replace(re, '');
	$('#'+name+'_selectbox').hide(500);

	$('#tab_edit_content .visible').removeClass("visible");
	$('#'+$(this).attr('name')).addClass("visible");
	$('#tab_edit_menu .visible').removeClass("visible");
	$(this).addClass("visible");

	var name = $(this).attr('name').replace(re, '');
	console.info(name);
	$('#'+name+'_selectbox').show(500);
});

//新增自訂標籤
function onAddTag(tag) {
}
function onRemoveTag(tag) {
}
function onChangeTag(input,tag) {
}
var a = $('#tag_edit_input').tagsInput({
	onAddTag: onAddTag,
	onRemoveTag: onRemoveTag,
	onChange: onChangeTag
});

$('#img_list IMG').on('click',function(){
	$("#temp_div IMG.editable").unwrap();
	$(this).addClass('editable');
	$(this).wrap('<div id="temp_div"></div>');
	imgreplacebox($('#temp_div'));
	$("#new_img").attr("src", $(this).attr("src"));
	console.info($(this).attr("src"));
});

$('#google').on('click',function(){
	if ( $(this).hasClass('off') ) {
		$(this).removeClass('off')
		$(this).addClass('on');
	} else {
		$(this).removeClass('on')
		$(this).addClass('off');
	}
});

function replace_cut_img(el) {
	CKEDITOR_destroy();

	var article_text = $('#text_ckeditor').html();
	$('#text_ckeditor').html(article_text.replace($(el).attr('data-src'), $(el).attr('src')));
	
	CKEDITOR.replace("text_ckeditor",{ width:'800px',height:'500px' });
}
</script>