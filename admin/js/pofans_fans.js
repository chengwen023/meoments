var fans = new Object;

schedule_add = function(){
	url = 'pofans/schedule_insert';
	 
	$.fancybox.showLoading();
	$.ajax({
		url: url,
	  cache: false,
		dataType: 'html',
	  type: 'POST',	  
	  success: function(data){
	  	$.fancybox.hideLoading();
	  	$.fancybox.open(data,{	  
	  		wrapCSS : 'fancybox-background',
	  		autoCenter   : true,
		  	helpers : {
		        overlay : {
		        	closeClick: false
		        }
	        },
	        afterShow : function(){
	        	//$('.fancybox-inner').css('width', '620px');
	        },
	        beforeClose : function(){
	        	
	        	var ask = confirm("是否關閉 新增/編輯 資料視窗!?");
	        	
	        	if(ask) {
	        		
	        	}
	        	return(ask);
	        	
	        }
		  	});
	  }
	});
	
	
}
fans.del_published = function(index){
	
	url = 'pofans/published_remove';
	$.ajax({
		url: url,
	  cache: false,
		dataType: 'html',
	  type: 'POST',
	  data:{
		  index:index  
	  },
	  success: function(data){
		  	if(data) console.info(data);
		  	else location.reload();
	  }
	});
	
}
fans.del_schedule = function(index){
	
	url = 'pofans/schedule_remove';
	$.ajax({
		url: url,
	  cache: false,
		dataType: 'html',
	  type: 'POST',
	  data:{
		  index:index  
	  },
	  success: function(data){
		  	if(data) console.info(data);
		  	else location.reload();
	  }
	});
	
}
fans.add = function(){
	
	FB.login(function(response){
		if(response.authResponse){									
			access_token =   FB.getAuthResponse()['accessToken'];
			console.info(access_token);
			url = 'pofans/fans_add_list';
			 
			$.fancybox.showLoading();
			$.ajax({
				url: url,
			  cache: false,
				dataType: 'html',
			  type: 'POST',
			  data:{
				  access_token:access_token
			  },
			  success: function(data){
			  	$.fancybox.hideLoading();
			  	$.fancybox.open(data,{	  
			  		wrapCSS : 'fancybox-background',
			  		autoCenter   : true,
				  	helpers : {
				        overlay : {
				        	closeClick: false
				        }
			        },
			        afterShow : function(){
			        	//$('.fancybox-inner').css('width', '620px');
			        },
			        beforeClose : function(){
			        	
			        	var ask = confirm("是否關閉 新增/編輯 資料視窗!?");
			        	
			        	if(ask) {
			        		
			        	}
			        	return(ask);
			        	
			        }
				  	});
			  }
			});
		}else{
			alert('請先登入');							
		}
	},{scope: 'publish_actions, manage_pages'});
	
}

fans.add_only = function(){
	url = 'pofans/fans_add_list_only';
	$.fancybox.showLoading();
	$.ajax({
		url: url,
	  	cache: false,
		dataType: 'html',
	  	type: 'POST',
	  	data:{
	  	},
	  	success: function(data){
	  		$.fancybox.hideLoading();
	  		$.fancybox.open(data,{	  
		  		wrapCSS : 'fancybox-background',
		  		autoCenter   : true,
			  	helpers : {
			        overlay : {
			        	closeClick: false
			        }
		      	},
		        afterShow : function(){
		        	//$('.fancybox-inner').css('width', '620px');
		        },
		        beforeClose : function(){
		        	
		        	var ask = confirm("是否關閉 新增/編輯 資料視窗!?");
		        	
		        	if(ask) {
		        		
		        	}
		        	return(ask);
		        	
		        }
		  	});
	  	}
	});
}