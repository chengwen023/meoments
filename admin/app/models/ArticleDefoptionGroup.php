<?php 
class ArticleDefoptionGroup extends \Phalcon\Mvc\Model
{
	public $account_index;
	
	
	public function initialize()
	{
		$this->hasMany("auto_index", "ArticleDefoptions", "defoptions_group_index");
		
		$this->belongsTo("create_account_index", "AdminAccount", "auto_index");
		$this->belongsTo("update_account_index", "AdminAccount", "auto_index");
	}
	
	public function beforeValidationOnCreate()
	{
		$this->online = '1';
		$this->create_time = date('Y-m-d H:i:s');
		$this->update_time = date('Y-m-d H:i:s');
		if($this->account_index){
			$this->create_account_index = $this->account_index;
			$this->update_account_index = $this->account_index;
		}
				
	}
	
	public function beforeValidationOnUpdate()
	{
		
		$this->update_time = date('Y-m-d H:i:s');	
		if($this->account_index){	
			$this->update_account_index = $this->account_index;
		}
	}
	
	public function beforeCreate()
	{
		
	}
	
	public function beforeUpdate()
	{
		//Set the modification date
		$this->update_time = date('Y-m-d H:i:s');
	}
	
	public function setAccountIndex($index){
		$this->account_index = $index;
		
	}
}

?>