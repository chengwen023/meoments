<?php
use Phalcon\Events\Event,
	Phalcon\Mvc\User\Plugin,
	Phalcon\Mvc\Dispatcher,
	Phalcon\Mvc\Dispatcher\Exception as DispatchException,
	Phalcon\Acl;




class Security extends Plugin
{

	public function beforeDispatch(Event $event, Dispatcher $dispatcher)
    {
    	
    	
    }

    public function beforeException(Event $event, Dispatcher $dispatcher, $exception)
    {
    	
    	//Handle 404 exceptions
    	if ($exception instanceof DispatchException) {
    		$this->flash->error("Handle 404 exceptions");
    		
    		return false;
    	}
    
    	//Handle other exceptions
    	$this->flash->error("Handle 503 exceptions");
//     	$dispatcher->forward(array(
//     			'controller' => 'index',
//     			'action' => 'index',//show503'
//     	));
    
    	return false;
    }    
    
	public function beforeExecuteRoute(Event $event, Dispatcher $dispatcher)
    {

        //Check whether the "auth" variable exists in session to define the active role
        $role = 'Guests';
        //Register two roles, Users is registered users
        if ($this->session->has("userInfo")) {
        	$role = 'Users';
        		
        }
        //Take the active controller/action from the dispatcher
        $controller = $dispatcher->getControllerName();
        $action = $dispatcher->getActionName();

        //Obtain the ACL list
        $acl = $this->getAcl();
		
        //Check if the Role have access to the controller (resource)
        $allowed = $acl->isAllowed($role, $controller, $action);
        if ($allowed != Acl::ALLOW) {

            //If he doesn't have access forward him to the index controller
            $this->flash->error("You don't have access to this module");
            if($controller != 'index') $this->response->redirect('');
            $dispatcher->forward(
                array(
                    'controller' => 'index',
                    'action' => 'index'
                )
            );

            //Returning "false" we tell to the dispatcher to stop the current operation
            return false;
        }else{
//         	echo $role;
//         	echo $controller;
//         	echo $action;
        	
        }

    }
    
    public function getAcl()
    {
    	
    
	    //Create the ACL
		$acl = new Phalcon\Acl\Adapter\Memory();
		
		//The default action is DENY access
		$acl->setDefaultAction(Phalcon\Acl::DENY);
		
		
		//and guests are users without a defined identity
		$roles = array(
		    'users' => new Phalcon\Acl\Role('Users'),
		    'guests' => new Phalcon\Acl\Role('Guests')
		);
		foreach ($roles as $role) {
		    $acl->addRole($role);
		}
		
		//Private area resources (backend)
		$privateResources = array(
				'account' 	=> array('index','group','grouplist','groupjson','actions','groupdefaultjson','listjson'),
				'pofans' 	=> array('index','fans','fanslist','fansjson','actions','published','get_fans_group','set_fans_group','ikuso_fans'),
				'horo'		=> array('index','validate','list','remove'),
				'article'	=> array('index','actions','list','group','defoption','cut_img'),
				'topic'		=> array('index','list','group'),
				
		);
		foreach ($privateResources as $resource => $actions) {
			$acl->addResource(new Phalcon\Acl\Resource($resource), $actions);
		}
		
		//Public area resources (frontend)
		$publicResources = array(
				'index' => array('index'),				
				'login' => array('index','validate'),
				'img' => array('index','text'),
				
				
		);
		foreach ($publicResources as $resource => $actions) {
			$acl->addResource(new Phalcon\Acl\Resource($resource), $actions);
		}
		
		//Grant access to public areas to both users and guests
		foreach ($roles as $role) {
			foreach ($publicResources as $resource => $actions) {				
				foreach ($actions as $action) {
					$acl->allow($role->getName(), $resource, $action);
				}
			}
		}
		
		//Grant access to private area only to role Users
		foreach ($privateResources as $resource => $actions) {
			foreach ($actions as $action) {
				$acl->allow('Users', $resource, $action);
			}
		}
		
		return $acl;
    }

}