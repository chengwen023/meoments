<style>
#tips_box{
	position: fixed;
	font-size: 16px;
	display: none;
	padding: 5px 10px;
	background: #fff;
	box-shadow: 1px 1px 2px 1px rgba(33,66,99,.5);
	border-radius:5px;
	z-index: 10000;
}

.article_total {
	float: left;
	margin: 25px 10px 20px 10px;
	font-size: 16px;
	font-weight: bold;
}
.article_total span{
	color: rgb(0,0,255);
}

</style>

<div class="daily_left">
	<form id="list_orderby" method="post">
		<input id="order_by" type="hidden" name="order_by">
		<input id="days_selected" type="hidden" name="days_selected" value="{days}">
		&nbsp;日期：
		<select id="days" name="days">
		{days_list}
	　		<option id="{day}" value="{day}">{day}</option>
		{/days_list}
		</select>

		<!-- &nbsp;<input type="submit" value="查詢"> -->
	</form>

	<div class="article_total">文章總數：<span>{article_total}</span> &nbsp;&nbsp; 總觀看次數：<span>{total_views}</span></div>

	<table cellpadding="0" cellspacing="0" width="100%" class="tDefault tLight">
		<thead>
			<tr >
				<td title="編號" tips ="使用滑鼠點選 可以排序該欄位" class="order_control" data-orderby = "id">編號<i class="icon-chevron-{by-id}"></i></td>
				<td>標題</td>
				<td title="一日觀看次數" tips ="使用滑鼠點選 可以排序該欄位" class="order_control" data-orderby = "day_views">{hot_hours}HR觀看次數<i class="icon-chevron-{by-day_views}"></i></td>
				<td title="上架時間" tips ="使用滑鼠點選 可以排序該欄位" class="order_control" data-orderby = "online_datetime">上架時間<i class="icon-chevron-{by-online_datetime}"></i></td>
				<td title="發布時間" tips ="使用滑鼠點選 可以排序該欄位" class="order_control" data-orderby = "create_date">發布時間<i class="icon-chevron-{by-create_date}"></i></td>
				<td title="作者" tips ="使用滑鼠點選 可以排序該欄位" class="order_control" data-orderby = "create_user_id">作者<i class="icon-chevron-{by-create_user_id}"></i></td>
				<td title="POFANS ID" tips ="使用滑鼠點選 可以排序該欄位" class="order_control" data-orderby = "pofans2_id">POFANS ID<i class="icon-chevron-{by-pofans2_id}"></i></td>
			</tr>			
		</thead>
		
		<tbody class="statistics_tbody">
			{content}
			<tr>
				<td class="hot_{hot}">{id}</td>
				<td><a href="{toments_url}{id}" target="_blank">{article_title}</a></td>
				<td>{day_views}</td>
				<td>{online_datetime}</td>
				<td>{create_date}</td>
				<td>{author_name}</td>
				<td><a href="{pofans_url}{pofans2_id}" target="_blank">{pofans2_id}</a></td>								
			</tr>			
			{/content}
		</tbody>
	</table>
</div>

<style>
thead TD{
white-space: nowrap;
}
.hide, .topicview_{
	display: none;
}
.topicview_hide{
	display: initial;
}
.external_link{
	background: none;
}

form#list_orderby {
	float: left;
	margin: 20px 10px;
}

.hot_1:BEFORE {
	background: url(images/gold.png) no-repeat left;
	content: '';
	padding: 10px 0px 10px 30px;
	position: absolute;
	margin-left: -30px;
}
</style>

<script>
$(document).ready(function() {
    var days = $('#days_selected').val();
    $('#'+days).attr("selected", "selected");

    $('#days').on("change", function() {
		$('#list_orderby').submit();
	});	
});

function order_by(this_td){
	$('#order_by').val( $(this_td).attr('data-orderby'));
	$('#list_orderby').submit();
}

$(".order_control").live( "click", function() {
	var order_name = $(this).attr('data-orderby');
	console.info(this);
	if(order_name) order_by(this);
	
	return false;
});	

$('[tips]').hover(
	function() {
	    var x = $(this).offset().left;
	    var y = $(this).offset().top;
		var tips_box = document.createElement('div');
	    $(tips_box).attr("id","tips_box").html($(this).attr('tips')).appendTo('body');

	    var tips_width = $('#tips_box').width()+40;
	    var tips_height = $('#tips_box').height();
	  
	    var this_width = $(this).width();
	    var this_height = $(this).height();
	 
	    var diff_x = (tips_width + x > $(window).width())?$(window).width()-(tips_width + x):((this_width-$('#tips_box').width())/2);
	    var c_x = x + diff_x ;

	    var diff_y = (tips_height + y > $(window).height())?$(window).height()-(tips_height + y):(this_height+(tips_height)/2);
	    var c_y = y + diff_y ;
		 
	    $(tips_box).css('left',c_x).css('top',c_y).show();
	  			   
	}, function() {
		$('#tips_box').remove();
	}
);

</script>