<STYLE>
tbody IMG {
	display: inline-block;
}
tbody IMG.focus {
	border:2px dotted red;
}
<!--
.online A {
	position: relative;
}

.online A:first-child {
	z-index: 100;
}

.online A:not(:first-child ) {
	left: -62px;
	z-index: 1;
	opacity: 0;
	filter: alpha(opacity = 0);
	-moz-transform: rotateY(180deg);
	-webkit-transform: rotateY(180deg);
	transform: rotateY(180deg);
}

.online.off A:first-child {
	opacity: 0;
	filter: alpha(opacity = 0);
	-moz-transform: rotateY(-180deg);
	-webkit-transform: rotateY(-180deg);
	transform: rotateY(-180deg);
}

.online.off A:not(:first-child ) {
	opacity: 1;
	filter: alpha(opacity = 100);
	-moz-transform: rotateY(0deg);
	-webkit-transform: rotateY(0deg);
	transform: rotateY(0deg);
}
-->
</STYLE>




<div class="widget" >
	<table cellpadding="0" cellspacing="0"  class="tLight">
		<form id="list_orderby" method="post">
		<input id="order_by" type="hidden" name="order_by" value="">
		<thead>
			<tr>				
				<td title="群組名稱">名稱</td>				
				<td title="新增資料" width="auto;" style="cursor: pointer;" onclick="article_group.add();">新增資料<i class="icon-plus-sign"></i></td>
			</tr>			
		</thead>
		</form>
		<tbody>
			{content}
			<tr id="user_{id}" class="user_row">
				<td><span class="text_editable" data-index={id} data-name="name">{name}</span></td>				
				<td>	
					<a href="javascript:void(0);" class="tablectrl_small bRed " original-title="Del" title="刪除" alt="刪除" onclick="article_group.del({id});">
					<span class="iconb" data-icon="" ></span></a>		
					
					<span class="online {off}" data-index="{id}"> 
						<a href="javascript:void(0);" class="tablectrl_small bBlue" original-title="Edit" title="上架中"
							alt="上架中">上架中<span class="iconb" data-icon=""></span></a>
						<a href="javascript:void(0);" class="tablectrl_small bGold" original-title="Edit" title="下架中" 
							alt="下架中">下架中<span class="iconb" data-icon=""></span></a>
					</span>			
				</td>				
			</tr>
			{/content}
		</tbody>
	</table>
</div>
<script src="js/article.js"></script>
<script src="js/span_editor.js"></script>
<script type="text/javascript" >
$(".online").click(function() {
	var index= $(this).attr('data-index'); 
	var online = $(this).hasClass("off")
	console.info(index);
	if(online) $(this).removeClass("off");
	else $(this).addClass("off");

	if(index)
		$.ajax({
		  url: 'news_tag/tag_online/',
		  dataType: 'html',
		  type: "POST",
		  data: {
			  online:online,
			  index:index
		  },
		  success: function(data){
			  console.info(data);
		  	/**/
		  }
		});
	
});
$(function() {
	set_span_editor();
	$("SPAN.text_editable").attr('contenteditable',true).each(function(){
		if(!$(this).html() || $(this).html() == '') $(this).html('請輸入資料');

		}).on("focus",function(){
			if( $(this).html() == '請輸入資料') $(this).html('');
			
		}).on("blur",function(){
			if(!$(this).html() || $(this).html() == '') $(this).html('請輸入資料');
			
		}).on("keyup",function(){
			var text = $(this).html();
			var filed = $(this).attr('data-name');

			
			var index = $(this).attr('data-index');
						
			if(index && text != '請輸入資料') {
				$.ajax({
					  url: 'news_tag/update_tag/',
					  dataType: 'html',
					  type: "POST",
					  data: {			  
						  index:index,
						  filed:filed,
						  value:text
						  
					  },
					  success: function(data){
						  
						  if(data) {
							  $(this).html(data);
						  }
					  }
					});

			};

		});
	
	});
</script>