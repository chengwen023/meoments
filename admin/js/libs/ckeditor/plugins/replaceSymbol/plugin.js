CKEDITOR.plugins.add('replaceSymbol', {	
	requires : 'menubutton',
	init : function(editor) {
		// console.info('symbol');
		var valuesConfigStrings = editor.config.replaceSymbol,
		plugin = this,		
		items = {},
		parts,
		curValueId, // 2-letter value identifier.
		valueButtonId, // Will store button namespaced identifier, like "value_en".
		i;
		
		// Registers command.
		editor.addCommand( 'replaceSymbol', {
			contextSensitive: true,			
			exec: function( editor, valueId ) {
				var item = items[ 'value_' + valueId ];
				 console.info(item.replace);	
				 editor.insertHtml(item.replace);
				
					var range = editor.createRange();
					 
					 console.info(range);
					 range.moveToElementEditEnd( range.root );
					 editor.getSelection().selectRanges( [ range ] );		
					
					
			}
		} );
		
		for ( i = 0; i < valuesConfigStrings.length; i++ ) {
			parts = valuesConfigStrings[ i ].split( ':' );
			curValueId = parts[ 0 ];
			valueButtonId = 'value_' + curValueId;

			items[ valueButtonId ] = {
				label: parts[ 1 ],
				langId: curValueId,
				group: 'replaceSymbol',
				order: i,
				icon: this.path + 'omega.png',
				// Tells if this value is left-to-right oriented (default: true).
				replace: ( '' + parts[ 2 ] ).toLowerCase() ,
				onClick: function() {			
					editor.execCommand( 'replaceSymbol', this.langId );
					
				},
				role: 'menuitemcheckbox'
			};

			
		}

		editor.addMenuGroup( 'replaceSymbol', 1 );		
		editor.addMenuItems( items );
		
		
		editor.ui.add( 'replaceSymbol', CKEDITOR.UI_MENUBUTTON, {
			label : '插入符號',
			icon: this.path + 'omega.png',
			command: 'replaceSymbol',
			onMenu: function() {
				var activeItems = {};				
				for ( var prop in items )
					activeItems[ prop ] = CKEDITOR.TRISTATE_OFF;
				return activeItems;
			}
		} );
		
						
	}
});
