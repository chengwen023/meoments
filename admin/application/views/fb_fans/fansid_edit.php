<div>
	<form id="form_folder">
		<div class="tb-green">
			<table>
				<thead>
					<tr class="column1">
						<th scope="col" colspan="2">
							<?php echo (!empty($ikuso_fansid['auto_index']) && $ikuso_fansid['auto_index'] > 0)? '編輯':'新增';?>管理員
							<input type="hidden" name="auto_index" value="<?php echo (!empty($ikuso_fansid['auto_index'])?$ikuso_fansid['auto_index']:'');?>">
						</th>
					</tr>
				</thead>
				<tr>
					<td width="66">
						分類名稱
					</td>
					<td>
						<?php echo empty($ikuso_fansid['fans_name'])? '':$ikuso_fansid['fans_name'];?>
					</td>
				</tr>				
				<tr>
					<td width="66">
						分類名稱
					</td>
					<td>
						<?php echo empty($ikuso_fansid['fans_id'])? '':$ikuso_fansid['fans_id'];?>
					</td>
				</tr>
				<tr>
					<td width="66">
						粉絲團群組
					</td>
					<td >
						<ul style="width: 500px;list-style: none;">
						<?php foreach($topic_group AS $value => $group):?>
						<li style="float: left;width:100px;margin-right: 10px;">
						<input class="fans_group" name="fans_gids[]" type="checkbox" value="<?php echo $group['auto_index'];?>"  <?php echo (isset($group['checked']) && $group['checked'])?'checked="checked"':''?> /><?php echo $group['name'];?>
						</li>
						<?php endforeach;?>
						</ul>
					</td>
				</tr>
				
				<tr>
					<td>
					</td>
					<td colspan="2">
						<input type="submit" value="送出" style="padding:3px;font-size:14px;" />
					</td>
				</tr>
			</table>
		</div><!-- / tb-green-->
	</form>
</div>
<script>

	$(function(){
		// 註冊驗證
		$("#form_folder").validate({			
			submitHandler: function(form) {
				
				$.fancybox.showLoading();
				// 透過 Ajax 驗證是否註冊
				$.ajax({
				  url: '_admin/fb_fans/actions/validate_fansid',
				  cache: false,
				  dataType: 'json',
				  type: "POST",
				  data: $("#form_folder").serialize(),
				  success: function(data){
					
				  	$.fancybox.hideLoading();
				  	
				  	if(data.success == 'Y' ){
				  		alert(data.msg);
				  		location.reload();
				  	}else{
				  		alert(data.msg);
				  		$('input[name="id"]').focus();
				  		return false;
				  	}
				  	/**/
				  }
				});
				//form.submit();
				return false;
			}
		});
		// end of $("#signupForm").validate
	});
</script>