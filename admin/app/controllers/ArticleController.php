<?php
class ArticleController extends BaseController
{

	public $settings;
	
	public function initialize()
	{
		if ($this->request->isPost()) {		
			$this->order_by = $this->request->getPost('order_by');				
		}
		
	}
	
	function cut_imgAction(){
	
		$img = $this->request->getPost('img');			
		$cut_info = $this->request->getPost('cut_info');			
		
		$imgTool = new Imgtools();
		echo $imgTool->cut_base64img($img,$cut_info);
		exit;
	}
	
	public function indexAction()
	{
		
		$this->action = 'list';
		$this->listAction();
		return;
	}
	
	function defoptionAction($viewType = false){
		$orderBy = $this->order_by($this->order_by,'ArticleDefoptionGroup');
	
		$content = ArticleDefoptionGroup::find();
	
		if($content != false) $content = $content->toArray();
	
		$viewContent['content'] = $content;
		if($viewType == 'json') return json_encode($viewContent,true);
	
		$view = new contentView();
		$main_content = $view->getRender($this->controller, $this->action,$viewContent);
		
	
		$this->template = array(
				"main_content" => $main_content,
		);
	
	}
	
	function groupAction($viewType = false){
		$orderBy = $this->order_by($this->order_by,'ArticleGroup');
		
		$content = ArticleGroup::find();
	
		if($content != false) $content = $content->toArray();
	
		$viewContent['content'] = $content;
		if($viewType == 'json') return json_encode($viewContent,true);
	
		$view = new contentView();
		$main_content = $view->getRender($this->controller, $this->action,$viewContent);
	
		$this->template = array(
				"main_content" => $main_content,
		);
	
	}
	
public function listAction($pg = 1, $user = '',$viewType = false)
	{
		$orderBy = $this->order_by($this->order_by,'Article');
		
		
		
		// 讀取 題目 使用者資訊
		$sql = "select admin_account.id AS value,CONCAT(admin_account.id,'(' ,sum(case when article.online = '0' then 0 else 1 end),'/',count(admin_account.auto_index) ,')'  ) AS label
				FROM admin_account
				INNER JOIN article ON admin_account.auto_index = article.create_account_index
				GROUP BY admin_account.auto_index
				";
		
		


		
		
		$temp_selector ['current_url'] =  ( '/article/list/' . $pg ) . "/";
		
		$temp_selector ['select_name'] = (! isset ( $temp_userid ) && ! isset ( $temp_userid ) && $user) ? $user : 'All User';
		$temp_selector ['select_option'] = $this->db->query($sql)->fetchAll();
		self::$select_result[] = $temp_selector;
		
		
		$select_user_index = AdminAccount::findFirst(array("id = '".$user."'"));
		
		if( $select_user_index != false ) $select_user_index = "create_account_index = ".$select_user_index->auto_index;

		$total_nums = Article::count(array( $select_user_index));
		$page_one = 10;
		$content = Article::find(array( $select_user_index ,"limit" => ($pg-1)*$page_one.", ".$page_one));
		
		$content->rewind();
		while ($content->valid()) {
		    $Article = $content->current();
		    $ArticleItem = $Article->toArray();
		    $ArticleItem['id'] = $Article->get_AdminAccount_field('id');

		    $ArticleItem['edit_control'] = ($ArticleItem['online'])?'hide':'';

		   
		   if($ArticleItem['article_group'] && false != $group_array = json_decode($ArticleItem['article_group'],true) )
		   {
			   $group_array = ArticleGroup::find(array(" auto_index in (".implode(',', $group_array).") AND online = '1'",));
		   	
			   if($group_array != false ) {
			   		$ArticleItem['article_group']= implode(',', array_column($group_array->toArray(), 'name'));
			   }
		   }
		   
		 			
		 			
		    
		    
		    $ArticleList[] = $ArticleItem; 
		    $content->next();
		}
		
		
		$page ['start'] = 1;
		$page ['end'] = ceil ( $total_nums / $page_one );
		$page ['url'] = "/".$this->controller.'/'.$this->action;
		$page ['page_num'] = $pg;
		$page ['url_end'] = '/'.$user ;
		
		
		$p = new Pagetheme;
		$page = $p->page($page);
		
		
		$viewContent['page'] = $page;
		$viewContent['content'] = $ArticleList;
		if($viewType == 'json') return json_encode($viewContent,true);
		
		$view = new contentView();
		$main_content = $view->getRender($this->controller, $this->action,$viewContent);
		
		$this->template = array(
				"main_content" => $main_content,
		);
			
	}

	function actionsAction($action = false) {
	
		if ($this->request->isPost()) {
			$Login = $this->session->get('userInfo')->toArray();
	
			$date = date("Y-m-d H:i:s");
			$account_id = $Login['auto_index'];
				
				
			/*
			 * group
			 */
			if($action == 'del_group'){				
				$index = $this->request->getPost('index');
				$content = ArticleGroup::findFirst("auto_index = '".$index."'")->delete();
				if($content == false) echo '無此筆資料';				
			}
			
			if($action == 'create_group'){
			
				$value = $this->request->getPost('value');
				
				$add_input = array ('name' =>$value);
				$content  = new ArticleGroup();
				$content->setAccountIndex($account_id);			
				
				if ($content->create($add_input) == false) {
					foreach ($content->getMessages() as $message) {
						echo $message, "\n";
					}
				}
			}
			
			if($action == 'update_group'){
					
				$index = $this->request->getPost('index');
				$filed = $this->request->getPost('filed');
				$value = $this->request->getPost('value');
			
				$add_input = array ($filed =>$value);
				
				$content = ArticleGroup::findFirst("auto_index = '".$index."'");
				$content->setAccountIndex($account_id);
				
				if ($content->update($add_input) == false) {
					foreach ($content->getMessages() as $message) {
						echo $message, "\n";
					}
				}
				
			}
			/*
			 * Article add_article
			 */
			if($action == 'del_article'){
				$index = $this->request->getPost('index');
				$content = Article::findFirst("auto_index = '".$index."'")->delete();
				$content = ArticleFromIndex::findFirst("article_index = '".$index."'")->delete();
				
				
			}
			if($action == 'add_article') {
			
				$index = $this->request->getPost('index');
				
				$ArticleGroup = ArticleGroup::find()->toArray();
				$ArticleDefoptionGroup = ArticleDefoptionGroup::find()->toArray();
				foreach ($ArticleDefoptionGroup AS $key => $group){					
					$ArticleDefoptionGroup[$key]['options'] = ArticleDefoptions::find("defoptions_group_index = '".$group['auto_index']."'")->toArray();					
				}
				
				$content = Article::findFirst("auto_index = '".$index."'");
				if($content != false){
						
					$content =$content->toArray();
					
					$articleGroupIndexs = json_decode($content['article_group'],true);
					foreach ($ArticleDefoptionGroup AS $group){
						if($group['auto_index'] == $content['article_defoption'])	{								
							$group['checked'] = 'checked';
						}
						$group['options'] = ArticleDefoptions::find("defoptions_group_index = '".$group['auto_index']."'")->toArray();
						$content['ArticleDefoptionGroup'][] = $group;
					}
					foreach ($ArticleGroup AS $group){
						if(in_array($group['auto_index'], $articleGroupIndexs))	{							
							$group['checked'] = 'checked';
						}
						$content['ArticleGroup'][] = $group;
					}
					
				}else{
					
					/*default value*/
						
					$content ['auto_index'] = 0;
					$content ['article_imgurl'] = 'http://goquiz88.com/img/goquiz88_facebook_share.jpg';
					$content ['auto_index'] = 0;
					$content ['online'] = 0;
					$content ['article_title'] = '';
					$content ['article_optiontitle'] = '';
					$content ['article_text'] = '';
					$content ['article_options'][] = array('choose'=>'','title'=>'','text'=>'');
					$content ['from_table'] = '';
					$content ['from_index'] = '';
					$content['ArticleDefoptionGroup']= $ArticleDefoptionGroup;
					$content['ArticleGroup']= $ArticleGroup;
					
					$horoIndex = $this->request->getPost('horo');
					
					if($horoIndex){
						
						$connection =  array(
								"host" => "localhost",
								"username" => "ifunso_user",
								"password" => "AbcAbcAbc1985",
								"dbname" => "ifunso_horoscope",
								"charset" => 'utf8',
						);
						$this->DBconnect = $this->connectDB($connection);
						
						$sql = "SELECT pk_horoscope,title_horoscope,text_horoscope,pofans_text,img_file FROM horoscope 
								WHERE pk_horoscope = ".$horoIndex." limit 1 ";
						
						
						$horoResult = $this->DBconnect->fetchOne($sql);
						$content ['article_imgurl'] = 'http://horoscope.ifunso.com/uploads/result/'.$horoResult['img_file'];	
						$content ['article_title'] = $horoResult['title_horoscope'];
						$content ['article_optiontitle'] = $horoResult['pofans_text'];
						$content ['article_text'] = $horoResult['text_horoscope'];	

						$content ['from_table'] = 'horoscope';
						$content ['from_index'] = $horoIndex;
					}
					
					
						
				}
				$this->view->content = $content;
				echo  $this->view->start()->render('article', 'edit')->finish()->getContent();
					
			}
			
			
			if($action == 'validate') {
					
			
			
				$index = $this->request->getPost('auto_index');
				$article_array = $this->request->getPost('article_array');
				$article_array['article_group'] = json_encode($article_array['article_group']);
				$add_input = $article_array; 
				$from_table = $this->request->getPost('from_table');
				$from_index = $this->request->getPost('from_index');
				
				$content = Article::findFirst("auto_index = '".$index."'");
				
				if($content != false){
					$content->setAccountIndex($account_id);
						
					$defoptionGroup = $content->auto_index;
					$content->update($add_input);
					
						
				}else{
					$content       = new Article();
					$content->setAccountIndex($account_id);
					
					if ($content->create($add_input) == false) {
						foreach ($content->getMessages() as $message) {
							echo $message, "\n";
						}
					}else {
						$horoIndex = $content->auto_index;
					}
					
					if($horoIndex && $from_table && $from_index){
						
						$horoArticle = new ArticleFromIndex;
						$horo_input = array('article_index'=>$horoIndex,'from_index'=>$from_index,'from_table'=>$from_table);
						
						if ($horoArticle->create($horo_input) == false) {
							foreach ($horoArticle->getMessages() as $message) {
								echo $message, "\n";
							}
						}
					}
						
					exit;	
				}
				
				
			}
			/*
			 * defoptions
			 */
			if($action == 'del_defoptions') {
			
				$index = $this->request->getPost('index');
				$content = ArticleDefoptionGroup::findFirst("auto_index = '".$index."'");
				if($content != false){						
					$defoptionGroup = $content->auto_index;
					if($defoptionGroup){
						$content->delete();
						ArticleDefoptions::find("defoptions_group_index=".$defoptionGroup)->delete();
					}
						
				}else{
					echo '無此筆資料';
						
				}
				
					
			}
			if($action == 'add_defoptions') {
				
				$index = $this->request->getPost('index');			
				$content = ArticleDefoptionGroup::findFirst("auto_index = '".$index."'");
				if($content != false){		
					
					$content =$content->toArray();					
					$content['article_options'] = ArticleDefoptions::find("defoptions_group_index = '".$index."'")->toArray();	
				}else{
					/*default value*/
					$defoptions = array('choose'=>'','title'=>'','text'=>'','pic_url' => 'http://goquiz88.com/img/goquiz88_facebook_share.jpg');
					$content ['auto_index'] = 0;
					$content ['online'] = 0;
					$content ['defoptions_group_name'] = '';
					$content ['article_options'][] = $defoptions;
					
				}
				$this->view->content = $content;
				echo  $this->view->start()->render('article', 'defoption_edit')->finish()->getContent();
					
			}
			
			
			if($action == 'validate_defoptions') {
			
				
				
				$index = $this->request->getPost('auto_index');
				$defoption = $this->request->getPost('defoption');
				
			
				$add_input = array (
						'defoptions_group_name' => $defoption['defoptions_group_name']
				);
				$add_options = isset($defoption['options'])?$defoption['options']:false;
								
				$content = ArticleDefoptionGroup::findFirst("auto_index = '".$index."'");
				
				
				if($content != false){
					$content->setAccountIndex($account_id);
					
					$defoptionGroup = $content->auto_index;
					$content->update($add_input);
					
					ArticleDefoptions::find("defoptions_group_index=".$defoptionGroup)->delete();
					foreach ($add_options AS $add_option){
						$option       = new ArticleDefoptions();
						
						$option->setArticleDefoptionGroupIndex($defoptionGroup);
							
						if($option->create($add_option) == false){
							foreach ($option->getMessages() as $message) {
								echo $message, "\n";
							}
						}
							
					}
					
				}else{
					$content       = new ArticleDefoptionGroup();
					$content->setAccountIndex($account_id);
					
					if ($content->create($add_input) == false) {						
						foreach ($content->getMessages() as $message) {
							echo $message, "\n";
						}
					} else {
						$defoptionGroup = $content->auto_index;
						
						ArticleDefoptions::find("defoptions_group_index=".$defoptionGroup)->delete();
						foreach ($add_options AS $add_option){
							$option       = new ArticleDefoptions();
							$option->setArticleDefoptionGroupIndex($defoptionGroup);
							
							if($option->create($add_option) == false){
								foreach ($option->getMessages() as $message) {
									echo $message, "\n";
								}
							}
							
						}
						
					}
					
					
				}
				exit;
				/**/
			}
			
				
		}
	
		
	
	
	
	
	
	}
}
?>