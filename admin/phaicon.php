<?php 
header('Content-Type: text/html; charset=utf-8');
// phpinfo();
// exit;
try {
	$config = new Phalcon\Config\Adapter\Ini('./app/config/config.ini');
	
	//Register an autoloader
	$loader = new \Phalcon\Loader();
	
	$loader->registerDirs(
	    array(
	        $config->application->controllersDir,
	        $config->application->pluginsDir,
	        $config->application->libraryDir,
	        $config->application->modelsDir,
    		
	    )
	)->register();

		
		
	//Create a DI
	$di = new Phalcon\DI\FactoryDefault();
	
	//Start the session the first time a component requests the session service
	$di->set('session', function() {
		$session = new Phalcon\Session\Adapter\Files();
		$session->start();
		return $session;
	});
	
	//Setup the database service
	$di->set('db', function() use ($config){
		return new \Phalcon\Db\Adapter\Pdo\Mysql(array(
			"host" => $config->database->host,
        	"username" => $config->database->username,
        	"password" => $config->database->password,
        	"dbname" => $config->database->name,
			'charset'   =>$config->database->charset,
		));
	});

	//Setup the view component
	$di->set('view', function() use ($config){
		$view = new \Phalcon\Mvc\View();
		$view->setViewsDir($config->application->viewsDir);
		$view->registerEngines(array(
				".volt" => 'Phalcon\Mvc\View\Engine\Volt',
				".phtml" => 'Phalcon\Mvc\View\Engine\Volt'
		));
		
		return $view;
	});
	
	$di->set('cookies', function() {
		$cookies = new Phalcon\Http\Response\Cookies();
		$cookies->useEncryption(false);
		return $cookies;
	});

// 		//Setup a base URI so that all generated URIs include the "tutorial" folder
// 		$di->set('url', function(){
// 			$url = new \Phalcon\Mvc\Url();
// 			$url->setBaseUri('/phaicon/');
// 			return $url;
// 		});
		
	
	$di->set('dispatcher', function() use ($di) {

    //Obtain the standard eventsManager from the DI
    $eventsManager = $di->getShared('eventsManager');

    //Instantiate the Security plugin
    $security = new Security($di);
   
    //Listen for events produced in the dispatcher using the Security plugin
    $eventsManager->attach('dispatch', $security);
    
    $dispatcher = new Phalcon\Mvc\Dispatcher();

    //Bind the EventsManager to the Dispatcher
    $dispatcher->setEventsManager($eventsManager);

    	return $dispatcher;
	});
	
	
	//Handle the request
	$application = new \Phalcon\Mvc\Application($di);
// 	$application->useImplicitView(false);
	echo $application->handle()->getContent();

} catch(Exception $e) {
	echo "PhalconException: ", $e->getMessage();
}

class contentView extends \Phalcon\Mvc\View
{
	public function __Construct($ViewsDir = false)
	{
		$config = new Phalcon\Config\Adapter\Ini('./app/config/config.ini');
		$this->setViewsDir(($ViewsDir)?$ViewsDir:$config->application->viewsDir);		
	}
	
}


class BaseController extends \Phalcon\Mvc\Controller
{
	public $template;
	public $order_by;
	public $topSearch;
	public $controller;
	public $action;
	
	static $title = '';
	static $menu = array();
	static $breadcrumbs = array();
	static $topbar_control = array();
	static $select_result =  array();
	
	
	public function beforeDispatch($dispatcher){
		
		
	
	}
	public function beforeExecuteRoute($dispatcher)
	{
		if (!$this->session->has("userInfo")) {
			return $this->dispatcher->forward(array(
					'controller' => 'login',
					'action' => 'index'
			));
		}
		
		$this->controller = $dispatcher->getControllerName();
		$this->action = $dispatcher->getActionName();
		
		
		
		self::$menu = $this->adminMenu();
		self::$title = $this->getControllerTitle();
	}
	
	
	
	public function afterExecuteRoute($dispatcher)
	{
		
		if($template = $this->template){
			$template['menu'] = self::$menu;
			$template['title'] = self::$title;
			$template['breadcrumbs'] = self::$breadcrumbs;
			$template['topbar_control'] = self::$topbar_control;
			$template['select_result'] = self::$select_result;
			echo $this->view->getRender('', 'template',$template,
					function($view) {	$view->setRenderLevel(Phalcon\Mvc\View::LEVEL_LAYOUT);	}
			);
		}
	}
	
	
	public function onConstruct()
	{
	
	
	}
	
	protected function page_set(){
		
		
	}
	
	protected function adminMenu()
	{
		$menu = array();
		if(!$this->session->has('userInfo')) return $menu;
		
		$Login = $this->session->get('userInfo');
		$competence_menu = array_keys(json_decode($Login->competence,true));
		
		$group_name = AdminMenu::find(array(
				" auto_index in (".implode(',', $competence_menu).") AND online = '1'",
				"group" => array("group_name"),
		))->toArray() ;
		
		foreach ($group_name AS $group){
			$temp_list =$group;
			$temp_list['child_list']= AdminMenu::find(array(
					"auto_index in (".implode(',', $competence_menu).") AND group_name = :group_name: AND online = '1'",
					"bind" => array('group_name' => $group['group_name'] ),
			))->toArray();
			
			$menu[] = $temp_list;
		}
		
		return $menu;
	}
	
	protected function order_by($order_name,$order_page = ''){
	
		$order_by = $this->cookies->get($order_page.'-order_by')->getValue();
	
	
		if(isset($order_name) && $order_name){
			if(isset($order_by['by-'.$order_name]) && $order_by['by-'.$order_name] != 'down') $order_by['by-'.$order_name] = "down";
			else {
				unset($order_by);
				$order_by['by-'.$order_name] = "up";
			}
			$this->cookies->set($order_page.'-order_by', $order_by);
		}
	
		return (is_array($order_by))?$order_by:array();
	}
	
	protected function getControllerTitle()
	{
		$url = $this->controller;
		if($this->action != 'index') $url .= "/".$this->action;
		
		$title = AdminMenu::findFirst(array(
		"url = :url:",
		"bind" => array('url' => $url ),
		));
		
		if($title != false)	{
			$title = $title->toArray();
			return $title['child_name'];
		}
		else return $url;
	}
	
	protected function topSearchSql($field = array(),$topSearch = FALSE)
	{
		if(!$topSearch) $topSearch = $this->topSearch;
		if($topSearch && count($field)) {
			
			$where = implode(' LIKE :topSearch: OR ', $field)." LIKE :topSearch: ";
			
			$topSearch = array($where,
		
				"bind" => array('topSearch' => '%' . $topSearch . '%')
			);
		}
		return $topSearch;
	}
	
	
	protected function connectDB($connection){
		return $connection = new \Phalcon\Db\Adapter\Pdo\Mysql($connection);
		
		
	}
}

class IndexController extends BaseController
{

	public function indexAction()
	{
		$this->response->redirect('account');
	}

	
}
?>
