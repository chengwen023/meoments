<?php
if ($_SERVER ["SERVER_NAME"] == '127.0.0.1' || $_SERVER ["SERVER_NAME"] == 'localhost' || $_SERVER ["SERVER_NAME"] == 'shoda.tw') {
	$ImgServerUrl = $curServerURL . '/news/file/';
	$baseUrl = '/git/web/';
	$articleUrl = 'http://127.0.0.1/news/tpl/code/adonis.php?id=';
} else {
	$ImgServerUrl = 'http://file.meoments.com/';
	$baseUrl = '/web/';
	$articleUrl = 'http://meoments.com/';
}


$fb_admin = '100009535309931';
$fb_app_id = '588005798009398';
$img_t = "CONCAT('$ImgServerUrl/n', `news_article_onshelf`.id, '/t.jpg')";
$img_tm = "CONCAT('$ImgServerUrl/n', `news_article_onshelf`.id, '/t_m.jpg')";
$listUrl = "CONCAT('$articleUrl', `news_article_onshelf`.id,'/')";
$tagUrl = "CONCAT('$curServerURL/tag/', name,'/')";
$authorUrl = "CONCAT('$curServerURL/author/', `admin_account`.memo,'/')";

$memcached = new iplayfun_memcached ( 'meoments' );
// $memcached->deleteAllKeys ();
$memcached->set_prefix ( '[Global]' );



$categoryUrl = "CONCAT('$curServerURL/category/',name,'/')";

// 設定參數列表
$sql = "SELECT code as id,value,type FROM config WHERE onshelf = 1";
$global [] = [
		'key' => 'config',
		'func' => $memcached->SQLfunc ( $sql )
];
// 分類列表
$global [] = [ 
		'key' => 'categories',
		'func' => $memcached->SQLfunc ( "SELECT id,name, $categoryUrl AS url FROM `categories` WHERE online = 1 " ) 
];
// 作者列表
$global [] = [
		'key' => 'admin_account',
		'func' => $memcached->SQLfunc ( "SELECT auto_index as id,memo AS name FROM `admin_account` WHERE online = '1' " )
];
// 廣告資訊
$sql = "SELECT * FROM `ad_code` ";
$global [] = [ 
		'key' => 'ad_code',
		'func' => $memcached->SQLfunc ( $sql ) 
];
// Fans資訊 "http://www.facebook.com/"
$sql = "SELECT CONCAT('http://www.facebook.com/',page_id ) AS url,page_id,toments_category FROM `pofans_fanslist` WHERE `online` = '1' ";
$global [] = [
		'key' => 'pofans_fanslist',
		'func' => $memcached->SQLfunc ( $sql )
];

// 關鍵字列表
$sql = "SELECT $tagUrl as url,id,name FROM news_tags WHERE online = '1' AND article_num > 0";
$global [] = [
		'key' => 'news_tags',
		'func' => $memcached->SQLfunc ( $sql )
];

// 熱門關鍵字
$sql = "SELECT $tagUrl as url,id,name FROM news_tags WHERE online = '1' AND article_num > 0 ORDER BY RAND() limit 20";
$global [] = [
		'key' => 'hot_news_tags',
		'func' => $memcached->SQLfunc ( $sql ),
		'expire' => 3600
];

// 推薦文章列表
$sql = "SELECT id, title, $img_tm AS img, $listUrl AS url FROM `news_article_onshelf` ORDER BY update_time DESC LIMIT 0,15";
$global [] = [ 
		'key' => 'article_list',
		'func' => $memcached->SQLfunc ( $sql ) ,
		'expire' => 3600
];

// 推薦文章列表
$sql = "SELECT `news_article_onshelf`.title, $img_tm AS img, $listUrl AS url FROM `news_article_onshelf`
	LEFT JOIN `admin_account` ON (`admin_account`.auto_index = `news_article_onshelf`.create_user_id )
	INNER JOIN (
		SELECT DISTINCT `id`
		FROM  `news_article_state`
		WHERE `online` =  '1' ORDER BY daily_views DESC LIMIT 0,15 
	) AS nas ON nas.id =  `news_article_onshelf`.`id` ";

$global [] = [
		'key' => 'daily_views_list',
		'func' => $memcached->SQLfunc ( $sql ),
		'expire' => 7200
];

$global  = $memcached->getMultiWithSet($global,86400);


if(isset($global['admin_account']) && is_array($global['admin_account']))
	foreach ( $global['admin_account'] AS $row ) {
			
		
		if(isset($author)&& is_array($author) && in_array( trim($row['name']),$author)) {
			$authorID = $row['id'];
		}
			
		$author_name[$row['id']] = $row;
	}

if(isset($global['config']) && is_array($global['config']))
	foreach ( $global['config'] AS $row ) {		
		$config_name[$row['id']] = ($row['type']=='json')?(JSON_decode($row['value'],true)):$row['value'];
	}
if(isset($global['news_tags']) && is_array($global['news_tags']))
		foreach ( $global['news_tags'] AS $row ) {
			
			if(isset($tag)&& is_array($tag) && in_array( trim($row['name']),$tag)) {
				$tagID = $row['id'];
				$row['active'] = 'active';
			}
			
				$tag_name[$row['id']] = $row;	
		}

if(isset($global['categories']) && is_array($global['categories']))
	foreach ( $global['categories'] AS $row ) {

		if(isset($category)&& is_array($category) && in_array( trim($row['name']),$category)) {
			$categoryID = $row['id'];
			$row['active'] = 'active';
		}

		$category_name[$row['id']] = $row;

	}

$fanslist = array();
if(isset($global['pofans_fanslist']) && is_array($global['pofans_fanslist']))
	foreach ( $global['pofans_fanslist'] AS $row ) {

		if(empty($row['toments_category']) ||in_array($categoryID, JSON_decode($row['toments_category']) )) $fanslist[] = $row;

	}

$categoryHot[0] = array('name'=>'熱門','url'=> 'http://meoments.com/category/Hot/');
if($categoryID == 'Hot'){
	$categoryHot[0]['active'] = 'active';
}
$category_name = array_merge($categoryHot,$category_name);
?>