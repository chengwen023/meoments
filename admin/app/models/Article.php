<?php 
class Article extends \Phalcon\Mvc\Model
{
	
	public $account_index;
	
	public function initialize()
	{
		$this->belongsTo("update_account_index", "AdminAccount", "auto_index");
		$this->belongsTo("create_account_index", "AdminAccount", "auto_index");		
	}
	
	public function beforeValidationOnCreate()
	{
		$this->online = '0';
		$this->facebook_imgurl = false;
		$this->create_time = date('Y-m-d H:i:s');
		$this->update_time = date('Y-m-d H:i:s');
		  
		if($this->account_index){
			$this->create_account_index = $this->account_index;
			$this->update_account_index = $this->account_index;
		}
	
	}
	
	public function beforeValidationOnUpdate()
	{
		$this->facebook_imgurl = false;
		$this->update_time = date('Y-m-d H:i:s');
		if($this->account_index){
			$this->update_account_index = $this->account_index;
		}
	}
	
	public function beforeSave()
	{
		
	}
	
	public function afterFetch()
	{
		if(!$this->article_imgurl)
			$this->article_imgurl = 'http://goquiz88.com/img/goquiz88_facebook_share.jpg';
	}
	
	public function get_AdminAccount_field($field = false)
	{
		if($field) $field = $this->AdminAccount->{$field};
		return $field;
		
	}
	
	public function setAccountIndex($index){
		$this->account_index = $index;
	
	}
}

?>