
var defoption_list = new Object;

defoption_list.add_edit = function(index){
	
	url = 'article_defoption/actions/add_defoptions';
	
	$.fancybox.showLoading();
	$.ajax({
		url: url,
	  cache: false,
		dataType: 'html',
	  type: 'POST',
	  data:{
		  index:index
	  },
	  success: function(data){
	  	$.fancybox.hideLoading();
	  	$.fancybox.open(data,{	  
	  		autoCenter   : true,
		  	helpers : {
		        overlay : {
		        	closeClick: false
		        }
	        },
	        afterShow : function(){
	        	//$('.fancybox-inner').css('width', '620px');
	        },
	        beforeClose : function(){
	        	var ask = confirm("是否關閉 新增/編輯 資料視窗!?");
	        	
	        	if(ask) {
	        		$('.add_topic_option').die('click');
		        	$("DIV.fb_view_control SPAN").die('click');
		        	$("IMG.editable").die('click');
	        		
	        	}
	        	return(ask);	
	        }
		  });
	  }
	});
}

defoption_list.del= function(index){
	$.ajax({
		url: 'article_defoption/actions/del',
	  cache: false,
	  dataType: 'json',
	  async : false,
	  type: 'POST',
	  data: 'index='+index,
	  success: function(data){		
		  console.info(data);
		  	if(data.success == 'Y'){
		  		$.fancybox.close(true);
		  		location.reload();
		  	}else{
		  		$.fancybox.close(true);
		  		alert(data.msg);
		  	}
	  }
	});
}