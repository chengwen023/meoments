<span onclick="show_td();">show</span>
<span onclick="hide_td();">hide</span>
<div class="widget">
	<table cellpadding="0" cellspacing="0" width="100%" class="tLight">
	<form id="list_orderby" method="post">
		<input id="order_by" type="hidden" name="order_by" value="">
		<thead>
			
			<tr>
				<td title="名稱" class="order_control" data-orderby = "name" >名稱<i class="icon-chevron-{by-name}"></i></td>
				<td title="樣板名稱" class="order_control" data-orderby = "class_name"  >樣板名稱<i class="icon-chevron-{by-class_name}"></i></td>				
				<td title="新增資料" width="150px;" style="cursor: pointer;" onclick="fans.add_edit('');" >新增資料<i class="icon-plus-sign"></i></td>
			</tr>
		</thead>
		<tbody>
			{content}
			<tr id="fans_{auto_index}" class="user_row">
				<td>{name}</td>
				<td>{class_name}</td>
				
				<td>
				<a href="javascript:void(0);" class="tablectrl_small bGreen" original-title="Edit" title="修改" alt="修改"><span class="iconb" data-icon="" onclick="fans.add_edit({auto_index});"></span></a>
				<a href="javascript:void(0);" class="tablectrl_small bGreen" original-title="Option" title="圖片編輯" alt="圖片編輯"><span class="iconb" data-icon="" onclick="fans.pic_list({auto_index});"></span></a>
				<a href="javascript:void(0);" class="tablectrl_small bRed" original-title="Del" title="刪除" alt="刪除"><span class="iconb" data-icon="" onclick="fans.del({auto_index});"></span></a>
				</td>				
			</tr>
			{/content}
			
		</tbody>
	</table>
</div>
<script src="js/fans.js"></script>
<script>

function order_by(this_td){

	$('#order_by').val( $(this_td).attr('data-orderby'));

	$('#list_orderby').submit();

}

$(".order_control").live( "click", function() {
	var order_name = $(this).attr('data-orderby');
	if(order_name) order_by(this);
	
	return false;
});	
</script>
