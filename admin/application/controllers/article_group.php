<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Article_group extends CI_Controller {
	
	var $menu_index;
	
	
	public function __construct(){
		parent::__construct();
		
		$this->userInfo = $this->session->userdata('userInfo');
		$this->load->model('theme_model', 'theme');
		$this->load->model('article_model', 'article');
		
		$this->data['admin_menu'] = $this->config->item('admin_menu');
		$this->data['admin_menuGroup'] = $this->config->item('admin_menuGroup');
		$this->data['userInfo'] = $this->userInfo;
		$this->data['menu'] = $this->config->item('admin_accountMenu');
		$this->data['breadcrumbs'] = $this->config->item('breadcrumbs');
		$this->data['title'] =$this->config->item('admin_title');
		
		$this->data['topbar_control']= array();
		$this->data['select_result'] = array();
		
		
	}
	
	function create_group(){
		
		$auto_index = $this->input->post ( 'auto_index' );
		
	
		$date = date ( "Y-m-d H:i:s" );
		$account_id = $this->config->item ( 'admin_accountIndex' );
	
		$add_input = array (
				'defoptions_group_name' => $topic_array['defoptions_group_name']
		);
		$add_options = isset($topic_array['options'])?$topic_array['options']:false;
	
		$add_input ['create_time'] = $date;
		$add_input ['create_account_index'] = $account_id;
		$add_input ['update_time'] = $date;
		$add_input ['update_account_index'] = $account_id;
	
		if($auto_index)
			$result = $this->article->check_fans_exist_and_update($add_input,$auto_index);
		else
			$result = $this->article->check_topic_exist_and_insert($add_input);
	
	}
	
	/*
	* index
	*/
	public function index(){
		$this->lists();
	}
	
	/**
	 *	list
	 */
	
	
	function view(){
			$data = $this->data;
			
			$this->parser->parse('defoption/defoption_view', $data);
		}

	function lists(){
			
		$data = $this->data;
		$this->menu_index = '文章分類管理';//
		if(false === $activestatus = $this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, 'V')){
			$this->parser->parse('template', $data);
			return false;
		} 
		
		$defaultstatus = (isset($activestatus))?$activestatus:$this->config->item('defaultstatus');
		
		
		$this->db->flush_cache();
		$this->db->select("*");
		$this->db->from("article_group");
		
		
		$query = $this->db->get();
		
		$data['content'] = $query->result_array();
		foreach($data['content'] AS $key => $list){
			$data['content'][$key]['off'] = ($list['online'])?'':'off';
		}
		
		$data['main_content'] = $this->parser->parse('article/article_group_list', $data, true);
		$this->parser->parse('template', $data);
	}

	function group_online(){
	
		$index = $this->input->post('index');
		$online = $this->input->post('online');
	
		if($online == 'true') $add_input = array('online' => '1');
		else $add_input = array('online' =>'0');
	
		$this->db->where('auto_index', $index);
		$this->db->update('article_group', $add_input);
	}
	
	function update_group(){
	
		$index = $this->input->post ( 'index' );
		$filed = $this->input->post ( 'filed' );
		$value = $this->input->post ( 'value' );
	
		if($index && $filed ){
			$add_input = array($filed => $value);
			$this->db->where('auto_index', $index);
			$this->db->update('article_group', $add_input);
		}
		else if($value){
			echo 'adonis';
			$date = date ( "Y-m-d H:i:s" );
			$account_id = $this->config->item ( 'admin_accountIndex' );
			$add_input ['name'] = $value;
			$add_input ['create_time'] = $date;
			$add_input ['create_account_index'] = $account_id;
			$add_input ['update_time'] = $date;
			$add_input ['update_account_index'] = $account_id;
				
			$this->db->insert('article_group', $add_input);
		}	
	}
	
	function del(){
		$this->menu_index = '文章分類管理'; //
		if (! $this->admin_model->checkCompetence_getMenuInfo ( $this->menu_index, 'D' )) {
			$result = array (
					'success' => 'N',
					'msg' => '權限不足'
			);
			echo json_encode ( $result );
			break;
		}
		
		$index = $this->input->post ( 'index' );
		$this->db->delete('article_group', array('auto_index' => $index));
		
	}
}