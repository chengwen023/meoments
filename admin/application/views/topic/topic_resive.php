<style>
#img_selectbox {
	display: none;
	width: 550px;
	height: 420px;
	text-align: center;
	background-color: #94b0dc;
}

.fb_view_control {
	background-color: #336699;
	color: #fff;
	height: 20px;
}

.fb_view_control SPAN {
	margin-right: 10px;
	padding: 2px 5px;
}

.fb_view_control SPAN:HOVER {
	cursor: pointer;
	color: #3366cc;
	background-color: #fff;
}

.fb_view {
	width: 500px;
	height: 375px;
	margin: auto;
	border: 1px solid red;
}

.fb_link {
	width: 500px;
	height: 260px;
}

.fb_link_s {
	width: 375px;
	height: 375px;
}

.topic_photo {
	border: 1px solid;
	box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3);
	padding: 7px 5px 7px 7px;
	margin-right: 15px;
	margin-bottom:15px;
	float: left;
	margin-top: 25px;
	position: relative;
}
.topic_photo:before{
	content: attr(data-title);
	position: absolute;
	top: -20px;
}
.topic_group {
	width: 340px;
	float: left;
	margin: 10px;
}

.topic_group STRONG {
	color: #000;
	float: right;
	cursor: pointer;
	border-top: 1px solid #999;
}

.topic_group UL {
	margin: 0;
	display: none;
}

.topic_group.editable UL.options_select {
	position: absolute;
	display: inline-block;
	margin-top: 30px;
	background-color: #fff;
	border: 1px solid #D6D6D6;
	border-radius: 10px;
	padding: 5px 5px;
	box-shadow: 3px 3px 4px rgba(0, 0, 0, 0.2);
	z-index: 10;
}

.topic_group UL LI {
	float: left;
	margin-right: 10px;
	cursor: pointer;
}

.topic_group UL LI:BEFORE {
	content: ' ';
	padding: 10px 0px 0px 30px;
}

.topic_group UL LI:HOVER {
	color: #585858;
}

.topic_group UL LI:HOVER:BEFORE {
	background: url(images/check_green.png) no-repeat left;
}

.topic_group #topic_group_name UL {
	display: block;
	width: 320px;
	float: left;
}

.topic_group #topic_group_name UL LI.checked:BEFORE {
	background: url(images/check_green.png) no-repeat left;
}

.audit_left.not_pass:BEFORE {
	content: ' ';
	padding: 10px 0px 10px 30px;
	background: url(images/not_pass.gif) no-repeat left;
	position: absolute;
	margin-left: -30px;
}

.audit_right.not_pass:AFTER {
	content: ' ';
	padding: 10px 0px 10px 30px;
	background: url(images/not_pass.gif) no-repeat left;
}
.topic_photo.not_pass:BEFORE {
	content: attr(data-title);
	padding: 0px 0px 00px 30px;
	background: url(images/not_pass.gif) no-repeat left;
}
.topic_photo.not_pass:AFTER{
	display: none;;
}
.defoption_selector {
	display: none;
}

.defoption_selector .defoption_pic:HOVER:BEFORE {
	content: ' ';
	padding: 100px;
	background: url(images/check_blue_o.png) no-repeat;
	position: absolute;
	z-index: 5;
	margin: -10px 20px;
}

.defoption_selector .defoption_pic .entry {
	margin: 0;
	padding-left: 0;
	width: 530px;
	margin-right: -35px;
}

.connect-div SPAN:HOVER {
	background: url("images/connect-bg.gif");
}

SPAN.onshelf_box,SPAN.onshelf_box:HOVER {
	background: url("images/onshelf-icon.png") no-repeat 13px 13px;
}

#onshelf_btn {
	margin-left: 87px;
	height: auto;
	width: 150px;
	font-size: 18px;
	color: #3a3a3a;
	cursor: pointer;
	text-align: center;
	padding: 5px;
	border: none;
	border-radius: 5px;
}

#plus_all {
	border: none;
	height: auto;
	font-size: 16px;
	text-align: right;
	padding-right: 10px;
	cursor: pointer;
}

#onshelf_errormsg {
	float: right;
	color: #C9533D;
	padding-right: 10px;
	width: 150px;
	line-height: 16px;
	text-align: right;
}

#defoption_select {
	margin-left: 87px;
	text-align: right;
	height: auto;
	border: none;
	border-top: 1px solid #dbdbdb;
	bottom: 0px;
	width: 150px;
	border-radius: 5px;
	cursor: pointer;
}

#defoption_select:AFTER {
	content: ' ';
	padding: 18px;
	background: url("images/star_half_left.png") no-repeat left;
	position: relative;
	z-index: 5;
}

#plus_state LI {
	width: auto;
	padding: 3px;
	background: none;
	cursor: pointer;
	float: left;
	margin-right: 10px;
}

#plus_state LI:BEFORE,#plus_all:BEFORE {
	content: ' ';
	padding: 10px 0px 0px 25px;
}

#plus_state LI:HOVER:BEFORE {
	background: url(images/check_green.png) no-repeat left;
}

#plus_state LI.checked:BEFORE,#plus_all:BEFORE {
	background: url(images/check_blue.png) no-repeat left;
}
</style>

<input type="hidden" name="auto_index" value="{auto_index}">
<div class="plugin_box" style="position: fixed; right: 40px;">
	<div id="img_selectbox">
		<div class="fb_view_control">
			<span data-fb="fb_view">原始圖片</span> <span data-fb="fb_link">連結圖片(大)</span>
			<span data-fb="fb_link_s">連結圖片(小)</span> <span data-fb="close"
				style="float: right;">關閉</span>
		</div>
		<div class="fb_view"></div>
	</div>
</div>




<div class="fancybox-content">
	<div class="fancybox-content-bg-bottom">
		<div class="fancybox-header">
			<ul>				
				<?php if(!empty($onshelf_purview) && $onshelf_purview && $online != '1'):?>						
					<li class="active"><a >上架</a></li>
					<li ><a onclick="topic.add_edit({auto_index});">編輯</a></li>
				<?php else:?>
					<li class="active"><a >觀看</a></li>
				<?php endif;?>
					
			</ul>

			<br class="clear">

			<div class="fancybox-logo" style="color: #919191;">
				<div style="width: 120px;float: left;">
				<span data-title="題目圖片" 
					class="topic_photo audit_left <?php if(isset($fail_state['fail_title_p']) && $fail_state['fail_title_p']):?>not_pass<?php endif;?>"
					name="fail_title_p"> <img class="editable" alt=""
					src="{topic_imgurl}" width="100px;">
				</span>
				
				<span data-title="FACEBOOK" 
					class="topic_photo audit_left <?php if(isset($fail_state['fail_facebook_p']) && $fail_state['fail_facebook_p']):?>not_pass<?php endif;?>"
					name="fail_facebook_p"> 
					<img data-name="facebook_imgurl" class="editable" alt="" src="{facebook_imgurl}" width="100px;">
				</span>
				</div>
				<div style="float: left; width: 350px; margin-right: 10px;">
					<h2>
						<span
							class="editable audit_right <?php if(isset($fail_state['fail_title_t'])  && $fail_state['fail_title_t']):?>not_pass<?php endif;?>"
							data-editable="topic_{auto_index}" name="fail_title_t">{topic}</span>
					</h2>
					<span
						style="border-bottom: 1px solid; display: block; font-size: 16px;"
						class="editable audit_right <?php if(isset($fail_state['fail_select_t'])  && $fail_state['fail_select_t']):?>not_pass<?php endif;?>"
						data-editable="topic_optiontitle_{auto_index}"
						name="fail_select_t">{topic_optiontitle}</span>
					<div class="topic_group">
						<div id="topic_group_name">
							<ul>
								{topic_group_list}
								<li class="{checked}" data-index="{group_index}">{name}</li>
								{/topic_group_list}
							</ul>
						</div>
						<strong id="options_select_name">{options_select_name}</strong>
						<?php if(!empty($onshelf_purview) && $onshelf_purview && $online != '1'):?>								
								<ul class="options_select">			
									<?php foreach($topic_select AS $value):?>	
									<li onclick="group_editor(this);"
								data-index="<?php echo $value['auto_index'];?>"><?php echo $value['select_name']?></li>
									<?php endforeach;?>					
								</ul>	
						<?php endif;?>	
										
					</div>

				</div>
				<span data-title="指令圖片" 
					class="topic_photo audit_right <?php if(isset($fail_state['fail_select_p']) && $fail_state['fail_select_p']):?>not_pass<?php endif;?>"
					style="margin-top: 25px;" name="fail_select_p"> <img
					class="editable" alt=""
					src="{options_imgurl}" width="100px;"></span>
				<br class="clear">
			</div>
		</div>

		<div class="fancybox-inside">
			<div class="fancybox-inside-bg-top">
				<div class="fancybox-inside-bg-bottom">
					<div class="resume slide">

						<div class="topic_onshelf_control">
							<div class="page-content">
								<div class="entry connect-div"
									style="padding-top: 0px; margin-top: 23px;">

									<span class="onshelf_box"> <span id="onshelf_btn">上架 Onshelf</span>
										<div id="onshelf_errormsg"></div>
										
									<?php if($options_select_hide == 3 || ($options_select_hide == 2 && count($topic_options) == 12)):?>
									 <span id="defoption_select">星座套圖</span>
									<?php endif;?>
								</span> <span> <span id="plus_all">優</span>
										<ul id="plus_state">
											{statistics_state}
											<li class="checked" data-index="{st_AI}">{state_name}</li>
											{/statistics_state}
										</ul>
									</span>
								</div>
							</div>

						</div>
						<div class="defoption_selector">
							{defoption_select_list}
							<div class="page-content defoption_pic"
								data-index="{defselect_index}">
								<div class="entry ">
									{options} <span class="gallery-item"> <img
										src="../ci_gamesapp/uploads/temp/{pic_url}"
										alt="{option_name}" title="{option_name}" width="75px;">
									</span> {/options}
								</div>
								<h2>
									<span class="topic_photo"> <img alt=""
										src="../ci_gamesapp/uploads/temp/{select_pic}" width="100px;">
									</span>
								</h2>
							</div>
							{/defoption_select_list}
						</div>

						<div class="topic_option_list">
							{topic_options}
							<div class="page-content">
								<div class="entry audit_left {fail_options_t}"
									style="font-size: 14px;" name="fail_options{AI}_t">

									<strong style="color: #000; display: block;"> <span
										class="data_title" data-title="選項" data-type="title">{choose}</span>
									</strong> <strong style="color: #000; display: block;"> <span
										class="data_title" data-title="標題" data-type="title">{title}</span>
									</strong> <span class="data_title editable" data-title="內容"
										data-type="info">{text}</span>
								</div>
								<h2>
									<div class="gallery-item audit_left {fail_options_p}"
										name="fail_options{AI}_p">
										<img class="editable"
											src="{text_imgurl}" alt=""
											width="75px" />
									</div>
								</h2>
							</div>
							{/topic_options}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br class="clear">
</div>


<script>
//FB圖片預覽
$("DIV.fb_view_control SPAN").on('click', function() {
	if( $(this).attr('data-fb') == "close"){
		$('#img_selectbox').hide();	
		
	}else{		
		$("DIV.fb_view").attr('class','fb_view');	
		$("DIV.fb_view").addClass( $(this).attr('data-fb'));
	}
});
$("IMG.editable").on('click', function() {
	$("IMG.editable").removeClass("focus");
	$(this).addClass("focus");
	
	$('#img_selectbox').show(500);
	 
	$('#img_selectbox .fb_view').attr('class','fb_view').css('background', 'url("'+$(this).attr("src")+'") center center no-repeat');
});

<?php if($onshelf_purview):?>

$(".topic_group #options_select_name").on("click",function(){
	if($(this).parent().hasClass("editable")) $(this).parent().removeClass("editable")
	else $(this).parent().addClass("editable");	
});

$(".audit_left, .audit_right").on("click",function(){
	if($(this).hasClass("not_pass"))  $(this).removeClass("not_pass");
	else $(this).addClass("not_pass");
	audit();
});

$("#defoption_select").on("click",function(){
	if($(".defoption_selector").is(':hidden') ) $(".defoption_selector").show();
	else $(".defoption_selector").hide();		
});


$("#plus_all").on("click",function(){
	$("#plus_state LI").removeClass("checked").addClass("checked");	
});
$("#plus_state LI").on("click",function(){
	if($(this).hasClass("checked"))  $(this).removeClass("checked");
	else $(this).addClass("checked");
});

$("#topic_group_name UL LI").on("click",function(){
	if($(this).hasClass("checked"))  $(this).removeClass("checked");
	else $(this).addClass("checked");

	var topic_index = $('INPUT[name="auto_index"]').val();
	if(topic_index<1) return false;
	
	var group_list = new Array();
	$('#topic_group_name UL LI.checked').each(function(){			
			group_list.push($(this).attr('data-index'));
		});

	$.ajax({
		  url: 'topic/group_topic/',
		  dataType: 'html',
		  type: "POST",
		  data: {
			  group_list:group_list,
			  index:topic_index
		  },
		  success: function(data){
			  if(data) alert(data);
		  	/**/
		  }
		});
});

$("#onshelf_btn").on("click",function(){
	if($("#onshelf_errormsg").html() == '上架處理中') return false;
	$("#onshelf_errormsg").html('');
	//檢查分類
	var group = $("#topic_group_name UL LI.checked").length;	

	//檢查錯誤
	var audit_check = $('.audit_left.not_pass, .audit_right.not_pass').length;

	var topic_index = $('INPUT[name="auto_index"]').val();

	if(group < 1) $("#onshelf_errormsg").append("至少需要一種分類<br/>");
	if(audit_check) $("#onshelf_errormsg").append("尚有錯誤，未修正");
	else if(group > 0 && topic_index > 0){
		$("#onshelf_errormsg").html('上架處理中');
		
		var plus_state = new Array();
		$('#plus_state LI.checked').each(function(){			
			plus_state.push($(this).attr('data-index'));
		});

		
		$.ajax({
			  url: 'topic/actions/validate_onshelf',
			  cache: false,
			  dataType: 'json',
			  type: "POST",
			  data: {
				  auto_index:topic_index,
				  plus_state:plus_state			
			  },
			  success: function(data){		
					
			  	if(data.success == 'Y' ){
			  		alert(data.msg);
			  		location.reload();
			  	}else{
			  		alert(data.msg);				  		
			  		return false;
			  	}
			  	/**/
			  },
			  error:function(XHR, textStatus){
					console.info(textStatus);
			  }
			});
			//form.submit();
			return false;

	}

	
});

$(".defoption_pic").on("click",function(){
	var topic_index = $('INPUT[name="auto_index"]').val();
	
	var index = $(this).attr("data-index");
	var selected_el = this;
	if(index < 1 || topic_index<1) return false;
	
	$.ajax({
		  url: 'topic/defoption_select/',
		  dataType: 'json',
		  type: "POST",
		  data: {			  
			  index:index,
			  topic_index:topic_index
		  },
		  success: function(data){
			  console.info(data);
			  set_defoption_pic(data);
			  
			  $(".defoption_selector").hide();
			  $(".defoption_selector .defoption_pic").removeClass("selected");
			  $(selected_el).addClass("selected");
		  	/**/
		  }
		});	
	
});

function set_defoption_pic(data){


	var select_pic = data['select_pic'];
	
	$('SPAN[name="fail_select_p"] IMG').attr("src","../ci_gamesapp/uploads/temp/"+select_pic);

	
	$(".topic_option_list .page-content").each(function(index,dom){		

		$(dom).find("H2 DIV.gallery-item IMG").attr("src","../ci_gamesapp/uploads/temp/"+data['options'][index].pic_url);
	});
	
}
function audit(){
	
	var topic_index = $('INPUT[name="auto_index"]').val();
	if(topic_index<1) return false;
	
	var audit_list = new Array();
	$('.audit_left.not_pass, .audit_right.not_pass').each(function(){
			eval('audit_list.push({"'+$(this).attr('name')+'":"true"});');	
			
		});

	
	$.ajax({
		  url: 'topic/audit_topic/',
		  dataType: 'html',
		  type: "POST",
		  data: {
			  audit_list:audit_list,
			  index:topic_index
		  },
		  success: function(data){
			  console.info(data);
		  	/**/
		  }
		});
	
	
}

function group_editor(el){	
	var topic_index = $('INPUT[name="auto_index"]').val();
	var select_index = $(el).attr("data-index");

	
	$.ajax({
		  url: 'topic/change_options_select/',
		  dataType: 'json',
		  type: "POST",
		  data: {
			  topic_index:topic_index,
			  select_index:select_index
		  },
		  success: function(data){
			  if(data) {
				alert(data);
			  }else{
				  $("#options_select_name").html($(el).html());
				  
			  }
			  $(".topic_group.editable").removeClass("editable");
		  }
		});
	
}

<?php endif;?>
</script>