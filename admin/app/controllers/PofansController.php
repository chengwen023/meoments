<?php
class PofansController extends BaseController
{

	public function initialize()
	{
	
		if ($this->request->isPost()) {	
			$this->order_by = $this->request->getPost('order_by');	
			$this->topSearch = $this->request->getPost('topSearch');
		}
	
		
	}
	
	
	
	function fanslistAction(){
		$this->action = 'fans';
		$this->fansAction();	
	}
	
	function fansjsonAction(){
		echo $this->fansAction('json');
		exit;
	}
	
	function get_fans_groupAction(){
		if ($this->request->isPost()) {
		
			$index = $this->request->getPost('index');
			$content = PofansFanslist::findFirst("page_id = '".$index."'");
			
			if($content != false) {
				$content = $content->toArray();
				echo $content['fans_group'];
			}
			
		}else{
			echo false;
		}
		
		
	}
	
	function set_fans_groupAction(){
	
		if ($this->request->isPost()) {
		
			echo $index = $this->request->getPost('index');
			$fans_group = $this->request->getPost('fans_group');
			
			$content = PofansFanslist::findFirst("page_id = '".$index."'");
			if($PofansFanslist != false)
			{
				$content->fans_group = json_encode($fans_group);
				$content->update();
			}
		}
		
	
	}
	
	function publishedAction($viewType = false){
		$orderBy = $this->order_by($this->order_by,'PofansPublished');
	
		$content = PofansPublished::find();
	
		if($content != false) $content = $content->toArray();
	
		$viewContent['content'] = $content;
		if($viewType == 'json') return json_encode($viewContent,true);
	
		$view = new contentView();
		$main_content = $view->getRender($this->controller, $this->action,$viewContent);
	
		$this->template = array(
				"main_content" => $main_content,
		);
	
	}
	
	public function indexAction()
	{	
		$this->action = 'list';
		$this->listAction();
		return;
	}
	
	public function listAction($viewType = false)
	{
		$orderBy = $this->order_by($this->order_by,'ArticleOnshelf');
	
	
		$content = ArticleOnshelf::find();
	
		$content->rewind();
		while ($content->valid()) {
			$Article = $content->current();
			$ArticleItem = $Article->toArray();
			$ArticleItem['id'] = $Article->get_AdminAccount_field('id');
			$ArticleItem['edit_control'] = ($ArticleItem['online'])?'hide':'';
			$ArticleItem['article_group']= implode(',', array_column(ArticleGroup::find(array(
					" auto_index in (".implode(',', json_decode($ArticleItem['article_group'])).") AND online = '1'",
			))->toArray(), 'name'));
	
			$ArticleList[] = $ArticleItem;
			$content->next();
		}
	
	
	
		$viewContent['content'] = $ArticleList;
		if($viewType == 'json') return json_encode($viewContent,true);
	
		$view = new contentView();
		$main_content = $view->getRender($this->controller, $this->action,$viewContent);
	
		$this->template = array(
				"main_content" => $main_content,
		);
			
	}
	
	function fansAction($viewType = false){
	
		self::$topbar_control[] = array('current_url'=>'JAVASCRIPT:fans.refresh();','i_class'=>'icon-refresh', 'control_name'=>'重載資料');
		self::$topbar_control[] = array('current_url'=>'JAVASCRIPT:fans.add();','i_class'=>'icon-plus', 'control_name'=>'新增粉絲團');
		
		$where = $this->topSearchSql(array('page_id','page_name'));
		
		$content = PofansFanslist::find($where);
		
		if($content != false) $content = $content->toArray();
		
		$viewContent['content'] = $content;
		$viewContent['ArticleGroup'] = ArticleGroup::find()->toArray();
		
		if($viewType == 'json') return json_encode($viewContent['content'],true);
		
		$view = new contentView('./app/views');
		$main_content = $view->getRender($this->controller, $this->action,$viewContent);
	
	
		$this->template = array(
				"main_content" => $main_content,
		);
	
	}
	
	function ikuso_fansAction($viewType = false){
		self::$topbar_control[] = array('current_url'=>'JAVASCRIPT:fans.refresh();','i_class'=>'icon-refresh', 'control_name'=>'重載資料');
		
		
		$where = $this->topSearchSql(array('fans_id','fans_name'));
		$content = IkusoFansid::find($where);
		
		if($content != false) $content = $content->toArray();		
		
		$viewContent['content'] = $content;
		if($viewType == 'json') return json_encode($viewContent['content'],true);
		$view = new contentView('./app/views');
		$main_content = $view->getRender($this->controller, $this->action,$viewContent);
		$this->template = array(
				"main_content" => $main_content,
		);
	}
	
}
?>