<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *	Adonis
 *	後台預設資料讀取
 */

class Article_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
		
		
	}
	
	
	function article_group($index = '',$field='*',$select_type = true){
		$this->db->flush_cache();
		$this->db->select($field,$select_type)->from('article_group')->where('online','1');
		if($index){
			$this->db->where('auto_index',$index);
		}
	
		$query = $this->db->get();
		$data = $query->result_array();
	
		if($index && is_array($data)) 	$data = array_pop($data);
		return $data;
	}
	function check_group_exist_and_insert($add_input){
		$result = array('success' => 'N', 'msg' => '');
	
		$this->db->flush_cache();
		$query = $this->db->select("COUNT(*) AS qun")
		->from("topic_group")
		->where("name", $add_input['name'])
		->where("online", '1')
		->get();
		$temp = $query->row_array();
	
		if($temp['qun'] == 0){
	
			$this->db->insert('topic_group', $add_input);
			$result['success'] = 'Y';
			$result['msg'] = '新增成功';
		}else{
			$result['msg'] = '此名稱已經有人用了哦!!!';
		}
		return $result;
	}
	
	function check_group_exist_and_update($add_input,$index){
		$result = array('success' => 'N', 'msg' => '');
	
		$this->db->flush_cache();
		$query = $this->db->select("COUNT(*) AS qun")
		->from("topic_group")
		->where("auto_index", $index)
		->where("online", '1')
		->get();
		$temp = $query->row_array();
	
		if($temp['qun'] == 0){
			$result['msg'] = '無該筆資料';
		}else{
	
			$this->db->where('auto_index', $index);
			$this->db->update('topic_group', $add_input);
			$result['success'] = 'Y';
			$result['msg'] = '修改成功';
		}
		return $result;
	}
	
	
	
	function get_article_defoption($index = '',$field='*',$select_type = true){
	
		$list = $this->article_defoption_group($index,$field,$select_type);		
		foreach($list AS $key => $v){
			$list[$key]['items'] = $this->article_defoption_By_group($v['auto_index']);			
		}
		return $list;
	}
	
	function article_defoption_group($index = '',$field='*',$select_type = true){
	
	
		$this->db->flush_cache();
		$this->db->select($field,$select_type)->from('article_defoption_group')->where('online','1');
		if($index){
			$this->db->where('auto_index',$index);
		}
	
		$query = $this->db->get();
		$data = $query->result_array();
	
		if($index && is_array($data)) 	$data = array_pop($data);
		return $data;
	}
	
	function article_defoption_By_group($index = '',$field='*',$select_type = true){
	
		$this->db->flush_cache();
		$this->db->select($field,$select_type)->from('article_defoptions')->where('online','1');
		if($index){
			$this->db->where('defoptions_group_index',$index);
		}
	
		$query = $this->db->get();
		$data = $query->result_array();
	
		
		return $data;
	}
	
	
	
	function remove_check_photo($img){
		if( is_file($img) === false ) return 'no file';
		else{
			unlink($img);
		}
	}
	
	function move_article_photo($img,$newUrl,$newDir){
		//是否建立目錄
		if(false !== $newDir && false === is_dir( $newDir ) ) {
			mkdir($newDir);
			chmod($newDir, 0777);
		}
	
		$filename = $newDir.$newUrl;
	
		//}else if(preg_match("/^data:image\/(".pathinfo($filename, PATHINFO_EXTENSION).");base64,/", $img)){
		if(preg_match("/^data:image\/(png|jpeg|gif);base64,/", $img)){
			//delet前置data
	
			$img = preg_replace("/^data:image\/(png|jpeg|gif);base64,/", '', $img);
	
	
			$img = str_replace(' ', '+', $img);
			//轉換base64 data
			$data = base64_decode($img);
			file_put_contents($filename, $data);
		}else{
			file_put_contents($filename, file_get_contents($img));
		}
	
		if( is_file($filename) === false ) return 'no file';
	
		return $newUrl;
	}
	/**
	 *	後台帳號資料讀取
	 */
	
	function check_article_exist_and_update($add_input,$add_options,$index){
		$result = array('success' => 'N', 'msg' => '');
		
		$this->db->flush_cache();
		$query = $this->db->select("COUNT(*) AS qun")
		->from("article")
		->where("article_title", $add_input['article_title'])
		->get();
		$temp = $query->row_array();
	
		if($temp['qun'] == 0){
			$result['msg'] = '無該筆資料';
		}else{
	
			$this->db->where('auto_index', $index);
			$this->db->update('article', $add_input);
				
			//重新建立選項
			$article_index = $this->db->insert_id();
			
			if($article_index && is_array($add_options)){
				$this->db->delete('article_options', array('select_index' => $article_index));
				foreach($add_options AS $key => $title){
					if($title){
						$add_option = array('choose'=>$title['choose'],'title'=>$title['title'],'text_imgurl'=>$title['text_imgurl'],'article_index'=>$article_index);
						$this->db->insert('article_options', $add_option);
					}
				}
			}
				
			$result['success'] = 'Y';
			$result['index'] = $index;
		}
		return $result;
	}
	
	function check_article_exist_and_insert($add_input,$add_options){
		$result = array('success' => 'N', 'msg' => '');
	
		$this->db->flush_cache();
		$query = $this->db->select("COUNT(*) AS qun")
		->from("article")
		->where("article_title", $add_input['article_title'])
		->get();
		$temp = $query->row_array();
	
		if($temp['qun'] == 0){
		
			$this->db->insert('article', $add_input);
			
			//重新建立選項
			$article_index = $this->db->insert_id();
			
			if($article_index && is_array($add_options)){
				$this->db->delete('article_options', array('select_index' => $article_index));
				foreach($add_options AS $key => $title){
					if($title){
						$add_option = array('choose'=>$title['choose'],'title'=>$title['title'],'text_imgurl'=>$title['text_imgurl'],'article_index'=>$article_index);
						$this->db->insert('article_options', $add_option);
					}
				}
			}
				
				
				
				
			$result['success'] = 'Y';
			$result['index'] = $article_index;
		}else{
			$result['msg'] = '此題目已經有了哦!!!';
		}
		return $result;
	}
	
	function check_article_from_index($add_input){
		$result = array('success' => 'N', 'msg' => '');
	
		$this->db->flush_cache();
		$query = $this->db->select("COUNT(*) AS qun")
		->from('article_from_index')
		->where("article_index", $add_input['article_index'])
		->get();
		$temp = $query->row_array();
	
		if($temp['qun']<1){		
			$this->db->delete('article_from_index', array('article_index' => $add_input['article_index']));			
		}
		$this->db->insert('article_from_index', $add_input);
		
	}
	
	function del_article_by_index($index){
		$this->db->flush_cache();
		$this->db->delete('article', array('auto_index' => $index));
		$this->db->delete('article_options', array('article_index' => $index));
		
		$this->db->delete('article_from_index', array('article_index' => $index));
		
		$tmp = array('success' => 'Y', 'msg' => '已刪除', 'index' => $index);
		return $tmp;
		
	}
	
	function onshelf_article_by_index($index){
	
		$result = array('success' => 'N', 'msg' => '');
	
		$this->db->flush_cache();
		$query = $this->db->select("*")
		->from("article")
		->where("auto_index", $index)
		->where("online", '0')
		->get();
		$temp = $query->result_array();
		$data = array_pop($temp);
	
		if(!is_array($data)){
			$result['msg'] = '無此資料';
				
		}else{
			$date = date("Y-m-d H:i:s");
			$account_id = $this->config->item('admin_accountIndex');
				
			$topic_shelf = array();
			
			
				
			
				
			/**/
				
			//轉至上架資料表
				
			$topic_shelf['article_title'] = $data['article_title'];
			$topic_shelf['article_optiontitle'] = $data['article_optiontitle'];
			$topic_shelf['article_imgurl'] = $data['article_imgurl'];
			$topic_shelf['facebook_imgurl'] = $data['facebook_imgurl'];
			$topic_shelf['article_group'] = $data['article_group'];
			$topic_shelf['article_text'] = $data['article_text'];
			$topic_shelf['article_defoption'] = $data['article_defoption'];
			$topic_shelf['article_fromindex'] = $data['auto_index'];
			$topic_shelf['online'] = '1';
			$topic_shelf['create_time'] = $date;
			$topic_shelf['create_account_index'] = $account_id;
			$topic_shelf['update_time'] = $date;
			$topic_shelf['update_account_index'] = $account_id;
				
			$this->db->insert('article_onshelf', $topic_shelf);				
			$onshelf_index = $this->db->insert_id();
			
			$newUrl = 't.jpg';
			$newDir= '../uploads/q'.$onshelf_index.'/';
			
			$newFile = $this->move_article_photo( $data['article_imgurl'], $newUrl, $newDir);
			$newUrl2 = 't_m.jpg';
			$newFile2 = $this->move_article_photo( $data['article_imgurl'], $newUrl2, $newDir);
				
			if( $newFile != 'no file'){
				$this->db->flush_cache();
				$article = array("article_imgurl"=>$newFile);
				$this->db->where('auto_index', $onshelf_index);
				$this->db->update('article_onshelf', $article);
				
			}
			
				
				
			//設定為上架狀態
			$this->db->flush_cache();
			$topic = array("online"=>'1');
			$this->db->where('auto_index', $index);
			$this->db->update('article', $topic);
			$result['success'] = 'Y';
			$result['msg'] = '上架完成';
		}
		return $result;
	
	}
}