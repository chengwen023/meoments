<!DOCTYPE html>
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<base href="http://admin.starlove99.com/" />
<title>PhaIcon後台管理</title>


<link rel="Shortcut Icon" type="image/x-icon" href="favicon.ico">
<link href="css/style.default.css" rel="stylesheet" type="text/css">
<link href="css/style.dark.css" rel="stylesheet" type="text/css">
<link href="css/styles.css" rel="stylesheet" type="text/css">
<link href="css/admin.css" rel="stylesheet" type="text/css">




<!--[if IE]> <link href="css/ie.css" rel="stylesheet" type="text/css"> <![endif]-->

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
<script type="text/javascript" src="http://www.pureexample.com/js/lib/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="js/libs/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/libs/jquery.dragsort.js"></script>
<script type="text/javascript" src="js/libs/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="js/libs/fancybox/jquery.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="js/libs/fancybox/jquery.fancybox.css" media="screen" />
<link href="css/fancybox-style.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="js/angular.min.js"></script>
</head>


<body>

<!-- Top line begins -->
<div id="top">
    <div class="wrapper">
        <a href="index.html" title="" class="logo"><img src="../favicon.ico" alt="" width="28px"></a>
        
        <!-- Right top nav -->
        <div class="topNav">
            <ul class="userNav">
                <li><a title="搜尋" class="search animate0"></a></li>
                <li><a title="" class="screen animate0"></a></li>
                <li><a title="欄位顯示" class="settings animate0"></a></li>
                <li><a title="登出" href="login" title="" class="logout animate0"></a></li>
                
            </ul>            
            <div class="topSearch topSearchBox">
                <div class="topDropArrow"></div>
                
					<form action="" method="post">
                    <input type="text" placeholder="search..." name="topSearch" style="height: 28px;">
                    <input type="submit" value="">
                    </form>
            </div>
            <div class="topSearch topSettings">
                <div class="topDropArrow"></div>
                <UL></UL>
            </div>
        </div>
        
    </div>
</div>
<!-- Top line ends -->


<!-- Sidebar begins -->
<div id="sidebar" class="with">
    <div class="mainNav">
    	<!-- Tabs leftmenu -->
		 <div class="leftmenu">        
            <ul class="nav nav-tabs nav-stacked">
            	<li class="nav-header animate0 fadeInUp">後台管理</li>                
                 <?php foreach ($menu as $item) { ?>                 
                <li class="dropdown  fadeInUp"><a href="#"><i class="icon-list"></i><span><?php echo $item['group_name']; ?></span></a>
                	<ul>                	
                	<?php foreach ($item['child_list'] as $child_list) { ?>
                    	<li class="animate<?php echo $child_list['child_index']; ?> flipInX {add_class}">
                    		<a href="<?php echo $child_list['phaicon_controller']; ?>/<?php echo $child_list['phaicon_action']; ?>"><?php echo $child_list['child_name']; ?></a>
                    	</li>
                      <?php } ?>
                    </ul>
                </li>
               <?php } ?>
            </ul>
        </div>
       
    </div>
   
</div>
<!-- Sidebar ends -->
    
    
<!-- Content begins -->
<div id="content">

	    <div class="contentTop">
	        <span class="pageTitle"><i class="icon-picture"></i><?php echo $title; ?></span>
	        <ul class="quickStats">
	            <li>
	                <a href="" class="blueImg"><i class="icon-picture" style="margin: 8px auto"></i></a>
	                <div class="floatR"><strong class="blue">5489</strong><span>上架</span></div>
	            </li>
	            <li>
	                <a href="" class="redImg"><i class="icon-picture" style="margin: 8px auto"></i></a>
	                <div class="floatR"><strong class="blue">4658</strong><span>未上架</span></div>
	            </li>
	            <li>
	                <a href="" class="greenImg"><i class="icon-picture" style="margin: 8px auto"></i></a>
	                <div class="floatR"><strong class="blue">1289</strong><span>總數</span></div>
	            </li>
	        </ul>
	    </div>
	    
	    <!-- Breadcrumbs line -->
	    
	    <div class="breadLine">
	        <div class="bc">
	        	<ul class="breadcrumbs">
	        	<?php foreach ($breadcrumbs as $item) { ?>     
	                <li><a href="#"><?php echo $item['group_name']; ?></a></li>                
	                <li class="current"><a href="#" title=""><?php echo $item['child_name']; ?></a></li>
	            <?php } ?>
	            </ul>
	        </div>
	        
	        <div class="breadLinks">
	            <ul>
	            	<?php foreach ($topbar_control as $item) { ?>
	                <li><a title="" href="<?php echo $item['current_url']; ?>"><i class="<?php echo $item['i_class']; ?>"></i><span><?php echo $item['control_name']; ?></span></a></li>
	                <?php } ?>
	            
	            	<?php foreach ($select_result as $item) { ?>
	                <li class="has">
	                    <a title="" href="<?php echo $item['current_url']; ?> "><i class="icon-list"></i><span><?php echo $item['select_name']; ?></span></a>
	                    <ul>
	                    <?php foreach ($select_option as $item) { ?>
	                        <li><a href="<?php echo $item['current_url']; ?><?php echo $item['value']; ?>" title=""><i class="icon-user"></i><?php echo $item['label']; ?></a></li>
	                    <?php } ?>   
	                    </ul>
	                </li>
	               <?php } ?>
	            </ul>
	        </div>
	    </div>
    
    <!-- Main content -->
    <div class="wrapper"><?php echo $main_content; ?></div>
    <!-- Main content ends -->
</div>
<!-- Content ends -->
<script>

$('thead tr').find('TD').each(function(index){
	var title=$(this).attr("title");
	if(typeof(title) == "undefined") title=$(this).html();
	var add_li = document.createElement('LI');
	add_li.innerHTML=title;
	
	
	if(typeof(title) != "undefined") {
		$('.topSearch.topSettings UL').append(add_li);
		if($(this).hasClass("rowhide")) $(add_li).addClass("rowhide");		
	}
	
    if($("td:nth-child("+(index+1)+")").hasClass("rowhide"))
		{$("td:nth-child("+(index+1)+")").hide();  }	
});

$('.topSearch.topSearchBox INPUT[name="topSearch"]').on('keyup', function() {
	var re = new RegExp($(this).val());

	$('tbody tr').each(function(){
		var reg_tr = $(this);		
		reg_tr.find("TD").each(function(){
			var reg_td = $(this).html();
			reg_tr.hide();
			if (reg_td.match(re)) {
				reg_tr.show();
				return false;		  
			}				
		});		
	});
  });

$('DIV.topSearch.topSettings').hover(
		function(){},
		function(){	$(this).hide();});

$('.userNav LI A').hover(
		function(){	$(this).addClass('swing');},
		function(){	$(this).removeClass('swing');});

$(".topNav .userNav LI A.settings").on('click', function() {		
	   if($('.topSearch.topSettings').is(':visible'))
	    	{  $(".topSearch.topSettings").hide();  }
	    else 
	    	{  $(".topSearch").hide();$(".topSearch.topSettings").show();  } 
	   });

$(".topNav .userNav LI A.search").on('click', function() {
	   if($('.topSearch.topSearchBox').is(':visible'))
	    	{  $(".topSearch.topSearchBox").hide();  }
	    else 
	    	{  $(".topSearch").hide();$(".topSearch.topSearchBox").show().find('INPUT[name="topSearch"]').focus();  } 
	   });

var items = $('.topSearch.topSettings UL li').click(function() {
    var index = items.index(this);
    
    if($("td:nth-child("+(index+1)+")").is(':visible'))
	{ $(this).addClass("rowhide"); $("td:nth-child("+(index+1)+")").hide();  }
	else 
	{ $(this).removeClass("rowhide"); $("td:nth-child("+(index+1)+")").show();  } 
});
</script>

</body>
</html>