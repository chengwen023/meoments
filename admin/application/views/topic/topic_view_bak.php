
<style>
TABLE thead:BEFORE{
	content: '';
	position: absolute;
	right: 0;
	padding:7px;
	background-image: url("images/glyphicons-halflings.png");
	background-position: -288px -120px;
	background-repeat: no-repeat;
}

TABLE.close thead:BEFORE{	
	background-position: -313px -119px;
}
.img_control{
	border:5px solid red;
}
#img_selectbox{
	display:none;	
	width:550px;
	height:420px;
	text-align: center;
	background-color: #94b0dc;
}
.fb_view_control{
	background-color: #336699;
	color:#fff;
	height: 20px;
}
.fb_view_control SPAN{
	margin-right: 10px;
	padding: 2px 5px;
}
.fb_view_control SPAN:HOVER{	
	
	cursor: pointer;
	color:#3366cc;
	background-color: #fff;
}

.fb_view{	
	width:500px;
	height: 375px;
	margin: auto;
	border: 1px solid red;
}

.fb_link{	
	width:500px;
	height: 260px;
}
.fb_link_s{	
	width:375px;
	height: 375px;
}

IMG.editable{
	background-color: #94b0dc;
}

#choose_list LI INPUT[type="text"],#choose_list LI TEXTAREA{
	width:350px;
}
#choose_list LI TEXTAREA{
	height:80px;
}

#choose_list LI{
	clear:both;
	
	font-size: 24px;
	list-style-position: inside;	
}

#choose_list LI DIV:not([class="imgurl_box"], [class="imgurl_box imgedit_focus"]){
	width:400px;
	
}
#choose_list LI .list_info{
	float: right;
	display: inline-block;
}
#choose_list LI .list_info > DIV{
	float: right;
	font-size: 14px;
}

.del_btn{
	margin: 5px 20px;
	
	position: relative;
	cursor: pointer;
	text-align: center;
	width:80px;	
	border: 1px solid #D17A1C; 
	color:#D17A1C;
}


#choose_list LI DIV.del_btn{
	float:none;
	margin-left: 415px;
	margin-bottom: 10px;
}
.del_btn:HOVER{
	background-color:#D17A1C;
	color: #fff;
}


#choose_list LI DIV SPAN.data_title:before{
	content:attr(data-title);
	color:#D17A1C;
	line-height: 25px;
	margin-right: 10px;
}
#choose_list LI DIV SPAN.data_title:first-child{
	background: rgba(255,153,102,0.5);
	color: #993300;
	border-radius: 0 10px 10px 0;
}


#choose_list LI DIV SPAN.data_title{
	display: block;
	margin-right: 20px;
	border-bottom: 1px solid #666;
}


.fb_img_box{
	overflow: hidden;
	height: 375px;
}
.fb_img_box:AFTER {
	content: '';
	padding: 190px 25px;
	background-color: rgba(255,0,255,0.3);
	position: relative;
	left: 250px;
	top: -50%;
}
.fb_img_box:BEFORE {
	content: '';
	padding: 50% 25px;
	background-color: rgba(255,0,255,0.3);
	position: relative;
	left: 50px;
}
.imgurl_box{
	width:80px;
	height: 60px;
	border-style: dotted;
	text-align: center;
	line-height: 60px;	
	cursor: pointer;
	margin-left: 20px;
	
}

.imgurl_box.imgedit_focus{
	color:#D17A1C;
}

.submit_btn{	
	color: #66a3d3;	
	border-color: #66a3d3;
}
.submit_btn:HOVER{	
	color: #fff;
	background-color: #678197;
	border-color: #678197;
}
</style>
		
	<div id="topic_info" style="width:auto;">
		<form id="form_folder" onsubmit="return false">
		
		
		<div class="tb-green" style="width:650px;height:700px;float: right;">
		
			<table class="tLight" style="width:100%;">
				<thead>
					<tr class="column1">
						<td scope="col" colspan="3">
							觀看資料
							<input type="hidden" name="auto_index" value="{auto_index}">												
						</td>
					</tr>
				</thead>
				<tr>
					<td>題目</td>
					<td>{topic_title}</td>					
					<td>
						<div class="imgurl_box" ><span>
							<img  data-width="500" data-height="375" src="../uploads/q{topic_fromindex}/t{topic_img}" width="80px" />
							</span>
						</div>
					</td>
				</tr>				
				<tr>
					<td>
						分類
					</td>
					<td colspan="2">
						<ul style="list-style: none;">
						{topic_group_name}
							<li style="float: left;margin-right: 10px;">{name}</li>
						{/topic_group_name}
						</ul>
					</td>
				</tr>
				<tr>
					<td>
						選項類型
					</td>
					<td >
						<span style="padding: 3px 20px;border-bottom: 1px solid #394A58;">
							{options_select_name}
							</span>
						<div>							
							{topic_optiontitle}
						</div>
					</td>
					<td>
						<div id="options_imgurlbox" class="imgurl_box" >
							<span>
							<img  data-width="500" data-height="375" src="../uploads/q{topic_fromindex}/s{select_img}" width="80px" />
							</span>
						</div>
						
					</td>
				</tr>				
			</table>
			
			<table class="tLight" style="width:100%;">
				<thead>
					<tr class="column1">
						<td scope="col" >
							題目選項	
						</td>
					</tr>
				</thead>
				<tr>
					<td style="text-align: right;">
						<span class="list_sorting">排序</span>
						<span class="list_editing">編輯</span>						
					</td>
				</tr>
				<tr>
					<td width="66">					
							<ol id="choose_list">
								{topic_options}
									<li>
										<div class="list_info">
											<div class="imgurl_box" data-type="title"><span><img  data-width="500" data-height="375" src="../uploads/q{topic_fromindex}/{optionImg}" width="80px" cache="no-cache"/></span></div>
											<div style="width:460px;margin-bottom: 20px;">
												<span class="data_title" data-title="選項" data-type="title">{choose}</span>
												<span class="data_title" data-title="標題" data-type="title">{title}</span>
												<span class="data_title editable" data-title="內容" data-type="info">{text}</span>										
											</div>
										</div>
									</li>
								{/topic_options}
							</ol>							
						</td>						
					</tr>				
				</table>
			</div>
			<div class="plugin_box" style="position: fixed;right: 40px;">
				<div id="img_selectbox" >
					<div class="fb_view_control">
						<span data-fb="fb_view">原始圖片</span>				
						<span data-fb="fb_link">連結圖片(大)</span>
						<span data-fb="fb_link_s">連結圖片(小)</span>
						<span data-fb="close" style="float:right;">關閉</span>
					</div>
					<div class="fb_view"></div>
				</div>
			</div>	
		</form>
	</div><!-- / tb-green-->
		
<script src="js/img_editor.js"></script>
<script>

	
//	$("#choose_list").dragsort("destroy");


$(document).ready(function() {
	/**/
	$("DIV.tb-green TABLE THEAD").on('click', function() {	
		if($(this).parent().hasClass("close")){		
			$(this).parent().removeClass("close");
			$(this).parent().children("tbody").show();
		}else{
			$(this).parent().addClass("close");
			$(this).parent().children("tbody").hide();
		}
		
	});

	$("SPAN.data_title.editable").on('click', function() {
		CKEDITOR.replace(this,{ width:'480px' });
	});	


	$('.list_sorting').on('click', function() {
		for ( instance in CKEDITOR.instances ){
	         CKEDITOR.instances[instance].destroy();
		}
		$("SPAN.data_title.editable").hide();	
		
		$("#choose_list").dragsort("destroy");
		$("#choose_list").dragsort();
	});

	$('.list_editing').on('click', function() {
		$("#choose_list").dragsort("destroy");
		$("SPAN.data_title.editable").show();
	});
	/**/
// 	$("#choose_list").dragsort();
// 	$("#choose_list").dragsort("destroy");

	
	$("DIV.fb_view_control SPAN").on('click', function() {
		if( $(this).attr('data-fb') == "close"){
			$('#img_selectbox').hide();	
			
		}else{		
			$("DIV.fb_view").attr('class','fb_view');	
			$("DIV.fb_view").addClass( $(this).attr('data-fb'));
		}
	});

	$("IMG.editable").on('click', function() {
		$("IMG.editable").removeClass("focus");
		$(this).addClass("focus");
		
		$('#img_selectbox').show(500);
		 
		$('#img_selectbox .fb_view').attr('class','fb_view').css('background', 'url("'+$(this).attr("src")+'") center center no-repeat');
	});

	
	


});	
</script>