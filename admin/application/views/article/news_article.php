<style>
#tips_box{
	position: fixed;
	font-size: 16px;
	display: none;
	padding: 5px 10px;
	background: #fff;
	box-shadow: 1px 1px 2px 1px rgba(33,66,99,.5);
	border-radius:5px;
	z-index: 10000;
}

.online:not(.off) A:not(:first-child ) {
	display: none;
}

.online.off A:first-child {
	display: none;
}

.article_online:not(.off) A.edit {
	display: none;
}

[class^="icon-"] {
	margin-right: 0px;
}

.google_ad_1 {
	cursor: pointer;
	background: url(images/google_online.png) right bottom no-repeat;
}

.google_ad_0 {
	cursor: pointer;
	opacity: 0.4;
	background: url(images/google_offline.png) right bottom no-repeat;
}

.change_article_id:not(.off) {
	display: none;
}

.change_article_id_2,
.change_article_id_3 {
	display: none;
}

</style>

<div class="widget adonis">
	<form id="list_orderby" method="post">
		<input id="order_by" type="hidden" name="order_by">
		<input id="fans_selected" type="hidden" name="fans_selected" value="{fans}">
		<input id="days_selected" type="hidden" name="days_selected" value="{days}">
		作者：<select name="fans">
		{fans_list}
	　		<option id="{auto_index}" value="{auto_index}">{id}</option>
		{/fans_list}
		</select>
		
		&nbsp;日期：
		<select name="days">
		{days_list}
	　		<option id="{day}" value="{day}">{day}</option>
		{/days_list}
		</select>

		&nbsp;<input type="submit" value="查詢">
	</form>

	<form id="list_orderby" method="post">
		
		編號：<input id="article_id" name="article_id" style="width:60px; height:30px; background:rgb(238,233,233);">
		&nbsp;<input type="submit" value="查詢">
	</form>

	<table cellpadding="0" cellspacing="0" width="100%" class="tDefault tLight">
		<thead>
			<tr >
				<td title="編號" tips ="使用滑鼠點選 可以排序該欄位" class="order_control" data-orderby = "id">編號<i class="icon-chevron-{by-id}"></i></td>
				<td title="標題">標題</td>
				<td title="封面圖片">封面圖片</td>
				<td title="建立時間" class="order_control" data-orderby="create_time">建立時間<i class="icon-chevron-{by-create_time}"></i></td>
				<td title="分類">分類</td>
				<td title="建立者" class="order_control" data-orderby="create_user_id">建立者<i class="icon-chevron-{by-create_user_id}"></i></td>
				<td title="更新者" class="order_control rowhide" data-orderby="update_user_id">更新者<i class="icon-chevron-{by-update_user_id}"></i></td>
				<td title="來源" class="order_control" data-orderby="source_mode">來源<i class="icon-chevron-{by-source_mode}"></i></td>
				<td>來源標題</td>
				<td title="今日頂噓" class="order_control rowhide" data-orderby="daily_click_total">今日頂噓<i class="icon-chevron-{by-daily_click_total}"></i></td>
				<td title="總頂噓" class="order_control rowhide" data-orderby="click_total">總頂噓<i class="icon-chevron-{by-click_total}"></i></td>
				<td title="今日人氣" class="order_control" data-orderby="daily_views">今日人氣<i class="icon-chevron-{by-daily_views}"></i></td>
				<td title="總人氣" class="order_control" data-orderby="views">總人氣<i class="icon-chevron-{by-views}"></i></td>
				<td title="讚數" class="order_control rowhide" data-orderby="like_count">讚數<i class="icon-chevron-{by-like_count}"></i></td>
				<td title="分享數" class="order_control rowhide" data-orderby="share_count">分享數<i class="icon-chevron-{by-share_count}"></i></td>
				<td title="google廣告" class="order_control" data-orderby="google_ad">google<i class="icon-chevron-{by-google_ad}"></i></td>
				<td title="上下架" class="order_control" data-orderby="online">上下架<i class="icon-chevron-{by-online}"></i></td>
			</tr>
		</thead>
		
		<tbody>
			{content}
			<tr id="user_{id}" class="user_row" >
				<td style="white-space:nowrap;"><a href="{front_url}{id}" target="_blank">{id}</a></td>
				<td><a href="{front_url_online}{id}/" target="_blank">{article_title}</a></td>
				<td><a href="http://file.meoments.com/n{id}/f.jpg" target="_blank"><img src="http://file.meoments.com/n{id}/f.jpg" style="height: 126px;"/></a></td>
				<td>{create_time}</td>
				<td>{article_category}</td>
				<td>{create_user_id}</td>
				<td>{update_user_id}</td>
				<td>{source} / {source_mode}</td>
				<td class="fb_title"><a href="{source_url}" target="_blank">{fb_title}</td>
				<td>{daily_click_total}</td>
				<td>{click_total}</td>
				<td>{daily_views}</td>
				<td>{views}</td>
				<td>{like_count}</td>
				<td>{share_count}</td>
				<td><div class="google_ad_{google_ad}" alt="" style="width: 40px; height: 40px;" onclick="change_google({id});"></div></td>

				<td style="white-space:nowrap;">
					<span class="article_online {off}" data-index="{id}"> 
						<a href="javascript:void(0);" class="view tablectrl_small bBlue" original-title="View" title="觀看" alt="觀看" onclick="article.add_edit_news_article({id}, 'view');">
						<span class="iconb" data-icon="" ></span></a>	
						<a href="javascript:void(0);" class="edit tablectrl_small bGreen" original-title="Edit" title="編輯" alt="編輯" onclick="article.add_edit_news_article({id}, 'edit');">
						<span class="iconb" data-icon="" ></span></a>
						<a href="javascript:void(0);" class="Comment tablectrl_small {comment_count}" original-title="Comment" title="編輯留言" alt="編輯留言" onclick="article.edit_news_comment({id}, {fb_id})">
						<span class="icon-comments"></span></a>
					</span>	
					<!-- <a href="javascript:void(0);" class="delete tablectrl_small bRed {edit_control}" original-title="Del" title="刪除" alt="刪除" onclick="article.del({id});">
					<span class="iconb" data-icon="" ></span></a> -->
					<span class="online {off}" data-index="{id}"> 
						<a href="javascript:void(0);" class="tablectrl_small bBlue" original-title="Edit" title="上架中"
							alt="上架中">上架中<span class="iconb" data-icon=""></span></a>
						<a href="javascript:void(0);" class="tablectrl_small bGold" original-title="Edit" title="下架中" 
							alt="下架中">下架中<span class="iconb" data-icon=""></span></a>
					</span>
					<a href="javascript:void(0);" class="change_article_id tablectrl_small bRed change_article_id_{group} {off}"  data-index="{id}"><span class="icon-refresh "></span></a>
					<a href="http://i-kuso.com/pofans2/message/index.php?id={pofans2_id}" target="_blank">{pofans2_id}</a>		
				</td>														
			</tr>			
			{/content}
		</tbody>
	</table>
	
</div>
{page_list}
<script src="js/article.js"></script>
<script src="js/span_editor.js"></script>
<style>

thead TD{
white-space: nowrap;
}
.hide, .topicview_{
	display: none;
}
.topicview_hide{
	display: initial;
}
/*.tLight tbody td{
	padding: 0px 16px;
}*/
.external_link{
	background: none;
}
	.more_page{
		text-align: center;
	}
	
	.more_page SPAN {
		-moz-box-shadow:inset 0px 1px 0px 0px #ffffff;
		-webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;
		box-shadow:inset 0px 1px 0px 0px #ffffff;
		background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #dfdfdf));
		background:-moz-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
		background:-webkit-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
		background:-o-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
		background:-ms-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
		background:linear-gradient(to bottom, #ededed 5%, #dfdfdf 100%);
		filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#dfdfdf',GradientType=0);
		background-color:#ededed;
		-moz-border-radius:6px;
		-webkit-border-radius:6px;
		border-radius:6px;
		border:1px solid #dcdcdc;
		display:inline-block;
		cursor:pointer;
		color:#777777;
		font-family:arial;
		font-size:15px;
		font-weight:bold;
		padding:6px 14px;
		text-decoration:none;
		text-shadow:0px 1px 0px #ffffff;
	}
	.more_page SPAN.page_selected {		
		color:#c92200;
		text-shadow:0px 1px 0px #ded17c;
	}
	
	.more_page SPAN:hover {
		color:#ffffff;
		text-shadow:0px 1px 0px #528ecc;
		-moz-box-shadow:inset 0px 1px 0px 0px #bbdaf7;
	    -webkit-box-shadow:inset 0px 1px 0px 0px #bbdaf7;
	    box-shadow:inset 0px 1px 0px 0px #bbdaf7;
		background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #378de5), color-stop(1, #79bbff));
		background:-moz-linear-gradient(top, #378de5 5%, #79bbff 100%);
		background:-webkit-linear-gradient(top, #378de5 5%, #79bbff 100%);
		background:-o-linear-gradient(top, #378de5 5%, #79bbff 100%);
		background:-ms-linear-gradient(top, #378de5 5%, #79bbff 100%);
		background:linear-gradient(to bottom, #378de5 5%, #79bbff 100%);
		filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#378de5', endColorstr='#79bbff',GradientType=0);
		background-color:#378de5;
		
		border:1px solid #84bbf3;
	}
	.more_page SPAN:active {
		position:relative;
		top:1px;
	}
	form#list_orderby {
		margin: 20px 30px 20px 10px;
		display: inline-block;
	}
	.tLight tbody td.red_string {
		color: red; 
	}
	.tLight tbody td.blue_string {
		color: blue; 
	}
	.tLight tbody td.green_string {
		color: green; 
	}
	.tLight tbody td {
		max-width: 300px;
		overflow: hidden;
		text-overflow : ellipsis;
	}
	.tLight tbody td.fb_title {
		max-width: 250px;
		overflow: hidden;
		text-overflow : ellipsis;
	}
</style>

<script>
$(document).ready(function() {
	var fans = $('#fans_selected').val();
    $('#'+fans).attr("selected", "selected");

    var day = $('#days_selected').val();
    $('#'+day).attr("selected", "selected");
});

function order_by(this_td){
	$('#order_by').val( $(this_td).attr('data-orderby'));
	$('#list_orderby').submit();
}

function change_google(index) {
	if ( $('#user_'+index).find('.google_ad_1').length > 0 ) {
		$('#user_'+index+' .google_ad_1').addClass('google_ad_0').removeClass('google_ad_1');
		var status = 0;
	} else {
		$('#user_'+index+' .google_ad_0').addClass('google_ad_1').removeClass('google_ad_0');
		var status = 1;
	}

	$.ajax({
		url: 'news/change_google/',
	  	dataType: 'json',
	  	type: "POST",
	  	data: {
	  		index: index,
			status: status	
	  	},
	  	success: function(data){
	  		console.info(data);
	  	},error: function(data){
	  		console.info(data);
	  	}
	});
}

$(".order_control").live( "click", function() {
	var order_name = $(this).attr('data-orderby');
	console.info(this);
	if(order_name) order_by(this);
	
	return false;
});	

$('[tips]').hover(
	function() {
	    var x = $(this).offset().left;
	    var y = $(this).offset().top;
		var tips_box = document.createElement('div');
	    $(tips_box).attr("id","tips_box").html($(this).attr('tips')).appendTo('body');

	    var tips_width = $('#tips_box').width()+40;
	    var tips_height = $('#tips_box').height();
	  
	    var this_width = $(this).width();
	    var this_height = $(this).height();
	 
	    var diff_x = (tips_width + x > $(window).width())?$(window).width()-(tips_width + x):((this_width-$('#tips_box').width())/2);
	    var c_x = x + diff_x ;

	    var diff_y = (tips_height + y > $(window).height())?$(window).height()-(tips_height + y):(this_height+(tips_height)/2);
	    var c_y = y + diff_y ;
		 
	    $(tips_box).css('left',c_x).css('top',c_y).show();
	  			   
	}, function() {
		$('#tips_box').remove();
	}
);

$(".online").click(function() {
	var index= $(this).attr('data-index'); 
	var online = $(this).hasClass("off")
	var span = $(this);
	// console.info(span.next());
	if(online) {
		$(this).removeClass("off");
		span.next().removeClass("off");
		$('.article_online[data-index='+index+']').removeClass("off");
		
	} else { 
		$(this).addClass("off");
		span.next().addClass("off");
		$('.article_online[data-index='+index+']').addClass("off");
	}

	if(index)
		$.ajax({
		  url: 'news/news_article_online/',
		  dataType: 'json',
		  type: "POST",
		  data: {
			  online:online,
			  index:index
		  },
		  success: function(data){
		  	if ( data['success'] == 'N' ) {
		  		if(!online) {
					span.removeClass("off");
					span.next().removeClass("off");
					$('.article_online[data-index='+index+']').removeClass("off");
				} else { 
					span.addClass("off");
					span.next().addClass("off");
					$('.article_online[data-index='+index+']').addClass("off");
				}
				alert(data['msg']);
		  	} else {
		  		console.info(data['msg']);
		  	}
		  	
		  },error: function(data){
		  	console.info(data);
		  }
		});
});

$(".change_article_id").click(function() {
	if(confirm("是否確定更換文章ID? ")){
		var index= $(this).attr('data-index'); 

		if(index)
			$.ajax({
			  url: 'news/change_article_id/',
			  dataType: 'json',
			  type: "POST",
			  data: {
				  index:index
			  },
			  success: function(data){
			  	console.info(data);
			  	location.reload();
			  },error: function(data){
			  	console.info(data);
			  }
			});
	}
});
</script>