<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Article extends CI_Controller {
	var $menu_index;
	public function __construct() {
		parent::__construct ();
		
		$this->menu_index = 6; //
		$this->load->helper ( 'url' );
		$this->userInfo = $this->session->userdata ( 'userInfo' );
		$this->load->model ( 'article_model', 'article' );
		$this->load->model ( 'topic_model', 'topic' );
		$this->load->model ( 'theme_model', 'theme' );
		
		$this->data ['admin_menu'] = $this->config->item ( 'admin_menu' );
		$this->data ['admin_menuGroup'] = $this->config->item ( 'admin_menuGroup' );
		$this->data ['userInfo'] = $this->userInfo;
		$this->data ['menu'] = $this->config->item ( 'admin_accountMenu' );
		$this->data ['breadcrumbs'] = $this->config->item ( 'breadcrumbs' );
		$this->data ['title'] = $this->config->item ( 'admin_title' );
		
		$this->data ['topbar_control'] = array ();
		$this->data ['select_result'] = array ();
	}
	/*
	 * index
	 */
	public function index($pg = 1) {
		$this->lists ( $pg );
	}
	
	public function move_article_photo() {
		
		$img = $this->input->post ( 'img' );
		
		$check_dir = '../uploads/check/';
		$check_url= 'check'.((int)100*microtime(true)).$this->input->post ( 'index' ).'.jpg';
		$check_img =  $check_dir.$this->article->move_article_photo($img,$check_url,$check_dir);
		
		
		echo (preg_match("/http|https|data\:/", $check_img))?$check_img:'http://starlove99.com/'.$check_img;
		
	}
	public function remove_check_photo() {
		
		$img = $this->input->post ( 'img' );
		
		$img = str_replace('http://starlove99.com/', '', $img);
		$this->article->remove_check_photo($img);
	}
	function cut_img(){
	
		$img = $this->input->post ( 'img' );
		$cut_info = $this->input->post ( 'cut_info' );
		echo $this->admin_model->cut_base64img($img,$cut_info);
		
	}
	
	function create_article(){
		$topic_array = $this->input->post ( 'topic_array' );
		$options_select_index = $this->input->post ( 'options_select_index' );
		$from_table = $this->input->post ( 'from_table' );
		$from_index = $this->input->post ( 'from_index' );
		
		$auto_index = $this->input->post ( 'auto_index' );
		
		$date = date ( "Y-m-d H:i:s" );
		$account_id = $this->config->item ( 'admin_accountIndex' );
		
		$article_group = (isset($topic_array['article_group']) && is_array($topic_array['article_group']))?json_encode($topic_array['article_group']):'';
		
		$add_input = array (
				'article_title' => $topic_array['article_title'],
				'article_optiontitle' => $topic_array['article_optiontitle'],
				'facebook_imgurl' => '',
				'article_imgurl' => $topic_array['article_imgurl'],
				'article_text' => $topic_array['article_text'],
				'article_defoption' => $topic_array['article_defoption'],
				
				'article_group'=> $article_group,
		);
		$add_options = isset($topic_array['options'])?$topic_array['options']:false;
		
		$add_input ['create_time'] = $date;
		$add_input ['create_account_index'] = $account_id;
		$add_input ['update_time'] = $date;
		$add_input ['update_account_index'] = $account_id;
		
		if($auto_index) 
			$result = $this->article->check_article_exist_and_update($add_input,$add_options,$auto_index);
		else 
			$result = $this->article->check_article_exist_and_insert($add_input,$add_options);
		
		
		if(isset($result['index']) && $result['index'] && is_array($topic_array['article_img'])){
			$article_text = $add_input['article_text'];
			//是否建立目錄
			$newDir = '../uploads/q'.$result['index'].'/';
			if(false !== $newDir && false === is_dir( $newDir ) ) {
				mkdir($newDir);
				chmod($newDir, 0777);
			}
			foreach($topic_array['article_img'] AS $key =>$img){
				echo $from = str_replace('http://starlove99.com/', '', $img);
				echo $to = $newDir.$key.'.jpg';
// 				$newimg = $this->article->move_article_photo($img,$key.'.jpg','../uploads/q'.$result['index'].'/');
				rename($from,$to);
				$replace = 'http://starlove99.com/uploads/q'.$result['index'].'/'.$key.'.jpg';				 
				$article_text = str_replace($img,$replace, $article_text);
			}
			
			$article = array("article_text"=>$article_text);
			$this->db->where('auto_index', $result['index']);
			$this->db->update('article', $article);
				
		}
		if(isset($result['index']) && $result['index'] && $from_table && $from_table){
			$from_index_input['article_index'] = $result['index'];
			$from_index_input['from_table'] = $from_table;
			$from_index_input['from_index'] = $from_index;
			
			$this->article->check_article_from_index($from_index_input);
			
		}
	}
	
	/**
	 * list
	 */
	function horo_lists($pg = 1, $user = '') {
		
		$page_one = 20;
		$topSearch = $this->input->post ( 'topSearch' );
		$order_name = $this->input->post ( 'order_by' );
		$orderBy = $this->theme->order_by ( $order_name ,'horo_lists');
		$data = array_merge ( $this->data, $orderBy );
		
		if ($pg || $user)
			$this->menu_index = 'ifunso星座'; //

		$data = $this->data;
		$this->menu_index = 'ifunso星座'; //
		if (false === $activestatus = $this->admin_model->checkCompetence_getMenuInfo ( $this->menu_index, 'V' )) {
			$this->parser->parse ( 'template', $data );
			return false;
		}
		
		$defaultstatus = (isset ( $activestatus )) ? $activestatus : $this->config->item ( 'defaultstatus' );
		
		if ($this->menu_index)
			$data ['breadcrumbs'] = $this->config->item ( 'breadcrumbs' );
		
		$data['title'] = $this->admin_model->getTitle($this->menu_index);
		
		//讀取以建立的題目INDEX
		$this->db->flush_cache();
		$this->db->select("from_index");
		$this->db->from("article_from_index");
		$this->db->where('article_from_index.from_table', 'horoscope');
		
		$query = $this->db->get();
		
		$horoscope_index = $query->result_array();
		foreach($horoscope_index AS $value){
			$horoscope_notin[] = $value['from_index'];
		}
// 		var_dump($horoscope);
// 		exit;
		$horoscope = $this->load->database('ifunso_horoscope',true);
		$horoscope->flush_cache();
		$horoscope->select("*");
		$horoscope->from("horoscope");
		$horoscope->order_by('monthly','DESC');
		if(isset($horoscope_notin) && is_array($horoscope_notin)){
			$horoscope->where_not_in('horoscope.pk_horoscope', $horoscope_notin);
		}
		
		
		$total_query = clone $horoscope;
		
		$page ['total_page'] = $total_query->get ()->num_rows;
		
		$horoscope->limit ( $page_one, ($pg - 1) * $page_one );
		$query = $horoscope->get();
	
		$data['content'] = $query->result_array();
		foreach ($data['content'] AS $key => $val){
		
			$data['content'][$key]['id'] = (int) $val['pk_horoscope'];
			$data['content'][$key]['horo_title'] = $val['title_horoscope'];
		}
		$page ['start'] = 1;
		$page ['end'] = ceil ( $page ['total_page'] / $page_one );
		$page ['url'] = 'article/horo_lists';
		$page ['page_num'] = $pg;
		$page ['url_end'] = '';
		
		$data ['page_list'] = $this->theme->page ( $page );
		

		
		$data['main_content'] = $this->parser->parse('article/horoscope_list', $data, true);
		$this->parser->parse('template', $data);
	}
	
	function lists($pg = 1, $user = '') {
		$page_one = 10;
		$topSearch = $this->input->post ( 'topSearch' );
		$order_name = $this->input->post ( 'order_by' );
		$orderBy = $this->theme->order_by ( $order_name ,'article_list');
		$data = array_merge ( $this->data, $orderBy );
		
		if ($pg || $user)
			$this->menu_index = '文章列表'; //
		
		if (false === $activestatus = $this->admin_model->checkCompetence_getMenuInfo ( $this->menu_index, 'V' )) {
			$this->parser->parse ( 'template', $data );
			return false;
		}
		
		$defaultstatus = (isset ( $activestatus )) ? $activestatus : $this->config->item ( 'defaultstatus' );
		
		$purview = 'O';
		if ($this->admin_model->checkCompetence_getMenuInfo ( $this->menu_index, $purview )) {
			$onshelf_purview = true;
		} else {
			$onshelf_purview = false;
		}
		
		if ($this->menu_index)
			$data ['breadcrumbs'] = $this->config->item ( 'breadcrumbs' );
		
		$data ['onshelf_purview'] = $onshelf_purview;
		
		// 讀取 題目 使用者資訊
		$sql = "select admin_account.id AS value,CONCAT(admin_account.id,'(' ,sum(case when article.online = '0' then 0 else 1 end),'/',count(admin_account.auto_index) ,')'  ) AS label 
				FROM admin_account 
				INNER JOIN article ON admin_account.auto_index = article.create_account_index
				GROUP BY admin_account.auto_index 
				";
		$this->db->flush_cache ();
		$query = $this->db->query ( $sql );
		
		preg_match ( '/select_(?P<user_id>[0-9]+)/', $user, $user_id );
		if ($user_id)
			$temp_userid ['select_id'] = $user_id ['user_id'];
		
		preg_match ( '/^(?P<group_id>[0-9]+)/', $user, $group_id );
		if ($group_id)
			$temp_userid ['group_id'] = $group_id ['group_id'];
		
		$temp_selector ['current_url'] = base_url ( 'article/lists/' . $pg ) . "/";
		
		$temp_selector ['select_name'] = (! isset ( $temp_userid ) && ! isset ( $temp_userid ) && $user) ? $user : 'All User';
		$temp_selector ['select_option'] = $query->result_array ();
		$data ['select_result'] [] = $temp_selector;
		
		if (isset ( $temp_userid ['group_id'] ))
			$temp_name = $this->article->article_group ( $temp_userid ['group_id'], 'name AS group_name' );
		$temp_selector ['select_name'] = (isset ( $temp_name ) && isset ( $temp_name ['group_name'] )) ? $temp_name ['group_name'] : 'All Group';
		$temp_selector ['select_option'] = $this->article->article_group ( '', 'name AS label, auto_index AS value' );
		$data ['select_result'] [] = $temp_selector;
		
		if (isset ( $temp_userid ['select_id'] ))
			$temp_name = $this->topic->topic_select ( $temp_userid ['select_id'], 'select_name' );
		$temp_selector ['select_name'] = (isset ( $temp_name ) && isset ( $temp_name ['select_name'] )) ? $temp_name ['select_name'] : 'All Select';
		$temp_selector ['select_option'] = $this->topic->topic_select ( '', 'select_name AS label, CONCAT("select_",topic_select.auto_index ) AS value' );
		$data ['select_result'] [] = $temp_selector;
		
		$this->db->flush_cache ();
		$this->db->select ( "article.*,statistics_onshelf.fail_state AS fail_state,admin_account.id" );
		$this->db->from ( "article" );
		$this->db->join ( 'admin_account', 'admin_account.auto_index = article.create_account_index' );
		$this->db->join ( "statistics_onshelf", 'statistics_onshelf.topic_index = article.auto_index', 'left' );
		/*
		 * if($defaultstatus == 'S'){ $this->db->where('create_account_index',$this->config->item('admin_accountIndex')); }else if($defaultstatus == 'G'){ $this->db->where_in('create_account_index',$this->config->item('admin_accountGroupIndexArray')); } /*
		 */
		
		if ($topSearch) {
			$this->db->where ( "article.article_title like '%" . $topSearch . "%'" );
			$this->db->or_where ( "article.auto_index", $topSearch );
		}
		
		if ($user) {
			
			if (isset ( $temp_userid ['select_id'] )) {
				$this->db->where ( 'article.options_select', $temp_userid ['select_id'] );
			} else if (isset ( $temp_userid ['group_id'] )) {
				$this->db->where ( "article.article_group like '%\"" . $temp_userid ['group_id'] . "\"%'" );
			} else {
				$this->db->where ( 'admin_account.id', $user );
			}
		}
		if (is_array ( $orderBy )) {
			foreach ( $orderBy as $key => $value ) {
				if (! $key)
					continue;
				if ($value == "down")
					$this->db->order_by ( 'article.' . str_replace ( 'by-', '', $key ) . ' DESC' );
				else
					$this->db->order_by ( 'article.' . str_replace ( 'by-', '', $key ) . ' ASC' );
			}
		} else
			$this->db->order_by ( 'article.online ASC, article.create_account_index ASC' );
		
		$total_query = clone $this->db;
		
		$page ['total_page'] = $total_query->get ()->num_rows;
		
		$this->db->limit ( $page_one, ($pg - 1) * $page_one );
		$query = $this->db->get ();
		
		$data ['title'] = $this->admin_model->getTitle ( $this->menu_index );
		
		$data ['content'] = $query->result_array ();
		foreach ( $data ['content'] as $key => $list ) {
			if ($data ['content'] [$key] ['online'] < 1)
				$data ['content'] [$key] ['edit_control'] = '';
			else
				$data ['content'] [$key] ['edit_control'] = 'hide';
			$temp_group = $this->article->article_group ();
			$fans_groupname = array ();
			$temp_groupindex = json_decode ( $list ['article_group'] );
			
			$data ['content'] [$key] ['fail_state'] = (false !== strpos ( $list ['fail_state'], 'true' )) ? '請修正問題' : '';
			
			foreach ( $temp_group as $value ) {
				if (is_array ( $temp_groupindex ) && in_array ( $value ['auto_index'], $temp_groupindex ))
					$fans_groupname [] = $value ['name'];
			}
			
			$data ['content'] [$key] ['group_names'] = ($temp_group) ? implode ( ',', $fans_groupname ) : '無';
			$data ['content'] [$key] ['select_optionName'] = (isset ( $select_option ['select_name'] )) ? $select_option ['select_name'] : '無';
		}
		
		$page ['start'] = 1;
		$page ['end'] = ceil ( $page ['total_page'] / $page_one );
		$page ['url'] = 'article/lists';
		$page ['page_num'] = $pg;
		$page ['url_end'] = '/' . $user;
		
		$data ['page_list'] = $this->theme->page ( $page );
		
		$data ['main_content'] = $this->parser->parse ( 'article/article_list', $data, true );
		$this->parser->parse ( 'template', $data );
	}
	/**
	 * actions
	 */
	function horo_add() {
				
				$index = $this->input->post ( 'index' );
				if (! $index) {
					
					echo $this->load->view ( 'purview_error', '' );
					return false;
				}else{
					$purview = 'E';
				}
				
				$this->menu_index = 'ifunso星座'; //
				
				if ($this->admin_model->checkCompetence_getMenuInfo ( $this->menu_index, $purview )) {
					$onshelf_purview = true;
				} else {
					$onshelf_purview = false;
				}
			
				
				/*default value*/
				$from_table = 'horoscope';
				
				$horoscope = $this->load->database('ifunso_horoscope',true);
				
				$horoscope->select ( $from_table.'.* ' )->from ( $from_table );
				$horoscope->where ( $from_table.'.pk_horoscope', $index );
				
				$query = $horoscope->get ();
				$result = $query->result_array ();
				
				
				if (is_array ( $result ) && count($result)) {
					
					$data = array_pop ( $result );
			
					$data ['from_table'] = $from_table;
					$data ['from_index'] = $index;
					$data ['onshelf_purview'] = $onshelf_purview;
					$data ['article_group_list'] = $this->article->article_group ('',' *,"" AS checked ',false);
					$data ['topic_select'] = $this->topic->topic_select ();
					$data ['article_defoption_list'] = $this->article->get_article_defoption ();
					$data ['auto_index'] = 0;
					$data ['online'] = 0;
					
					$data ['article_options'][] = array('choose'=>'','title'=>'','text'=>'');
					$data ['article_imgurl'] = 'http://horoscope.ifunso.com/uploads/result/'.$data['img_file'];					
					
					$data ['article_title'] = $data['title_horoscope'];
					$data ['article_text'] = $data['text_horoscope'];
					$data ['article_optiontitle'] = '';//$data['pofans_text'];
			
				}
			
				$this->parser->parse ( 'article/article_edit', $data );
				
	}
	
	function actions($ac) {
		switch ($ac) {
			case 'add_article' :
			
				$index = $this->input->post ( 'index' );
				if (! $index) {
					$purview = 'A';
// 					echo $this->load->view ( 'purview_error', '' );
// 					return false;
				}else{
					$purview = 'E';
				}
			
				$this->menu_index = '文章列表'; //
				
				if ($this->admin_model->checkCompetence_getMenuInfo ( $this->menu_index, $purview )) {
					$onshelf_purview = true;
				} else {
					$onshelf_purview = false;
				}
			
				
				/*default value*/
				$data ['onshelf_purview'] = $onshelf_purview;
				$data ['article_group_list'] = $this->article->article_group ('',' *,"" AS checked ',false);				
				$data ['topic_select'] = $this->topic->topic_select ();
				$data ['article_defoption_list'] = $this->article->get_article_defoption ();
				
				$data ['article_imgurl'] = 'http://goquiz88.com/img/goquiz88_facebook_share.jpg';
				$data ['auto_index'] = 0;
				$data ['online'] = 0;
				$data ['article_title'] = '';
				$data ['article_optiontitle'] = '';
				$data ['article_text'] = '';
				$data ['article_options'][] = array('choose'=>'','title'=>'','text'=>'');
				$data ['from_table'] = '';
				$data ['from_index'] = '';
				
				$this->db->select ( 'article.*,statistics_onshelf.fail_state AS fail_state ' )->from ( 'article' );
				$this->db->join ( "statistics_onshelf", 'statistics_onshelf.topic_index = article.auto_index', 'left' );
				$this->db->where ( 'article.auto_index', $index );
					
				$query = $this->db->get ();
				$result = $query->result_array ();
				if (is_array ( $result ) && count($result)) {
					$data = array_pop ( $result );
			
					$data ['from_table'] = '';
					$data ['from_index'] = '';
					$data ['article_defoption_list'] = $this->article->get_article_defoption ();
					
					$data ['article_group_list'] = $this->article->article_group ('',' *,"" AS checked ',false);
					$data ['topic_select'] = $this->topic->topic_select ();
					
					
					$fans = json_decode ( $data ['article_group'] );
					foreach($data['article_group_list'] AS $key => $value){
						$data['article_group_list'][$key]['group_index'] = $value['auto_index'];
						$data['article_group_list'][$key]['checked'] = ((is_array($fans)) && in_array($value['auto_index'], $fans))?'checked':'';
					}
			
					
					$fans = json_decode ( $data ['article_group'] );
						
					foreach($data['article_defoption_list'] AS $key => $value){						
						$data['article_defoption_list'][$key]['checked'] = ($value['auto_index'] == $data['article_defoption'])?'checked':'';
					}
					
					$this->db->select ( 'auto_index AS st_AI , state_name' )->from ( 'statistics_state' );
					$query = $this->db->get ();
					$data ['statistics_state'] = $query->result_array ();
			
					$this->db->select ( '*' )->from ( 'article_options' )->where ( 'article_index', $index );
					$query = $this->db->get ();
					$data ['article_options'] = $query->result_array ();
					$temp_i = 0;
					foreach ( $data ['article_options'] as $key => $i ) {
						$data ['article_options'] [$key] ['optionIndex'] = (++ $temp_i);
						$data ['article_options'] [$key] ['optionImg'] = $data ['article_options'] [$key] ['optionIndex'] . substr ( $i ['text_imgurl'], - 4 );
						$data ['article_options'] [$key] ['AI'] = $i ['auto_index'];
					}
				}
			
				$this->parser->parse ( 'article/article_edit', $data );
				break;
			case 'del' :
				$this->menu_index = '文章列表'; //
				if (! $this->admin_model->checkCompetence_getMenuInfo ( $this->menu_index, 'D' )) {
					$result = array (
							'success' => 'N',
							'msg' => '權限不足'
					);
					echo json_encode ( $result );
					break;
				}
	
				$index = $this->input->post ( 'index' );
				$result = $this->article->del_article_by_index ( $index );
	
				header ( 'Content-type: text/plain; charset=utf-8' );
				echo json_encode ( $result );
				break;
		}
	}
}