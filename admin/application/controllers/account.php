<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends CI_Controller {	
	var $menu_index;
	
	
	public function __construct(){
		parent::__construct();
		
		
		$this->userInfo = $this->session->userdata('userInfo');
		$this->load->model('theme_model', 'theme');
		
		$this->data['admin_menu'] = $this->config->item('admin_menu');
		$this->data['admin_menuGroup'] = $this->config->item('admin_menuGroup');
		$this->data['userInfo'] = $this->userInfo;
		$this->data['menu'] = $this->config->item('admin_accountMenu');
		$this->data['breadcrumbs'] = $this->config->item('breadcrumbs');
		$this->data['title'] =$this->config->item('admin_title');
		
		$this->data['topbar_control']= array();
		$this->data['select_result'] = array();
	}
	/*
	* index
	*/
	public function index(){		
		$this->lists();
	}
	
	/**
	 *	list
	 */
	function lists(){
		
		
		$order_name = $this->input->post('order_by');	
		$orderBy = $this->theme->order_by($order_name);
		$data = array_merge($this->data, $orderBy);
		
		if(false === $activestatus = $this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, 'V')){
			$this->parser->parse('template', $data);
			return false;
		} 
		$defaultstatus = (isset($activestatus))?$activestatus:$this->config->item('defaultstatus');
		
		
		$admin_group = $this->config->item('admin_group');
		
		$data['content'] = $this->admin_model->get_all_account_lists('',$orderBy);
		
		foreach($data['content'] AS $key => $value){
			$admin_group = $this->admin_model->get_groupdefault($value['admingroup_index']);
			$data['content'][$key]['group_name'] = (isset($admin_group['group_name']))?$admin_group['group_name']:'無';

			if ( $data['content'][$key]['online'] == 1 ) {
				$data['content'][$key]['lock'] = 'bBlue';
			} else {
				$data['content'][$key]['lock'] = 'bRed';
			}
			
		}
		
		
		
		$data['status'] = $this->config->item('admin_group');
		
		
		$data['main_content'] = $this->parser->parse('account/account_list', $data, true);
		$this->parser->parse('template', $data);
	}
	
	function group(){		
		$order_name = $this->input->post('order_by');	
		$orderBy = $this->theme->order_by($order_name, 'admin_group');
		$data = array_merge($this->data, $orderBy);
		
		if(false === $activestatus = $this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, 'V')){
			//$this->load->view('template', $data);
			return false;
		} 
		$defaultstatus = (isset($activestatus))?$activestatus:$this->config->item('defaultstatus');
			
	
		$data['content'] = $this->admin_model->get_all_group_lists('',$orderBy);
		
		$com_status = $this->config->item('admin_competencestatus');
		foreach($data['content'] AS $key => $list){			
			$data['content'][$key]['purview'] = ($list['group_defaultstatus'])?$com_status[$list['group_defaultstatus']]:'無';
		}
		
	
		$data['main_content'] = $this->parser->parse('account/account_grouplist', $data, true);
		$this->parser->parse('template', $data);
		
		
	}
	
	function purview(){
		$data = $this->data;
		$this->menu_index = '特殊權限列表';//
		if(false === $activestatus = $this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, 'V')){
			$this->load->view('template', $data);
			return false;
		}
		$defaultstatus = (isset($activestatus))?$activestatus:$this->config->item('defaultstatus');
		
		
		$data['content'] = $this->admin_model->get_all_purview_lists($defaultstatus);
	
		
		
		$com_status = $this->config->item('admin_competencestatus');
		$competence = $this->config->item('admin_competence');
		foreach($data['content'] AS $key => $list){
			$data['content'][$key]['competence'] = ($list['competence_value'])?$competence[$list['competence_value']]:'無';
			$data['content'][$key]['purview'] = ($list['purview_value'])?$com_status[$list['purview_value']]:'無';
		}
	
	
		$data['main_content'] = $this->parser->parse('account/account_list', $data, true);
		$this->parser->parse('template', $data);
	}
	
	/**
	 *	actions
	 */
	function actions($ac){
		
		
		switch($ac){
			case 'competence':
				$index = (int) $this->input->post('index');
				$data = $this->data;
				
				$data['group_info'] = $this->admin_model->get_groupdefault($index);
				
			
				$data['competence'] = $this->config->item('admin_group');
				$data['admin_competence'] = $this->config->item('admin_competence');
				$data['default'] = $this->admin_model->get_groupdefault();
				
				
				echo $this->load->view('account/account_groupcompetence', $data, true);
				
				break;
			case 'add_account':
				
				$index =  $this->input->post("index");
				$this->menu_index = '帳號列表';//
				$purview = ($index)?'E':'A';
				if(!$this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, $purview)){
					echo $this->load->view('account/purview_error', '');
					return false;
				}
				
				
				$data = $this->data;
				
				
				$data['competence'] = $this->config->item('admin_group');
				$data['admin_competence'] = $this->config->item('admin_competence');
				$data['default'] = $this->admin_model->get_groupdefault();
				
				if($index){
					$this->db->select('*')->from('admin_account')->where('auto_index',$index);
					$query = $this->db->get();
					$result = $query->result_array();
					if(is_array($result)){
						$data['account_info'] = array_pop($result);						
						$data['account_info']['account_competence']  = json_decode($data['account_info']['competence'],true);
					}
				}
								
				echo $this->load->view('account/account_edit', $data, true);
				break;
			case 'add_group':
				
				$index =  $this->input->post("index");
				$this->menu_index = '帳號群組';//
				$purview = ($index)?'E':'A';
				if(!$this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, $purview)){
					echo $this->load->view('account/purview_error', '');
					return false;
				}
				
				$data['status'] = $this->config->item('admin_group');
			
				$data['admin_menu'] = $this->config->item('admin_menu');
			
				$data['competence'] = $this->config->item('admin_group');
				$data['admin_competence'] = $this->config->item('admin_competence');
				$data['default'] = $this->admin_model->get_groupdefault();
				$data['group_defaultstatus'] = $this->config->item('admin_competencestatus');
				if($index){
					$this->db->select('*')->from('admin_group')->where('auto_index',$index);
					$query = $this->db->get();
					$result = $query->result_array();
					if(is_array($result)){
						$data['group_info'] = array_pop($result);				
						$data['group_info']['group_default']  = json_decode($data['group_info']['group_default'],true);						
					}
				}
				
				
				
				echo $this->load->view('account/account_groupedit', $data, true);
				break;
			case 'edit_password':
				$index =  $this->input->post("index");
				$this->menu_index = '帳號列表';//
				$purview = ($index)?'E':'A';
				if(!$this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, $purview)){
					echo $this->load->view('account/purview_error', '');
					return false;
				}
				
				
				$data = $this->data;
				
				
				$data['competence'] = $this->config->item('admin_group');
				$data['admin_competence'] = $this->config->item('admin_competence');
				$data['default'] = $this->admin_model->get_groupdefault();
				
				if($index){
					$this->db->select('*')->from('admin_account')->where('auto_index',$index);
					$query = $this->db->get();
					$result = $query->result_array();
					if(is_array($result)){
						$data['account_info'] = array_pop($result);
						$data['account_info']['account_competence']  = json_decode($data['account_info']['competence'],true);
					}
				}
				
				echo $this->load->view('account/account_editpw', $data, true);
				break;
				break;
			case 'validate_group':
				
				$this->menu_index = '帳號群組';//
				if(!$this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, 'E')){
					$result = array('success' => 'N', 'msg' => '權限不足');
					echo json_encode($result);
					break;
				}
				
				$index =  $this->input->post("auto_index");
				$date = date("Y-m-d H:i:s");
				$account_id = $this->config->item('admin_accountIndex');
				$online = '1';
				$add_input = array("group_name"=>'','group_defaultstatus'=>'','group_default'=>'','online'=>$online);
			
				foreach ($add_input AS $key => $value){
					if(!$value){
						switch ($key){
							case "group_default":
								$add_input[$key] = json_encode( $this->input->post("group_default"),true);
								break;							
							default:
								$add_input[$key] = $this->input->post($key);
								break;
						}
					}
				}
				
				if($index) $result = $this->admin_model->check_group_exist_and_upload($add_input,$index);
				else $result = $this->admin_model->check_group_exist_and_insert($add_input);				
				header('Content-type: text/plain; charset=utf-8');
				echo json_encode($result);
				
				break;
			case 'validate':
				
				
				$this->menu_index = '帳號列表';//				
				if(!$this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, 'E')){
					$result = array('success' => 'N', 'msg' => '權限不足');
					echo json_encode($result);
					break;
				}
				
				$index =  $this->input->post("auto_index");
				
				$date = date("Y-m-d H:i:s");
				$account_id = $this->config->item('admin_accountIndex');
				$online = '1';
				$add_input = array("id"=>'','pw'=>'','admingroup_index'=>0,'competence'=>'','online'=>$online,'memo'=>'');
				foreach ($add_input AS $key => $value){
					if(!$value){
						switch ($key){
							case "competence":
								$add_input[$key] = json_encode( $this->input->post("competence"),true);
								break;
							case "admingroup_index":
								$add_input[$key] = (int) $this->input->post($key);
								break;
							default:
								$add_input[$key] = $this->input->post($key);
								break;
						}
					}
				}
				
				if($index) {					
					$add_input['update_time'] = $date;
					$add_input['update_account_index'] = $account_id;
					$result = $this->admin_model->check_user_exist_and_upload($add_input,$index);
				}
				else {
					$add_input['create_time'] = $date;
					$add_input['update_time'] = $date;
					$add_input['create_account_index'] = $account_id;
					$add_input['update_account_index'] = $account_id;
					
					$result = $this->admin_model->check_user_exist_and_insert($add_input);				
				}
				header('Content-type: text/plain; charset=utf-8');
				echo json_encode($result);
				break;
			case 'validate_password':
			
			
				$this->menu_index = '帳號列表';//
				if(!$this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, 'E')){
						$result = array('success' => 'N', 'msg' => '權限不足');
								echo json_encode($result);
								break;
				}
			
				$index =  $this->input->post("auto_index");
			
				$date = date("Y-m-d H:i:s");
				$account_id = $this->config->item('admin_accountIndex');
				
				$index =  $this->input->post("auto_index");
				$pw =  $this->input->post("pw");
				$confim_pw =  $this->input->post("confim_pw");
				
				if($index && $pw == $confim_pw) {					
					$edit_password = array('pw'=>md5($pw),'update_time'=>$date,'update_account_index'=>$account_id);
					$this->db->where('auto_index', $index);
					$this->db->update('admin_account', $edit_password);
					$result['success'] = 'Y';
					$result['msg'] = '修改成功';
				}
				
				echo json_encode($result);
				break;
			case 'del':

				$this->menu_index = '帳號列表';
				$result = array('success' => 'N', 'msg' => '權限不足');
				if( !$this->admin_model->checkCompetence_getMenuInfo($this->menu_index, 'E') ){
					echo json_encode($result);
					break;
				}
			
				$date = date("Y-m-d H:i:s");
				$account_id = $this->config->item('admin_accountIndex');
				
				$index = $this->input->post("index");
				
				if( is_numeric($index) ) {
					$this->db->flush_cache();
					$this->db->select('id')->from('news_article')->where('create_user_id', $index);
					$query = $this->db->get();
					$result = $query->result_array();

					$id_array = array();
					
					if ( !empty($result) ) {
						foreach ($result as $key => $value) {
							$id_array[] =  $value['id'];

							$dir =  '../file/n'.$value['id'];
							$this->admin_model->rrmdir($dir);
						}

						$this->db->flush_cache();
						$this->db->where_in('article_id', $id_array);
						$this->db->update('facebook_post_content', array('article_id' => 0));

						$this->db->flush_cache();
						$this->db->where_in('article_id', $id_array);
						$this->db->update('facebook_post_statistics', array('article_id' => 0));

						$tables = array('news_article', 'news_article_onshelf', 'news_article_state');
						$this->db->flush_cache();
						$this->db->where_in('id', $id_array);
						$this->db->delete($tables);
					}

					$this->db->flush_cache();
					$this->db->where('auto_index', $index);
					$this->db->delete('admin_account'); 
	
					$result['success'] = 'Y';
					$result['msg'] = '修改成功';
				}

				echo json_encode($result);
				break;
			case 'lock':

				$this->menu_index = '帳號列表';
				$result = array('success' => 'N', 'msg' => '權限不足');
				if( !$this->admin_model->checkCompetence_getMenuInfo($this->menu_index, 'E') ){
					echo json_encode($result);
					break;
				}
			
				
				$index = $this->input->post("index");
				$status = $this->input->post("status");
				$status = sprintf("%d",($status + 1) % 2);
				
				if( is_numeric($index) ) {
					$this->db->flush_cache();
					$this->db->where('auto_index', $index);
					$this->db->update('admin_account', array('online' => $status)); 
	
					$result['success'] = 'Y';
					$result['msg'] = '修改成功';
				}

				echo json_encode($result);
				break;
		}
	}
	
	/**
	 *	Ajax
	 */
	function ajax($ac){
		
		switch($ac){
			case 'groupdefault':
				
				$index = (int) $this->input->post('value');
				$result  = $this->admin_model->get_groupdefault($index);
				
				$data['index'] = $result['auto_index'];
				$data['default'] = json_decode($result['group_default'],true);
						
				echo json_encode($data);
				break;
			
		}
	}
}