<style>
.topic_group{
	width: 340px;
	float: left;
	margin: 10px;
	cursor: pointer;
}
.topic_group STRONG{
	color:#000;
	float:right;
}
.topic_group UL{
	margin: 0;
	display: none;
}
.topic_group.editable UL{	
	display: block;
	background-color: #fff;
	border: 1px solid #D6D6D6;
	border-radius: 10px;
	padding: 5px 5px;
}
.topic_group UL LI{
	float: left;
	margin-right: 10px;
}
.topic_group UL LI:BEFORE{
	content:' ';
	padding: 10px 0px 0px 30px;
}
.topic_group UL LI:HOVER:BEFORE{
	background: url(images/check.gif) no-repeat left;
}

</style>

<div class="fancybox-content">
	<div class="fancybox-content-bg-bottom">
		<div class="fancybox-header">
			<ul>
				<li class="active"><a rel="resume">觀看</a></li>
				<li><a rel="contact">上架</a></li>
				<li><a rel="connect">編輯</a></li>
			</ul>
			<br class="clear">
			<div class="fancybox-logo" style="color:#919191;">
				<span
					style="border: 1px solid; box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3); padding: 7px 5px 7px 7px; margin-right: 15px; float: left;">
					<img alt="" src="../uploads/q{topic_fromindex}/t{topic_img}"
					width="100px;">
				</span> 
				<div style="float: left;width:350px;margin-right: 10px;">
					<h2><span class="editable" data-editable="topic_{auto_index}">{topic_title}</span></h2> 
					<span style="border-bottom: 1px solid;display: block;font-size: 16px;" class="editable" data-editable="topic_optiontitle_{auto_index}">{topic_optiontitle}</span>
					<div class="topic_group">
						<strong id="options_select_name">{options_select_name}</strong>
						<ul>						
							<li onclick="group_editor(this);">系統(隨機)</li>
							<li onclick="group_editor(this);">自訂(N選1)</li>
							<li onclick="group_editor(this);">星座(固定選項)</li>
							<li onclick="group_editor(this);">單一結果</li>						
						</ul>
					</div>
					
				</div>
				<span style="border: 1px solid; box-shadow: 1px 1px 2px rgba(0,0,0,0.3);padding: 7px 5px 7px 7px;
margin-right: 15px;float: right;margin-top: 25px;">
					<img alt="" src="../uploads/q{topic_fromindex}/s{select_img}" width="100px;">
				</span>
				
				<br class="clear">
			</div>
		</div>
		<div class="fancybox-inside">
			<div class="fancybox-inside-bg-top">
				<div class="fancybox-inside-bg-bottom">
					<div class="resume slide">
						{topic_options}
						<div class="page-content">
							<div class="entry" style="font-size: 14px;">		
														
								<strong style="color:#000;display: block;">
									<span class="data_title" data-title="選項" data-type="title">{choose}</span>
								</strong>
								<strong style="color:#000;display: block;">
									<span class="data_title" data-title="標題" data-type="title">{title}</span>
								</strong>
								<span class="data_title editable" data-title="內容" data-type="info">{text}</span>	
							</div>
							<h2>
							<div class="gallery-item">
								<img src="../uploads/q{topic_fromindex}/{optionImg}" alt="" width="75px" />
							</div>
							</h2>
						</div>
						{/topic_options}						
					</div>
				</div>
			</div>
		</div>
	</div>
	<br class="clear">
</div>