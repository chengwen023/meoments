<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<base href="" />
<title>PhaIcon後台管理</title>
<link href="css/style.default.css" rel="stylesheet" type="text/css">
<link href="css/style.dark.css" rel="stylesheet" type="text/css">
<link href="css/styles.css" rel="stylesheet" type="text/css">
<!--[if IE]> <link href="css/ie.css" rel="stylesheet" type="text/css"> <![endif]-->
<style type="text/css">.CRZ{table-layout:fixed;}.CRZ td,.CRZ th{overflow:hidden}.CRC{height:0px;position:relative;}.CRG{margin-left:-5px;position:absolute;z-index:5;}.CRG .CRZ{position:absolute;background-color:red;filter:alpha(opacity=1);opacity:0;width:10px;height:100%;top:0px}.CRL{position:absolute;width:1px}.CRD{ border-left:1px dotted black}</style>
<style type="text/css">.glyph {display: inline-block;text-align: center;padding: .75em;margin: .75em 1em .75em 1em;width: 38px;}.fs1 {font-size: 2em;}.widget [class*="icos-"] { float: none; }</style>

</head>
<link href="css/admin.css" rel="stylesheet" type="text/css">


<!--[if IE]> <link href="css/ie.css" rel="stylesheet" type="text/css"> <![endif]-->

<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>

<script type="text/javascript" src="js/libs/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/libs/fancybox/jquery.fancybox.js"></script>
<link rel="stylesheet" type="text/css"
	href="js/libs/fancybox/jquery.fancybox.css" media="screen" />
<style>
/*login*/
.input_div {
	display: none
}

.fancybox-inner .input_div {
	display: block;
}

INPUT:not([type="submit"]){
	height: 30px;
}

#recover INPUT[type="submit"] {
	margin-top: 15px;
}

.loginPic .loginActions A.logleft,.loginPic .loginActions A.logback {
	animation: fadeOutLeft .5s;
	-moz-animation: fadeOutLeft .5s;
	-webkit-animation: fadeOutLeft .5s;
	-o-animation: fadeOutLeft .5s;
}

.loginPic .loginActions A.logright {
	animation: fadeOutRight .5s;
	-moz-animation: fadeOutRight .5s;
	-webkit-animation: fadeOutRight .5s;
	-o-animation: fadeOutRight .5s;
}

.loginPic:HOVER .loginActions A.logleft,.loginPic:HOVER .loginActions A.logback
	{
	animation: fadeInLeft .5s;
	-moz-animation: fadeInLeft .5s;
	-webkit-animation: fadeInLeft .5s;
	-o-animation: fadeInLeft .5s;
	opacity: 1;
}

.loginPic:HOVER .loginActions A.logright {
	animation: fadeInRight .5s;
	-moz-animation: fadeInRight .5s;
	-webkit-animation: fadeInRight .5s;
	-o-animation: fadeInRight .5s;
	opacity: 1;
}

.loginWrapper form {
	animation: fadeIn .5s;
	-moz-animation: fadeIn .5s;
	-webkit-animation: fadeIn .5s;
	-o-animation: fadeIn .5s;
}

#login A.LoginUser IMG{
	border-radius: 5px;	
}

#login A.LoginUser:BEFORE {
	content: '';
	padding: 55px;
	position: absolute;
	left: 65px;
/* IE9 SVG, needs conditional override of 'filter' to 'none' */
background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIxMDAlIiB5Mj0iMTAwJSI+CiAgICA8c3RvcCBvZmZzZXQ9IjAlIiBzdG9wLWNvbG9yPSIjZmZmZmZmIiBzdG9wLW9wYWNpdHk9IjAuMSIvPgogICAgPHN0b3Agb2Zmc2V0PSI1MCUiIHN0b3AtY29sb3I9IiNmZmZmZmYiIHN0b3Atb3BhY2l0eT0iMC40Ii8+CiAgICA8c3RvcCBvZmZzZXQ9IjU1JSIgc3RvcC1jb2xvcj0iI2ZmZmZmZiIgc3RvcC1vcGFjaXR5PSIwIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
background: -moz-linear-gradient(-45deg,  rgba(255,255,255,0.1) 0%, rgba(255,255,255,0.4) 50%, rgba(255,255,255,0) 55%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, right bottom, color-stop(0%,rgba(255,255,255,0.1)), color-stop(50%,rgba(255,255,255,0.4)), color-stop(55%,rgba(255,255,255,0))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(-45deg,  rgba(255,255,255,0.1) 0%,rgba(255,255,255,0.4) 50%,rgba(255,255,255,0) 55%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(-45deg,  rgba(255,255,255,0.1) 0%,rgba(255,255,255,0.4) 50%,rgba(255,255,255,0) 55%); /* Opera 11.10+ */
background: -ms-linear-gradient(-45deg,  rgba(255,255,255,0.1) 0%,rgba(255,255,255,0.4) 50%,rgba(255,255,255,0) 55%); /* IE10+ */
background: linear-gradient(135deg,  rgba(255,255,255,0.1) 0%,rgba(255,255,255,0.4) 50%,rgba(255,255,255,0) 55%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1affffff', endColorstr='#00ffffff',GradientType=1 ); /* IE6-8 fallback on horizontal gradient */
	
}
</style>
<body>
	<div class="loginWrapper <?php echo $flipped; ?>">
		<!-- Current user form -->
		<form id="login">
			<div class="loginPic">
				<a class="LoginUser" href="javascript:void(0);" title=""><img
					src="http://images.gamebase.com.tw/gb_img/8/000/074/74328.jpg"
					alt="" width="110px"></a> <span><?php echo $username; ?></span>					
				<div class="loginActions">
					<div>
						<a href="javascript:void(0);" title="Change user" class="logleft"></a>
					</div>
					<div>
						<a href="javascript:void(0);" title="HomePage" class="logright"></a>
					</div>
				</div>
			</div>
		</form>

		<div class="input_div">
			<form action="login/validate" method="post">
				<h6>
					<img class="pageTitle" src="../favicon.ico" alt="" width="20px"
						style="margin-left: 21px; margin-top: 0px;"> <span
						class="pageTitle" style="float: right; margin-top: 0px;"><?php echo $username; ?></span>
				</h6>

				<input type="hidden" name="username"
					placeholder="Confirm your email" class="loginUsername"
					value="<?php echo $username; ?>"> <input type="password" name="password"
					placeholder="Password" class="loginPassword"> <input type="submit"
					name="submit" value="Login" class="buttonM bGreyish">
			</form>
		</div>



		<!-- New user form -->
		<form action="login/validate" id="recover" method="post">
			<div class="loginPic">
				<a href="#" title=""><img src="images/userLogin2.png" width="110px" alt=""></a>
				<div class="loginActions">
					<div>
						<a href="javascript:void(0);" title="" class="logback"></a>
					</div>
					<div>
						<a href="javascript:void(0);" title="HomePage" class="logright"></a>
					</div>
				</div>
			</div>

			<input type="text" name="username" placeholder="Your username"
				class="loginUsername"> 
			<input type="password" name="password"
				placeholder="Password" class="loginPassword"> 
			<input type="submit"
				name="submit" value="Login" class="buttonM bGreyish">


		</form>

	</div>
	<script>
$('#recover INPUT[name="username"]').focus();
$(".LoginUser").click(function() {
	$.fancybox.open($(".input_div"),{closeBtn:false});
	$('.input_div INPUT[name="password"]').focus();
});

$(".logback").click(function() {
  $( ".loginWrapper" ).removeClass("flipped");
  
});
$(".logleft").click(function() {
	  $( ".loginWrapper" ).addClass("flipped");
	  $('#recover INPUT[name="username"]').focus();
	});
</script>
</body>
</html>