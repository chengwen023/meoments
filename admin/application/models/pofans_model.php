<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *	Adonis
 *	後台預設資料讀取
 */

class Pofans_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
		
	}
	
	function schedule_to_published($add_input,$index_filed = 'auto_index'){
		$temp_index = $add_input[$index_filed];
		unset($add_input[$index_filed]);
		$this->db->insert('pofans_published', $add_input);
		
		$this->schedule_del_by_index($temp_index,$index_filed);
	}
	function schedule_to_failure($add_input,$index_filed = 'auto_index'){
		$temp_index = $add_input[$index_filed];
		unset($add_input[$index_filed]);
		$this->db->insert('pofans_failure', $add_input);
		$this->schedule_del_by_index($temp_index,$index_filed);
	}
	function schedule_del_by_index($index,$index_filed = 'auto_index'){
		$this->db->delete('pofans_schedule', array($index_filed => $index));
	}
	function pofans_fanslist($index = '',$field='*',$select_type = true){
		$this->db->flush_cache();
		$this->db->select($field,$select_type)->from('pofans_fanslist');
		if($index){
			$this->db->where('auto_index',$index);
		}
	
		$query = $this->db->get();
		$data = $query->result_array();
	
		if($index && is_array($data)) 	$data = array_pop($data);
		return $data;
	}
	
	function fanslist_bygroup($index = '',$field='*',$select_type = true){
		$this->db->flush_cache();
		$this->db->select($field,$select_type)->from('pofans_fanslist');
		if($index && is_array($index)){
			foreach($index as $fans_group)
				$this->db->or_like("fans_group",'"'.$fans_group.'"');
			$query = $this->db->get();
			$data = $query->result_array();
			return $data;
		}
		
		
		return false;
		
		
	}
	
	
	function check_plan_exist_and_insert($add_input){
		$result = array('success' => 'N', 'msg' => '');
		
		$this->db->flush_cache();
		$query = $this->db->select("*")
		->from("pofans_plan")
		->where("plan_time", $add_input['plan_time'])
		->get();
		$temp = $query->row_array();
		
		if($query->num_rows <1 ){
		
			$this->db->insert('pofans_plan', $add_input);
			$result['success'] = 'Y';
			$result['msg'] = '新增成功';
		}else{
			$this->db->where('plan_time', $add_input['plan_time']);
			$this->db->update('pofans_plan', $add_input);
			$result['success'] = 'Y';
			$result['msg'] = '修改成功';
		}
		return $result;
	}
	
	function check_fans_exist_and_insert($add_input){
		
	
		if($this->check_fans_exist($add_input['page_id']) == 0){	
			$this->db->insert('pofans_fanslist', $add_input);
			$result['success'] = 'Y';
			$result['msg'] = '新增成功';
		}else{
			$result['msg'] = '此名稱已經有人用了哦!!!';
		}
		return $result;
	}
	
	function check_fans_exist($page_id){
		$result = array('success' => 'N', 'msg' => '');
	
		$this->db->flush_cache();
		$query = $this->db->select("COUNT(*) AS qun")
		->from("pofans_fanslist")
		->where("page_id", $page_id)
		->get();
		$temp = $query->row_array();
	
		return $temp['qun'];
	}
}