<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Source extends CI_Controller {
	
	
	var $fans_info;
	
	
	public function __construct(){
		parent::__construct();
		
		include_once (FCPATH.'AutoLoad.php');
		include_once (FCPATH.'class_curl.php');
		include_once (FCPATH.'ClassFacebooApi.php');

		$this->userInfo = $this->session->userdata('userInfo');
		$this->load->model('theme_model', 'theme');
		$this->load->model('admin_model', 'admin');
		
		
		$this->data['admin_menu'] = $this->config->item('admin_menu');
		$this->data['admin_menuGroup'] = $this->config->item('admin_menuGroup');
		$this->data['userInfo'] = $this->userInfo;
		$this->data['menu'] = $this->config->item('admin_accountMenu');
		$this->data['breadcrumbs'] = $this->config->item('breadcrumbs');
		$this->data['title'] =$this->config->item('admin_title');
		
		$this->data['topbar_control']= array();
		$this->data['select_result'] = array();
	}

	public function index(){
		//$this->lists();
		$this->fansid_lists();
	}
	

	function fansid_lists($pg = 1, $user = ''){
		/*$defaultstatus= 'S';
		$activestatus = 'A';*/

		// $fans = graph_helper::url_api('https://zh-tw.facebook.com/appledaily.tw');
		// $fans = json_decode($fans, true);
		// echo $fans['link'];
		// exit;

		//$id = '388562611313743';
		//$secret = 'f70065795bd39240a57309316dca5969';
		//$input = 'oauth/access_token?client_id='.$id.'&client_secret='.$secret.'&grant_type=client_credentials';
		
		// $pofans = new pofans2_fb_crud();
		// echo "<pre>";
		// var_dump($pofans->token_debug(graph_helper::$test_token));
		// exit;

		$page_one = 20;

		$order_name = $this->input->post('order_by');
		$orderBy = $this->theme->order_by($order_name, 'facebook_post_source_list');
		$data = array_merge($this->data, $orderBy);

		$account_id = $this->config->item('admin_accountIndex');
		$group = $this->admin->get_groupIndex($account_id);
		/*$this->menu_index = 'I-Kuso 粉絲團管理';
		if(!$this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, 'V')){
			$this->load->view('template', $data);
			return false;
		}*/
		
		/*$data['topbar_control'][] = array('current_url'=>'JAVASCRIPT:fans.refresh();','i_class'=>'icon-refresh', 'control_name'=>'重載資料');
		$data['select_result'] = array();
		
		$data['title'] = $this->admin_model->getTitle($this->menu_index);*/
		

		$this->db->flush_cache();
		$this->db->select("*");
		$this->db->from("facebook_post_source_list");

		if ( is_array($orderBy) ){
			foreach($orderBy AS $key => $value){
				if(!$key) continue;
				if($value == "down") $this->db->order_by('facebook_post_source_list.'.str_replace('by-','', $key).' DESC');
				else $this->db->order_by('facebook_post_source_list.'.str_replace('by-','', $key).' ASC');
			}
		} else $this->db->order_by('id','ASC');
		
		$total_query = clone $this->db;
		$page['total_page'] = $total_query->get()->num_rows;
		$this->db->limit($page_one, ($pg - 1) * $page_one);

		$query = $this->db->get();
		$data['content'] = $query->result_array();
		foreach($data['content'] AS $key => $list){
			$data['content'][$key]['off'] = ($list['is_enable']) ? '' : 'off';
			$data['content'][$key]['language'] = ($data['content'][$key]['language'] == 1) ? '中' : '英';
		}
		
		$page['start'] = 1;
		$page['end'] = ceil ($page['total_page'] / $page_one);
		$page['url'] = 'source/fansid_lists';
		$page['page_num'] = $pg;
		$page['url_end'] = '/' . $user;
		$data['page_list'] = $this->theme->page($page);

		$data['group'] = $group;
		$data['main_content'] = $this->parser->parse('source/news_fans_list', $data, true);
		$this->parser->parse('template', $data);
	}
	
	/**
	 *	actions
	 */
	function actions($ac){
	
		switch($ac){
			case 'add_fans':
			
				$this->menu_index = '新增粉絲團';

				$language_list[] = array('num' => '1', 'language' => '中文');
				$language_list[] = array('num' => '2', 'language' => '英文');
				$data['test'] = 'test';
				$data['language_list'] = $language_list;
				// echo $this->load->view('source/fans_edit', $data);
				$this->parser->parse('source/fans_edit', $data);
				break;	
			case 'online':
				$index = $this->input->post('index');
				$online = $this->input->post('online');
				
				if($online == 'true') $add_input = array('is_enable' => '1');
				else $add_input = array('is_enable' => '0');
				
				$this->db->where('id', $index);
				$this->db->update('facebook_post_source_list', $add_input);
				break;
			case 'validate':
				$index = $this->input->post();
				$facebook = new pofans2_fb_crud;
				$fans = $facebook->graph_api('/'.$index['name'])->asArray();
				if ( !isset($fans['likes']) ) {
					echo 'N';
					break;
				}

				$this->db->flush_cache();
				$this->db->select("*");
				$this->db->from("facebook_post_source_list");
				$this->db->where("fb_group_id", $fans['id']);
				$query = $this->db->get();
				if ( !empty($query->result_array()) ) {
					echo 'N';
					break;
				}

				//$sql = "INSERT INTO `source_fans` (`id`, `url`, `online`) VALUES (NULL, '".$index['name']."', '1')";
				$sql = "INSERT INTO `facebook_post_source_list` (`fb_group_id`, `fb_url`, `fb_sitename`, `likes`, `talking_about_count`, `language`) 
						VALUES ('".$fans['id']."', '".$fans['link']."', '".$fans['name']."', '".$fans['likes']."', '".$fans['talking_about_count']."', '".$index['language_list']."')";
				mysql_query($sql);
				echo 'Y';
				break;
			case 'del':
				$index = $this->input->post('index');
				$result = $this->admin_model->del_fans_by_pk($index);
				
				header('Content-type: text/plain; charset=utf-8');
				echo json_encode($result);
				break;
		}
	}
}