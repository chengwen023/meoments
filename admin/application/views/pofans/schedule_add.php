<script src="js/tips.js"></script>
<script src="js/span_editor.js"></script>
<style>

#img_selectbox {
	display: none;
	width: 550px;
	height: 420px;
	text-align: center;
	background-color: #94b0dc;
}

.fb_view_control {
	background-color: #336699;
	color: #fff;
	height: 20px;
}

.fb_view_control SPAN {
	margin-right: 10px;
	padding: 2px 5px;
}

.fb_view_control SPAN:HOVER {
	cursor: pointer;
	color: #3366cc;
	background-color: #fff;
}

.fb_view {
	width: 500px;
	height: 375px;
	margin: auto;
	border: 1px solid red;
}

.fb_link {
	width: 500px;
	height: 260px;
}

.fb_link_s {
	width: 375px;
	height: 375px;
}

.topic_photo {
	border: 1px solid;
	box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3);
	padding: 7px 5px 7px 7px;
	margin-right: 15px;
	margin-bottom:15px;
	float: left;
	margin-top: 25px;
	position: relative;
}
.topic_photo:before{
	content: attr(data-title);
	position: absolute;
	top: -20px;
}
.topic_group {
	width: 340px;
	float: left;
	margin: 10px;
}

.topic_group STRONG {
	color: #000;
	float: right;
	cursor: pointer;
	border-bottom: 1px solid #999;
}

.topic_group UL {
	margin: 0;
	display: none;
}

.topic_group.editable UL.options_select {
	position: absolute;
	display: inline-block;
	margin-top: 30px;
	background-color: #fff;
	border: 1px solid #D6D6D6;
	border-radius: 10px;
	padding: 5px 5px;
	box-shadow: 3px 3px 4px rgba(0, 0, 0, 0.2);
	z-index: 10;
}

.topic_group UL LI {
	float: left;
	margin-right: 10px;
	cursor: pointer;
	width: 130px;
}

.topic_group UL LI:AFTER {
	content: ' ';
	padding: 10px 0px 0px 30px;
}

.topic_group UL LI:HOVER {
	color: #585858;
}

.topic_group UL LI:HOVER:AFTER {
	background: url(images/check_green.png) no-repeat left;
}

.topic_group #topic_group_name UL {
	display: block;
	width: 450px;
	float: left;
}

.topic_group #topic_group_name UL LI.checked:AFTER {
	background: url(images/check_green.png) no-repeat left;
}

.audit_left.not_pass:BEFORE {
	content: ' ';
	padding: 10px 0px 10px 30px;
	background: url(images/not_pass.gif) no-repeat left;
	position: absolute;
	margin-left: -30px;
}

.audit_right.not_pass:AFTER {
	content: ' ';
	padding: 10px 0px 10px 30px;
	background: url(images/not_pass.gif) no-repeat left;
}
.topic_photo.not_pass:BEFORE {
	content: attr(data-title);
	padding: 0px 0px 00px 30px;
	background: url(images/not_pass.gif) no-repeat left;
}
.topic_photo.not_pass:AFTER{
	display: none;;
}
.data_title:BEFORE {
	content: attr(data-title);
	position: absolute;
	color: #000;	
	font-weight: bolder;
}

input[type="text"] {
	width: 100%;
	font-size: 18px;
	height: 24px;
	margin-bottom: 5px;
}

input[type="text"]:HOVER {
	background-color: rgba(33, 153, 233, 0.3);
}

input[type="text"].empty {
	border-color: rgba(241, 193, 154, 1);
}
IMG.errorsize{
	/*border-color: rgba(230, 128, 45, 1); */
	border:3px solid red; 
	 

}

.topic_photo.audit_right.dropping:AFTER {
	content: ' ';
	background-color: rgba(33, 33, 33, 0.3);
	width: 114px;
	position: absolute;
	height: 91px;
	border: 1px solid red;
	margin-top: -83px;
	margin-left: -9px;
}

.topic_photo.audit_left.dropping:AFTER {
	content: ' ';
	background-color: rgba(33, 33, 33, 0.3);
	width: 114px;
	position: absolute;
	height: 91px;
	border: 1px solid red;
	margin-top: -83px;
	margin-left: -9px;
}

.topic_option_list {
	counter-reset: option_div;
}

.topic_option_list .page-content.dropimg-content:BEFORE {
	counter-increment: option_div;
	content: counter(option_div);
	font-size: 135px;
	position: absolute;
	margin-top: 60px;
	color: #fff;
	text-shadow: 1px 2px 4px rgba(199, 199, 199, 1);
	font-family: sans-serif;
}
.page-content.dropimg-content[data-insert="insert"]:BEFORE {
	content: ' ';
	padding: 50px;
	background: url(images/check_blue_f.png) no-repeat;
	position: absolute;
	z-index: 5;
	margin: -10px 20px;
	right: 0;
}
.dropimg-content.dropping:AFTER {
	content: ' ';
	background-color: rgba(33, 33, 33, 0.3);
	width: 100%;
	position: absolute;
	height: 180px;
	border: 1px solid red;
}

#img_control {
	background: url(images/arrow_right.png) no-repeat center;
	padding: 40px 100px;
	width: 735px;
	/* height: 200px; */
	position: absolute;
	z-index: 1000;
	background-color: rgba(33, 33, 33, 0.3);
}

.topic_photo #img_control {
	margin-left: -90px;
	left: 0;
}
.audit_right #img_control {	
	left: -483px;
}
#img_control #old_img {
	float: left;
	padding: 8px;
	background-color: #f6f6f6;
	border: 1px dashed #b2b2b2;
	-webkit-box-shadow: 3px 3px 4px rgba(0, 0, 0, 0.2);
	-moz-box-shadow: 3px 3px 4px rgba(0, 0, 0, 0.2);
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
}

#img_control #new_img {
	float: right;
	padding: 8px;
	background-color: #f6f6f6;
	border: 1px dashed #b2b2b2;
	-webkit-box-shadow: 3px 3px 4px rgba(0, 0, 0, 0.2);
	-moz-box-shadow: 3px 3px 4px rgba(0, 0, 0, 0.2);
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
}

#img_control DIV#control_btn {
	top: 0;
	right: 0;
	position: absolute;
	color: #fff;
	font-size: 24px;
	overflow: hidden;
	width: 50px;
	height: 100%;
}

SPAN.text_editable{
	padding:2px 10px; 
	padding-right: 20px;
	display: inline-block;
	min-width: 120px;
}
SPAN.text_editable:HOVER,SPAN.text_editable:FOCUS{
	border-bottom: 1px solid rgb(102,153,204);
}

.ie-icon:BEFORE {
	width: 32px;
	height: 32px;
}

.ie-icon:HOVER:BEFORE {
	margin-left: -5px;
	padding: 5px;
	border-radius: 5px;
	background: rgba(0, 0, 0, 0.5);
}


#upload_file::-webkit-file-upload-button {
  visibility: hidden;
}
#upload_file::before {
	content: ' ';
	display: inline-block;
	background: url("images/uploadfile.png") no-repeat left center;
	padding: 30px;
	cursor:pointer;
    position: absolute;
}
.topic_insert{
	font-size: 28px;
	cursor: pointer;
	color:rgb(35, 35, 35);
}
.topic_insert:HOVER{	
	color:rgb(255, 113, 9);
}
</style>

<div class="fancybox-content">
	<div class="fancybox-content-bg-bottom">
		<div class="fancybox-header" style="min-height: 400px;">
			<ul>		
			<li class="{active}">						
				<span class="icon-publish topic_insert" onclick="topic_insert('');" tips="新增" ></span>				
			</ul>
			
			<br class="clear">

			<div class="fancybox-logo" style="color: #919191;">
				<div style="width: 120px;float: left;">
				<span data-title="連結圖片" class="topic_photo audit_left " tips="連結對應圖片"> 
					<img data-name="topic_imgurl" class="editable" alt=""src="http://goquiz88.com/img/goquiz88_facebook_share.jpg" width="100px;">
				</span>				
				<span data-title="FACEBOOK" class="topic_photo audit_left " tips="FACEBOOK圖片格式"> 
					<img data-name="facebook_imgurl" class="editable" alt="" src="http://goquiz88.com/img/goquiz88_facebook_share.jpg" width="100px;">
				</span>
				</div>
				<div style="float: left; width: 350px; margin-right: 10px;margin-top: 30px;">
					<span id="page_id" contenteditable="true" class="text_editable data_tips" data-title="請輸入粉絲團ID" value="" data-name="page_id"></span>
					<select onchange="change_fansid(this.value);">
						<option value="" selected="selected">選擇粉絲團</option>
						{fans_list}
						<option value="{page_id}">{page_name}</option>
						{/fans_list}
					</select>
				
					<span id="link" contenteditable="true" class="text_editable data_tips" data-title="請輸入連結" style="display: block;"value="" data-name="link"></span>
					<input data-name="post_time" type="datetime-local" name="post_time" tips="發佈時間">
					<textarea id="message" data-name="message" data-tips="message" tips="請輸入顯示訊息" style="height: 150px;" onchange="$(this).html(this.value);"></textarea>

				</div>
				
				<br class="clear">
			</div>
		</div>
		
		
	</div>
	<br class="clear">
	
</div>


<script>

$(function() {
	var config = {			
			filed_name : ['data-title','tips'],	
			class_Name : 'tips_box',
			tipbox_name: "tip_box",		
	}



	tips = new Tips(config);

	
	
	set_span_editor();
	$("SPAN.text_editable").attr('contenteditable',true).each(function(){
		if(!$(this).html() || $(this).html() == '')  {
			$(this).html($(this).attr('data-title'));
			
		}

		
		}).on("focus",function(){
			if( $(this).html() == $(this).attr('data-title')) $(this).html('');
		}).on("blur",function(){
			if(!$(this).html() || $(this).html() == '') $(this).html($(this).attr('data-title'));
		}).on("keyup",function(){
			var text = $(this).html();
			
		});
	
});
function change_fansid(val){
	if(val){
		$('#page_id').html(val);
	}
}

function topic_insert(){
	var fans_list = get_post_val();
	console.info(fans_list);

	if(confirm("是否確定新增資料? ")){
		url = 'pofans/schedule_add'; 
		
		
		$.ajax({
			url: url,
		  cache: false,
			dataType: 'html',
		  type: 'POST',
		  data:{
			  fans_list:fans_list
		  },
		  success: function(data){	
			  console.info(data);
		  }
		});
	}else if(fans_list.length<1){
		
	}
}

function get_post_val(){

	var post_id = new Array('page_id','message','link','post_time');
	
	var schedule = {
			  "page_id": '',
			  "message": '',
			  "link": '',
			  "post_time": '',
			}
	for( var key in post_id){		

		var temp = $('[data-name="'+post_id[key]+'"').attr('data-title');
		
		if(typeof(temp) != 'undefined' && $('[data-name="'+post_id[key]+'"').html() == temp) {
			console.info(post_id[key]);	
				return false;
		}
		
		if($('[data-name="'+post_id[key]+'"').get(0).tagName.toLowerCase() == "img") schedule[post_id[key]] = $('[data-name="'+post_id[key]+'"').attr("src");
		if($('[data-name="'+post_id[key]+'"').get(0).tagName.toLowerCase() == "span" ) schedule[post_id[key]] = $('[data-name="'+post_id[key]+'"').html();
		if($('[data-name="'+post_id[key]+'"').get(0).tagName.toLowerCase() == "input" ) schedule[post_id[key]] = $('[data-name="'+post_id[key]+'"').val();
		if($('[data-name="'+post_id[key]+'"').get(0).tagName.toLowerCase() == "textarea" ) schedule[post_id[key]] = $('[data-name="'+post_id[key]+'"').html();

	}
	
	return schedule;
	
}

















</script>