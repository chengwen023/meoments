<style>
#choose_list LI INPUT[type="text"],#choose_list LI TEXTAREA{
	width:350px;
}
#choose_list LI TEXTAREA{
	height:80px;
}

#choose_list LI{
	clear:both;
	width: 500px;		
}

#choose_list LI DIV:not([class="imgurl_box"], [class="imgurl_box imgedit_focus"]){
	width:400px;
}

#choose_list LI DIV{
	float: right;
}

#choose_list LI DIV.del_btn{
	float:none;
	position: relative;
	cursor: pointer;
	text-align: center;
	width:80px;	
	border: 1px solid #D17A1C; 
	color:#D17A1C;
	margin-left: 415px;
	margin-bottom: 10px;
}

#choose_list LI DIV.del_btn:HOVER{
	background-color:#D17A1C;
	color: #fff;
}
.imgurl_box{
	width:80px;
	height: 60px;
	border-style: dotted;
	text-align: center;
	line-height: 60px;	
	cursor: pointer;
	margin-left: 20px;
	
}

.imgurl_box.imgedit_focus{
	color:#D17A1C;
}

.submit_btn{	
	color: #66a3d3;	
	border-color: #66a3d3;
}
.submit_btn:HOVER{	
	color: #fff;
	background-color: #678197;
	border-color: #678197;
}
</style>


	
	<div id="topic_info" style="width:1200px;">
		<form id="form_folder" onsubmit="return false">
		<div class="tb-green" style="width:600px;float: left;">
			<table>
				<thead>
					<tr class="column1">
						<th scope="col" colspan="3">
							<?php echo (!empty($topic['auto_index']) && $topic['auto_index'] > 0)? '編輯':'新增';?>資料
							<input type="hidden" name="ifunso_test_fk" value="<?php echo (!empty($topic['tests_question_fk'])?$topic['tests_question_fk']:'');?>">
							<input type="submit" class="submit_btn" value="新增完成" style="float:right ;padding:3px;font-size:14px;" />							
						</th>
					</tr>
				</thead>
				<tr>
					<td width="66">
						題目
					</td>
					<td>
						
						<input style="width: 100%;" type="text" name="topic" value="<?php echo empty($topic['topic'])? '':$topic['topic'];?>" />
						
						<div >
						<?php echo$topic['topic_description'];?>
						</div>
					</td>
					<td>						
						<div class="imgurl_box" ><span>
							<?php if(!empty($topic['topic_imgurl'])):?>
							<img src="uploads/temp/<?php echo $topic['topic_imgurl']; ?>" width="80px" />
							<?php else:?>
							<img src="uploads/default_pic.jpg" width="70px" />
							<?php endif;?>
							</span>
							<input name="topic_imgurl" type="hidden" value="<?php echo $topic['topic_imgurl']?>"/>
							<a href="uploads/temp/<?php echo $topic['topic_imgurl']; ?>" target="_blank">開啟原圖</a>
						</div>
					</td>
				</tr>				
				<tr>
					<td width="66">
						分類
					</td>
					<td colspan="2">
						<ul style="width: 500px;list-style: none;">
						<?php foreach($topic_group AS $value):?>
						<li style="float: left;margin-right: 10px;">
						<input class="fans_group" name="topic_group[]" type="checkbox" value="<?php echo $value['auto_index'];?>"  <?php echo (isset($value['checked']) && $value['checked'])?'checked="checked"':'';?> /><?php echo $value['name'];?>
						</li>
						<?php endforeach;?>
						</ul>
					</td>
				</tr>
				<tr>
					<td width="66">
						選項指令
					</td>
					<td >
						<span style="padding: 3px 20px;border-bottom: 1px solid #394A58;">
							ifunso_Tests
						</span>												
						<input type="hidden" name="options_select" value="<?php echo (!empty($topic['options_select_hide'])?$topic['options_select_hide']:'');?>">
						<div>
						<textarea style="width:100%;" name="topic_optiontitle"><?php echo empty($topic['topic_optiontitle'])?'':$topic['topic_optiontitle'];?></textarea>						
						</div>
					</td>
					<td>						
						<div id="options_imgurlbox" class="imgurl_box" >
							<span>
							<?php if(!empty($topic['options_imgurl'])):?>
							<img src="uploads/temp/<?php echo $topic['options_imgurl']; ?>" width="80px" />
							<?php else:?>
							<img src="uploads/temp/default_pic.jpg" width="70px" />
							<?php endif;?>
							</span><input name="options_imgurl" type="hidden" value="<?php echo empty($topic['options_imgurl'])? 'default_pic.jpg':$topic['options_imgurl'];?>"/></div>
					</td>
				</tr>				
			</table>
			</div>
			<div class="tb-green"style="width:550px;height:600px;float: right;">
			<table>
				<thead>
					<tr class="column1">
						<th scope="col" >
							題目選項
						</th>
					</tr>
				</thead>
				<tr>
					<td width="66">					
							<ol id="choose_list">
							<?php foreach($topic_options AS $value):?>
									
								<li>
									<div class="imgurl_box" ><span><img src="uploads/temp/<?php echo $value['text_imgurl']?>" width="70px" /></span>
										<input type="hidden" name="options[text_imgurl][]" value="<?php echo $value['text_imgurl']?>" /></div>
									<div>
										選項<input type="text" name="options[choose][]" value="<?php echo $value['choose']?>"/><br/>
										標題<input type="text" name="options[title][]" value="<?php echo $value['title']?>" /><br/>
										內容<textarea name="options[text][]"><?php echo $value['text']?></textarea>											
									</div>
								</li>
							<?php endforeach;?>
							</ol>							
						</td>						
					</tr>				
				</table>
			</div>	
			</form>
			<div id="img_selectbox" style="padding-top:15px;width:595px;height:600px;float: left;overflow:scroll;">
			<div id="upload_img" class="control_imgtype" style="float:left;display: none;">
				<input type="file" name="file_upload" id="img" />
			</div>
			</div>
		</div><!-- / tb-green-->
		
			
			
		
	
	<div id="img_clipbox" style="padding-top:15px;width:500px;float: left;overflow:visible;"></div>




<script>





var default_inputs = '';

var site = '<?php echo $this->config->item('base_url'); ?>';
upload_clip(site, 'img');
set_imgurlbox();

function set_imgurlbox(){
	$(".imgurl_box").live( "click", function() {
		$(".imgurl_box").removeClass("imgedit_focus");
		$(this).addClass("imgedit_focus");
		$(this).children('INPUT[type="hidden"]').val('');
		img_select(this);
		return false;
	});	
	
}

function img_select(this_div){

	$("#upload_img").show();

}

function upload_clip(site, target_box){
	// 上傳圖片
	$("#"+target_box).uploadify({
		'buttonCursor'		: 'hand',
		'fileTypeDesc'		: 'Image Files',
    'fileTypeExts'		: '*.gif; *.jpg; *.png',
    'buttonText' 			: '選擇上傳',
    width							: 68,
    height						: 24,
		'multi'    				: false,
	  'swf'        			: site+'js/libs/uploadify/uploadify.swf',
	  'uploader'			  : site+'_admin/uploads/queImg',
    'onUploadSuccess' : function(file, data, response){
    	tmp = data.split(",");
    	if(tmp[0] != 'N'){
        	
	    	item = site+'uploads/source/'+tmp[0];
	  		
				$.ajax({
					url: '_admin/choose/actions/upload_img',
				  cache: false,
				  dataType: 'html',
				  async : false,
				  type: 'POST',
				  data : 'img='+item+'&width='+tmp[2]+'&height='+tmp[3],
				  success: function(data){
					  $('#topic_info').hide();
				  	  $('#img_clipbox').html(data);
				  }
				});
			}else{
				alert('上傳圖片最小為 500 x 375 !!');
			}
    }
	});
}
//確認選好圖片，進入裁圖畫面
function img_check(){
	if($('.img_select').length < 1){
		alert('圖片最少選擇一張!!');
		return false;
	}else if($('.img_select').length > 1){
		alert('圖片最多只能選擇一張!!');
		return false;
	}
	
	$.ajax({
		url: '_admin/choose/actions/check_img',
	  cache: false,
	  dataType: 'html',
	  async : false,
	  type: 'POST',
	  data : 'img='+$('.img_select').attr('rel')+'&alt='+$('.img_select').attr('alt')+'&item='+$('input[name="search_item"]').val()+'&pg='+$('.img_pg').attr('rel'),
	  success: function(data){		  
	  	tmp = data.split(',');
	  	if(tmp[0] == 'N'){
	  		alert(tmp[1]);	  		
	  	}else{
	  		$('#topic_info').hide();
	  		$('#img_clipbox').html(data);
	  		
		  }
	  }
	});
}


function chooseType_changed(type){
	if(type=='2'){
		$("#options_imgurlbox").show();
	}else{
		$("#options_imgurlbox").hide();
	}
	
	if(type == '1' || type == '2'  ) $('#choose_addbtn').show();		
	else {
		$('#choose_addbtn').hide();
	}	

	$.ajax({
		  url: '_admin/topic/choose_data/',
		  dataType: 'html',
		  type: "POST",
		  data: {
			  type:type
		  },
		  success: function(data){

			  $('#choose_list').html(data);
			  default_inputs = data;
			  set_imgurlbox();
		  	/**/
		  }
		});
}

function add_choose(){
	if(default_inputs != ''){
		$('#choose_list').children('li').last().after(default_inputs);
		set_imgurlbox();	
	}
	else{
		var selectType_index = $('INPUT[name="options_select"]').val();
		if(selectType_index<1)return false;
		$.ajax({
			  url: '_admin/topic/choose_data/',
			  cache: false,
			  dataType: 'html',
			  type: "POST",
			  data: {
				  type:selectType_index
			  },
			  success: function(data){

				  
				  default_inputs = data;
				  $('#choose_list').children('li').last().after(default_inputs);
				  set_imgurlbox();
			  	/**/
			  }
			});
	}
	
	
}


	$(function(){
		
		// 註冊驗證
		$("#form_folder").validate({	
			rules: {
				topic: {
					required: true
				},
				options_select: {
					required: function(element) {
						if( $(element).val() < 1 ){
							alert('請選擇選項類型!');
							$(element).focus();							
						}
						return true;
                    }
				},
				'options[choose][]': {
					required: true
				},
				'options[title][]': {
					required: true
				},'options[text][]': {
					required: true
				}				
			},
			messages: {
				topic: "題目資料不得為空",									
				'options[choose][]': "選項資料不得為空",
				'options[title][]': "選項資料不得為空",
				'options[text][]': "選項資料不得為空"					
			},		
			submitHandler: function(form) {				
				// 透過 Ajax 驗證是否註冊
				$.ajax({
				  url: '_admin/topic/actions/validate',
				  cache: false,
				  dataType: 'json',
				  type: "POST",
				  data: $("#form_folder").serialize(),
				  success: function(data){		
					  	
				  	if(data.success == 'Y' ){
				  		alert(data.msg);
				  		location.reload();
				  	}else{
				  		alert(data.msg);
				  		$('input[name="id"]').focus();
				  		return false;
				  	}
				  	/**/
				  }
				});
				//form.submit();
				return false;
			}
		});
		// end of $("#signupForm").validate
	});
</script>