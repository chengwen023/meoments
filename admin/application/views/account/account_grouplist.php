<div class="widget">
	<table cellpadding="0" cellspacing="0" width="100%" class="tLight">
		<form id="list_orderby" method="post">
		<input id="order_by" type="hidden" name="order_by" value="">
		<thead>
			<tr>
				<td title="群組名稱" class="order_control" data-orderby = "group_name">群組名稱<i class="icon-chevron-{by-group_name}"></i></td>
				<td title="預設權限" class="order_control" data-orderby = "group_defaultstatus">預設權限<i class="icon-chevron-{by-group_defaultstatus}"></i></td>
				<td title="新增資料" width="150px;" style="cursor: pointer;" onclick="account.edit_group('');">新增資料<i class="icon-plus-sign"></i></td>
			</tr>
		</thead>
		</form>
		<tbody>
			{content}
			<tr id="user_{auto_index}" class="user_row">
				<td>{group_name}</td>
				<td>{purview}</td>
				<td>
				<a href="javascript:void(0);" class="tablectrl_small bGreen" original-title="Edit" title="修改" alt="修改"><span class="iconb" data-icon="" onclick="account.edit_group({auto_index});"></span></a>
				</td>				
			</tr>
			{/content}
		</tbody>
	</table>
</div>

<script src="js/account.js"></script>
<script>
function order_by(this_td){

	$('#order_by').val( $(this_td).attr('data-orderby'));
	$('#list_orderby').submit();

}

$(".order_control").live( "click", function() {
	var order_name = $(this).attr('data-orderby');
	if(order_name) order_by(this);
	
	return false;
});	
</script>
