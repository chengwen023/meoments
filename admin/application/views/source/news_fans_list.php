<div class="widget">
	<table cellpadding="0" cellspacing="0" width="100%" class="tLight">
		<form id="list_orderby" method="post">
		<input id="order_by" type="hidden" name="order_by" value="">
		</form>
		
		<thead>
			<tr>
				<td title="名稱">名稱</td>
				<td title="讚數" class="order_control" data-orderby="likes">讚數<span><i class="icon-chevron-{by-likes}"></i></span></td>
				<td title="討論數" class="order_control" data-orderby="talking_about_count">討論數<span><i class="icon-chevron-{by-talking_about_count}"></i></span></td>
				<td title="語言" class="order_control" data-orderby="language">語言<span><i class="icon-chevron-{by-language}"></i></span></td>
				<td class="order_control group_{group}" title="動作" class="order_control" style="width: 120px;" data-orderby="is_enable"><span style="width:60px;">動作<i class="icon-chevron-{by-is_enable}"></i></span><i class="icon-plus-sign"></i></td>					
			</tr>
		</thead>
		
		<tbody>
			{content}
			<tr id="user_{id}" class="user_row">
				<td><a href="{fb_url}" target="_blank">{fb_sitename}</a></td>
				<td>{likes}</td>
				<td>{talking_about_count}</td>
				<td>{language}</td>
				<td class="group_{group}">
					<!--
					<a href="javascript:void(0);" class="tablectrl_small bGreen"
					original-title="Edit" title="修改" alt="修改"><span class="iconb"
						data-icon="" onclick="fans.group_edit({id});"></span></a>
					-->
					<span class="online {off}" data-index="{id}"> 
						<a href="javascript:void(0);" class="tablectrl_small bBlue" original-title="Edit" title="上架中"
							alt="上架中">上架中<span class="iconb" data-icon=""></span></a>
						<a href="javascript:void(0);" class="tablectrl_small bGold" original-title="Edit" title="下架中" 
							alt="下架中">下架中<span class="iconb" data-icon="">
					</span></a> 
							
				</span></td>
			</tr>
			{/content}
		</tbody>
	</table>
</div>	
{page_list}
<style>
.online A {
	position: relative;
}

.online A:first-child {
	z-index: 100;
}

.online A:not(:first-child ) {
	display:none;
	!left: -53px;
	z-index: 1;
	opacity: 0;
	filter: alpha(opacity = 0);
	-moz-transform: rotateY(180deg);
	-webkit-transform: rotateY(180deg);
	transform: rotateY(180deg);
}

.online.off A:first-child {
	display: none;
	opacity: 0;
	filter: alpha(opacity = 0);
	-moz-transform: rotateY(-180deg);
	-webkit-transform: rotateY(-180deg);
	transform: rotateY(-180deg);
}

.online.off A:not(:first-child ) {
	display:inline;
	opacity: 1;
	filter: alpha(opacity = 100);
	-moz-transform: rotateY(0deg);
	-webkit-transform: rotateY(0deg);
	transform: rotateY(0deg);
}

.group_2,
.group_3 {
	display: none;
}

.more_page{
	text-align: center;
}

.more_page SPAN {
	-moz-box-shadow:inset 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;
	box-shadow:inset 0px 1px 0px 0px #ffffff;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #dfdfdf));
	background:-moz-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
	background:-webkit-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
	background:-o-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
	background:-ms-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
	background:linear-gradient(to bottom, #ededed 5%, #dfdfdf 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#dfdfdf',GradientType=0);
	background-color:#ededed;
	-moz-border-radius:6px;
	-webkit-border-radius:6px;
	border-radius:6px;
	border:1px solid #dcdcdc;
	display:inline-block;
	cursor:pointer;
	color:#777777;
	font-family:arial;
	font-size:15px;
	font-weight:bold;
	padding:6px 14px;
	text-decoration:none;
	text-shadow:0px 1px 0px #ffffff;
}
.more_page SPAN.page_selected {		
	color:#c92200;
	text-shadow:0px 1px 0px #ded17c;
}

.more_page SPAN:hover {
	color:#ffffff;
	text-shadow:0px 1px 0px #528ecc;
	-moz-box-shadow:inset 0px 1px 0px 0px #bbdaf7;
    -webkit-box-shadow:inset 0px 1px 0px 0px #bbdaf7;
    box-shadow:inset 0px 1px 0px 0px #bbdaf7;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #378de5), color-stop(1, #79bbff));
	background:-moz-linear-gradient(top, #378de5 5%, #79bbff 100%);
	background:-webkit-linear-gradient(top, #378de5 5%, #79bbff 100%);
	background:-o-linear-gradient(top, #378de5 5%, #79bbff 100%);
	background:-ms-linear-gradient(top, #378de5 5%, #79bbff 100%);
	background:linear-gradient(to bottom, #378de5 5%, #79bbff 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#378de5', endColorstr='#79bbff',GradientType=0);
	background-color:#378de5;
	
	border:1px solid #84bbf3;
}
.more_page SPAN:active {
	position:relative;
	top:1px;
}
</style>

<script src="js/fans.js"></script>

<script>
$(".online").click(function() {
	var index = $(this).attr('data-index'); 
	var online = $(this).hasClass("off")

	if(online) $(this).removeClass("off");
	else $(this).addClass("off");

	if(index){
		$.ajax({
		  url: 'source/actions/online',
		  dataType: 'html',
		  type: "POST",
		  data: {
			  online:online,
			  index:index
		  },
		  success: function(data){
			  console.info(data);
		  }
		});
	}
});

//新增粉絲團
$(".order_control i").click(function() {
	console.log('s');
	url = 'source/actions/add_fans';
	 
	$.fancybox.showLoading();
	$.ajax({
		url: url,
	  cache: false,
		dataType: 'html',
	  type: 'POST',
	  data:{
		
	  },
	  success: function(data){
	  	$.fancybox.hideLoading();
	  	$.fancybox.open(data);
	  }
	});
	
});

function order_by(this_td){

	$('#order_by').val( $(this_td).attr('data-orderby'));

	$('#list_orderby').submit();

}

$(".order_control span").live( "click", function() {	
	var order_name = $(this).parents().attr('data-orderby');
	if(order_name) order_by($(this).parents());
	return false;
});	

</script>