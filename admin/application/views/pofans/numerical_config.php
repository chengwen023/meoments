<style type="text/css">
.section div {
	margin: 5px 10px;
}

.section div input {
	margin: 15px 0;
}

.section div div {
	width: 150px;
  	display: inline-block;
  	font-size: 14px;
}

.section {
	border: 2px solid rgba(70,130,180,.8);
	width: 450px;
	padding: 10px 0;
}

.title {
	margin: 50px 0 2px 10px;
	font-size: 16px;
	font-weight: bold;
}
</style>


<div class="title">POFANS熱門設定</div>
<div class="pofans section">
	{pofans}
	<div><div>{memo}：</div><input name="{code}" value="{value}"></div>
	{/pofans}
</div>

<div class="title">前台廣告設定</div>
<div class="content section">
	
	{content}
	<div><div>{memo}：</div><input name="{code}" value="{value}"></div>
	{/content}
</div>



<script type="text/javascript">
$("input").on("change", function() {
	var value = $(this).val();
	var field = $(this).attr('name');
	var url = 'pofans/numerical_config_update';
	$.ajax({
		url: url,
		cache: false,
		dataType: 'html',
		type: 'POST',
		data:{
			value: value,
			field: field
		},
		success: function(data){
			console.info('修改成功')
		},error: function(data){
			alert('修改失敗');
		}
	});
});	
</script>