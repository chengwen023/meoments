<?php 
class ArticleGroup extends \Phalcon\Mvc\Model
{
	public $account_index;
	
	public function initialize()
	{
		
	}
	
	public function beforeValidationOnCreate()
	{
		
		$this->horo_fans = false;
		$this->online = '0';
		$this->create_time = date('Y-m-d H:i:s');
		$this->update_time = date('Y-m-d H:i:s');
		if($this->account_index){
			$this->create_account_index = $this->account_index;
			$this->update_account_index = $this->account_index;
		}
	
	}
	
	public function beforeValidationOnUpdate()
	{
			 	
		$this->update_time = date('Y-m-d H:i:s');
		if($this->account_index){
			$this->update_account_index = $this->account_index;
		}
	}
	
	public function setAccountIndex($index){
		$this->account_index = $index;
	
	}
}

?>