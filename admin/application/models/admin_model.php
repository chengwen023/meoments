<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *	Adonis
 *	後台預設資料讀取
 */

class Admin_model extends CI_Model {
	var $userInfo;
	var $data;
	var $menu_index;
	
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->config('common_config', FALSE, TRUE);
		$this->userInfo = $this->session->userdata('userInfo');
		
		if(isset($this->userInfo['auto_index'])) $login_index = $this->userInfo['auto_index'];
		
		
		if( $this->config->item('admin_accountIndex') < 1 && isset($login_index) && $login_index > 0){			
			$this->config->set_item('admin_accountIndex', $login_index);
			$this->_adminGroup_load();
			$this->_adminMenu_load();
		}else if(false === strpos(uri_string(),"user/login")  && $this->config->item('admin_accountIndex') < 1 ){			
			redirect(base_url('user/login'), 'refresh');
		}
		
		
	}
	
	/**
	 *	後台帳號資料讀取
	 */
	
	function _adminGroup_load(){
		
		$data = $this->get_all_group_lists('online');
		
		foreach ($data AS $value){			
			$admin_group[$value['auto_index']] = $value['group_name'];
			if($this->userInfo['admingroup_index'] == $value['auto_index']) 
				$this->config->set_item('defaultstatus',$value['group_defaultstatus']);
				
		}
		
		$this->config->set_item('admin_group',$admin_group);
		
		
		
			
		//目前帳號的選單
		$this->db->flush_cache();
		if(isset($this->userInfo['auto_index'])){
			$this->db->select('competence')->from('admin_account')->where('online','1')->where('auto_index',$this->userInfo['auto_index'])->limit(1);
			$query = $this->db->get();
			$data = $query->result_array();
			if(is_array($data)) {
				$data = array_pop($data);
				$competence_menu = array_keys(json_decode($data['competence'],true));
					
				$this->db->select('group_name')->from('admin_menu')->where('online','1')->where_in('auto_index',$competence_menu)->group_by("group_name");
				$query = $this->db->get();
				$data = $query->result_array();
				$index = 1;
				foreach($data AS $key => $menu){
					$data[$key]['group_index'] = $index++;
					$this->db->select('child_name,url')->from('admin_menu')->where('online','1')->where_in('auto_index',$competence_menu)->WHERE("group_name",$menu['group_name']);
					$query = $this->db->get();
					$data[$key]['childmenu'] = $query->result_array();
					$childindex = 1;
					foreach($data[$key]['childmenu'] AS $ckey => $cmenu){
						$data[$key]['childmenu'][$ckey]['url'] = str_replace('_admin', '/admin', $cmenu['url']);
						
						if($cmenu['url'] == uri_string()) $data[$key]['childmenu'][$ckey]['add_class'] = 'active';
						$data[$key]['childmenu'][$ckey]['child_index'] = $childindex++;
					}
					
				}
				$this->config->set_item('admin_accountMenu',$data);
			}
		}
		
		//目前頁面資料
		
		$this->db->select('*')->from('admin_menu')->where('online','1')->where('url',uri_string())->limit(1);
		$query = $this->db->get();
		if(null === $menu = array_pop($query->result_array())) return false;
		
		
		$breadcrumbs[] = $menu;
		$this->config->set_item('breadcrumbs',$breadcrumbs);
		$this->config->set_item('admin_title',$menu['child_name']);
		
	}
	
	function _adminMenu_load(){
		
		$this->db->select('*')->from('admin_menu')->where('online','1')->order_by('group_name','ASC');
		if(isset($menu_index) && is_array($menu_index)){
			$this->db->where_in('auto_index',$menu_index);
		}
		
		
		$query = $this->db->get();
		$data = $query->result_array();
		
		$this->config->set_item('admin_menu',$data);
		$group_arr = array_keys(json_decode($this->userInfo['competence'],true));
		
		if(isset($group_arr) && is_array($group_arr)){
			$this->db->select('group_name')->from('admin_menu')->where_in('auto_index',$group_arr)->group_by("group_name");
			$query = $this->db->get();
			$data = $query->result_array();
			foreach($data AS $value){
				$group[] = $value['group_name'];
			}
		}
			
		
		$admin_menuGroup = $this->config->item('admin_menuGroup');
		
		foreach($admin_menuGroup AS $key => $check_group){
			if(isset($group) && is_array($group) && !in_array($check_group, $group)) unset($admin_menuGroup[$key]);
		}
		
		
		$this->config->set_item('admin_menuGroup',$admin_menuGroup);
		

	}
	
	function getTitle($menu_index){
		$this->db->flush_cache();
		$this->db->select("*");
		$this->db->from("admin_menu");
		$this->db->where("child_name", $menu_index);
		
		$query = $this->db->get();
		$data = $query->result_array();
		$data = array_pop($data);
		return $data['child_name'];
	}
	
	
	
	
	/**
	 *	user lists
	 */
	function get_all_group_lists($type = '',$order_by = ''){
		$this->db->flush_cache();
		$this->db->select("*");
		$this->db->from("admin_group");
		switch($type){
			case 'online':
				$this->db->where("online", '1');
				break;
			case 'S':
				$this->db->where('create_account_index',$this->config->item('admin_accountIndex'));
				break;
			case 'G':
				$this->db->where_in('create_account_index',$this->config->item('admin_accountGroupIndexArray'));
				break;
			default:
				break;			
		}
		
		if(is_array($order_by) ){
			foreach($order_by AS $key => $value){
				if(!$key) continue;
				if($value == "down") $this->db->order_by('admin_group.'.str_replace('by-','', $key).' DESC');
				else $this->db->order_by('admin_group.'.str_replace('by-','', $key).' ASC');
			}
		}else $this->db->order_by('admin_group.online DESC');
		
		$query = $this->db->get();
		$data = $query->result_array();
		foreach ($data AS $key => $value){
			$group_default = json_decode($value['group_default'],true);
			$data[$key]['group_default'] = implode(',', array_keys($group_default));
			
			$defaultstatus = $this->config->item('admin_competencestatus'); 
			$data[$key]['defaultstatus'] = $defaultstatus[$data[$key]['group_defaultstatus']];
		}
		return $data;
	}
	
	function get_all_account_lists($type = '',$order_by = ''){
		$this->db->flush_cache();
		$this->db->select("*");
		$this->db->from("admin_account");
		switch($type){
			case 'online':
				$this->db->where("online", '1');
				break;
			case 'S':
				$this->db->where('create_account_index',$this->config->item('admin_accountIndex'));
				break;
			case 'G':
				$this->db->where_in('create_account_index',$this->config->item('admin_accountGroupIndexArray'));
				break;
			default:
				break;			
		}
		if(is_array($order_by) ){
			foreach($order_by AS $key => $value){
				if(!$key) continue;
				if($value == "down") $this->db->order_by('admin_account.'.str_replace('by-','', $key).' DESC');
				else $this->db->order_by('admin_account.'.str_replace('by-','', $key).' ASC');
			}			
		}else $this->db->order_by('admin_account.update_time DESC');
		
	
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	
	function get_all_purview_lists($type = ''){
		$this->db->flush_cache();
		$this->db->select("*");
		$this->db->from("admin_purview");
		$this->db->join('admin_menu', 'admin_menu.auto_index = admin_purview.menu_index');
		$this->db->join('admin_account', 'admin_account.auto_index = admin_purview.account_index');
		switch($type){			
			case 'S':
				$this->db->where('admin_purview.create_account_index',$this->config->item('admin_accountIndex'));
				break;
			case 'G':
				$this->db->where_in('admin_purview.create_account_index',$this->config->item('admin_accountGroupIndexArray'));
				break;
			default:
				break;			
		}
	
		$query = $this->db->get();
		$data = $query->result_array();
		
		return $data;
	}
	
	function get_groupdefault($index=''){
		$this->db->flush_cache();
		$this->db->select("*");
		$this->db->from("admin_group");
		if($index){
			$this->db->where("auto_index", $index);
		}
		
		$query = $this->db->get();
		$data = $query->result_array();
		if($index && is_array($data)) return array_pop($data);
		return $data;
	}
	
	function get_groupIndex($index=''){
		$this->db->flush_cache();
		$this->db->select("admingroup_index");
		$this->db->from("admin_account");
		$this->db->where("auto_index", $index);
		$this->db->limit(1);		
		$query = $this->db->get();
		$result = $query->result_array();

		return $result[0]["admingroup_index"];
	}
	
	function get_editAccountIndex($table, $index, $account_index){
		$this->db->flush_cache();
		$this->db->select("create_account_index");
		$this->db->from($table);
		$this->db->where("auto_index", $index);
		$this->db->limit(1);		
		$query = $this->db->get();
		$result = $query->result_array();
		$temp = array_pop($result);
		return ($account_index == $temp['create_account_index']);
	}
	/**
	 *	判斷帳號名稱是否存在
	 *	如果不存在，則新增
	 *	存在，則發出 alert
	 */
	function check_user_exist_and_insert($add_input){
		$result = array('success' => 'N', 'msg' => '');
		
		$this->db->flush_cache();
		$query = $this->db->select("COUNT(*) AS qun")
		->from("admin_account")
		->where("id", $add_input['id'])
		->where("online", '1')
		->get();
		$temp = $query->row_array();
		
		if($temp['qun'] == 0){
			// 設定時區
			date_default_timezone_set("Asia/Taipei");
			$add_input['pw'] = md5($add_input['pw']);
			
			$this->db->insert('admin_account', $add_input);
			$result['success'] = 'Y';
			$result['msg'] = '新增成功';
		}else{
			$result['msg'] = '此名稱已經有人用了哦!!!';
		}
		return $result;
	}
	
	function check_user_exist_and_upload($add_input,$index){
		$result = array('success' => 'N', 'msg' => '');
		
		$this->db->flush_cache();
		$query = $this->db->select("COUNT(*) AS qun")
		->from("admin_account")
		->where("auto_index", $index)
		->where("online", '1')
		->get();
		$temp = $query->row_array();
		
		if($temp['qun'] == 0){
			$result['msg'] = '無該筆資料';
		}else{
			// 設定時區
			date_default_timezone_set("Asia/Taipei");
			//$add_input['pw'] = md5($add_input['pw']);
			if(isset($add_input['pw'])) unset($add_input['pw']);
			$this->db->where('auto_index', $index);
			$this->db->update('admin_account', $add_input);
			$result['success'] = 'Y';
			$result['msg'] = '修改成功';
		}
		return $result;		
	}
	
	
	/**
	 *	帳號群組
	 */
	function check_group_exist_and_insert($add_input){
		$result = array('success' => 'N', 'msg' => '');
	
		$this->db->flush_cache();
		$query = $this->db->select("COUNT(*) AS qun")
		->from("admin_group")
		->where("group_name", $add_input['group_name'])
		->get();
		$temp = $query->row_array();
	
		if($temp['qun'] == 0){				
			$this->db->insert('admin_group', $add_input);
			$result['success'] = 'Y';
			$result['msg'] = '新增成功';
		}else{
			$result['msg'] = '此名稱已經有人用了哦!!!';
		}
		return $result;
	}
	
	function check_group_exist_and_upload($add_input,$index){
		$result = array('success' => 'N', 'msg' => '');
	
		$this->db->flush_cache();
		$query = $this->db->select("COUNT(*) AS qun")
		->from("admin_group")
		->where("auto_index", $index)
		->where("online", '1')
		->get();
		$temp = $query->row_array();
	
		if($temp['qun'] == 0){
			$result['msg'] = '無該筆資料';
		}else{
				
			$this->db->where('auto_index', $index);
			$this->db->update('admin_group', $add_input);
			$result['success'] = 'Y';
			$result['msg'] = '修改成功';
		}
		return $result;
	}
	

	function get_user_purview(){
		$account_id = $this->config->item('admin_accountIndex');
		$menu_index = 6;
		$competence_value = 'V';
		
		
	}
	
	
	function checkCompetence_getMenuInfo($menu_index = '',$active){		
		//判斷功能選單
		$this->db->flush_cache();
		$this->db->select("*");
		$this->db->from("admin_menu");
		
		if($menu_index) {
			$this->db->where("child_name", $menu_index);			
		}else{
			$this->db->where("url", uri_string());
			$this->db->or_where("url", '_admin/'.uri_string().'/');
		}
		$query = $this->db->get();
		if(null === $menu = array_pop($query->result_array())) return false;
		
		//判斷選單權限
		$menu_index = $menu['auto_index'];
		if($menu_index){
			$breadcrumbs[] = $menu;
			$this->config->set_item('breadcrumbs',$breadcrumbs);
			$this->config->set_item('admin_title',$menu['child_name']);
		}
		
		
		$data = array();
		$account_index = $this->config->item('admin_accountIndex');
		
		$this->db->flush_cache();
		$this->db->select("admingroup_index,competence");
		$this->db->from("admin_account");
		
		if($account_index) $this->db->where("auto_index", $account_index);
		$query = $this->db->get();
		if(null === $data = array_pop($query->result_array())) return false;		
		
		$competence = json_decode($data['competence'],true);		
		
		$purview_where = array("menu_index"=>$menu_index,"competence_value"=>$active,"account_index"=>$account_index);
		
		//判斷特殊操作值
		$this->db->flush_cache();
		$this->db->select("purview_value");
		$this->db->from("admin_purview");
		$this->db->where($purview_where);
		$query = $this->db->get();
		$purview = array_pop($query->result_array());
		
		
		return ( in_array($menu_index, array_keys($competence ) ) )?( (in_array($active, $competence[$menu_index]) && isset($purview['purview_value']))?$purview['purview_value']:(in_array($active, $competence[$menu_index])) ):false;	
	}
	
	/*
	 * 粉絲團
	 * 
	 */
	
	
	
	function get_fansgroupdefault($fans_group,$fans_gids){	
		
		$group_names = array();
		if(is_array($fans_group)) {
			foreach($fans_group AS $value){
				if(isset($value['auto_index'])) $temp_group[$value['auto_index']] = $value['name'];
				if(isset($value['gid'])) $temp_group[$value['gid']] = $value['name'];
			}
			$gids = array_keys($temp_group);
		}
		
		if(is_array($temp_group) && is_array($fans_gids)){
			
			foreach($fans_gids AS $gid){
				if(in_array($gid, $gids)) array_push($group_names, $temp_group[$gid]);				
			}
			
		}
		
		return $group_names;
	}
	
	/**
	 *	del fans
	 */
	function del_fans_by_pk($index){
		$this->db->flush_cache();
		$this->db->where('auto_index', $index);
		$this->db->update('fb_fans', array('online' => '0'));
		$tmp = array('success' => 'Y', 'msg' => '已刪除', 'index' => $index);
		return $tmp;
	}
	
	/*
	 * 
	 * 
	 * 
	 */
	function move_file($file,$new_file,$rewrite = true){
	
		if($rewrite === false && is_file($new_file) === true ) return false;
		if( is_file($file) === false ) return fasle;
	
		file_put_contents($new_file, file_get_contents($file, true));
	
		if( is_file($new_file) === true ) {
			chmod($new_file, 0777);
			//製作縮圖
			$src_info = getimagesize($new_file);
			if($src_info["mime"] == "image/png"){
				$src_im = imagecreatefrompng($new_file);
			}else if($src_info["mime"] == "image/gif"){
				$src_im = imagecreatefromgif($new_file);
			}else
				$src_im = imagecreatefromjpeg($new_file);
				
			$alpha = 100;
			$refile = str_replace('.'.pathinfo($file, PATHINFO_EXTENSION), '_m.jpg', $new_file);			
			$resize = imagecreatetruecolor(300, 225) or die("error4!\n");
			imagecopyresampled($resize, $src_im, 0, 0, 0, 0, 300, 225, $src_info[0], $src_info[1]);
			imagejpeg($resize,$refile,$alpha);
			
			return true;
		}
		else return false;
	}
	
	function cut_base64img($basefile,$resize_arr=false){
		$filename = 'before_cut';
		$img_type = '';
		if(preg_match("/^data:image\/(?P<type>png|jpeg|gif);base64,/", $basefile,$match)){
			//delet前置data
		
			$img_type = 'image/'.$match['type'];
			
			$basefile = preg_replace("/^data:image\/(png|jpeg|gif);base64,/", '', $basefile);
			
		
			$basefile = str_replace(' ', '+', $basefile);
			//轉換base64 data
			$data = base64_decode($basefile);
			file_put_contents($filename, $data);
			
		}else{
			file_put_contents($filename, file_get_contents($basefile));
		}
		
		if( is_file($filename) === true ) {
			chmod($filename, 0777);
			//製作縮圖
			$src_info = getimagesize($filename);
			if($src_info["mime"] == "image/png"){
				$src_im = imagecreatefrompng($filename);
				
			}else if($src_info["mime"] == "image/gif"){
				$src_im = imagecreatefromgif($filename);
			}else
				$src_im = imagecreatefromjpeg($filename);
			if(!$img_type) $img_type = $src_info["mime"];
			$alpha = 100;
			
			$rate = ($src_info[0]>1000)?($src_info[0]/1000):1;  
			
			(int)$new_x = $rate*$resize_arr['x'];
			(int)$new_y = $rate*$resize_arr['y'];
			(int)$new_width = $rate*$resize_arr['w'];
			(int)$new_height = $rate*$resize_arr['h'];
			
			$a_height = 500 * ($new_height / $new_width);

			$resize = imagecreatetruecolor(500, $a_height) or die("error4!\n");
			imagecopyresampled($resize, $src_im, 0, 0, $new_x, $new_y, 500, $a_height, $new_width, $new_height);
			imagejpeg($resize,$filename,$alpha);
				
			return  $base64 = 'data:' . $img_type . ';base64,' .base64_encode(file_get_contents($filename));	
		}
		else return false;
	}

	function get_user_id_array() {
		$this->db->flush_cache ();
		$this->db->select ('auto_index, id');
		$this->db->from ('admin_account');
		$query = $this->db->get();
		$data = $query->result_array();

		$array = array();
		foreach ($data as $key => $value) {
			$array[$value['auto_index']] = $value['id'];
		}
		
		return $array;
	}

	function get_user_array($group='') {
		$this->db->flush_cache ();
		$this->db->select ('auto_index, id, memo');
		$this->db->from ('admin_account');
		$this->db->where('online', '1');
		if ( !empty($group) ) $this->db->where('admingroup_index', $group);
		$query = $this->db->get();
		$result = $query->result_array();
	
		return $result;
	}

	function rrmdir($dir) { 
	    if (is_dir($dir)) { 
	        $objects = scandir($dir); 
	        foreach ($objects as $object) { 
	            if ($object != "." && $object != "..") { 
	                if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object); else unlink($dir."/".$object); 
	            } 
	        } 
		    reset($objects); 
		    rmdir($dir); 
   	 	}
	}
}