<?php

if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Pofans extends CI_Controller {
	var $menu_index;
	public function __construct() {
		parent::__construct ();
		
		include_once (FCPATH.'AutoLoad.php');
		include_once (FCPATH.'class_curl.php');
		include_once (FCPATH.'ClassFacebooApi.php');
		
		
		$this->menu_index = 6; //
		$this->load->helper ( 'url' );
		$this->userInfo = $this->session->userdata ( 'userInfo' );
		$this->load->model ( 'topic_model', 'topic' );
		$this->load->model ( 'article_model', 'article' );
		
		$this->load->model ( 'theme_model', 'theme' );
		$this->load->model ( 'pofans_model', 'pofans' );
		$this->load->model ( 'statistics_model', 'statistics' );
		
		
		$this->data ['admin_menu'] = $this->config->item ( 'admin_menu' );
		$this->data ['admin_menuGroup'] = $this->config->item ( 'admin_menuGroup' );
		$this->data ['userInfo'] = $this->userInfo;
		$this->data ['menu'] = $this->config->item ( 'admin_accountMenu' );
		$this->data ['breadcrumbs'] = $this->config->item ( 'breadcrumbs' );
		$this->data ['title'] = $this->config->item ( 'admin_title' );
		
		$this->data ['topbar_control'] = array ();
		$this->data ['select_result'] = array ();
	}
	/*
	 * index
	 */
	public function index($pg = 1) {
		$this->lists ( $pg );
		/*ini_set('display_errors', '1');
		include_once (FCPATH.'AutoLoad.php');*/
		// public $appid = '806536466070254'; // your AppID
		// public $secret = 'ef661858f47225ac815f193ebc0da010'; // your secret	
	
		/*FacebookSession::setDefaultApplication($this->appid ,$this->secret);	
		$this->access_token = $session_token;
		$this->session = new FacebookSession($session_token);*/
	}
	/**
	 * fans
	 */
	function fans_add_list() {
		$token = $this->input->post ( 'access_token' );
		if($token){				
			$pofans = new pofans2_fb_crud($token);
		
			$pofans->set_logn_session();
			
			$user = $pofans->get_user_info();
			$data['user_id'] = $user['id'];
			$fans = $pofans->get_pages_info('/me');
			foreach ($fans AS $f){				
				if( $this->pofans->check_fans_exist($f->id) == 0) $data['fans_list'][] = $f;

			}	
		}
		
		$this->parser->parse ( 'pofans/pofans_fansadd', $data );
	}



	function fans_add_list_only() {
		// $this->parser->parse ( 'pofans/pofans_fansadd_only');
		$this->load->view('pofans/pofans_fansadd_only');
	}

	function fans_add_list_only_validate() {
		// $index = $this->input->post();
		// $fans = graph_helper::url_api($index['name']);
		// $fans = json_decode($fans, true);
		// $account_id = $this->config->item('admin_accountIndex');
		// $datetime = date('Y-m-d H:i:s');
		
		// $this->db->flush_cache();
		// $this->db->select("*");
		// $this->db->from("pofans_fanslist");
		// $this->db->where("page_id", $fans['id']);
		// $query = $this->db->get();
		// if ( !empty($query->result_array()) ) {
		// 	echo 'N';
		// 	break;
		// }

		// $sql = "INSERT INTO `pofans_fanslist` (`page_id`, `page_name`, `online`, `create_account_index`, `update_account_index`, `create_time`, `update_time`) 
		// 		VALUES ('".$fans['id']."', '".$fans['name']."', '1', '".$account_id."', '".$account_id."', '".$datetime."', '".$datetime."')";
		// mysql_query($sql);
		
		$index = $this->input->post();
		$facebook = new pofans2_fb_crud;
		$fans = $facebook->graph_api('/'.$index['name'])->asArray();
		if ( !isset($fans['likes']) ) {
			echo 'N';
			return;
		}

		$this->db->flush_cache();
		$this->db->select("*");
		$this->db->from("pofans_fanslist");
		$this->db->where("page_id", $fans['id']);
		$query = $this->db->get();
		if ( !empty($query->result_array()) ) {
			echo 'R';
			return;
		}

		$account_id = $this->config->item('admin_accountIndex');
		$datetime = date('Y-m-d H:i:s');

		$sql = "INSERT INTO `pofans_fanslist` (`page_id`, `page_name`, `online`, `create_account_index`, `update_account_index`, `create_time`, `update_time`) 
		 		VALUES ('".$fans['id']."', '".$fans['name']."', '1', '".$account_id."', '".$account_id."', '".$datetime."', '".$datetime."')";
		mysql_query($sql);

		$this->cache->memcached->delGlobalMemcached();
		echo 'Y';
	}

	function fansid_online(){
		$index = $this->input->post('index');
		$online = $this->input->post('online');
	
		if($online == 'true') $add_input = array('online' => '1');
		else $add_input = array('online' =>'0');
	
		$this->db->where('page_id', $index);
		$this->db->update('pofans_fanslist', $add_input);

		$this->cache->memcached->delGlobalMemcached();
	}
	function fans_add() {
		$fans_list = $this->input->post ( 'fans_list' );
		$user_id  = $this->input->post ( 'user_id' );
		
		$date = date ( "Y-m-d H:i:s" );
		$account_id = $this->config->item ( 'admin_accountIndex' );
		
		
		foreach ($fans_list AS $v){
			$add_input = $v;
			$add_input ['create_time'] = $date;
			$add_input ['create_account_index'] = $account_id;
			$add_input ['update_time'] = $date;
			$add_input ['update_account_index'] = $account_id;
			$add_input ['user_id'] = $user_id;
			$add_input ['online'] = '1';
			$this->pofans->check_fans_exist_and_insert($add_input);
		}
		
		
	}
	function get_fans_group(){
	
		$index = $this->input->post ( 'index' );
		$this->db->flush_cache ();
		$this->db->select ( "pofans_fanslist.fans_group" );
		$this->db->from ( "pofans_fanslist" );
		$this->db->where ( "pofans_fanslist.page_id",$index );
		$query = $this->db->get ();
		$data = $query->result_array ();
		if(is_array($data) && isset($data[0]['fans_group'])) echo $data[0]['fans_group'];
	}
	
	function set_fans_group(){
	
		$index = $this->input->post ( 'index' );
		$fans_group = $this->input->post ( 'fans_group' );
	
		$update_input = array('fans_group' => json_encode($fans_group));
		
		$this->db->where ( 'page_id', $index );
		$this->db->update ( 'pofans_fanslist' ,$update_input);
	}	
	function fans_lists($pg = 1, $user = '') {
		$defaultstatus= 'S';
		$activestatus = 'A';
		
		$online = is_numeric( $this->input->post('online') ) ? $this->input->post('online') : '1';

		$order_name = $this->input->post('order_by');	
		$orderBy = $this->theme->order_by($order_name,'fans_lists');
		$data = array_merge($this->data, $orderBy);
	
		$this->menu_index = 'pofans粉絲團列表';
		if(!$this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, 'V')){
			$this->load->view('template', $data);
			return false;
		}
		
		$data['topbar_control'][] = array('current_url'=>'JAVASCRIPT:fans.refresh();','i_class'=>'icon-refresh', 'control_name'=>'重載資料');
		$data['topbar_control'][] = array('current_url'=>'JAVASCRIPT:fans.add_only();','i_class'=>'icon-plus', 'control_name'=>'新增粉絲團');
		$data['select_result'] = array();
	
		
		$data['title'] = $this->admin_model->getTitle($this->menu_index);
		$data['topic_group'] = $this->article->article_group();
		
		$this->db->flush_cache();
		$this->db->select('*');
		$this->db->from('pofans_fanslist');
		$this->db->where('online', $online);
		

		if(is_array($orderBy) ){
			foreach($orderBy AS $key => $value){
				if(!$key) continue;
				if($value == "down") $this->db->order_by('pofans_fanslist.'.str_replace('by-','', $key).' DESC');
				else $this->db->order_by('pofans_fanslist.'.str_replace('by-','', $key).' ASC');
			}
		}else $this->db->order_by('create_time','ASC');
		
		
		$query = $this->db->get();
		$data['content'] = $query->result_array();
		$data['likes_total'] = 0;
		$data['online'] = $online;
		foreach($data['content'] AS $key => $list){
			$data['likes_total'] += $list['likes'];
			$data['content'][$key]['off'] = ($list['online'])?'':'off';
			$groups_pofans = (!empty($list['toments_category'])&&$list['toments_category']!='false') ? implode(',', json_decode($list['toments_category'])) : '';
			$data['content'][$key]['toments_category_edit'] = '<a href="#" class="groups_show" data-type="select2" data-pk="'.$list['page_id'].'" data-value="'.$groups_pofans.'" ></a>';
		}
		
		$data['main_content'] = $this->parser->parse('pofans/pofans_fanslist', $data, true);
		$this->parser->parse ( 'template', $data );		
	}
	/**
	 * pofans
	 */
	function update_plan(){
		$plan_time = $this->input->post('plan_time');
		$plan_list = $this->input->post('plan_list');
		
		$add_input = array('plan_time'=>$plan_time,'plan_list'=>json_encode($plan_list));
		
		$result= $this->pofans->check_plan_exist_and_insert($add_input);
		
	}
	
	function pofans_plan() {
		
		$page_one = 10;
		$topSearch = $this->input->post('topSearch');
		$order_name = $this->input->post('order_by');
		$orderBy = $this->theme->order_by ($order_name, 'pofans_list');
		$data = array_merge($this->data, $orderBy);
		
		
		foreach ($this->config->item('post_type') AS $val => $label){
			$data['post_type'][] = array('value' =>$val, 'label' =>$label);
		}
		
		$data['data_table'][] = array('table_name' =>'article','table_label' =>'星座文章','post_type'=>$data['post_type']);
		$data['data_table'][] = array('table_name' =>'topic','table_label' =>'星座投票','post_type'=>$data['post_type']);
		$data['data_table'][] = array('table_name' =>'question','table_label' =>'星座Q&A','post_type'=>$data['post_type']);
		
		$this->db->flush_cache();
		$this->db->select("*");
		$this->db->from("pofans_plan");
		$query = $this->db->get ();
		$data ['content'] = $query->result_array ();
		
		$plan_i = 0;
		foreach ( $data ['content'] as $key => $list ) {
			$list['plan_time'] = substr($list['plan_time'], 0,5);
			
			$plan_list = json_decode($list['plan_list'],true);
			if(is_array($plan_list)) {
				foreach ( json_decode($list['plan_list'],true) AS $plan_name){
					$list['plan_item'][]['plan_name'] = $plan_name;
				}
			}else{
				$list['plan_item'] = array();
			}
				
				
			$time_list['time_list'][] = $list;
			$plan_i++;
			
			if($plan_i>=6){
				$data ['time_part'][] = $time_list;
				$plan_i = 0;
				unset($time_list);
			}
			
		}
		
		$data ['main_content'] = $this->parser->parse ( 'pofans/pofans_plan', $data, true );
		$this->parser->parse ( 'template', $data );
	}
	function lists($pg = 1, $user = '') {
		$page_one = 10;
		$topSearch = $this->input->post ( 'topSearch' );
		$order_name = $this->input->post ( 'order_by' );
		$orderBy = $this->theme->order_by ( $order_name ,'pofans_list');
		$data = array_merge ( $this->data, $orderBy );
		
		if ($pg || $user)
			$this->menu_index = '文章列表'; //
		
		if (false === $activestatus = $this->admin_model->checkCompetence_getMenuInfo ( $this->menu_index, 'V' )) {
			$this->parser->parse ( 'template', $data );
			return false;
		}
		
		$defaultstatus = (isset ( $activestatus )) ? $activestatus : $this->config->item ( 'defaultstatus' );
		
		$purview = 'O';
		if ($this->admin_model->checkCompetence_getMenuInfo ( $this->menu_index, $purview )) {
			$onshelf_purview = true;
		} else {
			$onshelf_purview = false;
		}
		
		if ($this->menu_index)
			$data ['breadcrumbs'] = $this->config->item ( 'breadcrumbs' );
		
		$data ['onshelf_purview'] = $onshelf_purview;
		
		// 讀取 題目 使用者資訊
		$sql = "select admin_account.id AS value,CONCAT(admin_account.id,'(' ,sum(case when article_onshelf.online = '0' then 0 else 1 end),'/',count(admin_account.auto_index) ,')'  ) AS label 
				FROM admin_account 
				INNER JOIN article_onshelf ON admin_account.auto_index = article_onshelf.create_account_index
				GROUP BY admin_account.auto_index 
				";
		$this->db->flush_cache ();
		$query = $this->db->query ( $sql );
		
		preg_match ( '/select_(?P<user_id>[0-9]+)/', $user, $user_id );
		if ($user_id)
			$temp_userid ['select_id'] = $user_id ['user_id'];
		
		preg_match ( '/^(?P<group_id>[0-9]+)/', $user, $group_id );
		if ($group_id)
			$temp_userid ['group_id'] = $group_id ['group_id'];
		
		$temp_selector ['current_url'] = base_url ( 'article/lists/' . $pg ) . "/";
		
		$temp_selector ['select_name'] = (! isset ( $temp_userid ) && ! isset ( $temp_userid ) && $user) ? $user : 'All User';
		$temp_selector ['select_option'] = $query->result_array ();
		$data ['select_result'] [] = $temp_selector;
		
		if (isset ( $temp_userid ['group_id'] ))
			$temp_name = $this->article->article_group ( $temp_userid ['group_id'], 'name AS group_name' );
		$temp_selector ['select_name'] = (isset ( $temp_name ) && isset ( $temp_name ['group_name'] )) ? $temp_name ['group_name'] : 'All Group';
		$temp_selector ['select_option'] = $this->article->article_group ( '', 'name AS label, auto_index AS value' );
		$data ['select_result'] [] = $temp_selector;
		
		if (isset ( $temp_userid ['select_id'] ))
			$temp_name = $this->topic->topic_select ( $temp_userid ['select_id'], 'select_name' );
		$temp_selector ['select_name'] = (isset ( $temp_name ) && isset ( $temp_name ['select_name'] )) ? $temp_name ['select_name'] : 'All Select';
		$temp_selector ['select_option'] = $this->topic->topic_select ( '', 'select_name AS label, CONCAT("select_",topic_select.auto_index ) AS value' );
		$data ['select_result'] [] = $temp_selector;
		
		$this->db->flush_cache ();
		$this->db->select ( "article_onshelf.*" );
		$this->db->from ( "article_onshelf" );
		$this->db->join ( 'admin_account', 'admin_account.auto_index = article_onshelf.create_account_index' );
		/*
		 * if($defaultstatus == 'S'){ $this->db->where('create_account_index',$this->config->item('admin_accountIndex')); }else if($defaultstatus == 'G'){ $this->db->where_in('create_account_index',$this->config->item('admin_accountGroupIndexArray')); } /*
		 */
		
		if ($topSearch) {
			$this->db->where ( "article_onshelf.article_title like '%" . $topSearch . "%'" );
			$this->db->or_where ( "article_onshelf.auto_index", $topSearch );
		}
		
		if ($user) {
			
			if (isset ( $temp_userid ['select_id'] )) {
				$this->db->where ( 'article.options_select', $temp_userid ['select_id'] );
			} else if (isset ( $temp_userid ['group_id'] )) {
				$this->db->where ( "article_onshelf.article_group like '%\"" . $temp_userid ['group_id'] . "\"%'" );
			} else {
				$this->db->where ( 'admin_account.id', $user );
			}
		}
		if (is_array ( $orderBy )) {
			foreach ( $orderBy as $key => $value ) {
				if (! $key)
					continue;
				if ($value == "down")
					$this->db->order_by ( 'article_onshelf.' . str_replace ( 'by-', '', $key ) . ' DESC' );
				else
					$this->db->order_by ( 'article_onshelf.' . str_replace ( 'by-', '', $key ) . ' ASC' );
			}
		} else
			$this->db->order_by ( 'article_onshelf.online ASC, article_onshelf.create_account_index ASC' );
		
		$total_query = clone $this->db;
		
		$page ['total_page'] = $total_query->get ()->num_rows;
		
		$this->db->limit ( $page_one, ($pg - 1) * $page_one );
		$query = $this->db->get ();
		
		$data ['title'] = $this->admin_model->getTitle ( $this->menu_index );
		
		$data ['content'] = $query->result_array ();
		foreach ( $data ['content'] as $key => $list ) {
			if ($data ['content'] [$key] ['online'] < 1)
				$data ['content'] [$key] ['edit_control'] = '';
			else
				$data ['content'] [$key] ['edit_control'] = 'hide';
			$temp_group = $this->article->article_group ();
			$fans_groupname = array ();
			$temp_groupindex = json_decode ( $list ['article_group'] );
			
			
			foreach ( $temp_group as $value ) {
				if (is_array ( $temp_groupindex ) && in_array ( $value ['auto_index'], $temp_groupindex ))
					$fans_groupname [] = $value ['name'];
			}
			
			$data ['content'] [$key] ['group_names'] = ($temp_group) ? implode ( ',', $fans_groupname ) : '無';
			$data ['content'] [$key] ['select_optionName'] = (isset ( $select_option ['select_name'] )) ? $select_option ['select_name'] : '無';
		}
		
		
		
		
		$page ['start'] = 1;
		$page ['end'] = ceil ( $page ['total_page'] / $page_one );
		$page ['url'] = 'pofans/lists';
		$page ['page_num'] = $pg;
		$page ['url_end'] = '' ;
		$page ['current_url'] = base_url ( 'pofans/lists/' . $pg );
		$data ['page_list'] = $this->theme->page ( $page );
		
		$data ['main_content'] = $this->parser->parse ( 'pofans/pofans_list', $data, true );
		$this->parser->parse ( 'template', $data );	
	}
	
	/**
	 * schedule
	 */
	
	function schedule_lists($pg = 1, $user = '') {
		$defaultstatus= 'S';
		$activestatus = 'A';
	
		$order_name = $this->input->post('order_by');	
		$orderBy = $this->theme->order_by($order_name,'schedule_lists');
		$data = array_merge($this->data, $orderBy);
	
		$this->menu_index = 'pofans待發佈列表';//
		if(!$this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, 'V')){
			$this->load->view('template', $data);
			return false;
		}
		
		
		$data['topbar_control'][] = array('current_url'=>'JAVASCRIPT:schedule_add();','i_class'=>'icon-plus', 'control_name'=>'新增發佈資料');
		$data['select_result'] = array();
	
		$this->db->flush_cache();
		$this->db->select("*");
		$this->db->from("topic_group");	
		$query = $this->db->get();
	
		$data['title'] = $this->admin_model->getTitle($this->menu_index);
		$data['topic_group'] = $query->result_array();
		
		$this->db->flush_cache();
		$this->db->select("*");
		$this->db->from("pofans_schedule");

		if(is_array($orderBy) ){
			foreach($orderBy AS $key => $value){
				if(!$key) continue;
				if($value == "down") $this->db->order_by('pofans_schedule.'.str_replace('by-','', $key).' DESC');
				else $this->db->order_by('pofans_schedule.'.str_replace('by-','', $key).' ASC');
			}
		}else $this->db->order_by('auto_index','ASC');
		
		$post_type = array('message', 'link', 'photo', 'video');
		
		$query = $this->db->get();
		$data['content'] = $query->result_array();
		foreach($data['content'] AS $key => $list){
			
			foreach ($post_type AS $v){
				if($list[$v]) {
					unset($type_temp);
					$type_temp['label'] = $v;
					$type_temp['selected'] = ($list['post_type'] == $v)?'selected':'';					
					$post_typelist[] = $type_temp;
				}
			}
			
			$data['content'][$key]['post_typelist'] = $post_typelist;
			unset($post_typelist);
		}
		$data ['main_content'] = $this->parser->parse ( 'pofans/schedule_lists.php', $data, true );
		$this->parser->parse ( 'template', $data );
	
	}
	function schedule_insert() {
		
		
		$data = array();
		
		$data['fans_list'] = $this->pofans->pofans_fanslist();
		$this->parser->parse ( 'pofans/schedule_add', $data );
	}
	function schedule_add() {
		
		$add_input = $this->input->post ( 'fans_list' );
		$date = date ( "Y-m-d H:i:s" );
		
		
		$add_input['post_time'] =  strftime('%Y-%m-%d %H:%M:%S', strtotime($add_input['post_time']));
		$add_input ['create_time'] = $date;
		
		$this->db->insert('pofans_schedule', $add_input);
		
	}
	
	function schedule_add_from_article() {
	
		$index = $this->input->post ( 'index' );
		$type = $this->input->post ( 'type' );
		
		if(!is_array($index)) $index_arr[] = $index;
		else  $index_arr = $index;
		
		$date = date ( "Y-m-d H:i:s" );
		
		$this->db->select('*')->from('article_onshelf');
		if($index){
			$this->db->where_in('auto_index',$index_arr);
		}
	
		$query = $this->db->get();
		$data = $query->result_array();
		foreach($data AS $key => $list){
			if(!$list['article_group']) continue;
			
			$this->db->set('pofans_total', 'pofans_total+1', FALSE);
			$this->db->where('auto_index', $list['auto_index']);
			$this->db->update('article_onshelf');
			
			
			$img_url = '../uploads/q'.$list['auto_index'].'/';
			
			
			$add_input['message'] = $this->topic->cutText_for_pofans($list['article_optiontitle']);
			$add_input['link'] = '';
			$add_input['link_photo'] = '';
			$add_input['photo'] = $img_url.$list['article_imgurl'];
			$add_input['video'] = '';
			$add_input['post_type'] = ($type)?$type:'message';
			$add_input['create_time'] = $date;
			$add_input['post_time'] = $date;
			
			$group = $this->pofans->fanslist_bygroup(json_decode($list['article_group'],true),'page_id');
			
			foreach ($group AS $page_id){				
				$add_input['page_id'] = $page_id['page_id'];
				$this->db->insert('pofans_schedule', $add_input);
			}
			
			
		}
	}
	
	function schedule_type_by_index(){
		
		$index = $this->input->post ( 'index' );
		$post_type = $this->input->post ( 'type' );
		
		$this->db->where('auto_index', $index);
		$this->db->update('pofans_schedule', array('post_type'=>$post_type));
	}
	
	function schedule_post() {
	
		$this->db->flush_cache();
		$this->db->select("pofans_schedule.*,pofans_fanslist.access_token");
		$this->db->from("pofans_schedule");
		$this->db->join ( 'pofans_fanslist', 'pofans_fanslist.page_id = pofans_schedule.page_id' );
		
		$query = $this->db->get();
		$data['content'] = $query->result_array();
		foreach($data['content'] AS $key => $list){
			$po_fans = new pofans2_fb_crud($list['access_token']);
// 			$p = $po_fans->getPagesData('560448587402082');

			
			$post_facebook = array('message' => $list['message']);
			$type = 'feed';
			
			switch($list['post_type']){
				case 'message':
				default:
								
				case 'link':
					
					$post_facebook['link'] = $list['link'];
					$post_facebook['picture'] = $list['link_photo'];
					break;
				case 'photo':
					$type = 'photos';
					$src_info = getimagesize($list['photo']);
					$post_facebook['source'] =  new CURLFile(realpath($list['photo']), $src_info["mime"]);
					break;
					
			}
			
			
			$article = $po_fans->post_to_pages( $list['page_id'], $post_facebook,$type);
			if(is_array($article) && isset($article['id'])){
				unset($list['access_token']);
				$list['published_time'] = date ( "Y-m-d H:i:s" );
				$list['post_fb_id'] = $article['id'];
				$this->pofans->schedule_to_published($list);				
			}else{
				unset($list['access_token']);
				$list['published_time'] = date ( "Y-m-d H:i:s" );
				$list['failure_message'] = $article;
				$this->pofans->schedule_to_failure($list);
				
			}
				
		}
	}
	
	function published_lists($pg = 1, $user = '') {
		$defaultstatus= 'S';
		$activestatus = 'A';
	
		$order_name = $this->input->post('order_by');	
		$orderBy = $this->theme->order_by($order_name,'published_lists');
		$data = array_merge($this->data, $orderBy);
	
		$this->menu_index = 'pofans已發佈列表';//
		if(!$this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, 'V')){
			$this->load->view('template', $data);
			return false;
		}
		
		
		$data['topbar_control'][] = array('current_url'=>'JAVASCRIPT:schedule_add();','i_class'=>'icon-plus', 'control_name'=>'新增發佈資料');
		$data['select_result'] = array();
	
		$this->db->flush_cache();
		$this->db->select("*");
		$this->db->from("topic_group");	
		$query = $this->db->get();
	
		$data['title'] = $this->admin_model->getTitle($this->menu_index);
		$data['topic_group'] = $query->result_array();
		
		$this->db->flush_cache();
		$this->db->select("*");
		$this->db->from("pofans_published");

		if(is_array($orderBy) ){
			foreach($orderBy AS $key => $value){
				if(!$key) continue;
				if($value == "down") $this->db->order_by('pofans_published.'.str_replace('by-','', $key).' DESC');
				else $this->db->order_by('pofans_published.'.str_replace('by-','', $key).' ASC');
			}
		}else $this->db->order_by('auto_index','ASC');
		
		
		$query = $this->db->get();
		$data['content'] = $query->result_array();
		foreach($data['content'] AS $key => $list){
			
			
			
		}
		$data ['main_content'] = $this->parser->parse ( 'pofans/published_lists.php', $data, true );
		$this->parser->parse ( 'template', $data );
	
	
	}
	function schedule_remove() {
		$index = $this->input->post ( 'index' );
	
		if(!is_array($index)) $index_arr[] = $index;
		else  $index_arr = $index;
	
		$this->db->select('*')->from('pofans_schedule');		
		if($index){
			$this->db->where_in('auto_index',$index_arr);
		}
	
		$query = $this->db->get();
		$data = $query->result_array();
		foreach($data AS $key => $list){
			$this->db->delete('pofans_schedule', array('auto_index' => $list['auto_index']));
		}
	
	}
	function published_remove() {
		$index = $this->input->post ( 'index' );
		
		if(!is_array($index)) $index_arr[] = $index;
		else  $index_arr = $index;
	
		$this->db->select('pofans_published.post_fb_id,pofans_published.auto_index, pofans_fanslist.access_token')->from('pofans_published');
		$this->db->join ( 'pofans_fanslist', 'pofans_fanslist.page_id = pofans_published.page_id' );
		if($index){
			$this->db->where_in('auto_index',$index_arr);
		}
	
		$query = $this->db->get();
		$data = $query->result_array();
		foreach($data AS $key => $list){			
			$po_fans = new pofans2_fb_crud($list['access_token']);
			$po_fans->DelPost($list['post_fb_id']);
			
			$this->db->delete('pofans_published', array('auto_index' => $list['auto_index']));
		}
		
	}
	function test() {
		$data = array();
		$this->parser->parse ( 'pofans/pofans_test.php', $data, false );
	
	}

	function numerical_config() {
		$defaultstatus= 'S';
		$activestatus = 'A';
	
		$data = $this->data;
	
		$this->menu_index = 'pofans數值設定';
		if( !$this->admin_model->checkCompetence_getMenuInfo($this->menu_index, 'V') ){
			$this->load->view('template', $data);
			return false;
		}
		
		$this->db->flush_cache();
		$this->db->select('*');
		$this->db->from('config');
		
		$query = $this->db->get();
		$result = $query->result_array();
		foreach ($result as $key => $value) {
			if ( preg_match('/pofans/', $value['code'] ) ) {
				$data['pofans'][] = $value;
			} else  {
				$data['content'][] = $value;
			}
		}

		$data ['main_content'] = $this->parser->parse ( 'pofans/numerical_config', $data, true );
		$this->parser->parse ( 'template', $data );
	}

	function numerical_config_update() {
		$value = $this->input->post('value');
		$field = $this->input->post('field');

		if ( $field == 'pofans_hot_percent' ) {
			if ( !is_numeric($value) || $value<0 || $value>100 ) return;
		}

		$add_input = array(
			'value' => $value
		);
		
		$this->db->flush_cache();
		$this->db->where('code', $field);
		$this->db->update('config', $add_input);

		$this->load->driver('cache');
		$this->cache->memcached->delGlobalMemcached('config');
	}

	public function get_toments_category(){
		$this->db->flush_cache();
		$this->db->select('id, name');
		$this->db->from('categories');

		$query = $this->db->get();
		$json = $query->result_array();

		$newjso = array();
		foreach($json as $key => $val){
			$newjson[] = array(
				'value' => $val['id'],
				'text' => $val['name']
			);
		}

		echo json_encode($newjson);
	}

	function save_toments_category(){
		$category_id = $this->input->post('value');
		$page_id = $this->input->post('pk');
		
		// if ( !is_array($category_id) ) return;

		$category_id = !empty($category_id) ? json_encode($category_id) : '';

		$this->db->flush_cache();
		$this->db->where('page_id', $page_id);
		$this->db->update('pofans_fanslist', array('toments_category' => $category_id));

		$this->load->driver('cache');
		$this->cache->memcached->delGlobalMemcached('pofans_fanslist');
	}

	function show_table() {
		$page_id = $this->input->post('page_id');

		$days_time = time();
		for($i=0; $i<30; $i++) {
			$days[] = date('Y-m-d', $days_time);
			$days_time -= 86400;
		}

		$this->db->flush_cache();
		$this->db->select('daily_likes');
		$this->db->from('pofans_fanslist');
		$this->db->where('page_id', $page_id);

		$query = $this->db->get();
		$json = array_pop($query->result_array());
		$json = json_decode($json['daily_likes'], true);
		
		$data['content'] = array();
		if ( !empty($json) )
		foreach ($json as $key => $value) {
			if ( in_array($key, $days) ) {
				$data['content'][$key]['date'] = $key;
				$data['content'][$key]['likes'] = number_format($value);
			}
		}

		echo $this->parser->parse('pofans/show_likes', $data, true);
	}

	function delete_fans() {
		$page_id = $this->input->post('page_id');

		$this->db->flush_cache();
		$this->db->where('page_id', $page_id);
		$this->db->delete('pofans_fanslist');
	}
}