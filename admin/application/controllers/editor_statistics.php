
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class editor_statistics extends CI_Controller {
	
	var $menu_index;
	
	public function __construct(){
		parent::__construct();
		
		$this->userInfo = $this->session->userdata('userInfo');
		$this->load->model('theme_model', 'theme');
		$this->load->model('article_model', 'article');
		$this->load->model('statistics_model', 'statistics');
		$this->load->model('admin_model', 'admin');
		
		$this->data['admin_menu'] = $this->config->item('admin_menu');
		$this->data['admin_menuGroup'] = $this->config->item('admin_menuGroup');
		$this->data['userInfo'] = $this->userInfo;
		$this->data['menu'] = $this->config->item('admin_accountMenu');
		$this->data['breadcrumbs'] = $this->config->item('breadcrumbs');
		$this->data['title'] =$this->config->item('admin_title');
		
		$this->data['topbar_control']= array();
		$this->data['select_result'] = array();	
	}
	
	public function index(){
		$this->daily_statistics();
	}

	function daily_statistics() {
		$order_name = $this->input->post('order_by');
		$orderBy = $this->theme->order_by($order_name ,'daily_statistics');
		$data = array_merge($this->data, $orderBy);

		$this->menu_index = '日統計';
		if( false === $activestatus = $this->admin->checkCompetence_getMenuInfo($this->menu_index, 'V') ) {
			$this->parser->parse('template', $data);
			return false;
		} 
		$defaultstatus = (isset($activestatus)) ? $activestatus : $this->config->item('defaultstatus');

		$start_date = (!empty($this->input->post('start_date'))) ? $this->input->post('start_date') : date('Y-m-d',time()-86400);
		$end_date = (!empty($this->input->post('end_date'))) ? $this->input->post('end_date') : date('Y-m-d',time()-86400);

		$days = (strtotime($end_date) - strtotime($start_date)) / 86400 + 1;
		if ( is_array($orderBy) && !empty($orderBy) ) {
			foreach ( $orderBy as $key => $value ) {
				if (!$key) continue;

				$key = str_replace('by-', '', $key);
				if ($value == "down")
					$sql_order = ' ORDER BY ' . $key . ' DESC';
				else {
					$sql_order = ' ORDER BY ' . $key . ' ASC';
				}
			}
		} else {
			$sql_order = ' ORDER BY views DESC';
		}

		$data['content'] = $this->statistics->get_section_statistics($start_date, $end_date, $sql_order);
		$data['article_count_total'] = 0;
		$data['inner_1_total'] = 0;
		$data['inner_2_total'] = 0;
		$data['outer_1_total'] = 0;
		$data['outer_2_total'] = 0;
		$data['outer_3_total'] = 0;
		$data['article_time_total'] = 0;
		$data['like_unlike_total'] = 0;
		$data['views_total'] = 0;
		$data['average_views_total'] = 0;
		$user = array();
		$total_array = array('article_count', 'inner_1', 'inner_2', 'outer_1', 'outer_2', 'outer_3', 'article_time', 'like_unlike', 'views');
		foreach ($data['content'] as $key => $value) {
			$user[] = $value['account_id'];
			$data['content'][$key]['average_views'] = floor($data['content'][$key]['views'] / $days);
			$data['average_views_total'] += $data['content'][$key]['average_views'];
			$data['author'][$key]['average_views_total_author'] = $data['content'][$key]['average_views'];
			
			$data['author'][$key]['id_total_author'] = $value['id'];
			foreach ($total_array as $total) {
				$data[$total.'_total'] += $value[$total];
				$data['author'][$key][$total.'_total_author'] = $value[$total];
				$data['author'][$key][$total.'_avg_author'] = floor($value[$total]/$days);
			}
			
		}

		$author_count = isset($key) ? $key+1 : 1;

		$data['contribution_total'] = 0;
		foreach ($data['content'] as $key => $value) {
			$data['content'][$key]['contribution'] = number_format(floor($value['views']/$data['views_total']*100*100)/100,2);
			$data['contribution_total'] += $data['content'][$key]['contribution'];
		}
		$data['contribution_avg'] = number_format(floor($data['contribution_total']/$author_count*100)/100,2);;
		$data['average_views_avg'] = floor($data['average_views_total'] / $author_count);
		foreach ($total_array as $total) {
			$data[$total.'_avg'] = floor($data[$total.'_total'] / $author_count);
		}

		$data['daily'] = $this->statistics->get_section_day_statistics($start_date, $end_date);
		foreach ($data['daily'] as $key => $value) {
			$date = $value['date'];
			$data['daily'][$key]['date'] .= $this->statistics->get_chinese_weekday($date );

			foreach( $user AS $index => $id ) {
				$result = $this->statistics->get_section_author_statistics($date, $date, $id);
				if ( !empty($result) && is_array($result) ) {
					$array = array_pop($result);
					$data['daily'][$key]['person'][] = $array;
				}
			}
		}

		$data['start_date'] = $start_date;
		$data['end_date'] = $end_date;
		$data['main_content'] = $this->parser->parse('statistics/daily_statistics', $data, true);
		$this->parser->parse('template', $data);
	}

	function daily_hot() {
		$order_name = $this->input->post('order_by');
		$orderBy = $this->theme->order_by($order_name ,'daily_hot');
		$data = array_merge($this->data, $orderBy);

		$this->menu_index = '日熱門文章';
		if( false === $activestatus = $this->admin->checkCompetence_getMenuInfo($this->menu_index, 'V') ) {
			$this->parser->parse('template', $data);
			return false;
		} 
		$defaultstatus = (isset($activestatus)) ? $activestatus : $this->config->item('defaultstatus');

		if ( !empty($this->input->post('days')) ) {
			$days = $this->input->post ('days');
		} else {
			$days = date('Y-m-d');
		}

		$this->db->flush_cache();
		$this->db->select('dad.*, dad.title as article_title, aa.id as author_name');
		$this->db->from('day_article_details as dad');
		$this->db->like('create_date', $days);
		$this->db->where('hot', 1);
		
		$this->db->join ('admin_account AS aa', 
				'aa.auto_index = dad.create_user_id', 'left');
		
		$sql_order = '';
		if ( is_array($orderBy) && !empty($orderBy) ) {
			foreach ( $orderBy as $key => $value ) {
				if (!$key) continue;

				$key = str_replace('by-', '', $key);
				if ($value == "down")
					$this->db->order_by($key . ' DESC');
				else {
					$this->db->order_by($key . ' ASC');
				}
			}
		} else {
			$this->db->order_by('day_views DESC');
		}

		$query = $this->db->get();
		$data['content'] = $query->result_array();
		$data['article_total'] = count($data['content']);
		$data['total_views'] = 0;

		foreach ($data['content'] as $key => $value) {
			$data['total_views'] += $data['content'][$key]['day_views'];
		}

		$this->db->flush_cache();
		$this->db->select('value');
		$this->db->from('config');
		$this->db->where('code', 'pofans_hot_hours');
		$query = $this->db->get();
		$data['hot_hours'] = array_pop($query->result_array())['value'];

		$data['days_list'] = $this->statistics->get_day_list(7);
		$data['days'] = $days;
		$data['toments_url'] = 'http://toments.com/';
		$data['pofans_url'] = 'http://i-kuso.com/pofans2/message/index.php?id=';

		$data['main_content'] = $this->parser->parse('statistics/daily_hot', $data, true);
		$this->parser->parse('template', $data);
	}

	function show_table() {
		$author_name = $this->input->post('author_name');
		$author_id = $this->input->post('author_id');
		$table = $this->input->post('table');
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');


		$data['author_name'] = $author_name;
		$data['content'] = $this->statistics->one_daily($author_id, $table, $start_date);
		
		foreach ($data['content'] as $key => $value) {
			$data['content'][$key]['fb_id'] = ($data['content'][$key]['fb_id'] == 0 ) ? '站外' : '站內';
			if ( $data['content'][$key]['source_mode'] == 1 ) {
				$data['content'][$key]['source_mode'] = '改寫';
			} elseif ( $data['content'][$key]['source_mode'] == 2 ) {
				$data['content'][$key]['source_mode'] = '翻譯';
			} elseif ( $data['content'][$key]['source_mode'] == 3 ) {
				$data['content'][$key]['source_mode'] = '參考';
			}

			if ( empty($data['content'][$key]['fb_group_id']) ) {
				$data['content'][$key]['source_url'] = $data['content'][$key]['article_source'];
			} else {
				$data['content'][$key]['source_url'] =' https://www.facebook.com/'.$data['content'][$key]['fb_group_id'].'/posts/'.$data['content'][$key]['fb_post_id'];
			}
		}

		echo $this->parser->parse('statistics/show_table', $data, true);
	}

}
