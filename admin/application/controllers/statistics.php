<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Statistics extends CI_Controller {
	
	var $menu_index;
	
	
	public function __construct(){
		parent::__construct();
		
		$this->menu_index = 7;//
		$this->load->helper('url');
		$this->userInfo = $this->session->userdata('userInfo');
		$this->load->model('admin_model', 'admin');
		$this->load->model('topic_model', 'topic');
		$this->page_obj->collapse = 'user';
		
		
		$this->data['admin_menu'] = $this->config->item('admin_menu');
		$this->data['admin_menuGroup'] = $this->config->item('admin_menuGroup');
		$this->data['userInfo'] = $this->userInfo;
		
		// css & js
		$this->page_obj->css[] = 'js/jquery.treeview/jquery.treeview';
		$this->page_obj->css[] = 'js/Jcrop-v0.9.12/css/jquery.Jcrop.min';
		$this->page_obj->js[] = 'js/jquery.treeview/jquery.treeview';
		$this->page_obj->js[] = 'js/Jcrop-v0.9.12/js/jquery.Jcrop.min';
		$this->page_obj->js[] = 'js/img_box';
		$this->page_obj->js[] = 'js/google_img';
		$this->page_obj->js[] = 'js/horoscope';
		$this->page_obj->js[] = 'js/choose';
		
		
	}
	/*
	* index
	*/
	public function index(){
		$this->state_lists();
	}
	
	function state_lists(){
		$accountIndex = $this->config->item('admin_accountIndex');
	
		$data = $this->data;
		$this->menu_index = '審核狀態管理';//
		if(false === $activestatus = $this->admin->checkCompetence_getMenuInfo( $this->menu_index, 'V')){
			$this->load->view('admin/template', $data);
			return false;
		}
	
		$defaultstatus = (isset($activestatus))?$activestatus:$this->config->item('defaultstatus');
	
	
	
	
	
		
		$data['title'] = $this->admin->getTitle($this->menu_index);
	
		
		$this->db->flush_cache();
		$this->db->select("*");
		$this->db->from("statistics_state");
		$query = $this->db->get();
		
		$data['content'] = $query->result_array();
		foreach($data['content'] AS $key => $list){
			
		}
	
	
	
	
		$this->page_obj->js[] = 'js/validation/jquery.validate.min';
	
		$this->page_obj->main_content = $this->load->view('admin/state_lists', $data, true);
		$this->load->view('admin/template', $data);
	}
	
	/**
	 *	list
	 */
	function lists(){
		$accountIndex = $this->config->item('admin_accountIndex');
		
		$data = $this->data;
		$this->menu_index = '測驗列表';//
		if(false === $activestatus = $this->admin->checkCompetence_getMenuInfo( $this->menu_index, 'V')){
			$this->load->view('admin/template', $data);
			return false;
		}
		
		$defaultstatus = (isset($activestatus))?$activestatus:$this->config->item('defaultstatus');
		
		
		
		
		
		$this->db->flush_cache();
		$this->db->select("admin_account ");
		$this->db->from("admin_account");
		$this->db->where("admin_account.auto_index",$accountIndex);
		$this->db->group_by("online");
		
		$data['title'] = $this->admin->getTitle($this->menu_index);
		
		$data['content'] = $query->result_array();
		foreach($data['content'] AS $key => $list){
			
			$this->db->flush_cache();
			$this->db->select("topic.online,count(online) AS online_total ");
			$this->db->from("topic");
			$this->db->where("topic.create_account_index",$accountIndex);
			$this->db->group_by("online");
			
			$query = $this->db->get();
			$row = $query->result_array();
			
			$temp_group = $this->topic->topic_group();
			$fans_groupname = array();
			$temp_groupindex = json_decode($list['topic_group']);
			foreach($temp_group AS $value){
				if(is_array($temp_groupindex) && in_array($value['auto_index'],$temp_groupindex) )
					$fans_groupname[] = $value['name'];
			}
			$select_option = $this->topic->topic_select($list['options_select']);
			$data['content'][$key]['group_names'] = ($temp_group)?implode(',', $fans_groupname):'無';
			$data['content'][$key]['select_optionName'] = (isset($select_option['select_name']))?$select_option['select_name']:'無';
		}
		
		
		
		
		$this->page_obj->js[] = 'js/validation/jquery.validate.min';
		
		$this->page_obj->main_content = $this->load->view('admin/topic_list', $data, true);
		$this->load->view('admin/template', $data);
	}
	

	/**
	 *	actions
	 */
	function actions($ac){
	
	
		switch($ac){
	
			case 'add_state':
					
				$index = $this->input->post('index');
				$this->menu_index = '審核狀態管理';//
				$purview = ($index)?'E':'A';
				if(!$this->admin->checkCompetence_getMenuInfo( $this->menu_index, $purview)){
					echo $this->load->view('admin/purview_error', '');
					return false;
				}
				
				
				$defaultstatus = (isset($activestatus))?$activestatus:$this->config->item('defaultstatus');
				
				if($index && $defaultstatus == 'S'){
					$account_index = $this->userInfo['auto_index'];
					if(!$this->admin->get_editAccountIndex('topic',$index,$account_index)){
						echo $this->load->view('admin/purview_error', '');
						return false;
					}
					
				}
					
				$data = $this->data;
					
					
	
				$data['competence'] = $this->config->item('admin_group');
				$data['admin_competence'] = $this->config->item('admin_competence');
				$data['topic_group'] = $this->topic->topic_group();
				
				$data['topic_select'] = $this->topic->topic_select();
				$data['topic_options'] = array();
				
				
				
				if($index){
					$this->db->select('*')->from('statistics_state')->where('auto_index',$index);
					$query = $this->db->get();
					$result = $query->result_array();
					if(is_array($result)){
						$data['statistics_state'] = array_pop($result);						
					}
				}
				/**/
				
				echo $this->load->view('admin/statistics_state_edit', $data, true);
				
				break;
			case 'validate_state':
				$this->menu_index = '審核狀態管理';//
				if(!$this->admin->checkCompetence_getMenuInfo( $this->menu_index, 'E')){
					$result = array('success' => 'N', 'msg' => '權限不足');
					echo json_encode($result);
					break;
				}
	
	
				$index = (int) $this->input->post('auto_index');	
	
				$add_input = array("state_name"=>$this->input->post("state_name"));			
				
	
				if($index) {
					$this->db->where('auto_index', $index);
					$this->db->update('statistics_state', $add_input);
					$result['success'] = 'Y';
					$result['msg'] = '修改成功';
				}
				else {					
					$this->db->insert('statistics_state', $add_input);
					$result['success'] = 'Y';
					$result['msg'] = '新增成功';
						
				}
				header('Content-type: text/plain; charset=utf-8');
				echo json_encode($result);
	
	
				break;
	
			case 'validate_onshelf':
				$this->menu_index = '測驗列表';//
				if(!$this->admin->checkCompetence_getMenuInfo( $this->menu_index, 'O')){
					$result = array('success' => 'N', 'msg' => '權限不足');
					echo json_encode($result);
					break;
				}
	
	
				$index = $this->input->post('auto_index');
				$result = $this->topic->onshelf_topic_by_index($index);
	
				header('Content-type: text/plain; charset=utf-8');
				echo json_encode($result);
				break;
			case 'state_del':
				$this->menu_index = '審核狀態管理';//
				if(!$this->admin->checkCompetence_getMenuInfo( $this->menu_index, 'D')){
					$result = array('success' => 'N', 'msg' => '權限不足');
					echo json_encode($result);
					break;
				}
				
	
				$index = $this->input->post('index');

				if($index){
					$this->db->flush_cache();
					$this->db->where('auto_index', $index);
					$this->db->delete('statistics_state');
				}
				$result = array('success' => 'Y', 'msg' => '');
				header('Content-type: text/plain; charset=utf-8');
				echo json_encode($result);
				break;
		}
	}
}