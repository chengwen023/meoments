<STYLE>
tbody IMG {
	display: inline-block;
}
tbody IMG.focus {
	border:2px dotted red;
}

.online A {
	position: relative;
}

.online A:first-child {
	z-index: 100;
}

.online A:not(:first-child ) {
	top: -26px;
	left: 26px;
	z-index: 1;
	opacity: 0;
	filter: alpha(opacity = 0);
	-moz-transform: rotateY(180deg);
	-webkit-transform: rotateY(180deg);
	transform: rotateY(180deg);
}

.online.off A:first-child {
	opacity: 0;
	filter: alpha(opacity = 0);
	-moz-transform: rotateY(-180deg);
	-webkit-transform: rotateY(-180deg);
	transform: rotateY(-180deg);
}

.online.off A:not(:first-child ) {
	opacity: 1;
	filter: alpha(opacity = 100);
	-moz-transform: rotateY(0deg);
	-webkit-transform: rotateY(0deg);
	transform: rotateY(0deg);
}

.glyphicon-ok:before {
	content: "確定";
}
.glyphicon-remove:before {
	content: "取消";
}
</STYLE>




<div class="widget" >
	<table cellpadding="0" cellspacing="0"  class="tLight">
		<form id="list_orderby" method="post">
		<input id="order_by" type="hidden" name="order_by" value="">
		<thead>
			<tr>				
				<td title="群組名稱">名稱</td>				
				<td title="新增資料" width="150px;" style="cursor: pointer;" onclick="category_list.add_edit(0);">新增資料<i class="icon-plus-sign"></i></td>
				<td title="pofans對應">pofans對應</td>
				<!--
				<td title="上架分類">名稱</td>
				<td title="下架分類">名稱</td>
				-->
			</tr>			
		</thead>
		</form>
		<tbody>
			{content}
			<tr id="user_{id}" class="user_row">
				<td>{name}</td>				
				<td>	
					<a href="javascript:void(0);" class="tablectrl_small bGreen" original-title="Edit" title="修改" alt="修改" onclick="category_list.add_edit({id});">
					<span class="iconb" data-icon="" ></span></a>
					<span class="online {off}" data-index="{id}"> 
						<a href="javascript:void(0);" class="tablectrl_small bBlue" original-title="Edit" title="上架中"
							alt="上架中">上架中<span class="iconb" data-icon=""></span></a>
						<a href="javascript:void(0);" class="tablectrl_small bGold" original-title="Edit" title="下架中" 
							alt="下架中">下架中<span class="iconb" data-icon=""></span></a>
					</span>					
				</td>
				<td>{pofans2_category_edit}</td>					
			</tr>
			{/content}
		</tbody>
	</table>
</div>
<script src="js/category_list.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<link media="all" type="text/css" rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/3.5.1/select2-bootstrap.css">
<link media="all" type="text/css" rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/3.5.1/select2.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/3.5.1/select2.js"></script>
<script>
$(".online").click(function() {
	var index= $(this).attr('data-index'); 
	var online = $(this).hasClass("off")
	console.info(index);
	if(online) $(this).removeClass("off");
	else $(this).addClass("off");

	if(index)
		$.ajax({
		  url: 'news_category/category_online/',
		  dataType: 'html',
		  type: "POST",
		  data: {
			  online:online,
			  index:index
		  },
		  success: function(data){
			  console.info(data);
		  	/**/
		  }
		});
	
});
$(document).ready(function(){
	$.getJSON( "news_category/get_pofans2_category", function( json ) {
		$('.groups_show').editable({
			title: 'pofans群組',
			url:'news_category/save_pofans2_category',
			source: json,
			select2: {
			   width: 200,
			   multiple: true,
			   allowClear: true
			},
			success: function(res){
				console.log(res);
			}
		});
	 });
});
</script>