<?php
session_start();

require_once('facebook_load.php');
// added in v4.0.5
use Facebook\FacebookHttpable;
use Facebook\FacebookCurl;
use Facebook\FacebookCurlHttpClient;
 
// added in v4.0.0
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookOtherException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\GraphSessionInfo;
use Facebook\GraphUser;


// start session

 
// init app with app id and secret
FacebookSession::setDefaultApplication( '788431084588360','41622ae60d6313b7b1689b049860978d' );


// login helper with redirect_uri
$helper = new FacebookRedirectLoginHelper( 'http://toments.com/web/code/facebook_mobile.php' );

// see if a existing session exists
if ( isset( $_SESSION ) && isset( $_SESSION['fb_token'] ) ) {
	// create new session from saved access_token
	$session = new FacebookSession( $_SESSION['fb_token'] );

	// validate the access_token to make sure it's still valid
	try {
		if ( !$session->validate() ) {
			$session = null;
		}
	} catch ( Exception $e ) {
		// catch any exceptions
		$session = null;
	}
}

if ( !isset( $session ) || $session === null ) {
	// no session exists

	try {
		$session = $helper->getSessionFromRedirect();
	} catch( FacebookRequestException $ex ) {
		// When Facebook returns an error
		// handle this better in production code
		print_r( $ex );
	} catch( Exception $ex ) {
		// When validation fails or other local issues
		// handle this better in production code
		print_r( $ex );
	}

}

// see if we have a session
if ( isset( $session ) ) {

	// save the session
	$_SESSION['fb_token'] = $session->getToken();
	// create a session using saved token or the new one we generated at login
	$session = new FacebookSession( $session->getToken() );


} else {
	// show login url
	$fb_URL = $helper->getLoginUrl( array( 'email','user_about_me','publish_actions' ) );		
	header("Location: ".$fb_URL);
}

echo "redirect:".$_SESSION['redirect'];
if($_SESSION['redirect']) header("Location: ".$_SESSION['redirect']);
?>