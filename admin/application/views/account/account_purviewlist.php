<div class="widget">
	<table cellpadding="0" cellspacing="0" width="100%" class="tLight">
		<thead>
			<tr>
				<td>帳號名稱</td>
				<td>選單</td>
				<td>操作權限</td>
				<td>對應群組</td>
			</tr>
		</thead>
		<tbody>
			{content}
			<tr id="user_{auto_index}" class="user_row">
				<td>{id}</td>
				<td>{child_name}</td>
				<td>{competence}</td>
				<td>{purview}</td>				
			</tr>
			{/content}
		</tbody>
	</table>
</div>
<script src="js/account.js"></script>
<script>
	$(function(){
		$('.fans_form').submit(function(e){
			e.preventDefault();
			fans.add_fans();
		});
	});
</script>