<style>
form#form_folder {
	width: 400px;
}

form#form_folder div{
	text-align: center;
	margin: 10px 0;
}

form#form_folder div input{
	padding: 5px;
	font-size: 14px;
}

form #admin_name {
	width: 300px;
	height: 30px;
}
</style>

<div>
	<form id="form_folder">
		<div>粉絲團網址<input id="admin_name" type="text" name="name" value="" /><span style="color:#A73939;">(*)</span><div>
		<div>語言
			<select name="language_list">
			{language_list}
		　		<option id="{num}" value="{num}">{language}</option>
			{/language_list}
			</select>
			<span style="color:#A73939;">(*)</span>
		</div>

		<div><input type="submit" value="送出" /></div>
	</form>
</div>
<script>
$(function(){
	// 註冊驗證
	$("#form_folder").validate({			
		submitHandler: function(form) {
			console.info($("#admin_name").val());
			
			$.fancybox.showLoading();
			// 透過 Ajax 驗證是否註冊
			$.ajax({
			  url: 'source/actions/validate',
			  cache: false,
			  dataType: 'html',
			  type: "POST",
			  data: $("#form_folder").serialize(),
			  success: function(data){
				
				$.fancybox.hideLoading();
				
				if(data == 'Y' ){
					console.log(data);
					location.reload();
				}else{
					console.log(data);
					return false;

				}
			  }
			});
			return false;
		}
	});
});
</script>