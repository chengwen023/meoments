
<link href="css/iframeEDT.css" rel="stylesheet" type="text/css">
<script src="js/tool.js"></script>
<script src="js/span_editor.js"></script>
<style>
#img_selectbox {
	display: none;
	width: 550px;
	height: 420px;
	text-align: center;
	background-color: #94b0dc;
}

.fb_view_control {
	background-color: #336699;
	color: #fff;
	height: 20px;
}

.fb_view_control SPAN {
	margin-right: 10px;
	padding: 2px 5px;
}

.fb_view_control SPAN:HOVER {
	cursor: pointer;
	color: #3366cc;
	background-color: #fff;
}

.fb_view {
	width: 500px;
	height: 375px;
	margin: auto;
	border: 1px solid red;
}

.fb_link {
	width: 500px;
	height: 260px;
}

.fb_link_s {
	width: 375px;
	height: 375px;
}

.topic_photo {
	border: 1px solid;
	box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3);
	padding: 7px 5px 7px 7px;
	margin-right: 15px;
	margin-bottom:15px;
	float: left;
	margin-top: 25px;
	position: relative;
}
.topic_photo:before{
	content: attr(data-title);
	position: absolute;
	top: -20px;
}
.topic_group {
	width: 340px;
	float: left;
	margin: 10px;
}

.topic_group STRONG {
	color: #000;
	float: right;
	cursor: pointer;
	border-bottom: 1px solid #999;
}

.topic_group UL {
	margin: 0;
	display: none;
}

.topic_group.editable UL.options_select {
	position: absolute;
	display: inline-block;
	margin-top: 30px;
	background-color: #fff;
	border: 1px solid #D6D6D6;
	border-radius: 10px;
	padding: 5px 5px;
	box-shadow: 3px 3px 4px rgba(0, 0, 0, 0.2);
	z-index: 10;
}

.topic_group UL LI {
	float: left;
	margin-right: 10px;
	cursor: pointer;
	width: 130px;
}

.topic_group UL LI:AFTER {
	content: ' ';
	padding: 10px 0px 0px 30px;
}

.topic_group UL LI:HOVER {
	color: #585858;
}

.topic_group UL LI:HOVER:AFTER {
	background: url(images/check_green.png) no-repeat left;
}

.topic_group #topic_group_name UL {
	display: block;
	width: 450px;
	float: left;
}

.topic_group #topic_group_name UL LI.checked:AFTER {
	background: url(images/check_green.png) no-repeat left;
}

.audit_left.not_pass:BEFORE {
	content: ' ';
	padding: 10px 0px 10px 30px;
	background: url(images/not_pass.gif) no-repeat left;
	position: absolute;
	margin-left: -30px;
}

.audit_right.not_pass:AFTER {
	content: ' ';
	padding: 10px 0px 10px 30px;
	background: url(images/not_pass.gif) no-repeat left;
}
.topic_photo.not_pass:BEFORE {
	content: attr(data-title);
	padding: 0px 0px 00px 30px;
	background: url(images/not_pass.gif) no-repeat left;
}
.topic_photo.not_pass:AFTER{
	display: none;;
}
.data_title:BEFORE {
	content: attr(data-title);
	position: absolute;
	color: #000;
	margin-left: -40px;
	font-weight: bolder;
}

input[type="text"] {
	width: 100%;
	font-size: 18px;
	height: 24px;
	margin-bottom: 5px;
}

input[type="text"]:HOVER {
	background-color: rgba(33, 153, 233, 0.3);
}

input[type="text"].empty {
	border-color: rgba(241, 193, 154, 1);
}
IMG.errorsize{
	/*border-color: rgba(230, 128, 45, 1); */
	border:3px solid red; 
	 

}

.topic_photo.audit_right.dropping:AFTER {
	content: ' ';
	background-color: rgba(33, 33, 33, 0.3);
	width: 114px;
	position: absolute;
	height: 91px;
	border: 1px solid red;
	margin-top: -83px;
	margin-left: -9px;
}

.topic_photo.audit_left.dropping:AFTER {
	content: ' ';
	background-color: rgba(33, 33, 33, 0.3);
	width: 114px;
	position: absolute;
	height: 91px;
	border: 1px solid red;
	margin-top: -83px;
	margin-left: -9px;
}

.topic_option_list {
	counter-reset: option_div;
}

.topic_option_list .page-content.dropimg-content:BEFORE {
	counter-increment: option_div;
	content: counter(option_div);
	font-size: 135px;
	position: absolute;
	margin-top: 60px;
	color: #fff;
	text-shadow: 1px 2px 4px rgba(199, 199, 199, 1);
	font-family: sans-serif;
}

.dropimg-content.dropping:AFTER {
	content: ' ';
	background-color: rgba(33, 33, 33, 0.3);
	width: 100%;
	position: absolute;
	height: 180px;
	border: 1px solid red;
}

#img_control {
	background: url(images/arrow_right.png) no-repeat center;
	padding: 40px 100px;
	width: 735px;
	/* height: 200px; */
	position: absolute;
	z-index: 1000;
	background-color: rgba(33, 33, 33, 0.3);
}

.topic_photo #img_control {
	margin-left: -90px;
	left: 0;
}
.audit_right #img_control {	
	left: -483px;
}
#img_control #old_img {
	float: left;
	padding: 8px;
	background-color: #f6f6f6;
	border: 1px dashed #b2b2b2;
	-webkit-box-shadow: 3px 3px 4px rgba(0, 0, 0, 0.2);
	-moz-box-shadow: 3px 3px 4px rgba(0, 0, 0, 0.2);
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
}

#img_control #new_img {
	float: right;
	padding: 8px;
	background-color: #f6f6f6;
	border: 1px dashed #b2b2b2;
	-webkit-box-shadow: 3px 3px 4px rgba(0, 0, 0, 0.2);
	-moz-box-shadow: 3px 3px 4px rgba(0, 0, 0, 0.2);
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
}

#img_control DIV#control_btn {
	top: 0;
	right: 0;
	position: absolute;
	color: #fff;
	font-size: 24px;
	overflow: hidden;
	width: 50px;
	height: 100%;
}

SPAN.text_editable{
	padding-right: 20px;
}
SPAN.text_editable:HOVER,SPAN.text_editable:FOCUS{
	border-bottom: 1px solid rgb(102,153,204);
}

.ie-icon:BEFORE {
	width: 32px;
	height: 32px;
}

.ie-icon:HOVER:BEFORE {
	margin-left: -5px;
	padding: 5px;
	border-radius: 5px;
	background: rgba(0, 0, 0, 0.5);
}


#upload_file::-webkit-file-upload-button {
  visibility: hidden;
}
#upload_file::before {
	content: ' ';
	display: inline-block;
	background: url("images/uploadfile.png") no-repeat left center;
	padding: 30px;
	cursor:pointer;
    position: absolute;
}

</style>

<input type="hidden" name="auto_index" value="{auto_index}">
<div class="plugin_box" style="position: fixed; right: 40px;z-index: 100;">
	<div id="img_selectbox">
		<div class="fb_view_control">
			<span data-fb="fb_view">原始圖片</span> <span data-fb="fb_link">連結圖片(大)</span>
			<span data-fb="fb_link_s">連結圖片(小)</span> <span data-fb="close"
				style="float: right;">關閉</span>
		</div>
		<div style="margin: auto; height: 25px;">
			<span style="float: left;"><input id="upload_file" type="file" onchange="upload_img();"/></span>
		</div>
		<div class="fb_view"></div>
	</div>
</div>




<div class="fancybox-content">
	<div class="fancybox-content-bg-bottom">
		<div class="fancybox-header">
			<ul>				
				<?php if(!empty($onshelf_purview) && $onshelf_purview && $online != '1'):?>						
					<li ><a onclick="topic.reviseview({auto_index});">上架</a></li>
				<?php else:?>
					<li ><a onclick="topic.reviseview({auto_index});">觀看</a></li>
				<?php endif;?>
				<li class="active"><a onclick="topic.save_edit({auto_index});">編輯</a></li>
			</ul>

			<br class="clear">

			<div class="fancybox-logo" style="color: #919191;">
				<div style="width: 120px;float: left;">
				<span data-title="題目圖片" 
					class="topic_photo audit_left <?php if(isset($fail_state['fail_title_p']) && $fail_state['fail_title_p']):?>not_pass<?php endif;?>"
					name="fail_title_p"> 
					<img data-name="topic_imgurl" class="editable" alt=""src="{topic_imgurl}" width="100px;">
				</span>
				
				<span data-title="FACEBOOK" 
					class="topic_photo audit_left <?php if(isset($fail_state['fail_facebook_p']) && $fail_state['fail_facebook_p']):?>not_pass<?php endif;?>"
					name="fail_facebook_p"> 
					<img data-name="facebook_imgurl" class="editable" alt="" src="{facebook_imgurl}" width="100px;">
				</span>
				</div>
				<div style="float: left; width: 350px; margin-right: 10px;">
					<h2>
						<span contenteditable="true"  
							class="text_editable audit_right <?php if(isset($fail_state['fail_title_t'])  && $fail_state['fail_title_t']):?>not_pass<?php endif;?>"
							data-name="topic" name="fail_title_t">{topic}</span>
					</h2>
					<span 
						style="display: block; font-size: 16px;"
						class="text_editable audit_right <?php if(isset($fail_state['fail_select_t'])  && $fail_state['fail_select_t']):?>not_pass<?php endif;?>"
						data-name="topic_optiontitle"
						name="fail_select_t">{topic_optiontitle}</span>
					<div class="topic_group">
						<strong id="options_select_name">{options_select_name}</strong>
						<div id="topic_group_name">
							<ul>
								{topic_group_list}
								<li class="{checked}" data-index="{group_index}">{name}</li>
								{/topic_group_list}
							</ul>
						</div>
						
						<?php if(!empty($auto_index)):?>								
								<ul class="options_select">			
									<?php foreach($topic_select AS $value):?>	
									<li onclick="group_editor(this);"
								data-index="<?php echo $value['auto_index'];?>"><?php echo $value['select_name']?></li>
									<?php endforeach;?>					
								</ul>	
						<?php endif;?>	
										
					</div>

				</div>
				<span data-title="指令圖片" 
					class="topic_photo audit_right <?php if(isset($fail_state['fail_select_p']) && $fail_state['fail_select_p']):?>not_pass<?php endif;?>"
					style="margin-top: 25px;" name="fail_select_p"> 
					<img data-name="options_imgurl" class="editable" alt="" src="{options_imgurl}" width="100px;"></span>
				<br class="clear">
			</div>
		</div>

		<div class="fancybox-inside">
			<div class="fancybox-inside-bg-top">
				<div class="fancybox-inside-bg-bottom">
					<div class="resume slide">

						<div class="topic_option_list">
							{topic_options}
							<div class="page-content dropimg-content">
								<div class="entry audit_left {fail_options_t}"
									style="font-size: 14px;" name="fail_options{AI}_t">

									<strong style="color: #000; display: block;"> 
									<span contenteditable="true" class="text_editable data_title" data-title="選項" data-type="title" value="{choose}" data-index="{AI}" data-name="choose">
										{choose}
									</span>
									</strong> 
									<strong style="color: #000; display: block;"> 
									<span contenteditable="true" class="text_editable data_title" data-title="標題" data-type="title" value="{title}" data-index="{AI}" data-name="title">
										{title}
									</span>
									</strong> 
									<span class="text_editable data_title" data-title="內容" data-type="info" data-index="{AI}" data-name="text">{text}</span>
								</div>
								<h2>
									<div class="gallery-item audit_left {fail_options_p}" name="fail_options{AI}_p">
										<img class="editable" data-name="text_imgurl" data-index="{AI}" src="{text_imgurl}" alt="" width="75px" />
									</div>
								</h2>
							</div>
							{/topic_options}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br class="clear">
</div>


<script>

$(function() {
	set_span_editor();
	$("SPAN.text_editable").attr('contenteditable',true).each(function(){
		if(!$(this).html() || $(this).html() == '') $(this).html('請輸入資料');


		}).on("focus",function(){
			if( $(this).html() == '請輸入資料') $(this).html('');
		}).on("blur",function(){
			if(!$(this).html() || $(this).html() == '') $(this).html('請輸入資料');
		}).on("keyup",function(){
			var text = $(this).html();
			var filed = $(this).attr('data-name');


			var type = ( typeof($(this).attr('data-index')) == 'undefined' )?'topic':'topic_options';
			var index = ( typeof($(this).attr('data-index')) == 'undefined' )?$('INPUT[name="auto_index"]').val():$(this).attr('data-index');
						
			if(index && text != '請輸入資料') {
				$.ajax({
					  url: 'topic/update_topic/',
					  dataType: 'html',
					  type: "POST",
					  data: {			  
						  index:index,
						  filed:filed,
						  value:text,
						  type:type,
						  
					  },
					  success: function(data){
						  
						  if(data) {
							  $(this).html(data);
						  }
					  }
					});

			};

		});
	
	});


//FB圖片預覽
$("DIV.fb_view_control SPAN").on('click', function() {
	if( $(this).attr('data-fb') == "close"){
		$('#img_selectbox').hide();	
		
	}else{		
		$("DIV.fb_view").attr('class','fb_view');	
		$("DIV.fb_view").addClass( $(this).attr('data-fb'));
	}
});
$("IMG.editable").on('click', function() {
	$("IMG.editable").removeClass("focus");
	$(this).addClass("focus");
	
	$('#img_selectbox').show(500);
	 
	$('#img_selectbox .fb_view').attr('class','fb_view').css('background', 'url("'+$(this).attr("src")+'") center center no-repeat');
});

<?php if($onshelf_purview):?>

$(".topic_group #options_select_name").on("click",function(){
	if($(this).parent().hasClass("editable")) $(this).parent().removeClass("editable")
	else $(this).parent().addClass("editable");	
});



$("#defoption_select").on("click",function(){
	if($(".defoption_selector").is(':hidden') ) $(".defoption_selector").show();
	else $(".defoption_selector").hide();		
});



$("#topic_group_name UL LI").on("click",function(){
	if($(this).hasClass("checked"))  $(this).removeClass("checked");
	else $(this).addClass("checked");

	var topic_index = $('INPUT[name="auto_index"]').val();
	if(topic_index<1) return false;
	
	var group_list = new Array();
	$('#topic_group_name UL LI.checked').each(function(){			
			group_list.push($(this).attr('data-index'));
		});

	$.ajax({
		  url: 'topic/group_topic/',
		  dataType: 'html',
		  type: "POST",
		  data: {
			  group_list:group_list,
			  index:topic_index
		  },
		  success: function(data){
			  if(data) alert(data);
		  	/**/
		  }
		});
});


function audit(){
	
	var topic_index = $('INPUT[name="auto_index"]').val();
	if(topic_index<1) return false;
	
	var audit_list = new Array();
	$('.audit_left.not_pass, .audit_right.not_pass').each(function(){
			eval('audit_list.push({"'+$(this).attr('name')+'":"true"});');	
			
		});

	
	$.ajax({
		  url: 'topic/audit_topic/',
		  dataType: 'html',
		  type: "POST",
		  data: {
			  audit_list:audit_list,
			  index:topic_index
		  },
		  success: function(data){
			  console.info(data);
		  	/**/
		  }
		});
	
	
}

function group_editor(el){	
	var topic_index = $('INPUT[name="auto_index"]').val();
	var select_index = $(el).attr("data-index");

	
	$.ajax({
		  url: 'topic/change_options_select/',
		  dataType: 'json',
		  type: "POST",
		  data: {
			  topic_index:topic_index,
			  select_index:select_index
		  },
		  success: function(data){
			  if(data) {
				alert(data);
			  }else{
				  $("#options_select_name").html($(el).html());
				  
			  }
			  $(".topic_group.editable").removeClass("editable");
		  }
		});
	
}

<?php endif;?>


//檔案拖拉

$("DIV.dropimg-content, SPAN.topic_photo").each(function(){
	this.addEventListener("dragenter", dragEnter, false);
	this.addEventListener("dragexit", dragExit, false);
	this.addEventListener("dragover", dragOver, false);
	this.addEventListener("dragleave", dragLeave, false);
	this.addEventListener("drop", drop, false);	
});

function dragEnter(evt) {
	$('#img_control').remove(); 
	$(".dropping").removeClass("dropping");
	evt.stopPropagation();
	evt.preventDefault();
	$(this).addClass("dropping");
}

function dragExit(evt) {
	evt.stopPropagation();
	evt.preventDefault();
}

function dragLeave(evt) {
	evt.stopPropagation();
	evt.preventDefault();	
	if( $(this).hasClass("dropping")) $(this).removeClass("dropping");
}
function dragOver(evt) {
	evt.stopPropagation();
	evt.preventDefault();
	if(!$(this).hasClass("dropping")) $(this).addClass("dropping");	
}

function drop(evt) {

	if(( typeof( $(this).attr('data-src')) == 'undefined')) 
		$(this).attr('data-src',$(this).attr('src'));

	
	if(( typeof( $(this).attr('data-width')) == 'undefined')) 
		$(this).attr('data-width',this.naturalWidth);
	if( typeof( $(this).attr('data-height')) == 'undefined') 
		$(this).attr('data-height',this.naturalHeight);
	

	
	$(this).removeClass("dropping");
	evt.stopPropagation();
	evt.preventDefault();

	imgreplacebox(this);
	
	var dat = $(evt.dataTransfer.getData('text/html'));
    var img = dat.attr('src');
    if (!img) img = dat.find("img").attr('src');
    
	var files = evt.dataTransfer.files;
	var count = files.length;

	
	// Only call the handler if 1 or more files was dropped.
	if (count > 0 && handleFiles(files)){
		$(this).addClass('Upload_img');		
	}
	else if(img){		
		$("#new_img").attr('src',img);		
	}else{
		init_img_controllor(true);
	}
}

//檔案讀取

function handleFiles(files) {
	
	var file = files[0];
	var check_file  = null;
	if(typeof(file) != "undefined")
		check_file = file.type.match('image');

	if(check_file == null) {
		alert('上傳檔案需為圖檔');			
		return false;
	}
	var reader = new FileReader();

	reader.onloadend = handleReaderLoadEnd;

	reader.readAsDataURL(file);
	return true;
}

function handleReaderLoadEnd(evt) {
	$("#new_img").attr('src',evt.target.result);

}




function init_img_controllor(inputfile){
	//初始化
	$("#img_control").remove();
	$(".Upload_img").removeClass("Upload_img");
	if(inputfile) $("#upload_file").val('');
	console.info('init');
}
function save_img(){

	
	
	var old_img = $("#img_control #old_img").attr("src");
	var new_img = $("#img_control #new_img").attr("src");
	var filed = $("#img_control").parent().find("IMG.editable");
	var index = ( typeof($(filed).attr('data-index')) == 'undefined' )?$('INPUT[name="auto_index"]').val():$(filed).attr('data-index');
	var type = ( typeof($(filed).attr('data-index')) == 'undefined' )?'topic':'topic_options';
	console.info(index);
	console.info(filed);

	$.ajax({
		  url: 'topic/update_topic/',
		  dataType: 'html',
		  type: "POST",
		  data: {			  
			  index:index,
			  filed:filed.attr('data-name'),
			  value:new_img,
			  type:type
		  },
		  success: function(data){
			  
			  if(data) {
				  alert(data);
				  $("#img_control").remove();
				  $('#img_selectbox').hide();
			  }else{
				  $("#img_control").parent().find("IMG.editable").attr("src",new_img);
				  $("#img_control").remove();
				  $('#img_selectbox').hide();
				  
			  }
				  
		  	/**/
		  }
		});
	
}

function upload_img(){
	
	var div_el = $('IMG.editable.focus').parents('DIV.page-content.dropimg-content');
	var span_el = $('IMG.editable.focus').parents('SPAN.topic_photo');

	imgreplacebox((span_el.length)?span_el:div_el);


	var fileInput = document.getElementById('upload_file');
	 
	if( handleFiles(fileInput.files) ){
		if(span_el.length) span_el.addClass('Upload_img');
		else div_el.addClass('Upload_img');

	}else {
		init_img_controllor(true);
		return false;

	}
	/**/
	
}



function imgreplacebox(el){

	init_img_controllor(false);
	
	var old_img = $(el).find("IMG.editable").attr("src");

	var img_control = document.createElement('div');
	$(img_control).attr("id","img_control");
	
	$(el).prepend(img_control);
	
	
	$("#img_control").prepend('<img id="new_img"  width="200px"  />')
	$("#img_control").prepend('<img id="old_img"  src ="'+old_img+'" width="200px"  />')
	
	
	
	
	var control_btn = document.createElement('div');
	$(control_btn).attr("id","control_btn");
	$("#img_control").append(control_btn);

	
	
	var control_bar = new Tool(canvasToolConfig);
	
	$("#new_img").load(function() {

		$('#img_selectbox').hide();	
		if( typeof( $(this).attr("data-src") ) == "undefined" || 
			typeof( $(this).attr("data-width") ) == "undefined" ||
			typeof( $(this).attr("data-height") ) == "undefined")
		{				
			return false;
		}
			
		
		var width = this.naturalWidth;
		var height = this.naturalHeight;

		
		if(width == $(this).attr("data-width") && height == $(this).attr("data-height")) 
		{
			if( false == confirm("是否變更圖片") ){
				reduction_img(this);
			}else if(false) {
				save_img(this);
			}
		}	
		else {
			alert('圖片規格不符');
			reduction_img(this);		
		}	
		
	})
	
	$("#new_img,#old_img").on("click",function(){		

		
		$('#img_selectbox').show(500);
		$('#img_selectbox .fb_view').attr('class','fb_view').css('background', 'url("'+$(this).attr("src")+'") center center no-repeat');

		});
	$("#new_img").error(function(){
		if( typeof($(this).attr("data-src")) != 'undefined' ){
			alert('圖片讀取有誤!');
			reduction_img(this);	
		}
		
	});
}
</script>