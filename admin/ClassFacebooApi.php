<?php 
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\GraphSessionInfo;
use Facebook\FacebookClientException;
use Facebook\FacebookServerException;
use Curl\Curl;

class pofans2_fb_crud 
{
	public $appid = '792264297536375'; // your AppID
	public $secret = 'c6c8fe7316e88b4b16e7d987664485e8'; // your secret	
	private $access_token;
	public $session;
	public $session_long='';

    public function __construct($session_token = null, $appid = null, $secret = null)
    {
	
		if(!isset($session_token)) $session_token = graph_helper::$test_token;
		if(!empty($appid) && !empty($secret)){
			$this->appid = $appid;
			$this->secret = $secret;
		}
		FacebookSession::setDefaultApplication($this->appid ,$this->secret);	
		$this->access_token = $session_token;
		$this->session = new FacebookSession($session_token);

    }
	
	function set_logn_session(){
		try{
			$this->session_long = $this->session->getLongLivedSession($this->appid ,$this->secret);
		}catch(Exception $ex){
			print_r($ex);
		}		
	}
	
	function get_session(){
		return  $this->session_long;
		
// 		return $this->session->getExchangeToken($this->appid ,$this->secret);
	}
	 
	 /**
	 * 取得token 授權使用者資訊
	 *
	 * @return array 使用者資訊
	 */	
	function get_user_info($id=''){
		$instruct = !empty($id) ? "/".$id : "/me";
		$request = (new FacebookRequest(
		  $this->session,
		  'GET',
		  $instruct
		))->execute()->getGraphObject()->asArray();
		return $request;
	}
	
	 /**
	 * 取得目前使用者管理的粉絲頁資訊
	 *
	 * @return array 粉絲頁資訊
	 */
	function get_pages_info($id=''){
		$instruct = !empty($id) ? "/".$id."/accounts" : "/me/accounts";
		$request = new FacebookRequest(
		  $this->session_long,
		  'GET',
		  $instruct
		);
		$response = $request->execute();
		$graphArray = $response->getGraphObject()->asArray();
		return (isset($graphArray['data'])) ? $graphArray['data'] : false;
	}
	
	/*自訂graph api 指令*/
	/*if instruct is fql , input '/fql?q='.{your fql}, plz.*/
	function graph_api($instruct='', $method = 'GET', $input = array(), $curl = false){
	  try {
			$request = new FacebookRequest(
			  $this->session,
			  $method,
			  $instruct,
			  $input
			);
			$response = $request->execute()->getGraphObject();
			return $response;
		} catch(FacebookRequestException $ex) {
			return false;
		} catch(Exception $ex) {
			return false;
		}	

	}
	//取得粉絲團資料
	function getPagesData($url_or_ID){
		try {
			$request = new FacebookRequest(
			  $this->session,
			  "GET",
			  "/".$url_or_ID
			);
			return $request->execute()->getGraphObject()->asArray();
		} catch(FacebookRequestException $ex) {
			$error = $ex->getRawResponse();
			$save = json_decode($error);
			return $save->error->message;
		} catch(Exception $ex) {
			return $ex;
		}	
	}
	
	/*刪掉粉絲訊息*/
	function DelPost($post_id){
		try {
			$request = new FacebookRequest(
			  $this->session,
			  'DELETE',
			  '/'.$post_id
			);
			$response = $request->execute();
			$graphObject = $response->getGraphObject();
			return $graphObject;
		} catch(FacebookRequestException $ex) {
			$error = $ex->getRawResponse();
			$save = json_decode($error);
			return $save->error->message;
		} catch(Exception $ex) {
			return $ex;
		}	
	}
	
	
	
	/*發送到粉絲團專頁*/
	function post_to_pages($id, $input = array(), $type='feed', $curl = false){
		$instruct = '/'.$id.'/'.$type;
		if($curl){
			// unset($input['link']);
			$response = $this->PostPagesCurl($id, $type, $input);	
			if(isset($response->error)){
				/********************************************************************
				* 8/25發現facebook連結文章時存在著回傳錯誤但文章卻正常發佈出去的問題
				*********************************************************************/
				if($response->error->message == 'Unsupported post request.'){
					//echo "連結發佈發生例外";
					return $this->ExceptionDeal($id, $input['link']);
				}
				/********************************************************************
				* 
				*********************************************************************/
				return $response->error->message;
			} else {
				// show($response);
				return ['id' => $response->id, "post_id" => isset($response->post_id) ? $response->post_id : ""];
			}
			
		} else {
			try {

				$response = (new FacebookRequest(
				$this->session, 'POST', $instruct, $input
				))->execute()->getGraphObject();
				return $response->asArray();
				
			} catch(FacebookRequestException $ex) {
				// When Facebook returns an error
				// handle this better in production code
				$error = $ex->getRawResponse();
				$save = json_decode($error);
				return $save->error->message;
			} catch(Exception $ex) {
				// When validation fails or other local issues
				// handle this better in production code
				return $ex;
			}
		}
	}
	
	private function PostPagesCurl($id, $type, $input = array()){
		$curl = new Curl();
		$input['message'] = $input['message'];
		$input['access_token'] = $this->access_token;
		$curl->post('https://graph.facebook.com/'.$id.'/'.$type, $input);
		// show($curl->response);
		return $curl->response;
	}
	
	private function ExceptionDeal($id, $link){
		$content = $this->graph_api("/".$id."/posts?fields=link,id&limit=5")->asArray();
		$search = array();
		if(!isset($content['data'])) return '完全沒有文章';
		foreach($content['data'] as $value){
			if(trim($value->link) == trim($link)){
				// $post_id = explode("_", $value->id);
				$search = [
					'id' => $value->id,
					'ex' => '例外處理'
				];
				break;
			}
		}
		return !empty($search) ? $search : "找不到文章資訊";
	}
	
	public function token_debug($debug_token){
		$request = new FacebookRequest($this->session, 'GET', '/debug_token',
			array(  
			 'input_token' => $debug_token,           
			 'access_token' => $this->access_token,
			)
		);
		return $request->execute()->getGraphObject()->asArray();
	 }

	
}

class graph_helper{

	public static $test_token = 'CAALQj2gF23cBAEKe3V5qQAII5oH1fDl1tPKz1qcG7iyN7GTu72lJ5ZAlz0fPZCkwAXTrL1wxLztNtpcoviJ6N4YT3fRI25sXZCKpc50YpZAltXxMb8qg1RFTe86aCIY3xysTV8q2VrK0HIelvOEJXitGGK0NsBwAnvO1t3UmBZAZAuSrkfDZB0ZC8A765jrlxsZCZAmUh4yOr59cbvMh9UK3dF';
	
	public static function url_api($instruct, $https = false){
		$protocol = ($https) ? 'https' : 'http';
		
		return file_get_contents($protocol.'://graph.facebook.com/'.$instruct);

	}

	/*public static function token_dubg($token){

		return self::url_api('debug_token?input_token='.$token, true);
	}*/
}
?>