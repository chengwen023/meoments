<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *	Adonis
 *	後台預設資料讀取
 */

class Category_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}
	
	function check_category_exist_and_insert($add_input, $add_options){
		$result = array('success' => 'N', 'msg' => '');
		
		//檢查有無重複名稱父分類
		$this->db->flush_cache();
		$query = $this->db->select("COUNT(*) AS qun")
		->from("categories")
		->where("name", $add_input['name'])
		->where("online", '1')
		->get();
		$temp = $query->row_array();
	
		if ( $temp['qun'] == 0 ) {
			$date = date('Y-m-d H:i:s');
			$add_input['create_time'] = $date;
			$add_input['update_time'] = $date;
			$this->db->insert('categories', $add_input);		

			//建立子分類
			$parent_category_index = $this->db->insert_id();
			if ( $parent_category_index ) {
				// $this->db->delete('article_defoptions', array('defoptions_group_index' => $defoptions_select_index));
				foreach ( $add_options AS $key => $url ) {
					$url['name'] = preg_replace("/(\s|\&nbsp\;|　|\xc2\xa0)/", "", strip_tags($url['name']));
					$url['parent_id'] = $parent_category_index;
					$url['create_time'] = $date;
					$url['update_time'] = $date;						
					$this->db->insert('categories', $url);
				}
			}

			$this->load->driver('cache');
			$this->cache->memcached->delGlobalMemcached();
			
			$result['success'] = 'Y';
			$result['msg'] = $parent_category_index;
		}else{
			$result['msg'] = '此題目已經有了哦!!!';
		}

		return $result;
	}
	
	function check_category_exist_and_update($add_input, $add_options, $index){
		$result = array('success' => 'N', 'msg' => '');
	
		$this->db->flush_cache();
		$query = $this->db->select("COUNT(*) AS qun")
		->from("categories")
		->where("id", $index)
		->get();
		$temp = $query->row_array();
	
		if( $temp['qun'] == 0 ) {
			$result['msg'] = '無該筆資料';
		} else {
	
			$this->db->where('id', $index);
			$this->db->update('categories', $add_input);
			
			//子分類更名、新增、刪除
			$parent_category_index = $index;
			if ( $parent_category_index ) {
				// //將所有父類別下的子分類online改為0
				// $this->db->flush_cache();
				// $this->db->where('parent_id', $parent_category_index);
				// $this->db->update('categories', array('online' => '0'));

				$date = date('Y-m-d H:i:s');
				foreach($add_options AS $key => $url) {
					$this->db->flush_cache();
					$query = $this->db->select("COUNT(*) AS qun")
					->from("categories")
					->where("id", $url['id'])
					->get();
					$temp = $query->row_array();
					
					
					$url['name'] = preg_replace("/(\s|\&nbsp\;|　|\xc2\xa0)/", "", strip_tags($url['name']));

					if ( $temp['qun'] == 0 ) {
						$url['parent_id'] = $parent_category_index;
						$url['create_time'] = $date;
						$url['update_time'] = $date;						
						$this->db->insert('categories', $url);
					} else {
						$this->db->flush_cache();
						$this->db->where('id', $url['id']);
						$this->db->update('categories', array('name' => $url['name'], 'online' => $url['online'], 'update_time' => $date));
					}
				}

				/*$this->db->delete('article_defoptions', array('defoptions_group_index' => $defoptions_select_index));				
				foreach($add_options AS $key => $url){
					$url['defoptions_group_index'] = $defoptions_select_index;						
					$this->db->insert('article_defoptions', $url);
				}*/
			}

			$this->load->driver('cache');
			$this->cache->memcached->delGlobalMemcached();
			
			$result['success'] = 'Y';
			$result['msg'] = '修改成功';
		}
		return $result;
	}
	
	function del_category_by_index($index) {
		$this->db->flush_cache();
		// $this->db->delete('article_defoption_group', array('auto_index' => $index));
		// $this->db->delete('article_defoptions', array('defoptions_group_index' => $index));
		$this->db->delete('categories', array('id' => $index));
		$tmp = array('success' => 'Y', 'msg' => '已刪除', 'index' => $index);

		$this->load->driver('cache');
		$this->cache->memcached->delGlobalMemcached();
		
		return $tmp;
	}
}