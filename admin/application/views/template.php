<!DOCTYPE html>
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<base href="<?php echo base_url(); ?>" />
<title>後台管理</title>


<link rel="Shortcut Icon" type="image/x-icon" href="favicon.ico">
<link href="css/style.default.css" rel="stylesheet" type="text/css">
<link href="css/style.dark.css" rel="stylesheet" type="text/css">
<link href="css/styles.css" rel="stylesheet" type="text/css">
<link href="css/admin.css" rel="stylesheet" type="text/css">




<!--[if IE]> <link href="css/ie.css" rel="stylesheet" type="text/css"> <![endif]-->

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/libs/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/libs/jquery.dragsort.js"></script>
<script type="text/javascript" src="js/libs/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="js/libs/fancybox/jquery.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="js/libs/fancybox/jquery.fancybox.css" media="screen" />
<link href="css/fancybox-style.css" rel="stylesheet" type="text/css">
</head>


<body>

<!-- Top line begins -->
<div id="top">
    <div class="wrapper">
        <a href="index.html" title="" class="logo"><img src="../favicon.ico" alt="" width="28px"></a>
        
        <!-- Right top nav -->
        <div class="topNav">
            <ul class="userNav">
                <li><a title="搜尋" class="search animate0"></a></li>
                <li><a title="" class="screen animate0"></a></li>
                <li><a title="欄位顯示" class="settings animate0"></a></li>
                <li><a title="登出" href="<?php echo base_url('user/logout'); ?>" title="" class="logout animate0"></a></li>
                
            </ul>            
            <div class="topSearch topSearchBox">
                <div class="topDropArrow"></div>
                
					<form action="" method="post">
                    <input type="text" placeholder="search..." name="topSearch" style="height: 28px;">
                    <input type="submit" value="">
                    </form>
            </div>
            <div class="topSearch topSettings">
                <div class="topDropArrow"></div>
                <UL></UL>
            </div>
        </div>
        
    </div>
</div>
<!-- Top line ends -->


<!-- Sidebar begins -->
<div id="sidebar" class="with">
    <div class="mainNav">
    	<!-- Tabs leftmenu -->
		 <div class="leftmenu">        
            <ul class="nav nav-tabs nav-stacked">
            	<li class="nav-header animate0 fadeInUp">後台管理</li>                
                {menu}
                <li class="dropdown animate{group_index} fadeInUp"><a href="#"><i class="icon-list"></i><span>{group_name}</span></a>
                	<ul>                	
                	{childmenu}
                    	<li class="animate{child_index} flipInX {add_class}"><a href="{url}">{child_name}</a></li>
                     {/childmenu}
                    </ul>
                </li>
               {/menu}
            </ul>
        </div>
       
    </div>
   
</div>
<!-- Sidebar ends -->
    
    
<!-- Content begins -->
<div id="content">

	    <div class="contentTop">
	        <span class="pageTitle"><i class="icon-picture"></i>{title}</span>
	        <ul class="quickStats">
	            <li>
	                <a href="" class="blueImg"><i class="icon-picture" style="margin: 8px auto"></i></a>
	                <div class="floatR"><strong class="blue">5489</strong><span>上架</span></div>
	            </li>
	            <li>
	                <a href="" class="redImg"><i class="icon-picture" style="margin: 8px auto"></i></a>
	                <div class="floatR"><strong class="blue">4658</strong><span>未上架</span></div>
	            </li>
	            <li>
	                <a href="" class="greenImg"><i class="icon-picture" style="margin: 8px auto"></i></a>
	                <div class="floatR"><strong class="blue">1289</strong><span>總數</span></div>
	            </li>
	        </ul>
	    </div>
	    
	    <!-- Breadcrumbs line -->
	    
	    <div class="breadLine">
	        <div class="bc">
	        	<ul class="breadcrumbs">
	        	{breadcrumbs}
	                <li><a href="#">{group_name}</a></li>                
	                <li class="current"><a href="#" title="">{child_name}</a></li>
	            {/breadcrumbs}
	            </ul>
	        </div>
	        
	        <div class="breadLinks">
	            <ul>
	            	{topbar_control}
	                <li><a title="" href="{current_url}"><i class="{i_class}"></i><span>{control_name}</span></a></li>
	                {/topbar_control}
	            
	            	{select_result}
	                <li class="has">
	                    <a title="" href="{current_url}"><i class="icon-list"></i><span>{select_name}</span></a>
	                    <ul>
	                    {select_option}
	                        <li><a href="{current_url}{value}" title=""><i class="icon-user"></i>{label}</a></li>
	                    {/select_option}    
	                    </ul>
	                </li>
	                {/select_result}
	            </ul>
	        </div>
	    </div>
    
    <!-- Main content -->
    <div class="wrapper">{main_content}</div>
    <!-- Main content ends -->
</div>
<!-- Content ends -->
<script>

$('thead tr').find('TD').each(function(index){
	var title=$(this).attr("title");
	if(typeof(title) == "undefined") title=$(this).html();
	var add_li = document.createElement('LI');
	add_li.innerHTML=title;
	
	
	if(typeof(title) != "undefined") {
		$('.topSearch.topSettings UL').append(add_li);
		if($(this).hasClass("rowhide")) $(add_li).addClass("rowhide");		
	}
	
    if($("td:nth-child("+(index+1)+")").hasClass("rowhide"))
		{$("td:nth-child("+(index+1)+")").hide();  }	
});

$('.topSearch.topSearchBox INPUT[name="topSearch"]').on('keyup', function() {
	var re = new RegExp($(this).val());

	$('tbody tr').each(function(){
		var reg_tr = $(this);		
		reg_tr.find("TD").each(function(){
			var reg_td = $(this).html();
			reg_tr.hide();
			if (reg_td.match(re)) {
				reg_tr.show();
				return false;		  
			}				
		});		
	});
  });

$('DIV.topSearch.topSettings').hover(
		function(){},
		function(){	$(this).hide();});

$('.userNav LI A').hover(
		function(){	$(this).addClass('swing');},
		function(){	$(this).removeClass('swing');});

$(".topNav .userNav LI A.settings").on('click', function() {		
	   if($('.topSearch.topSettings').is(':visible'))
	    	{  $(".topSearch.topSettings").hide();  }
	    else 
	    	{  $(".topSearch").hide();$(".topSearch.topSettings").show();  } 
	   });

$(".topNav .userNav LI A.search").on('click', function() {
	   if($('.topSearch.topSearchBox').is(':visible'))
	    	{  $(".topSearch.topSearchBox").hide();  }
	    else 
	    	{  $(".topSearch").hide();$(".topSearch.topSearchBox").show().find('INPUT[name="topSearch"]').focus();  } 
	   });

var items = $('.topSearch.topSettings UL li').click(function() {
    var index = items.index(this);
    
    if($("td:nth-child("+(index+1)+")").is(':visible'))
	{ $(this).addClass("rowhide"); $("td:nth-child("+(index+1)+")").hide();  }
	else 
	{ $(this).removeClass("rowhide"); $("td:nth-child("+(index+1)+")").show();  } 
});
</script>

</body>
</html>