<?php 
	$start = microtime(true);
	session_start();
	header("Content-type: text/html; charset=utf-8");

	include_once("include/db.php");
	include_once("include/htmltpl.php");
	include_once("include/function.php");

	
	$curPageURL	=  curPageURL();
	$curServerURL = curPageURL(true);	
	$authorID = $_GET['author'];
	$author = explode("/", $_GET['author']);	
	$author = array_filter($author);
	$author_name = array();
	
	
	include_once('global_load.php');
	
	
	//判斷文章是否上架，導回首頁
	if(is_numeric($authorID) ) {
		$author_total = AuthorOnlineCkeck($authorID,$curServerURL);
	}else{
		//熱門分類
		$author_total = GetArticleTotal();
	}
	
	
	$author_title = $author_name[$authorID]['name'];
	
	if(!$author_title) $author_title = 'New';
	$author_url = $curServerURL."/author/$author_title/";
	
	$pageNum = (int)((isset($_GET['page']) && $_GET['page'] > 0)?$_GET['page']:1);
	$pageRow = 18;
	$pageTotal = ceil($author_total / $pageRow);
	if($pageNum > $pageTotal ) $pageNum = $pageTotal;
	  
	$limit = (($pageNum - 1)*$pageRow).", $pageRow";
	
	
	
	
	
	
	if(is_numeric($authorID)) $where = "AND `author_id` =  $authorID "; 	
	
	$order = ' `online_time` DESC ';
	
	$sql = " SELECT `news_article_onshelf`.title , $img_tm AS img, $listUrl AS url , nas.*
	FROM  news_article_onshelf  
	INNER JOIN (
		SELECT DISTINCT `id` , `tag` , `online_time` , `views`,`daily_views`
		FROM  `news_article_state` 
		WHERE `online` =  '1' $where  ORDER BY $order LIMIT $limit 
	) AS nas ON nas.id =  `news_article_onshelf`.id ";
	
	$memcached->set_prefix("author[$authorID]");
	$listKey = 'pagelist_'.$pageNum;
	$keyValues[] = [
			'key' => $listKey ,
			'func' => $memcached->SQLfunc( $sql ),
			'expire' => 7200
	];

	$keyValues  = $memcached->getMultiWithSet($keyValues,86400);
	
	
	foreach($keyValues[$listKey] AS $row){
		if($row['tag']) {			
			$row['tag'] = array_values(array_intersect_key( $tag_name,	array_flip( json_decode($row['tag'])) ));			
		}
		
		$row['online_time'] = date('Y-m-d', $row['online_time']);		
		$data['article_list'][] = $row;
		
	}
	
	$head = array(   
   'url'  => $curPageURL,
   'baseUrl'   => $baseUrl,
   'title'  => $author_title,
   'link:img_src' => $article['img'],
   'meta' => array(
            
     ['name' => 'name="keywords"'     ,'content'=> $author_title ],
     ['name' => 'name="description"'    ,'content'=> $author_title ],
     ['name' => 'property="og:description"'    ,'content'=> $author_title ],
     
     ['name' => 'property="og:title"'   ,'content'=> $author_title ],
     ['name' => 'property="og:image"'   ,'content'=> $ImgServerUrl.'/n'.$article['id'].'/f.jpg'],
     ['name' => 'property="og:type"'   ,'content'=> "article" ],
     ['name' => 'property="og:url"'   ,'content'=> $curPageURL ],
   	 ['name' => 'property="fb:admins"'    ,'content'=> $fb_admin ],
   	 ['name' => 'property="fb:app_id"'    ,'content'=> $fb_app_id ],
   )
 );
	
	
	
	
	
	$data['HEAD'][] = $head;
	$data['MENU'] = $category_name;
	$data['mobile_MENU'] = $category_name;
	$data['curServerURL'] = $curServerURL;
	
	$data['pageUrl'] = $author_url;
	$data['pageNum'] = $pageNum;
	$data['pageTotal'] = $pageTotal;
	
	$HTML['HTML'][] = $data;
	
	echo tpl_get_html("./tpl/","author.tpl.html",$HTML);
	
	
?>