
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News_category extends CI_Controller {
	
	var $menu_index;
	
	public function __construct(){
		parent::__construct();
		
		$this->userInfo = $this->session->userdata('userInfo');
		$this->load->model('theme_model', 'theme');
		// $this->load->model('defoption_model', 'defoption');
		$this->load->model('category_model', 'category');
		
		$this->data['admin_menu'] = $this->config->item('admin_menu');
		$this->data['admin_menuGroup'] = $this->config->item('admin_menuGroup');
		$this->data['userInfo'] = $this->userInfo;
		$this->data['menu'] = $this->config->item('admin_accountMenu');
		$this->data['breadcrumbs'] = $this->config->item('breadcrumbs');
		$this->data['title'] =$this->config->item('admin_title');
		
		$this->data['topbar_control']= array();
		$this->data['select_result'] = array();
		
		
	}
	
	function create_category(){
		$topic_array = $this->input->post('topic_array');		
		$auto_index = $this->input->post('auto_index');
		
		$date = date ("Y-m-d H:i:s");
		$account_id = $this->config->item('admin_accountIndex');
		
		$add_input = array (
			'name'       => preg_replace("/(\s|\&nbsp\;|　|\xc2\xa0)/", "", strip_tags($topic_array['defoptions_group_name'])),
			'parent_id'  => 0
		);

		$add_options = isset($topic_array['options']) ? $topic_array['options'] : false;
		// $add_input ['create_time'] = $date;
		// $add_input ['create_account_index'] = $account_id;
		// $add_input ['update_time'] = $date;
		// $add_input ['update_account_index'] = $account_id;
		
		//判斷父分類
		if ( $topic_array['id'] != 0 ) {
			$result = $this->category->check_category_exist_and_update($add_input, $add_options, $topic_array['id']);
		} else {
			$result = $this->category->check_category_exist_and_insert($add_input, $add_options);
		}

		$this->cache->memcached->delGlobalMemcached();
	}
	
	public function index(){
		$this->lists();
	}

	public function get_pofans2_category(){
		$json = file_get_contents('http://i-kuso.com/pofans2/api/get_groups.php?back_door=true');
		$json = json_decode($json, true);
		$newjso = array();
		foreach($json as $key => $val){
			$newjson[] = array(
				'value' => $key,
				'text' => $val
			);
		}

		echo json_encode($newjson);
	}

	function save_pofans2_category(){
		$pofans_id = $this->input->post('value');
		$id = $this->input->post('pk');
		
		if ( !is_array($pofans_id) ) return;
		echo $pofans_id = json_encode($pofans_id);

		$this->db->flush_cache();
		$this->db->where('id', $id);
		$this->db->update('categories', array('pofans2_category' => $pofans_id));
	}

	function lists(){
		$data = $this->data;
		$this->menu_index = 'POFANS分類管理';
		if(false === $activestatus = $this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, 'V')){
			$this->parser->parse('template', $data);
			return false;
		}
		
		// $defaultstatus = (isset($activestatus))?$activestatus:$this->config->item('defaultstatus');
		
		$this->db->flush_cache();
		$this->db->select("*");
		$this->db->from("categories");
		$this->db->where("parent_id",'0');
		
		$query = $this->db->get();
		
		$data['content'] = $query->result_array();
		foreach($data['content'] AS $key => $list){
			$data['content'][$key]['off'] = ($list['online']) ? '' : 'off';
			$groups_pofans = !empty($list['pofans2_category']) ? implode(',', json_decode($list['pofans2_category'])) : '';
			$data['content'][$key]['pofans2_category_edit'] = '<a href="#" class="groups_show" data-type="select2" data-pk="'.$list['id'].'" data-value="'.$groups_pofans.'" ></a>';
		}

		$data['main_content'] = $this->parser->parse('source/news_category_list', $data, true);
		$this->parser->parse('template', $data);
	}
	
	function choose_data(){
		$type = $this->input->post('type');
		
		$this->db->select("*")->from("topic_defaultoptions")->where("select_index",$type);
			$query = $this->db->get();
			$data = $query->result_array();
			foreach($data AS $item){
				echo '<li><div>選項:'.$item['option_name'].'<input type="hidden" name="options[choose][]" value="'.$item['auto_index'].'"/></div><div class="imgurl_box" ><span><img src="uploads/temp/default_pic.jpg" width="70px" /></span><input type="hidden" value="default_pic.jpg" name="options[text_imgurl][]"/></div></li>';
			}
	}
	
	/**
	 *	actions
	 */
	//上下架分類
	function category_online(){
		$index = $this->input->post('index');
		$online = $this->input->post('online');
	
		if($online == 'true') $add_input = array('online' => '1');
		else $add_input = array('online' =>'0');

		$add_input ['update_time'] = date( "Y-m-d H:i:s" );
	
		$this->db->where('id', $index);
		$this->db->update('categories', $add_input);

		$this->load->driver('cache');
		$this->cache->memcached->delGlobalMemcached();
	}

	
	
	function actions($ac){
		switch($ac){		
			case 'add_category' :
				$index = $this->input->post ( 'index' );
				if (! $index) {
					$purview = 'A';
				}else{
					$purview = 'E';
				}
			
				$this->menu_index = '文章列表';
				
				if ($this->admin_model->checkCompetence_getMenuInfo( $this->menu_index, $purview )) {
					$onshelf_purview = true;
				} else {
					$onshelf_purview = false;
				}
			
				/*default value*/
				$defoptions[] = array('id' =>'0', 'name'=>'請輸入分類名', 'parent_id'=>'0', 'online'=>'1');
				$data['article_options'] = $defoptions;
				$data['id'] = 0;
				$data['name'] = '請輸入分類名';
				$data['parent_id'] = 0;
				$data['online'] = 1;

				$this->db->flush_cache();
				$this->db->select ('*')->from ('categories');				
				$this->db->where ('id', $index);
					
				$query = $this->db->get();
				$result = $query->result_array();

				if ( is_array($result) && count($result) ) {
					$this->db->flush_cache();
					$this->db->select ('*')->from('categories')->where('parent_id', $index);
					$query = $this->db->get();
					
					$article_options = $query->result_array();
					if ( !is_array( $article_options ) || !count($article_options) )
					$article_options = $defoptions;
					
					$merge['article_options'] = $article_options;
					$data = array_merge($merge, array_pop($result));
				}

				$this->parser->parse( 'source/news_category_edit', $data );
				break;
			case 'del' :
				$this->menu_index = '新聞分類管理';
				if ( !$this->admin_model->checkCompetence_getMenuInfo($this->menu_index, 'D') ) {
					$result = array (
							'success' => 'N',
							'msg' => '權限不足'
					);
					echo json_encode($result);
					break;
				}
			
				$index = $this->input->post ('index');
				$result = $this->category->del_category_by_index($index);
				$this->cache->memcached->delGlobalMemcached();

				header ( 'Content-type: text/plain; charset=utf-8' );
				echo json_encode ( $result );
				break;
		}
	}
}



/*function news_categories() {
	$this->menu_index = '分類管理';
	
	if (false === $activestatus = $this->admin_model->checkCompetence_getMenuInfo ( $this->menu_index, 'V' )) {
		$this->parser->parse ( 'template', $data );
		return false;
	}
	$data = $this->data;

	$data['title'] = $this->admin_model->getTitle( $this->menu_index );

	$data['main_content'] = $this->parser->parse('article/news_categories', $data, true);
	$this->parser->parse('template', $data);
}*/