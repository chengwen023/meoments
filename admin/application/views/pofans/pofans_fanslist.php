<div id="fb-root"></div>
<script>
(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/zh_TW/all.js#xfbml=1&version=v2.1&appId=806536466070254";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

<div class="online_tab">
	<a href="javascript:void(0);" class="tablectrl_small bBlue" title="上架" alt="上架中" data-online="1" >上架中粉絲團</a>
	<a href="javascript:void(0);" class="tablectrl_small bGold" title="下架" alt="下架中" data-online="0">下架中粉絲團</a> 
</div>

<div class="widget">
	<table cellpadding="0" cellspacing="0" class="tLight daily_left" style="float: left;">
		<form id="list_orderby" method="post">
			<input id="order_by" type="hidden" name="order_by" value="">
			<input id="online" type="hidden" name="online" value="">
		</form>

		<thead>
			<tr>
				<td title="名稱">名稱</td>
				<td title="粉絲團ID">粉絲團ID</td>
				<td title="管理帳號">管理帳號</td>
				<td title="讚數">讚數</td>
				<td title="動作" class="order_control" data-orderby = "online">動作<i class="icon-chevron-{by-online}"></i></td>					
				<td title="toments分類對應">toments分類對應</td>
			</tr>
		</thead>
		
		<tbody>
			{content}
			<tr id="{page_id}" data-title="{page_name}" class="user_row select_tr">
				<!-- <td><a href="http://www.facebook.com/{page_id}" target="_blank">{page_name}</a></td> -->
				<td><div class="fb-like-box" data-href="http://www.facebook.com/{page_id}" data-width="260" data-colorscheme="light" data-show-faces="false" data-header="true" data-stream="false" data-show-border="true"></div></td>
				<td>{page_id}</td>
				<td><a href="http://www.facebook.com/{user_id}" target="_blank">{user_id}</a></td>
				<td class='likes' onclick="show_likes({page_id});">{likes}</td>
				<td>
					<span class="online {off}" data-index="{page_id}"> 
						<a href="javascript:void(0);" class="tablectrl_small bBlue" original-title="Edit" title="上架"
							alt="上架中">上架中<span class="iconb" data-icon=""></span></a>
						<a href="javascript:void(0);" class="tablectrl_small bGold" original-title="Edit" title="下架" 
							alt="下架中">下架中<span class="iconb" data-icon=""></span></a> 	
					</span>

					<a href="javascript:void(0);" class="tablectrl_small bRed" original-title="Del" title="刪除" alt="刪除" onclick="delete_fans({page_id});">
					<span class="iconb" data-icon="" ></span></a>
				</td>
				<td>{toments_category_edit}</td>
			</tr>
			{/content}
			<tr class="likes_total">
				<td>總和</td>
				<td></td>
				<td></td>
				<td class='likes'>{likes_total}</td>
				<td></td>
				<td></td>					
			</tr>
		</tbody>
	</table>
	<div class="group_match" data-title="分類" style="display: none;">
		<div class="group_match_title" data-index="">請先點選左邊粉絲團</div>
		<ul>
			{topic_group}
			<li data-index="{auto_index}">{name}</li>
			{/topic_group}
		</ul>
	</div>
</div>


<div class="daily_right">
</div>

<style>
.online A {
	position: relative;
}

/*.online A:first-child {
	z-index: 100;
}

.online A:not(:first-child ) {
	left: -53px;
	z-index: 1;
	opacity: 0;
	filter: alpha(opacity = 0);
	-moz-transform: rotateY(180deg);
	-webkit-transform: rotateY(180deg);
	transform: rotateY(180deg);
}

.online.off A:first-child {
	opacity: 0;
	filter: alpha(opacity = 0);
	-moz-transform: rotateY(-180deg);
	-webkit-transform: rotateY(-180deg);
	transform: rotateY(-180deg);
}

.online.off A:not(:first-child ) {
	opacity: 1;
	filter: alpha(opacity = 100);
	-moz-transform: rotateY(0deg);
	-webkit-transform: rotateY(0deg);
	transform: rotateY(0deg);
}*/

.online:not(.off) A:not(:first-child ) {
	display: none;
}

.online.off A:first-child {
	display: none;
}

.group_match {
	float: right;
	width: 550px;
	height: 600px;
	overflow: scroll;
	position: relative;
}

.group_match:BEFORE {
	content: attr(data-title);
	display: block;
	font-size: 22px;
	border-bottom: 1px solid #333;
	margin-bottom: 5px;
}

.group_match LI {
	float: left;
	width: 120px;
	border-bottom: 1px solid rgba(0,0,0,.1);
	line-height: 24px;
	cursor: pointer;
}
.group_match LI.selected{
	background: rgba(33,66,99,0.2);
border-radius: 5px;
}
.group_match LI:HOVER {
	border-bottom: 1px solid rgba(0,0,0,1);
}
.group_match LI:BEFORE {
	content: attr(data-index);
	float: left;
	margin-right: 5px;
	padding: 0 2px;
	border-right: 1px solid #369;
	width: 20px;
	text-align: right;
	color: #369;
}
.group_match_title{
	position: absolute;
	right: 0;
	top: 0;
	margin-right: 10px;
	color: #369;
	font-size: 20px;
}
.widget {
	margin-top: 0px;
}
.online_tab {
	padding: 20px; 
}
.daily_left {
	margin-right: 50px;
}
.daily_right {
	float: left;
	width: 400px;
}
tr.likes_total td {
	color: black;
}
tr.likes_total {
	background: rgba(30,144,255,.5);
}
table {
	margin-bottom: 50px;
}
.likes {
	cursor: pointer;
}
.glyphicon-ok:before {
	content: "確定";
}
.glyphicon-remove:before {
	content: "取消";
}
</style>

<script src="js/pofans_fans.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<link media="all" type="text/css" rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/3.5.1/select2-bootstrap.css">
<link media="all" type="text/css" rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/3.5.1/select2.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/3.5.1/select2.js"></script>

<script>
$(".group_match LI").on( "click", function() {
	if($(this).hasClass("selected") )$(this).removeClass("selected");
	else $(this).addClass("selected");

	var index = $(".group_match_title").attr("data-index");

	
	$.ajax({
		url: 'pofans/set_fans_group',
	  cache: false,
	  dataType: 'html',
	  type: 'POST',
	  data:{
		index:index,
		fans_group:get_group_selected()  
	  },
	  success: function(data){
	  	console.info(data);
	  }
	});
	
});	

// $(".select_tr").on("click",function(){
// 	var index = $(this).attr("id");
	
// 	if(index < 1) return false;
	
// 	$(".group_match_title").attr("data-index",index).html($(this).attr("data-title"))
	
// 	$.ajax({
// 		url: 'pofans/get_fans_group',
// 	  	cache: false,
// 		dataType: 'json',
// 	  	type: 'POST',
// 	  	data:{
// 			index:index  
// 	  	},
// 	  	success: function(data){
// 			console.info(data);
// 			$(".group_match LI").removeClass("selected");
// 		  	if(data){
// 				for(var k in data){
// 					$('.group_match LI[data-index="'+data[k]+'"]').addClass("selected");
// 				}
// 			}
// 	  	}
// 	});
	
// });

function get_group_selected(){

	var group_arr = new Array();
	$(".group_match LI.selected").each(function(){
		group_arr.push($(this).attr('data-index'))

		});
	return group_arr;
}
$(".online").click(function() {
	var index= $(this).attr('data-index'); 
	var online = $(this).hasClass("off")
	
	if(online) $(this).removeClass("off");
	else $(this).addClass("off");

	if(index)
		$.ajax({
		  url: 'pofans/fansid_online/',
		  dataType: 'html',
		  type: "POST",
		  data: {
			  online:online,
			  index:index
		  },
		  success: function(data){
			  console.info(data);
		  	/**/
		  }
		});
	
});
function order_by(this_td){
	$('#order_by').val( $(this_td).attr('data-orderby'));
	$('#list_orderby').submit();
}

$(".order_control").live( "click", function() {
	var order_name = $(this).attr('data-orderby');
	if(order_name) order_by(this);
	
	return false;
});

$(".online_tab a").click(function() {
	var online = $(this).attr('data-online');
	$('#online').val(online);
	$('#list_orderby').submit();
});

$(document).ready(function(){
	$.getJSON( "pofans/get_toments_category", function( json ) {
		$('.groups_show').editable({
			title: 'toments群組',
			url:'pofans/save_toments_category',
			source: json,
			select2: {
			   width: 200,
			   multiple: true,
			   allowClear: true
			},
			success: function(res){
				console.log(res);
			}
		});
	 });
});

function show_likes(page_id) {
	$('.daily_right').html('<div style="text-align: center;">更新中</div>');

	var url='pofans/show_table';
	$.ajax({
		url: url,
		cache: false,
		dataType: 'html',
		type: 'POST',
		data:{
			page_id: page_id,
		},
		success: function(data){
			$('.daily_right').html(data);
			
		},error: function(data){
			$('.daily_right').html('失敗');
		}
	});
}

$(document).ready(function() {
	$('.likes').each(function(){
		var num = formatNumber($(this).html());
		if ( num != 'NaN' ) {
			$(this).html(num);
		}
	});
});

function formatNumber(str, glue) {
	// 如果傳入必需為數字型參數，不然就噴 isNaN 回去
	if(isNaN(str)) {
		return 'NaN';
	}

	// 決定三個位數的分隔符號
	var glue= (typeof glue== 'string') ? glue: ',';
	var digits = str.toString().split('.'); // 先分左邊跟小數點
	var integerDigits = digits[0].split(""); // 獎整數的部分切割成陣列
	var threeDigits = []; // 用來存放3個位數的陣列

	// 當數字足夠，從後面取出三個位數，轉成字串塞回 threeDigits
	while (integerDigits.length > 3) {
		threeDigits.unshift(integerDigits.splice(integerDigits.length - 3, 3).join(""));
	}
	threeDigits.unshift(integerDigits.join(""));
	digits[0] = threeDigits.join(glue);

	return digits.join(".");
}

function sortNumber(a,b) {
	return a-b;
}

function delete_fans(page_id) {
	console.info(page_id);
	if( confirm('確定刪除粉絲團?') ) {
		var url='pofans/delete_fans';
		$.ajax({
			url: url,
			cache: false,
			dataType: 'html',
			type: 'POST',
			data:{
				page_id: page_id
			},success: function(data){
				location.reload();	
			},error: function(data){
			}
		});
	}
}

</script>