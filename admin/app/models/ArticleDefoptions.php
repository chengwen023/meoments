<?php 
class ArticleDefoptions extends \Phalcon\Mvc\Model
{
	public $defoptions_group_index;
	
	public function initialize()
	{
		$this->hasOne("defoptions_group_index", "ArticleDefoptionGroup", "auto_index");
	}
	
	public function beforeValidationOnCreate()
	{
		$this->online = '1';
		$this->text = false;		
	
	}
	
	public function setArticleDefoptionGroupIndex($index){
		$this->defoptions_group_index = $index;
	
	}
}

?>