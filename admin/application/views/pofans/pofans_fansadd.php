
<script src="js/span_editor.js"></script>
<style>
#img_selectbox {
	display: none;
	width: 550px;
	height: 420px;
	text-align: center;
	background-color: #94b0dc;
}

.fb_view_control {
	background-color: #336699;
	color: #fff;
	height: 20px;
}

.fb_view_control SPAN {
	margin-right: 10px;
	padding: 2px 5px;
}

.fb_view_control SPAN:HOVER {
	cursor: pointer;
	color: #3366cc;
	background-color: #fff;
}

.fb_view {
	width: 500px;
	height: 375px;
	margin: auto;
	border: 1px solid red;
}

.fb_link {
	width: 500px;
	height: 260px;
}

.fb_link_s {
	width: 375px;
	height: 375px;
}

.topic_photo {
	border: 1px solid;
	box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3);
	padding: 7px 5px 7px 7px;
	margin-right: 15px;
	margin-bottom:15px;
	float: left;
	margin-top: 25px;
	position: relative;
}
.topic_photo:before{
	content: attr(data-title);
	position: absolute;
	top: -20px;
}
.topic_group {
	width: 340px;
	float: left;
	margin: 10px;
}

.topic_group STRONG {
	color: #000;
	float: right;
	cursor: pointer;
	border-bottom: 1px solid #999;
}

.topic_group UL {
	margin: 0;
	display: none;
}

.topic_group.editable UL.options_select {
	position: absolute;
	display: inline-block;
	margin-top: 30px;
	background-color: #fff;
	border: 1px solid #D6D6D6;
	border-radius: 10px;
	padding: 5px 5px;
	box-shadow: 3px 3px 4px rgba(0, 0, 0, 0.2);
	z-index: 10;
}

.topic_group UL LI {
	float: left;
	margin-right: 10px;
	cursor: pointer;
	width: 130px;
}

.topic_group UL LI:AFTER {
	content: ' ';
	padding: 10px 0px 0px 30px;
}

.topic_group UL LI:HOVER {
	color: #585858;
}

.topic_group UL LI:HOVER:AFTER {
	background: url(images/check_green.png) no-repeat left;
}

.topic_group #topic_group_name UL {
	display: block;
	width: 450px;
	float: left;
}

.topic_group #topic_group_name UL LI.checked:AFTER {
	background: url(images/check_green.png) no-repeat left;
}

.audit_left.not_pass:BEFORE {
	content: ' ';
	padding: 10px 0px 10px 30px;
	background: url(images/not_pass.gif) no-repeat left;
	position: absolute;
	margin-left: -30px;
}

.audit_right.not_pass:AFTER {
	content: ' ';
	padding: 10px 0px 10px 30px;
	background: url(images/not_pass.gif) no-repeat left;
}
.topic_photo.not_pass:BEFORE {
	content: attr(data-title);
	padding: 0px 0px 00px 30px;
	background: url(images/not_pass.gif) no-repeat left;
}
.topic_photo.not_pass:AFTER{
	display: none;;
}
.data_title:BEFORE {
	content: attr(data-title);
	position: absolute;
	color: #000;
	margin-left: -40px;
	font-weight: bolder;
}

input[type="text"] {
	width: 100%;
	font-size: 18px;
	height: 24px;
	margin-bottom: 5px;
}

input[type="text"]:HOVER {
	background-color: rgba(33, 153, 233, 0.3);
}

input[type="text"].empty {
	border-color: rgba(241, 193, 154, 1);
}
IMG.errorsize{
	/*border-color: rgba(230, 128, 45, 1); */
	border:3px solid red; 
	 

}

.topic_photo.audit_right.dropping:AFTER {
	content: ' ';
	background-color: rgba(33, 33, 33, 0.3);
	width: 114px;
	position: absolute;
	height: 91px;
	border: 1px solid red;
	margin-top: -83px;
	margin-left: -9px;
}

.topic_photo.audit_left.dropping:AFTER {
	content: ' ';
	background-color: rgba(33, 33, 33, 0.3);
	width: 114px;
	position: absolute;
	height: 91px;
	border: 1px solid red;
	margin-top: -83px;
	margin-left: -9px;
}

.topic_option_list {
	counter-reset: option_div;
}

.topic_option_list .page-content.dropimg-content:BEFORE {
	counter-increment: option_div;
	content: counter(option_div);
	font-size: 135px;
	position: absolute;
	margin-top: 60px;
	color: #fff;
	text-shadow: 1px 2px 4px rgba(199, 199, 199, 1);
	font-family: sans-serif;
}
.page-content.dropimg-content[data-insert="insert"]:BEFORE {
	content: ' ';
	padding: 50px;
	background: url(images/check_blue_f.png) no-repeat;
	position: absolute;
	z-index: 5;
	margin: -10px 20px;
	right: 0;
}
.dropimg-content.dropping:AFTER {
	content: ' ';
	background-color: rgba(33, 33, 33, 0.3);
	width: 100%;
	position: absolute;
	height: 180px;
	border: 1px solid red;
}

#img_control {
	background: url(images/arrow_right.png) no-repeat center;
	padding: 40px 100px;
	width: 735px;
	/* height: 200px; */
	position: absolute;
	z-index: 1000;
	background-color: rgba(33, 33, 33, 0.3);
}

.topic_photo #img_control {
	margin-left: -90px;
	left: 0;
}
.audit_right #img_control {	
	left: -483px;
}
#img_control #old_img {
	float: left;
	padding: 8px;
	background-color: #f6f6f6;
	border: 1px dashed #b2b2b2;
	-webkit-box-shadow: 3px 3px 4px rgba(0, 0, 0, 0.2);
	-moz-box-shadow: 3px 3px 4px rgba(0, 0, 0, 0.2);
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
}

#img_control #new_img {
	float: right;
	padding: 8px;
	background-color: #f6f6f6;
	border: 1px dashed #b2b2b2;
	-webkit-box-shadow: 3px 3px 4px rgba(0, 0, 0, 0.2);
	-moz-box-shadow: 3px 3px 4px rgba(0, 0, 0, 0.2);
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
}

#img_control DIV#control_btn {
	top: 0;
	right: 0;
	position: absolute;
	color: #fff;
	font-size: 24px;
	overflow: hidden;
	width: 50px;
	height: 100%;
}

SPAN.text_editable{
	padding-right: 20px;
}
SPAN.text_editable:HOVER,SPAN.text_editable:FOCUS{
	border-bottom: 1px solid rgb(102,153,204);
}

.ie-icon:BEFORE {
	width: 32px;
	height: 32px;
}

.ie-icon:HOVER:BEFORE {
	margin-left: -5px;
	padding: 5px;
	border-radius: 5px;
	background: rgba(0, 0, 0, 0.5);
}


#upload_file::-webkit-file-upload-button {
  visibility: hidden;
}
#upload_file::before {
	content: ' ';
	display: inline-block;
	background: url("images/uploadfile.png") no-repeat left center;
	padding: 30px;
	cursor:pointer;
    position: absolute;
}
.topic_insert{
	font-size: 28px;
	cursor: pointer;
	color:rgb(35, 35, 35);
}
.topic_insert:HOVER{	
	color:rgb(255, 113, 9);
}
</style>

<div class="fancybox-content">
	<div class="fancybox-content-bg-bottom">
		<div class="fancybox-header" style="min-height: 80px;">
			<ul>		
			<li class="{active}">
						
				<span class="icon-publish topic_insert" onclick="topic_insert('{user_id}');" title="新增" alt="新增"></span>
				
			</ul>
			
			<br class="clear">

			
		</div>
		<div class="fancybox-inside">
			<div class="fancybox-inside-bg-top">
				<div class="fancybox-inside-bg-bottom">
					<div class="resume slide">

						<div class="topic_option_list">
							{fans_list}
							<div class="page-content dropimg-content">
								<div class="entry audit_left "style="font-size: 14px;" >

									<strong style="color: #000; display: block;"> 
									<span class="data_title" data-title="選項" data-type="title" value="{id}" data-name="page_id">{id}</span>
									</strong> 
									<strong style="color: #000; display: block;"> 
									<span contenteditable="true" class="text_editable data_title" data-title="標題" data-type="title" value="{name}" data-name="page_name">{name}</span>
									</strong>
									<span style="display: none;" data-name="access_token">{access_token}</span>
								</div>
								<h2>
									<div class="gallery-item audit_left " >
										<img class="editable" data-name="text_imgurl" src="http://graph.facebook.com/{id}/picture?width=96&height=96" alt="" width="75px" />
									</div>
								</h2>
							</div>
							{/fans_list}
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<br class="clear">
</div>


<script>

$(function() {
	set_span_editor();
	$("SPAN.text_editable").attr('contenteditable',true).each(function(){
		if(!$(this).html() || $(this).html() == '') $(this).html('請輸入資料');


		}).on("focus",function(){
			if( $(this).html() == '請輸入資料') $(this).html('');
		}).on("blur",function(){
			if(!$(this).html() || $(this).html() == '') $(this).html('請輸入資料');
		}).on("keyup",function(){
			var text = $(this).html();
			var filed = $(this).attr('data-name');


			var type = ( typeof($(this).attr('data-index')) == 'undefined' )?'topic':'topic_options';
			var index = ( typeof($(this).attr('data-index')) == 'undefined' )?$('INPUT[name="auto_index"]').val():$(this).attr('data-index');
						
			if(index && text != '請輸入資料') {
				$.ajax({
					  url: 'topic/update_topic/',
					  dataType: 'html',
					  type: "POST",
					  data: {			  
						  index:index,
						  filed:filed,
						  value:text,
						  type:type,
						  
					  },
					  success: function(data){
						  
						  if(data) {
							  $(this).html(data);
						  }
					  }
					});

			};

		});
	
	});
$(".page-content.dropimg-content").on("click",function(){
	if($(this).attr("data-insert") == 'insert') $(this).removeAttr('data-insert');
	else $(this).attr("data-insert",'insert');	
});

function topic_insert(user_id){
	var fans_list = get_post_val();
	console.info(fans_list);

	if(fans_list.length && confirm("是否確定新增資料? ")){
		
		if(fans_list){
			url = 'pofans/fans_add'; 
			
			$.fancybox.showLoading();
			$.ajax({
				url: url,
			  cache: false,
				dataType: 'html',
			  type: 'POST',
			  data:{
				  fans_list:fans_list,
				  user_id:user_id
			  },
			  success: function(data){	
				  location.reload();
			  }
			});
		}else{
			alert(fans_list.error_msg);
		}
	}else if(fans_list.length<1){
		alert('請選擇要新增的粉絲團');
	}
}

function get_post_val(){

	var fans_list = new Array();
	$('.topic_option_list .page-content.dropimg-content[data-insert="insert"]').each(function(){
			var option = {"page_id": '',"page_name": '','access_token':''};

			for( var key in option){	
				var temp = $(this).find('SPAN[data-name="'+key+'"]').attr('data-title');
				
				if(typeof(temp) != 'undefined' && $(this).find('SPAN[data-name="'+key+'"]').html() == temp) return false;

				option[key] = $(this).find('SPAN[data-name="'+key+'"]').html();
			}			
			
			fans_list.push(option);
		});

	
	
	
	return fans_list;
	
}
</script>