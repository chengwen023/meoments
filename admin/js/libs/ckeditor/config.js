/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	
	//換行模組
	config.enterMode = CKEDITOR.ENTER_BR;
	config.shiftEnterMode = CKEDITOR.ENTER_P;

	/*外接css檔*/
 	config.contentsCss = '../js/libs/css/article.css';

 	//是否強制複製來的內容去除格式 plugins/pastetext/plugin.js
    config.forcePasteAsPlainText = false;
	
	config.toolbar =
	[
//	    { name: 'document',    items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
		{ name: 'document',    items : [ 'Source' ] },
	    { name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
	    { name: 'editing',     items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
//	    { name: 'forms',       items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
//	    '/',
	    { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
//	    { name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
	    { name: 'paragraph', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
	    { name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
	    { name: 'insert',      items : [ 'Image'] },
	    '/',
	    { name: 'styles',      items : [ 'Styles','Format','Font','FontSize' ] },
	    { name: 'colors',      items : [ 'TextColor','BGColor' ] },
	    { name: 'tools',       items : [ 'Maximize', 'ShowBlocks','-','Youtube','ReplaceValue','replaceSymbol' ] }
	];
	
	config.extraPlugins = 'youtube,replaceValue,replaceSymbol';
//	config.forcePasteAsPlainText = true;
	
	//增加自訂功能
	//config.extraPlugins = 'replaceValue';
	/*自訂義toolbar*/
	config.allowedContent = true;
	config.protectedSource.push( /<dm300[\s\S]*?\/rt>/img);
	config.protectedSource.push( /<SUBCONTENT[\s\S]*?\/rt>/img);
 	config.extraAllowedContent = "dm300(*)[*]{*}; ";
 	config.extraAllowedContent = "dm334(*)[*]{*}; ";
 	config.extraAllowedContent = "dm728(*)[*]{*}; ";
 	config.extraAllowedContent = "SUBCONTENT(*)[*]{*}; ";
 	

	config.replaceValue = [ 'choose1:300x250:<dm300>300x250</dm300>', 'choose2:334x280:<dm334>334x280</dm334>', 'choose3:728x90:<dm728>728x90</dm728>','choose4:分頁:<subcontent>分頁</subcontent>'];

	config.replaceSymbol = [ 'symbol1:▲:▲', 'symbol2:▼:▼' ];

};
